package com.javaweb.common.utils;

import java.io.Serializable;

public class SsoJsonResult<T> implements Serializable {

        private static final long serialVersionUID = 1L;

        /**
         * 成功
         */
        public static final int SUCCESS = 0;//CommonConstants.SUCCESS;

        /**
         * 失败
         */
        public static final int error = 1;

        private int code;

        private String message;

        private T data;

        public static <T> SsoJsonResult<T> success() {
            return jsonResult(null, SUCCESS, "操作成功");
        }

        public static <T> SsoJsonResult<T> success(T data) {
            return jsonResult(data, SUCCESS, "操作成功");
        }

        public static <T> SsoJsonResult<T> success(T data, String msg) {
            return jsonResult(data, SUCCESS, msg);
        }

        public static <T> SsoJsonResult<T> error() {
            return jsonResult(null, error, "操作失败");
        }

        public static <T> SsoJsonResult<T> error(String msg) {
            return jsonResult(null, error, msg);
        }

        public static <T> SsoJsonResult<T> error(T data) {
            return jsonResult(data, error, "操作失败");
        }

        public static <T> SsoJsonResult<T> error(T data, String msg) {
            return jsonResult(data, error, msg);
        }

        public static <T> SsoJsonResult<T> error(int code, String msg) {
            return jsonResult(null, code, msg);
        }

        private static <T> SsoJsonResult<T> jsonResult(T data, int code, String msg) {
            SsoJsonResult<T> result = new SsoJsonResult<>();
            result.setCode(code);
            result.setMessage(msg);
            return result;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String msg) {
            this.message = msg;
        }
}
