package com.javaweb.common.utils;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.javaweb.common.constant.CommonConstants;

import java.io.Serializable;

public class JsonResultSYT<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 成功
     */
    public static final int SUCCESS = 0;//CommonConstants.SUCCESS;

    /**
     * 失败
     */
    public static final int error = CommonConstants.FAIL;

    private int msg_code;
    private String msg_info;


    public static <T> JsonResultSYT<T> success() {
        return jsonResult(null, SUCCESS, "操作成功");
    }

    public static <T> JsonResultSYT<T> success(T data) {
        return jsonResult(data, SUCCESS, "操作成功");
    }

    public static <T> JsonResultSYT<T> success(T data, String msg) {
        return jsonResult(data, SUCCESS, msg);
    }

    public static <T> JsonResultSYT<T> error() {
        return jsonResult(null, error, "操作失败");
    }

    public static <T> JsonResultSYT<T> error(String msg) {
        return jsonResult(null, error, msg);
    }

    public static <T> JsonResultSYT<T> error(T data) {
        return jsonResult(data, error, "操作失败");
    }

    public static <T> JsonResultSYT<T> error(T data, String msg) {
        return jsonResult(data, error, msg);
    }

    public static <T> JsonResultSYT<T> error(int code, String msg) {
        return jsonResult(null, code, msg);
    }

    private static <T> JsonResultSYT<T> jsonResult(T data, int code, String msg) {
        JsonResultSYT<T> result = new JsonResultSYT<>();
        result.setMsgCode(code);
        //result.setData(data);
        result.setMsgInfo(msg);
        return result;
    }

    @JsonProperty("msg_code")
    public int getMsgCode() {
        return msg_code;
    }

    public void setMsgCode(int code) {
        this.msg_code = code;
    }

    @JsonProperty("msg_info")
    public String getMsgInfo() {
        return msg_info;
    }

    public void setMsgInfo(String msg) {
        this.msg_info = msg;
    }


}
