package com.javaweb.common.utils;

import com.sun.istack.ByteArrayDataSource;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public final class MailUtils {
    private MailUtils() {}

    public static Session createSession(String username, String password, String emailName) {

        //	账号信息
        //	邮箱发送账号
        //	邮箱授权码

        //	创建一个配置文件，并保存
        Properties props = new Properties();

        //	SMTP服务器连接信息
        //  126——smtp.126.com
        //  163——smtp.163.com
        //  qq smtp.qq.com"

        props.put("mail.smtp.host", "smtp."+emailName+".com");//	SMTP主机名

        //  126——25
        //  163——645
        props.put("mail.smtp.port", "587");//	主机端口号
        props.put("mail.smtp.auth", "true");//	是否需要用户认证
        props.put("mail.smtp.starttls.enable", "true");//	启用TlS加密
        props.put("mail.smtp.starttls.required", "true");//	启用TlS加密



        Session session = Session.getInstance(props,new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication(username,password);
            }
        });

        //控制台打印调试信息
        session.setDebug(true);
        return session;

    }


    public static boolean SendPlainTextMail(String username, String password, String emailName,String subject,String body,String fromaddress,String toaddress)
    {
        Multipart mp = new MimeMultipart();
        //	创建Session会话
        Session session = createSession(username,password,emailName);
        try {
            //	创建邮件对象
            MimeMessage message = new MimeMessage(session);
            message.setSubject(subject);
            message.setText(body);
            message.setFrom(new InternetAddress(fromaddress));
            message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(toaddress));



            //	发送
            Transport.send(message);
            return true;
        }
        catch(Exception ex)
        {
            return false;
        }
    }

    public static boolean SendAttachMail(String username, String password, String emailName, String subject, String body, String fromaddress, String toaddress, String path, String file)
    {
        //	创建Session会话
        try {
            //	创建会话
            Session session = createSession(username,password,emailName);

            //	创建邮件
            MimeMessage message = new MimeMessage(session);
            message.setFrom(fromaddress);
            message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(toaddress));
            //message.setRecipients(MimeMessage.RecipientType.CC, new InternetAddress[] {new  InternetAddress("抄送人邮箱"),new InternetAddress("抄送人邮箱")});
            message.setSubject(subject);

            //	邮件主体
            BodyPart textPart = new MimeBodyPart();
            textPart.setContent(body,"text/html;charset=utf-8");

            //	邮件附件
            BodyPart filePart = new MimeBodyPart();
            filePart.setFileName(file+".zip");

            //	提交附件文件

            filePart.setDataHandler(new DataHandler(new ByteArrayDataSource(Files.readAllBytes(Paths.get(path)),"application/octet-stream")));

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(textPart);
            multipart.addBodyPart(filePart);

            //	将邮件装入信封
            message.setContent(multipart);

            Transport.send(message);
            return true;
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

}

