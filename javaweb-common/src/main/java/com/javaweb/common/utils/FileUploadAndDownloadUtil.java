package com.javaweb.common.utils;

import cn.hutool.core.lang.UUID;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


public class FileUploadAndDownloadUtil {
    private static Logger logger = LoggerFactory.getLogger(FileUploadAndDownloadUtil.class);
    //用户名
    private static final String userName = "xy";
    //密码
    private static final String password = "edoc2edoc2";
    //访问地址
    private static String baseUrl="http://vpn.sh-junpai.cn:8088";
    private static String folderId="1";



    public static String UserLogin(){
        String jsonBodys = "\"userName\":\"" + userName + "\",\"password\":\"" + password + "\"";
        String result=HttpUtils.sendPost2(baseUrl+"/api/services/Org/UserLogin","{"+jsonBodys+"}");
        String token=result.substring(result.indexOf("\"data\":")+8,result.indexOf("\"dataDescription\":")-2);
        return token;
    }

    public static String upload(MultipartFile file, String newfilenameString) throws IOException {
        String token = UserLogin();
        byte[] b = file.getBytes();
        int filesize = b.length;
        String UploadId = UUID.randomUUID().toString();
        String jsonBodys = "\"UploadId\":\"" + UploadId + "\",\"ParentId\":\"" + folderId + "\",\"fileSize\":\"" + filesize + "\",\"name\":\""+newfilenameString+"\",\"Token\":\"" + token + "\"";
        String result=HttpUtils.sendPost2(baseUrl+"/api/services/Transfer/StartUploadFile","{"+jsonBodys+"}");
        

        return "";
    }

}
