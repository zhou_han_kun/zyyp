package com.javaweb.common.config;

public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER("system"),

    /**
     * 九州通本地数据库
     */
    JZTLOCAL("jztlocal"),
    /**
     * 红缨数据库
     */
    HY("hy"),
    /**
     * 联合进销存
     */
    JXC("jxc"),
    /**
     * 九州通业务数据库
     */
    JZT("jzt"),

    /**
     * MDM接口数据库
     */
    MDM("mdm"),
    /**
     * 从库
     */

    SPSunPlatfromDb("sunplat");

    private final String name;

    DataSourceType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

