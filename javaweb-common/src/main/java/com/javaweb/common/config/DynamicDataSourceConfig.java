package com.javaweb.common.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class DynamicDataSourceConfig {

    @Bean(name="system")
    @ConfigurationProperties("spring.datasource.druid.system")
    public DataSource dataSource1(){
        return DruidDataSourceBuilder.create().build();
    }

    @Bean(name ="jzt")
    @ConfigurationProperties("spring.datasource.druid.jzt")
    public DataSource dataSource2(){
        return DruidDataSourceBuilder.create().build();
    }

    @Bean(name ="jztlocal")
    @ConfigurationProperties("spring.datasource.druid.jztlocal")
    public DataSource dataSourceJZTLocal(){
        return DruidDataSourceBuilder.create().build();
    }

    @Bean(name ="jxc")
    @ConfigurationProperties("spring.datasource.druid.jxc")
    public DataSource dataSourceJXC(){
        return DruidDataSourceBuilder.create().build();
    }

    @Bean(name ="mdm")
    @ConfigurationProperties("spring.datasource.druid.mdm")
    public DataSource dataSourceMDM(){
        return DruidDataSourceBuilder.create().build();
    }

    @Bean(name ="hy")
    @ConfigurationProperties("spring.datasource.druid.hy")
    public DataSource dataSourceHY(){
        return DruidDataSourceBuilder.create().build();
    }

    @Bean(name ="sunplat")
    @ConfigurationProperties("spring.datasource.druid.sunplat")
    public DataSource dataSourceSPSunPlatfromDb(){
        return DruidDataSourceBuilder.create().build();
    }

    @Bean(name="dynamicDataSource")
    @Primary
    public DynamicDataSource dataSource() {
        Map<Object, Object> targetDataSources = new HashMap<>(5);
        targetDataSources.put(DataSourceType.MASTER.getName(), dataSource1());
        targetDataSources.put(DataSourceType.JZT.getName(), dataSource2());
        targetDataSources.put(DataSourceType.JZTLOCAL.getName(), dataSourceJZTLocal());
        targetDataSources.put(DataSourceType.HY.getName(), dataSourceHY());
        targetDataSources.put(DataSourceType.JXC.getName(), dataSourceJXC());
        targetDataSources.put(DataSourceType.MDM.getName(), dataSourceMDM());
        targetDataSources.put(DataSourceType.SPSunPlatfromDb.getName(), dataSourceSPSunPlatfromDb());
        return new DynamicDataSource(dataSource1(), targetDataSources);
    }
}
