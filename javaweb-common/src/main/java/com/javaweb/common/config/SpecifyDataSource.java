package com.javaweb.common.config;

import java.lang.annotation.*;
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SpecifyDataSource {
    DataSourceType value() default DataSourceType.MASTER;
}
