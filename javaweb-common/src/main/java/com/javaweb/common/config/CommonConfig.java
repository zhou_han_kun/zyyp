// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.common.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class CommonConfig {

    /**
     * NC Webservice地址
     */
    public static String ncServiceUrl;

    /**
     * NC 公司编码
     */
    public static String ncOrgCode;

    /**
     * 判断NC单据号重复关键字
     */
    public static String ncBillKey;

    //NC销售订单视图
    public static String ncSaleOrder;

    //NC客户地址视图
    public static String ncCustAddr;

    //NC物料视图
    public static String ncInvInfo;

    //NC产品价格视图
    public static String ncPrmTarifflist;

    //sso单点登录服务地址
    public static String ssoServiceUrl;
    public static String ssoClientId;
    public static String ssoClientSecret;

    //日志目录

    public static String ncLogFilePath;
    //溯源接口appid
    public static String appId;
    //溯源接口秘钥
    public static String appSecret;

    //MDM接口appid
    public static String appTenantId;
    //MDM接口秘钥
    public static String appTenantSecret;
    //溯源接口企业id
    public static String enterpriseId;

    //溯源接口企业id
    public static String entId;
    public static String corpId;
    public static String systemId;
    //签名密钥
    public static String signKey;
    //数据加密公钥
    public static String dataKey;

    //淘宝获取药检报告url
    public static String taobaoURL;
    //淘宝药检报告接口appkey
    public static String taobaoAppKey;
    //淘宝药检报告接口appsecret
    public static String taobaoAppSecret;
    //药检报告保存路径
    public static String taobaoYjbgPath;

    //乐企电票
    public static String yonhyouTenantId;
    public static String yonhyouAppKey;
    public static String yonhyouAppSecret;
    //电子发票保存路径
    public static String yonyouInvoicePath;



    /**
     * 图片域名
     */
    public static String imageURL;

    /**
     * 是否演示环境：true是,false否
     */
    public static boolean appDebug;
    /**
     * 是否启动定时认为：true是,false否
     */
    public static boolean enableSchedule;

    public static String ysServiceURL;

    public static String ysUserNo;
    public static String ysPassword;
    public static boolean ysDebug;

    /**
     * fdl补差推送接口
     */
    public static String fdlBcUrl;
    public static String bcWorkId;
    public static String fdlRecordStatusUrl;

    /**
     * 简道云补差附件上传
     */
    public static String jdyFileUploadUrl1;
    public static String jdyFileUploadUrl2;
    public static String jdyFileUploadUrl4;
    public static String jdyAppId;
    public static String jdyEntryId;
    public static String jdyBcSqdhId;
    public static String jdyApiKey;
    public static String jdyBcFileId;

    /**
     * BIP税务云接口
     */
    public static String bipDomain;
    public static String bipAppkey;
    public static String bipAppsecret;
    public static String bipAccessTokenUrl;
    public static String bipOcrUrl;
    public static String cgfpUrl;

    /**
     * 中药云平台接口是否使用mock服务：true是,false否
     */
    public static boolean useMock;

    @Value("${javaweb.bip.domain}")
    public void setBipDomain(String domain){ bipDomain = domain; }
    @Value("${javaweb.bip.appkey}")
    public void setBipAppkey(String appkey){ bipAppkey = appkey; }
    @Value("${javaweb.bip.appsecret}")
    public void setBipAppsecret(String appSecret){ bipAppsecret = appSecret; }
    @Value("${javaweb.bip.getAccessTokenUrl}")
    public void setBipAccessTokenUrl(String accessTokenUrl){ bipAccessTokenUrl = accessTokenUrl; }
    @Value("${javaweb.bip.ocrUrl}")
    public void setBipOcrUrl(String ocrUrl){ bipOcrUrl = ocrUrl; }
    @Value("${javaweb.bip.cgfpUrl}")
    public void setCgfpUrl(String cgfpUrl){ this.cgfpUrl = cgfpUrl; }

    @Value("${javaweb.fdl.record_status_url}")
    public void setFdlRecordStatusUrl(String url) {
        fdlRecordStatusUrl = url;
    }
    @Value("${javaweb.fdl.bc-url}")
    public void setFdlBcUrl(String url) {
        fdlBcUrl = url;
    }
    @Value("${javaweb.fdl.workId}")
    public void setBcWorkId(String id) {
        bcWorkId = id;
    }
    @Value("${javaweb.jdy.fileUploadUrl1}")
    public void setJdyFileUploadUrl1(String url) {
        jdyFileUploadUrl1 = url;
    }
    @Value("${javaweb.jdy.fileUploadUrl2}")
    public void setJdyFileUploadUrl2(String url) {
        jdyFileUploadUrl2 = url;
    }
    @Value("${javaweb.jdy.fileUploadUrl4}")
    public void setJdyFileUploadUrl4(String url) {
        jdyFileUploadUrl4 = url;
    }
    @Value("${javaweb.jdy.app_id}")
    public void setJdyAppId(String appId) {
        jdyAppId = appId;
    }
    @Value("${javaweb.jdy.entry_id}")
    public void setJdyEntryId(String entryId) {
        jdyEntryId = entryId;
    }
    @Value("${javaweb.jdy.bc_sqdh_field}")
    public void setJdyBcSqdhId(String sqdhId) {
        jdyBcSqdhId = sqdhId;
    }
    @Value("${javaweb.jdy.bc_file_field}")
    public void setJdyBcFileId(String fileId) {
        jdyBcFileId = fileId;
    }
    @Value("${javaweb.jdy.api_key}")
    public void setJdyApiKey(String apiKey) {
        jdyApiKey = apiKey;
    }


    @Value("${javaweb.taobao.url}")
    public void setTaobaoURL(String url) {
        taobaoURL = url;
    }
    @Value("${javaweb.taobao.appkey}")
    public void setTaobaoAppKey(String appkey) {
        taobaoAppKey = appkey;
    }
    @Value("${javaweb.taobao.secret}")
    public void setTaobaoAppSecret(String secret) {
        taobaoAppSecret = secret;
    }
    @Value("${javaweb.taobao.yjbgpath}")
    public void setTaobaoYjbgPath(String yjbgpath) {
        taobaoYjbgPath = yjbgpath;
    }

    @Value("${javaweb.yonyou.tenantid}")
    public void setYonhyouTenantId(String tenantid) {
        yonhyouTenantId = tenantid;
    }
    @Value("${javaweb.yonyou.appkey}")
    public void setYonhyouAppKey(String appkey) {
        yonhyouAppKey = appkey;
    }
    @Value("${javaweb.yonyou.secret}")
    public void setYonyouAppSecret(String secret) {
        yonhyouAppSecret = secret;
    }
    @Value("${javaweb.yonyou.invoicepath}")
    public void setYonyouInvoicePath(String invoicepath) {
        yonyouInvoicePath = invoicepath;
    }



    @Value("${spring.ncservice.url}")
    public void setNcServiceUrl(String url) {
        ncServiceUrl = url;
    }

    @Value("${spring.ncservice.orgcode}")
    public void setNcOrgCode(String orgcode) {
        ncOrgCode = orgcode;
    }

    @Value("${spring.ncservice.billkey}")
    public void setNcBillKey(String billkey) {
        ncBillKey = billkey;
    }
    @Value("${spring.ncservice.saleorder}")
    public void setNcSaleOrder(String saleOrder) {
        ncSaleOrder = saleOrder;
    }

    @Value("${spring.ncservice.custaddr}")
    public void setNcCustAddr(String custaddr) {
        ncCustAddr = custaddr;
    }

    @Value("${spring.ncservice.invinfo}")
    public void setNcInvInfo(String invinfo) {
        ncInvInfo = invinfo;
    }

    @Value("${spring.ncservice.prmtarifflist}")
    public void setNcPrmTarifflist(String prmtarifflist) {
        ncPrmTarifflist = prmtarifflist;
    }

    @Value("${spring.ncservice.logpath}")
    public void setNcLogFilePath(String logpath) {
        ncLogFilePath = logpath;
    }

    @Value("${javaweb.daodikeji.appid}")
    public void setAppId(String id) {
        appId = id;
    }

    @Value("${javaweb.daodikeji.appsecret}")
    public void setAppSecret(String secret) {
        appSecret = secret;
    }

    @Value("${javaweb.daodikeji.enterpriseid}")
    public void setEnterpriseId(String eid) {
        enterpriseId = eid;
    }

    @Value("${javaweb.daodikeji.entid}")
    public void setEntId(String eid) {
        entId = eid;
    }

    @Value("${javaweb.daodikeji.usemock}")
    public void setUseMock(Boolean usemock) {
        useMock = usemock;
    }

    @Value("${javaweb.daodikeji.corpid}")
    public void setCorpId(String corpid) { corpId = corpid; }

    @Value("${javaweb.daodikeji.systemid}")
    public void setSystemId(String systemid) { systemId = systemid; }

    @Value("${javaweb.daodikeji.signkey}")
    public void setSignKey(String signkey) { signKey = signkey; }

    @Value("${javaweb.daodikeji.datakey}")
    public void setDataKey(String datakey) { dataKey = datakey; }

    @Value("${javaweb.mdm.tenant-key}")
    public void setAppTenantId(String id) {
        appTenantId = id;
    }

    @Value("${javaweb.mdm.tenant-secret}")
    public void setAppTenantSecret(String secret) {
        appTenantSecret = secret;
    }

    @Value("${javaweb.sunplatform.serviceurl}")
    public void setYsServiceURL(String url) { ysServiceURL = url; }

    @Value("${javaweb.sunplatform.ysuserno}")
    public void setYsUserNoL(String userno) { ysUserNo = userno; }

    @Value("${javaweb.sunplatform.yspassword}")
    public void setYsPassword(String pwd) { ysPassword = pwd; }

    @Value("${javaweb.sunplatform.debug}")
    public void setYsDebug(boolean val) { ysDebug = val; }
    /**
     * 图片域名赋值
     *
     * @param url 域名地址
     */
    @Value("${javaweb.image-url}")
    public void setImageURL(String url) {
        imageURL = url;
    }

    /**
     * 是否演示模式
     *
     * @param debug
     */
    @Value("${javaweb.app-debug}")
    public void setAppDebug(boolean debug) {
        appDebug = debug;
    }

    @Value("${javaweb.enableschedule}")
    public void setEnableSchedule(boolean value) {
        enableSchedule = value;
    }

    @Value("${javaweb.sso.url}")
    public void setSsoServiceUrl(String value) {
        ssoServiceUrl = value;
    }

    @Value("${javaweb.sso.clientid}")
    public void setSsoClientId(String value) {
        ssoClientId = value;
    }

    @Value("${javaweb.sso.clientsecret}")
    public void setSsoClientSecret(String value) {
        ssoClientSecret = value;
    }
}
