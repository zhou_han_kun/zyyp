package com.javaweb.common.enums;

public enum FileNameRule {
    billdate("billdate");


    private String rule;

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    FileNameRule(String rule) {
        this.rule = rule;
    }
}
