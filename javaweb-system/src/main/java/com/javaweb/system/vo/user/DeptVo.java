package com.javaweb.system.vo.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigInteger;
import java.util.Date;

@Data
public class DeptVo {
    private BigInteger id;
    /**
     * 部门名称
     */
    private String name;

    /**
     * 上级ID
     */
    private BigInteger pid;

    /**
     * 部门编号
     */
    private String code;

    /**
     * 部门全称
     */
    private String fullname;

    /**
     * 部门类型：1公司 2子公司 3部门 4小组
     */
    private Integer type;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 备注说明
     */
    private String note;

    /**
     * 添加人
     */
    private Integer createUser;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 更新人
     */
    private Integer updateUser;

    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    /**
     * 有效标识
     */
    private Integer mark;

}
