package com.javaweb.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.system.entity.UserOrg;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserOrgMapper extends BaseMapper<UserOrg> {

    @Select("select t1.*,t2.code,t2.name from sys_user_org t1 inner join sys_dict_data t2 on t1.org_code=t2.code where t2.dict_id=19 and user_id=#{userId} order by org_code")
    List<UserOrg> getOrgByUserId(Integer userId);

}
