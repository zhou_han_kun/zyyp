package ${packageName}.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
    * ${tableAnnotation}
    * </p>
*
* @author ${author}
* @since ${date}
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("${tableName}")
public class ${entityName} extends BaseEntity {

private static final long serialVersionUID = 1L;

<#if model_column?exists>
    <#list model_column as model>
        <#if model.changeColumnName?uncap_first != 'createUser' && model.changeColumnName?uncap_first != 'createTime' && model.changeColumnName?uncap_first != 'updateUser' && model.changeColumnName?uncap_first != 'updateTime' && model.changeColumnName?uncap_first != 'mark'>
            /**
            * ${model.columnComment!}
            */
            <#if (model.columnType?upper_case = 'VARCHAR' || model.columnType?upper_case = 'NVARCHAR'  || model.columnType?upper_case = 'CHAR' || model.columnType?upper_case = 'TEXT' || model.columnType?upper_case = 'MEDIUMTEXT')>
                private String ${model.changeColumnName?uncap_first};

            </#if>
            <#if (model.columnType?upper_case = 'DATETIME' || model.columnType?upper_case = 'DATE' || model.columnType?upper_case = 'TIME' || model.columnType?upper_case = 'YEAR' || model.columnType?upper_case = 'TIMESTAMP') >
                @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
                private Date ${model.changeColumnName?uncap_first};

            </#if>
            <#if (model.columnType?upper_case = 'TINYINT UNSIGNED' || model.columnType?upper_case = 'TINYINT')>
                private Integer ${model.changeColumnName?uncap_first};

            </#if>
            <#if (model.columnType?upper_case = 'SMALLINT UNSIGNED' || model.columnType?upper_case = 'SMALLINT' || model.columnType?upper_case = 'MEDIUMINT UNSIGNED' || model.columnType?upper_case = 'MEDIUMINT')>
                private Integer ${model.changeColumnName?uncap_first};

            </#if>
            <#if (model.columnType?upper_case = 'INT UNSIGNED' || model.columnType?upper_case = 'INT')>
                private Integer ${model.changeColumnName?uncap_first};

            </#if>
            <#if (model.columnType?upper_case = 'BIGINT UNSIGNED' || model.columnType?upper_case = 'BIGINT')>
                private Integer ${model.changeColumnName?uncap_first};

            </#if>
            <#if (model.columnType?upper_case = 'NUMERIC' || model.columnType?upper_case = 'DECIMAL')>
                private BigDecimal ${model.changeColumnName?uncap_first};

            </#if>
            <#if (model.columnType?upper_case = 'FLOAT UNSIGNED' || model.columnType?upper_case = 'FLOAT')>
                private Float ${model.changeColumnName?uncap_first};

            </#if>
            <#if (model.columnType?upper_case = 'DOUBLE UNSIGNED' || model.columnType?upper_case = 'DOUBLE')>
                private Double ${model.changeColumnName?uncap_first};

            </#if>
            <#if model.columnType?upper_case = 'BLOB'>
                private byte[] ${model.changeColumnName?uncap_first};

            </#if>
        </#if>
    </#list>
</#if>
}