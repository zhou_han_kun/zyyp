package com.biz.smpaa.webservice;

import cn.hutool.core.util.XmlUtil;
import com.javaweb.admin.service.Const;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.utils.FileUtils;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.namespace.QName;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class YsIntfce {
//    static String sUser = Const.YGPT_CORPCODE;
//    static String sPwd = Const.YGPT_PASSWORD;//"xyty001001";
    static String sUser = CommonConfig.ysUserNo;
    static String sPwd = CommonConfig.ysPassword;//"xyty001001";
    static String sJgbm = Const.YGPT_JGDM;
    static String sVersion = Const.YGPT_VERSION;
    static String sXxlx = "";
    static String sSign = "";
    static String xmlData = "";
    private static final QName SERVICE_NAME = new QName("http://main.service.local.wondersgroup.com/", "YsxtMainServiceImplService");

//    public static String SendRecv(String YQNO,String[] Qry)
//    {
//        //DataTable dtr = new DataTable();
//        String sr = "";
//        switch (YQNO)
//        {
//            case "TEST":
//                sr = SendRecvResult(Qry);
//                break;
//            case "YQ001":
//                sr = SendRecvYQ001(Qry);
//                break;
//            case "YQ002":
//                sr = SendRecvYQ002(Qry);
//                break;
//            case "YQ003":
//                sr = SendRecvYQ003(Qry);
//                break;
//            case "YQ004":
//                sr = SendRecvYQ004(Qry);
//                break;
//            case "YQ005":
//                sr = SendRecvYQ005old(Qry);//需要修改
//                break;
//            case "YQ006":
//                sr = SendRecvYQ006old(Qry);
//                break;
//            case "YQ007":
//                sr = SendRecvYQ007(Qry);
//                break;
//            case "YQ008":
//                sr = SendRecvYQ008(Qry);
//                break;
//            case "YQ009":
//                sr = SendRecvYQ009(Qry);
//                break;
//            case "YQ010":
//                sr = SendRecvYQ010(Qry);
//                break;
//            case "YQ011":
//                sr = SendRecvYQ011(Qry);
//                break;
//            case "YQ012":
//                sr = SendRecvYQ012(Qry);
//                break;
//            case "YQ013":
//                sr = SendRecvYQ013(Qry);
//                break;
//            case "YQ014":
//                sr = SendRecvYQ014(Qry);
//                break;
//            case "YQ015":
//                sr = SendRecvYQ015(Qry);
//                break;
//            case "YQ016":
//                sr = SendRecvYQ016(Qry);
//                break;
//            case "YQ017":
//                sr = SendRecvYQ017(Qry);
//                break;
//            case "YQ018":
//                sr = SendRecvYQ018(Qry);
//                break;
//            case "YQ034":
//                sr = SendRecvYQ034(Qry);
//                break;
//            default:
//
//                break;
//        }
//        sXxlx = YQNO;
//        xmlData = sr;
//
//        try {
//            FileUtils.writeLog("阳光接口日志" + YQNO, xmlData);
//            sSign = SHA1_Hash(xmlData);
//            URL wsdlURL = YsxtMainServiceImplService.WSDL_LOCATION;
//            YsxtMainServiceImplService ss = new YsxtMainServiceImplService(wsdlURL, SERVICE_NAME);
//            YsxtMainService port = ss.getYsxtMainServiceImplPort();
//            String result = "";
//            result = port.sendRecv(sUser, sPwd, sJgbm, sVersion, sXxlx, sSign, xmlData);
//            String[] r = new String[3];
//            r[0] = YQNO;
//            r[1] = xmlData;
//            r[2] = result;
//            FileUtils.writeLog("阳光接口日志" + YQNO, result);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        //string[] r = System.Text.RegularExpressions.Regex.Split(result, "<CWXX></CWXX>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
//
//
//        //if (YQNO == "YQ010")
//        //{
//        //    dtr = XmlToDtYQ010(result);
//        //}
//        //else if (YQNO == "YQ011")
//        //{
//        //    dtr = XmlToDtYQ011(result);
//        //}
//        //else if (YQNO == "YQ012")
//        //{
//        //    dtr = XmlToDtYQ012(result);
//        //}
//        return SendRecvResult(r);
//    }

    public static String SHA1_Hash(String xmlData)
    {
        StringBuilder sb = new StringBuilder();
        try {
            // 创建SHA实例
            MessageDigest md = MessageDigest.getInstance("SHA1");

            // 计算摘要
            byte[] digest = md.digest(xmlData.getBytes(Charset.forName("UTF-8")));

            // 将摘要转换为十六进制字符串

            for (byte b : digest) {
                sb.append(String.format("%02x", b));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return sb.toString().toUpperCase();
    }

    public static String[] SendYgRecv2019(String YQNO, String postXmlData)
    {
        String[] rtn = new String[7];
        try {
            sSign = SHA1_Hash(postXmlData);
            rtn[0] = YQNO;
            rtn[1] = postXmlData;
            FileUtils.writeLog("阳光接口日志" + YQNO, postXmlData);
            FileUtils.writeLog("阳光接口日志" + YQNO, sSign);
            //
            String resultXML = "";
            //if(!CommonConfig.ysDebug) {
                JaxWsDynamicClientFactory factory = JaxWsDynamicClientFactory.newInstance();
                QName operationName = new QName("http://main.service.local.wondersgroup.com/", "sendRecv");//如果有命名空间需要加上这个，第一个参数为命名空间名称，调用的方法名称
                String url = CommonConfig.ysServiceURL + "?wsdl";
                Client client = factory.createClient(url);
                client.getConduit().getTarget().getAddress().setValue(CommonConfig.ysServiceURL);
                Object[] res = null;
                res = client.invoke(operationName, sUser, sPwd, sJgbm, sVersion, YQNO, sSign, postXmlData);
                resultXML = res[0].toString();
//            }else {
//                if (YQNO.equals("YQ029")) {
//                    resultXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><XMLDATA><HEAD><JSSJ>20171011/150011/</JSSJ><ZTCLJG>00000</ZTCLJG><CWXX/><BZXX>进货发票传报</BZXX></HEAD><MAIN><FPID>20171011000000026310</FPID></MAIN><DETAIL><STRUCT><SXH>30</SXH><CLJG>00000</CLJG><CLQKMS/></STRUCT><STRUCT><SXH>40</SXH><CLJG>00000</CLJG><CLQKMS/></STRUCT><STRUCT><SXH>10</SXH><CLJG>00000</CLJG><CLQKMS/></STRUCT><STRUCT><SXH>20</SXH><CLJG>00000</CLJG><CLQKMS/></STRUCT><STRUCT><SXH>50</SXH><CLJG>00000</CLJG><CLQKMS/></STRUCT><STRUCT><SXH>70</SXH><CLJG>00000</CLJG><CLQKMS/></STRUCT><STRUCT><SXH>60</SXH><CLJG>00000</CLJG><CLQKMS/></STRUCT></DETAIL></XMLDATA>";
//                }else{
//                    //resultXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><XMLDATA><HEAD><JSSJ>20230627/124335/</JSSJ><ZTCLJG>11002</ZTCLJG><CWXX>传入的发票ID不存在，请检查！发票ID为：20220719000005714537</CWXX><BZXX>进货发票传报</BZXX></HEAD></XMLDATA>";
//                    resultXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><XMLDATA><HEAD><JSSJ>20240711/230444/</JSSJ><ZTCLJG>00000</ZTCLJG><CWXX>处理成功</CWXX><BZXX>进货发票传报</BZXX></HEAD></XMLDATA>";
//                }
//            }
            FileUtils.writeLog("阳光接口日志" + YQNO, resultXML);
            rtn[2] = resultXML;
            Document doc0 = XmlUtil.readXML(resultXML);
            Element elementRoot = doc0.getDocumentElement();
            Element xmlHead = XmlUtil.getElement(elementRoot, "HEAD");
//            Element JSSJ = XmlUtil.getElement(xmlHead, "JSSJ");//接收时间
//            Element ZTCLJG = XmlUtil.getElement(xmlHead, "ZTCLJG");//消息主体处理结果
//            Element CWXX = XmlUtil.getElement(xmlHead, "CWXX");//错误提示内容
            rtn[3] = CommonUtils.XmlGetNodeContent(xmlHead, "JSSJ");
            rtn[4] = CommonUtils.XmlGetNodeContent(xmlHead, "ZTCLJG");
            rtn[5] = CommonUtils.XmlGetNodeContent(xmlHead, "CWXX");
            rtn[6] = "";
            Element xmlDetail = XmlUtil.getElement(elementRoot, "DETAIL");
            if(xmlDetail!=null && xmlDetail.hasChildNodes()){
                Element xmlStruct = XmlUtil.getElement(xmlDetail, "STRUCT");
                String clqkms = CommonUtils.XmlGetNodeContent(xmlStruct, "CLQKMS");
                rtn[6] = clqkms;
            }
            //明细数据提取
            //XmlNode str = doc0.ChildNodes[1].ChildNodes[1];
            String clqkms = "";
        }
        catch(Exception ex)
        {
            rtn[6] = ex.getMessage();
        }
        if("00000".equals(rtn[4])){
            rtn[5] = "处理成功";
        }
        return rtn;
    }

//    public static String SendRecvResult(String[] Qry)
//    {
//        Document doc0 = XmlUtil.readXML(Qry[2]);
//        XmlNode xncwxx = doc0.ChildNodes[1].ChildNodes[0];
//        XmlNode nodeztcljg = xncwxx.SelectSingleNode("ZTCLJG");
//        XmlNode nodecwxx = xncwxx.SelectSingleNode("CWXX");
//        String cwxx = nodecwxx.InnerText;
//        String ztcljg = nodeztcljg.InnerText;
//        XmlNode str = doc0.ChildNodes[1].ChildNodes[1];
//        String clqkms = "";
//        String ddbh = "";//YQ005
//        if (str != null)
//        {
//            if (str.Name == "MAIN")
//            {
//                XmlNode nodeddbh = str.SelectSingleNode("DDBH");
//                {
//                    if (nodeddbh != null)
//                    {
//                        ddbh = nodeddbh.InnerText;
//                    }
//                }
//            }
//            if (str.Name != "DETAIL")
//            {
//
//                str = doc0.ChildNodes[1].ChildNodes[2];
//            }
//            if (str != null)
//            {
//                for (int i = 0; i < str.ChildNodes.Count; i++)
//                {
//                    XmlNode nodeclqkms = str.ChildNodes[i].SelectSingleNode("CLQKMS");
//                    if (nodeclqkms != null)
//                    {
//                        clqkms = clqkms + nodeclqkms.InnerText + ";";
//                    }
//
//                }
//            }
//        }
//        //nodeSelect.InnerText = "ANTU";
//
//        if (cwxx != "")
//        {
//            //Log.WriteLog(Qry[1], Qry[2]);
//            return "异常：" + Qry[0] + "返回[" + ztcljg + "]结果。" + cwxx + "|" + clqkms;
//        }
//        else if (Qry[0] == "YQ010" || Qry[0] == "YQ011" || Qry[0] == "YQ012" || Qry[0] == "YQ017" || Qry[0] == "YQ018" || Qry[0] == "YQ034")//还需要继续添加
//        {
//            return Qry[2];
//        }
//        else if (Qry[0] == "YQ005")
//        {
//            return ddbh;
//        }
//        else if (Qry[0] == "YQ015" || Qry[0] == "YQ007")
//        {
//            return doc0.ChildNodes[1].ChildNodes[1].ChildNodes[0].InnerText;
//        }
//        else
//        {
//            return ztcljg;
//        }
//
//
//    }
}
