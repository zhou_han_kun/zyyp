package com.biz.smpaa.webservice;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class YsUploadReuslt {
    private String code;
    private String msg;
}
