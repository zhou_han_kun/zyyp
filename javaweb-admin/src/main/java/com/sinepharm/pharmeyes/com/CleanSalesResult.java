package com.sinepharm.pharmeyes.com;

public class CleanSalesResult {
    private int code;
    private boolean success;
    private CleanSalesData data;
    private String msg;
    public void setCode(int code) {
        this.code = code;
    }
    public int getCode() {
        return code;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
    public boolean getSuccess() {
        return success;
    }

    public void setData(CleanSalesData data) {
        this.data = data;
    }
    public CleanSalesData getData() {
        return data;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    public String getMsg() {
        return msg;
    }
}
