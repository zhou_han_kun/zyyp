package com.sinepharm.pharmeyes.com;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@TableName("mdm_customer")
public class Customer {

    @TableId(value = "id",type = IdType.INPUT)
    private String id;
    private String createUser;
    private String createDept;
    private String createTime;
    private String updateUser;
    private String updateTime;
    private String status;
    private String isDeleted;
    private String groupMdmInvoiceDealer;
    private String processInstanceInfo;
    private String dataCode;

    /**
     *
     */
    private String datasSource;

    /**
     *
     */
    private String registerName;

    /**
     *
     */
    private String formerName;

    /**
     *
     */
    private String registerProvinceName;

    /**
     *
     */
    private String registerProvinceCode;

    /**
     *
     */
    private String registerCityName;

    /**
     *
     */
    private String registerCityCode;

    /**
     *
     */
    private String registerCountyName;

    /**
     *
     */
    private String registerCountyCode;

    /**
     *
     */
    private String registerAddress;

    /**
     *
     */
    private String registerAddressLongitudeBaidu;

    /**
     *
     */
    private String registerAddressLatitudeBaidu;

    /**
     *
     */
    private String registerAddressLongitudeGaode;

    /**
     *
     */
    private String registerAddressLatitudeGaode;

    /**
     *
     */
    private String registerAddressMapRunError;

    /**
     *
     */
    private String registerAddressMapRunStatus;

    /**
     *
     */
    private String uniformSocialCreditCode;

    /**
     *
     */
    private String attributeLevelFirst;

    /**
     *
     */
    private String attributeLevelSecond;

    /**
     *
     */
    private String attributeLevelThird;

    /**
     *
     */
    private String operationStatus;

    /**
     *
     */
    private String humanSocialLevel;

    /**
     *
     */
    private String humanSocialRank;

    /**
     *
     */
    private String healthCommissionLevel;

    /**
     *
     */
    private String healthCommissionCode;

    /**
     *
     */
    private String clientAttribute;

    /**
     *
     */
    private String clientLevel;

    /**
     *
     */
    private String clientClassify;

    /**
     *
     */
    /**
     *
     */
    private String discountAgreementType;

    /**
     *
     */
    private String ncSysCode;

    /**
     *
     */
    private String hongYingSysCode;

    /**
     *
     */
    private String crmSysCode;

    /**
     *
     */
    private String areaManagementSysCode;

    /**
     *
     */
    private String mergeDestinationName;

    /**
     *
     */
    private String mergeDestinationCode;

    /**
     *
     */
    private Integer isHis;

    /**
     *
     */
    private Integer version;
    private String mergeUser;

    /**
     *
     */
    private Integer isValid;

    /**
     *
     */
    private Integer isElectronicCommerce;

    private String byProductIsElectronicJson;
    /**
     *
     */
    private Integer isFlowOpen;

    private String byProductIsFlowOpenJson;
    /**
     *
     */
    private String chainHeadQuarterName;

    /**
     *
     */
    private String chainHeadQuarterCode;

    /**
     *
     */
    private String mergeUserName;

    /**
     *
     */
    private String businessStatus;

    /**
     *
     */
    private String operation;

    /**
     *
     */
    private String addTime;
    private String clientLevelUpdateTime;

    /**
     *
     */
    private Integer addType;

    /**
     *
     */
    private String businessPerson;

    /**
     *
     */
    private String businessDept;

    /**
     *
     */
    private String entityHongYingSysCode;

    /**
     *
     */
    private String entityRegisterName;

    /**
     *
     */
    private String generalHeadquarters;

    /**
     *
     */
    private String branchOffice;

    /**
     *
     */
    private String remarks;
    private String generalHospital;
    private String branchHospital;
    private String industryCode;
    private String industryName;

    /**
     *
     */
    private String operationStatusName;

    private String humanSocialLevelName;
    private String humanSocialRankName;
    private String healthCommissionLevelName;
    private String clientClassifyName;
    private String discountAgreementTypeName;
    private String values;
    private String invoiceNo;
    private String processInstanceId;
    /**
     *
     */
    private String isElectronicCommerceName;

    /**
     *
     */
    private String isFlowOpenName;

    private String businessStatusName;
    private String customerName;
    private String memoricCode;
    private String companyCode;
    private String applyCompanyKey;
    /**
     *
     */
    private String tenantName;

    /**
     *
     */
    private String tenantId;

    /**
     *
     */
    private String updateUserName;
    private String hospitalClassification;

    /**
     *
     */
    private String mthHospMedicineIncome;

    /**
     *
     */
    private String mthHospClinicAmount;

    /**
     *
     */
    private String mthHospBunkAmount;

    /**
     *
     */
    private String mthHospMicroEcologyQuantity;

    /**
     *
     */
    private String mthDigestiveDeptClinicAmount;

    /**
     *
     */
    private String mthPaediatricsDeptClinicAmount;

    /**
     *
     */
    private String mthPaediatricsDeptInpatientAmount;

    /**
     *
     */
    private String mthNeonatologyDeptClinicAmount;

    /**
     *
     */
    private String mthNeonatologyDeptInpatientAmount;

    /**
     *
     */
    private String mthDermatologyDeptClinicAmount;

    private String existingScaleGrade;
    private String distributionMark;
    private String creditPositionGrade;
    private String hasSaleTeamName;
    private String groupProvincialPlatMark;
    private String clientRelationGrade;
    private String blazeSpecHospital;
    private String mileStoneBificoCapsHosp;
    private String mileStoneBificoPowderHosp;
    private String mileStoneTaiersiHosp;
    private String mileStoneRibavirinHosp;
    /**
     *
     */
    private String mthDigestiveDeptInpatientAmount;

    /**
     *
     */
    private String distributorLabel;

    /**
     *
     */
    private String electronicCommerceLabel;

    private String isExistInvoice;
    private String isAddTerminal;
    private String alias;
    /**
     *
     */
    private String hisName;
    private Date ts;

    @TableField(exist = false)
    private List<HongYingList> hongYingList;
    @TableField(exist = false)
    private List<NCList> nCList;


    }
