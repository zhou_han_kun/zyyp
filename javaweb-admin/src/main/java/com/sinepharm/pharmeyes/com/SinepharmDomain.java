package com.sinepharm.pharmeyes.com;

import com.shyp.daodikeji.DDKJConfig;
import com.shyp.daodikeji.ISHYPDomain;
import com.shyp.daodikeji.SHYPConstants;

public class SinepharmDomain implements ISHYPDomain {
    @Override
    public DomainInfo getDomain(DDKJConfig config) {
        DomainInfo domainInfo = new DomainInfo(SinepharmConstants.DOMAIN_API,true);
        return domainInfo;
    }
}
