package com.sinepharm.pharmeyes.com;

public class EmployeeResult {

    private int code;
    private boolean success;
    private EmployeeData data;
    private String msg;
    public void setCode(int code) {
        this.code = code;
    }
    public int getCode() {
        return code;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
    public boolean getSuccess() {
        return success;
    }

    public void setData(EmployeeData data) {
        this.data = data;
    }
    public EmployeeData getData() {
        return data;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    public String getMsg() {
        return msg;
    }

}
