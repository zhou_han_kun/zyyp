/**
 * Copyright 2023 bejson.com
 */
package com.sinepharm.pharmeyes.com;
import java.util.Date;

public class Employee {

    private String ehrKey;
    private String personCode;
    private String personName;
    private String idType;
    private String idNumber;
    private String highestTitleRank;
    private String gender;
    private Date birthDate;
    private String nativePlace;
    private String householdType;
    private String nationality;
    private String polity;
    private String health;
    private String marital;
    private Date joinWorkDate;
    private String mobile;
    private String email;
    private String officePhone;
    private String usedName;
    private String personalStatus;
    private String retireDate;
    private String country;
    private String registeredHousehold;
    private String fileAddress;
    private String joinPolityDate;
    private String workAge;
    private String birthPlace;
    private String householdPostCode;
    private String educationMajorClassification;
    private String major;
    private String professionalTechnologyLevel;
    private String professionalQualificationLevel;
    private String housingProvidentFundAccount;
    private String socialSecurityAccount;
    private String foreignLanguageName;
    private String foreignLanguageLevel;
    private String interestAndSpeciality;
    private String groupWorkAge;
    private Date enterShanghaiPharmaceuticalsDate;
    private String workAgeAdjustment;
    private Date enterSecondLevelDate;
    private String personnelEnterFrom;
    private String isReturnee;
    private String socialSecurityPaymentType;
    private String school;
    private String companyAge;
    private String highestDegreeEducation;
    private String annualLeaveCountDate;
    private String directLeader;
    private String assurancePlace;
    private String bankName;
    private String bankAccount;
    private String bankCurrency;
    private String bankRegionCode;
    private String bankCity;
    private String bankProvince;
    private String unionBankNumber;
    private String workRegion;
    private String photo;
    private String education;
    private String primaryDegree;
    private String professionQualification;
    private String titleTechnologyPost;
    private String postCode;
    private String secondEducation;
    private String secondDegree;
    private String residenceStreet;
    private Date joinBeginDate;
    private Date endOrgDate;
    private String joinGroupDate;
    private String employeeCode;
    private String unitName;
    private String unitCode;
    private String personnelClassificationName;
    private String deptName;
    private String post;
    private String postSeriesName;
    private String primaryJob;
    private String turnsEvent;
    private String turnsType;
    private String turnsReason;
    private String primaryJobType;
    private Date beginDate;
    private String endDate;
    private String postStatus;
    private String remark;
    private String workPlace;
    private String personnelStatus;
    private String jobEnterFrom;
    private String isSpecialWorkType;
    private String postAttribute;
    private String costCenter;
    private String trialFlag;
    private String trialType;
    private String isLeave;
    private String deptKey;
    private String deptCode;
    private String processDefinitionId;
    private String processInstanceId;
    private String applyStatus;
    private String ccEmails;
    private String lastYearSaleAmount;
    private String lastYearCompleteRate;
    private String lastYearKpi;
    private String yearBeforeSaleAmount;
    private String yearBeforeCompleteRate;
    private String yearBeforeKpi;
    private String threeYearsAgoSaleAmount;
    private String threeYearsAgoCompleteRate;
    private String threeYearsAgoKpi;
    private Date lastUpdateTime;
    private String postRankSeries;
    private String postRankCode;
    private String uploadId;
    private String operation;
    private String salaryScheme;
    private String partTimeJob1;
    private String partTimeDeptName1;
    private String upPartTimeDeptName1;
    private String partTimeSuperiorLeader1;
    private String partTimeJob2;
    private String partTimeDeptName2;
    private String upPartTimeDeptName2;
    private String partTimeSuperiorLeader2;
    private String partTimeJob3;
    private String partTimeDeptName3;
    private String upPartTimeDeptName3;
    private String partTimeSuperiorLeader3;
    private String partTimeJob4;
    private String partTimeDeptName4;
    private String upPartTimeDeptName4;
    private String partTimeSuperiorLeader4;
    private String partTimeJob5;
    private String partTimeDeptName5;
    private String upPartTimeDeptName5;
    private String partTimeSuperiorLeader5;
    private String partTimeJob6;
    private String partTimeDeptName6;
    private String upPartTimeDeptName6;
    private String partTimeSuperiorLeader6;
    private String personnelClassification;
    private String examineType;
    private String upDeptName;
    private String upDeptCode;
    private String directLeaderName;
    private String updateUserName;
    private Date updateTime;
    public void setEhrKey(String ehrKey) {
        this.ehrKey = ehrKey;
    }
    public String getEhrKey() {
        return ehrKey;
    }

    public void setPersonCode(String personCode) {
        this.personCode = personCode;
    }
    public String getPersonCode() {
        return personCode;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }
    public String getPersonName() {
        return personName;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }
    public String getIdType() {
        return idType;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }
    public String getIdNumber() {
        return idNumber;
    }

    public void setHighestTitleRank(String highestTitleRank) {
        this.highestTitleRank = highestTitleRank;
    }
    public String getHighestTitleRank() {
        return highestTitleRank;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getGender() {
        return gender;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
    public Date getBirthDate() {
        return birthDate;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }
    public String getNativePlace() {
        return nativePlace;
    }

    public void setHouseholdType(String householdType) {
        this.householdType = householdType;
    }
    public String getHouseholdType() {
        return householdType;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }
    public String getNationality() {
        return nationality;
    }

    public void setPolity(String polity) {
        this.polity = polity;
    }
    public String getPolity() {
        return polity;
    }

    public void setHealth(String health) {
        this.health = health;
    }
    public String getHealth() {
        return health;
    }

    public void setMarital(String marital) {
        this.marital = marital;
    }
    public String getMarital() {
        return marital;
    }

    public void setJoinWorkDate(Date joinWorkDate) {
        this.joinWorkDate = joinWorkDate;
    }
    public Date getJoinWorkDate() {
        return joinWorkDate;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public String getMobile() {
        return mobile;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getEmail() {
        return email;
    }

    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }
    public String getOfficePhone() {
        return officePhone;
    }

    public void setUsedName(String usedName) {
        this.usedName = usedName;
    }
    public String getUsedName() {
        return usedName;
    }

    public void setPersonalStatus(String personalStatus) {
        this.personalStatus = personalStatus;
    }
    public String getPersonalStatus() {
        return personalStatus;
    }

    public void setRetireDate(String retireDate) {
        this.retireDate = retireDate;
    }
    public String getRetireDate() {
        return retireDate;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    public String getCountry() {
        return country;
    }

    public void setRegisteredHousehold(String registeredHousehold) {
        this.registeredHousehold = registeredHousehold;
    }
    public String getRegisteredHousehold() {
        return registeredHousehold;
    }

    public void setFileAddress(String fileAddress) {
        this.fileAddress = fileAddress;
    }
    public String getFileAddress() {
        return fileAddress;
    }

    public void setJoinPolityDate(String joinPolityDate) {
        this.joinPolityDate = joinPolityDate;
    }
    public String getJoinPolityDate() {
        return joinPolityDate;
    }

    public void setWorkAge(String workAge) {
        this.workAge = workAge;
    }
    public String getWorkAge() {
        return workAge;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }
    public String getBirthPlace() {
        return birthPlace;
    }

    public void setHouseholdPostCode(String householdPostCode) {
        this.householdPostCode = householdPostCode;
    }
    public String getHouseholdPostCode() {
        return householdPostCode;
    }

    public void setEducationMajorClassification(String educationMajorClassification) {
        this.educationMajorClassification = educationMajorClassification;
    }
    public String getEducationMajorClassification() {
        return educationMajorClassification;
    }

    public void setMajor(String major) {
        this.major = major;
    }
    public String getMajor() {
        return major;
    }

    public void setProfessionalTechnologyLevel(String professionalTechnologyLevel) {
        this.professionalTechnologyLevel = professionalTechnologyLevel;
    }
    public String getProfessionalTechnologyLevel() {
        return professionalTechnologyLevel;
    }

    public void setProfessionalQualificationLevel(String professionalQualificationLevel) {
        this.professionalQualificationLevel = professionalQualificationLevel;
    }
    public String getProfessionalQualificationLevel() {
        return professionalQualificationLevel;
    }

    public void setHousingProvidentFundAccount(String housingProvidentFundAccount) {
        this.housingProvidentFundAccount = housingProvidentFundAccount;
    }
    public String getHousingProvidentFundAccount() {
        return housingProvidentFundAccount;
    }

    public void setSocialSecurityAccount(String socialSecurityAccount) {
        this.socialSecurityAccount = socialSecurityAccount;
    }
    public String getSocialSecurityAccount() {
        return socialSecurityAccount;
    }

    public void setForeignLanguageName(String foreignLanguageName) {
        this.foreignLanguageName = foreignLanguageName;
    }
    public String getForeignLanguageName() {
        return foreignLanguageName;
    }

    public void setForeignLanguageLevel(String foreignLanguageLevel) {
        this.foreignLanguageLevel = foreignLanguageLevel;
    }
    public String getForeignLanguageLevel() {
        return foreignLanguageLevel;
    }

    public void setInterestAndSpeciality(String interestAndSpeciality) {
        this.interestAndSpeciality = interestAndSpeciality;
    }
    public String getInterestAndSpeciality() {
        return interestAndSpeciality;
    }

    public void setGroupWorkAge(String groupWorkAge) {
        this.groupWorkAge = groupWorkAge;
    }
    public String getGroupWorkAge() {
        return groupWorkAge;
    }

    public void setEnterShanghaiPharmaceuticalsDate(Date enterShanghaiPharmaceuticalsDate) {
        this.enterShanghaiPharmaceuticalsDate = enterShanghaiPharmaceuticalsDate;
    }
    public Date getEnterShanghaiPharmaceuticalsDate() {
        return enterShanghaiPharmaceuticalsDate;
    }

    public void setWorkAgeAdjustment(String workAgeAdjustment) {
        this.workAgeAdjustment = workAgeAdjustment;
    }
    public String getWorkAgeAdjustment() {
        return workAgeAdjustment;
    }

    public void setEnterSecondLevelDate(Date enterSecondLevelDate) {
        this.enterSecondLevelDate = enterSecondLevelDate;
    }
    public Date getEnterSecondLevelDate() {
        return enterSecondLevelDate;
    }

    public void setPersonnelEnterFrom(String personnelEnterFrom) {
        this.personnelEnterFrom = personnelEnterFrom;
    }
    public String getPersonnelEnterFrom() {
        return personnelEnterFrom;
    }

    public void setIsReturnee(String isReturnee) {
        this.isReturnee = isReturnee;
    }
    public String getIsReturnee() {
        return isReturnee;
    }

    public void setSocialSecurityPaymentType(String socialSecurityPaymentType) {
        this.socialSecurityPaymentType = socialSecurityPaymentType;
    }
    public String getSocialSecurityPaymentType() {
        return socialSecurityPaymentType;
    }

    public void setSchool(String school) {
        this.school = school;
    }
    public String getSchool() {
        return school;
    }

    public void setCompanyAge(String companyAge) {
        this.companyAge = companyAge;
    }
    public String getCompanyAge() {
        return companyAge;
    }

    public void setHighestDegreeEducation(String highestDegreeEducation) {
        this.highestDegreeEducation = highestDegreeEducation;
    }
    public String getHighestDegreeEducation() {
        return highestDegreeEducation;
    }

    public void setAnnualLeaveCountDate(String annualLeaveCountDate) {
        this.annualLeaveCountDate = annualLeaveCountDate;
    }
    public String getAnnualLeaveCountDate() {
        return annualLeaveCountDate;
    }

    public void setDirectLeader(String directLeader) {
        this.directLeader = directLeader;
    }
    public String getDirectLeader() {
        return directLeader;
    }

    public void setAssurancePlace(String assurancePlace) {
        this.assurancePlace = assurancePlace;
    }
    public String getAssurancePlace() {
        return assurancePlace;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
    public String getBankName() {
        return bankName;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }
    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankCurrency(String bankCurrency) {
        this.bankCurrency = bankCurrency;
    }
    public String getBankCurrency() {
        return bankCurrency;
    }

    public void setBankRegionCode(String bankRegionCode) {
        this.bankRegionCode = bankRegionCode;
    }
    public String getBankRegionCode() {
        return bankRegionCode;
    }

    public void setBankCity(String bankCity) {
        this.bankCity = bankCity;
    }
    public String getBankCity() {
        return bankCity;
    }

    public void setBankProvince(String bankProvince) {
        this.bankProvince = bankProvince;
    }
    public String getBankProvince() {
        return bankProvince;
    }

    public void setUnionBankNumber(String unionBankNumber) {
        this.unionBankNumber = unionBankNumber;
    }
    public String getUnionBankNumber() {
        return unionBankNumber;
    }

    public void setWorkRegion(String workRegion) {
        this.workRegion = workRegion;
    }
    public String getWorkRegion() {
        return workRegion;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    public String getPhoto() {
        return photo;
    }

    public void setEducation(String education) {
        this.education = education;
    }
    public String getEducation() {
        return education;
    }

    public void setPrimaryDegree(String primaryDegree) {
        this.primaryDegree = primaryDegree;
    }
    public String getPrimaryDegree() {
        return primaryDegree;
    }

    public void setProfessionQualification(String professionQualification) {
        this.professionQualification = professionQualification;
    }
    public String getProfessionQualification() {
        return professionQualification;
    }

    public void setTitleTechnologyPost(String titleTechnologyPost) {
        this.titleTechnologyPost = titleTechnologyPost;
    }
    public String getTitleTechnologyPost() {
        return titleTechnologyPost;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }
    public String getPostCode() {
        return postCode;
    }

    public void setSecondEducation(String secondEducation) {
        this.secondEducation = secondEducation;
    }
    public String getSecondEducation() {
        return secondEducation;
    }

    public void setSecondDegree(String secondDegree) {
        this.secondDegree = secondDegree;
    }
    public String getSecondDegree() {
        return secondDegree;
    }

    public void setResidenceStreet(String residenceStreet) {
        this.residenceStreet = residenceStreet;
    }
    public String getResidenceStreet() {
        return residenceStreet;
    }

    public void setJoinBeginDate(Date joinBeginDate) {
        this.joinBeginDate = joinBeginDate;
    }
    public Date getJoinBeginDate() {
        return joinBeginDate;
    }

    public void setEndOrgDate(Date endOrgDate) {
        this.endOrgDate = endOrgDate;
    }
    public Date getEndOrgDate() {
        return endOrgDate;
    }

    public void setJoinGroupDate(String joinGroupDate) {
        this.joinGroupDate = joinGroupDate;
    }
    public String getJoinGroupDate() {
        return joinGroupDate;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }
    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
    public String getUnitName() {
        return unitName;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode;
    }
    public String getUnitCode() {
        return unitCode;
    }

    public void setPersonnelClassificationName(String personnelClassificationName) {
        this.personnelClassificationName = personnelClassificationName;
    }
    public String getPersonnelClassificationName() {
        return personnelClassificationName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }
    public String getDeptName() {
        return deptName;
    }

    public void setPost(String post) {
        this.post = post;
    }
    public String getPost() {
        return post;
    }

    public void setPostSeriesName(String postSeriesName) {
        this.postSeriesName = postSeriesName;
    }
    public String getPostSeriesName() {
        return postSeriesName;
    }

    public void setPrimaryJob(String primaryJob) {
        this.primaryJob = primaryJob;
    }
    public String getPrimaryJob() {
        return primaryJob;
    }

    public void setTurnsEvent(String turnsEvent) {
        this.turnsEvent = turnsEvent;
    }
    public String getTurnsEvent() {
        return turnsEvent;
    }

    public void setTurnsType(String turnsType) {
        this.turnsType = turnsType;
    }
    public String getTurnsType() {
        return turnsType;
    }

    public void setTurnsReason(String turnsReason) {
        this.turnsReason = turnsReason;
    }
    public String getTurnsReason() {
        return turnsReason;
    }

    public void setPrimaryJobType(String primaryJobType) {
        this.primaryJobType = primaryJobType;
    }
    public String getPrimaryJobType() {
        return primaryJobType;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }
    public Date getBeginDate() {
        return beginDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    public String getEndDate() {
        return endDate;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }
    public String getPostStatus() {
        return postStatus;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getRemark() {
        return remark;
    }

    public void setWorkPlace(String workPlace) {
        this.workPlace = workPlace;
    }
    public String getWorkPlace() {
        return workPlace;
    }

    public void setPersonnelStatus(String personnelStatus) {
        this.personnelStatus = personnelStatus;
    }
    public String getPersonnelStatus() {
        return personnelStatus;
    }

    public void setJobEnterFrom(String jobEnterFrom) {
        this.jobEnterFrom = jobEnterFrom;
    }
    public String getJobEnterFrom() {
        return jobEnterFrom;
    }

    public void setIsSpecialWorkType(String isSpecialWorkType) {
        this.isSpecialWorkType = isSpecialWorkType;
    }
    public String getIsSpecialWorkType() {
        return isSpecialWorkType;
    }

    public void setPostAttribute(String postAttribute) {
        this.postAttribute = postAttribute;
    }
    public String getPostAttribute() {
        return postAttribute;
    }

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }
    public String getCostCenter() {
        return costCenter;
    }

    public void setTrialFlag(String trialFlag) {
        this.trialFlag = trialFlag;
    }
    public String getTrialFlag() {
        return trialFlag;
    }

    public void setTrialType(String trialType) {
        this.trialType = trialType;
    }
    public String getTrialType() {
        return trialType;
    }

    public void setIsLeave(String isLeave) {
        this.isLeave = isLeave;
    }
    public String getIsLeave() {
        return isLeave;
    }

    public void setDeptKey(String deptKey) {
        this.deptKey = deptKey;
    }
    public String getDeptKey() {
        return deptKey;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }
    public String getDeptCode() {
        return deptCode;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }
    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setApplyStatus(String applyStatus) {
        this.applyStatus = applyStatus;
    }
    public String getApplyStatus() {
        return applyStatus;
    }

    public void setCcEmails(String ccEmails) {
        this.ccEmails = ccEmails;
    }
    public String getCcEmails() {
        return ccEmails;
    }

    public void setLastYearSaleAmount(String lastYearSaleAmount) {
        this.lastYearSaleAmount = lastYearSaleAmount;
    }
    public String getLastYearSaleAmount() {
        return lastYearSaleAmount;
    }

    public void setLastYearCompleteRate(String lastYearCompleteRate) {
        this.lastYearCompleteRate = lastYearCompleteRate;
    }
    public String getLastYearCompleteRate() {
        return lastYearCompleteRate;
    }

    public void setLastYearKpi(String lastYearKpi) {
        this.lastYearKpi = lastYearKpi;
    }
    public String getLastYearKpi() {
        return lastYearKpi;
    }

    public void setYearBeforeSaleAmount(String yearBeforeSaleAmount) {
        this.yearBeforeSaleAmount = yearBeforeSaleAmount;
    }
    public String getYearBeforeSaleAmount() {
        return yearBeforeSaleAmount;
    }

    public void setYearBeforeCompleteRate(String yearBeforeCompleteRate) {
        this.yearBeforeCompleteRate = yearBeforeCompleteRate;
    }
    public String getYearBeforeCompleteRate() {
        return yearBeforeCompleteRate;
    }

    public void setYearBeforeKpi(String yearBeforeKpi) {
        this.yearBeforeKpi = yearBeforeKpi;
    }
    public String getYearBeforeKpi() {
        return yearBeforeKpi;
    }

    public void setThreeYearsAgoSaleAmount(String threeYearsAgoSaleAmount) {
        this.threeYearsAgoSaleAmount = threeYearsAgoSaleAmount;
    }
    public String getThreeYearsAgoSaleAmount() {
        return threeYearsAgoSaleAmount;
    }

    public void setThreeYearsAgoCompleteRate(String threeYearsAgoCompleteRate) {
        this.threeYearsAgoCompleteRate = threeYearsAgoCompleteRate;
    }
    public String getThreeYearsAgoCompleteRate() {
        return threeYearsAgoCompleteRate;
    }

    public void setThreeYearsAgoKpi(String threeYearsAgoKpi) {
        this.threeYearsAgoKpi = threeYearsAgoKpi;
    }
    public String getThreeYearsAgoKpi() {
        return threeYearsAgoKpi;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }
    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setPostRankSeries(String postRankSeries) {
        this.postRankSeries = postRankSeries;
    }
    public String getPostRankSeries() {
        return postRankSeries;
    }

    public void setPostRankCode(String postRankCode) {
        this.postRankCode = postRankCode;
    }
    public String getPostRankCode() {
        return postRankCode;
    }

    public void setUploadId(String uploadId) {
        this.uploadId = uploadId;
    }
    public String getUploadId() {
        return uploadId;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
    public String getOperation() {
        return operation;
    }

    public void setSalaryScheme(String salaryScheme) {
        this.salaryScheme = salaryScheme;
    }
    public String getSalaryScheme() {
        return salaryScheme;
    }

    public void setPartTimeJob1(String partTimeJob1) {
        this.partTimeJob1 = partTimeJob1;
    }
    public String getPartTimeJob1() {
        return partTimeJob1;
    }

    public void setPartTimeDeptName1(String partTimeDeptName1) {
        this.partTimeDeptName1 = partTimeDeptName1;
    }
    public String getPartTimeDeptName1() {
        return partTimeDeptName1;
    }

    public void setUpPartTimeDeptName1(String upPartTimeDeptName1) {
        this.upPartTimeDeptName1 = upPartTimeDeptName1;
    }
    public String getUpPartTimeDeptName1() {
        return upPartTimeDeptName1;
    }

    public void setPartTimeSuperiorLeader1(String partTimeSuperiorLeader1) {
        this.partTimeSuperiorLeader1 = partTimeSuperiorLeader1;
    }
    public String getPartTimeSuperiorLeader1() {
        return partTimeSuperiorLeader1;
    }

    public void setPartTimeJob2(String partTimeJob2) {
        this.partTimeJob2 = partTimeJob2;
    }
    public String getPartTimeJob2() {
        return partTimeJob2;
    }

    public void setPartTimeDeptName2(String partTimeDeptName2) {
        this.partTimeDeptName2 = partTimeDeptName2;
    }
    public String getPartTimeDeptName2() {
        return partTimeDeptName2;
    }

    public void setUpPartTimeDeptName2(String upPartTimeDeptName2) {
        this.upPartTimeDeptName2 = upPartTimeDeptName2;
    }
    public String getUpPartTimeDeptName2() {
        return upPartTimeDeptName2;
    }

    public void setPartTimeSuperiorLeader2(String partTimeSuperiorLeader2) {
        this.partTimeSuperiorLeader2 = partTimeSuperiorLeader2;
    }
    public String getPartTimeSuperiorLeader2() {
        return partTimeSuperiorLeader2;
    }

    public void setPartTimeJob3(String partTimeJob3) {
        this.partTimeJob3 = partTimeJob3;
    }
    public String getPartTimeJob3() {
        return partTimeJob3;
    }

    public void setPartTimeDeptName3(String partTimeDeptName3) {
        this.partTimeDeptName3 = partTimeDeptName3;
    }
    public String getPartTimeDeptName3() {
        return partTimeDeptName3;
    }

    public void setUpPartTimeDeptName3(String upPartTimeDeptName3) {
        this.upPartTimeDeptName3 = upPartTimeDeptName3;
    }
    public String getUpPartTimeDeptName3() {
        return upPartTimeDeptName3;
    }

    public void setPartTimeSuperiorLeader3(String partTimeSuperiorLeader3) {
        this.partTimeSuperiorLeader3 = partTimeSuperiorLeader3;
    }
    public String getPartTimeSuperiorLeader3() {
        return partTimeSuperiorLeader3;
    }

    public void setPartTimeJob4(String partTimeJob4) {
        this.partTimeJob4 = partTimeJob4;
    }
    public String getPartTimeJob4() {
        return partTimeJob4;
    }

    public void setPartTimeDeptName4(String partTimeDeptName4) {
        this.partTimeDeptName4 = partTimeDeptName4;
    }
    public String getPartTimeDeptName4() {
        return partTimeDeptName4;
    }

    public void setUpPartTimeDeptName4(String upPartTimeDeptName4) {
        this.upPartTimeDeptName4 = upPartTimeDeptName4;
    }
    public String getUpPartTimeDeptName4() {
        return upPartTimeDeptName4;
    }

    public void setPartTimeSuperiorLeader4(String partTimeSuperiorLeader4) {
        this.partTimeSuperiorLeader4 = partTimeSuperiorLeader4;
    }
    public String getPartTimeSuperiorLeader4() {
        return partTimeSuperiorLeader4;
    }

    public void setPartTimeJob5(String partTimeJob5) {
        this.partTimeJob5 = partTimeJob5;
    }
    public String getPartTimeJob5() {
        return partTimeJob5;
    }

    public void setPartTimeDeptName5(String partTimeDeptName5) {
        this.partTimeDeptName5 = partTimeDeptName5;
    }
    public String getPartTimeDeptName5() {
        return partTimeDeptName5;
    }

    public void setUpPartTimeDeptName5(String upPartTimeDeptName5) {
        this.upPartTimeDeptName5 = upPartTimeDeptName5;
    }
    public String getUpPartTimeDeptName5() {
        return upPartTimeDeptName5;
    }

    public void setPartTimeSuperiorLeader5(String partTimeSuperiorLeader5) {
        this.partTimeSuperiorLeader5 = partTimeSuperiorLeader5;
    }
    public String getPartTimeSuperiorLeader5() {
        return partTimeSuperiorLeader5;
    }

    public void setPartTimeJob6(String partTimeJob6) {
        this.partTimeJob6 = partTimeJob6;
    }
    public String getPartTimeJob6() {
        return partTimeJob6;
    }

    public void setPartTimeDeptName6(String partTimeDeptName6) {
        this.partTimeDeptName6 = partTimeDeptName6;
    }
    public String getPartTimeDeptName6() {
        return partTimeDeptName6;
    }

    public void setUpPartTimeDeptName6(String upPartTimeDeptName6) {
        this.upPartTimeDeptName6 = upPartTimeDeptName6;
    }
    public String getUpPartTimeDeptName6() {
        return upPartTimeDeptName6;
    }

    public void setPartTimeSuperiorLeader6(String partTimeSuperiorLeader6) {
        this.partTimeSuperiorLeader6 = partTimeSuperiorLeader6;
    }
    public String getPartTimeSuperiorLeader6() {
        return partTimeSuperiorLeader6;
    }

    public void setPersonnelClassification(String personnelClassification) {
        this.personnelClassification = personnelClassification;
    }
    public String getPersonnelClassification() {
        return personnelClassification;
    }

    public void setExamineType(String examineType) {
        this.examineType = examineType;
    }
    public String getExamineType() {
        return examineType;
    }

    public void setUpDeptName(String upDeptName) {
        this.upDeptName = upDeptName;
    }
    public String getUpDeptName() {
        return upDeptName;
    }

    public void setUpDeptCode(String upDeptCode) {
        this.upDeptCode = upDeptCode;
    }
    public String getUpDeptCode() {
        return upDeptCode;
    }

    public void setDirectLeaderName(String directLeaderName) {
        this.directLeaderName = directLeaderName;
    }
    public String getDirectLeaderName() {
        return directLeaderName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }
    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public Date getUpdateTime() {
        return updateTime;
    }

}