package com.sinepharm.pharmeyes.com;

public class NCSaleResult {
    private boolean success;
    private int code;
    private String message;
    private String msg;
    private int pages;
    private NCSaleData data;
    public void setSuccess(boolean value) {
        this.success = value;
    }
    public boolean getSuccess() {
        return success;
    }

    public void setCode(int code) {
        this.code = code;
    }
    public int getCode() {
        return code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public NCSaleData getData() {
        return data;
    }

    public void setData(NCSaleData data) {
        this.data = data;
    }
}
