package com.sinepharm.pharmeyes.com;

import org.apache.http.client.HttpClient;

public class SinepharmConstants {
    public enum SignType {
        MD5, HMACSHA256
    }

    public enum RequestMethod {
        GET, POST
    }
    public static final String DOMAIN_API = "sinepharm.pharmeyes.com:7443/external/master/v1";
    public static final String DOMAIN_API_TEST ="phmduat.pharmeyes.com:35000/external/master/v1";
    public static final String DOMAIN_API2 = "sphtianyi.sinoeyes.com:38888/external/master";
    public static final String DOMAIN_API_NOVERSION = "sphtianyi.sinoeyes.com:38888/external/master";
    public static final String DOMAIN_API_NOVERSION_TEST = "phmduat.pharmeyes.com:35000/external/master";
    public static final String DOMAIN_MOCK_API = "mockjs.xiaoyaoji.cn/mock/1mDb67vQpxA/open-api/v1";

    public static final String ZYYPT_DOMAIN_API = "61.129.134.231:30303/api/service/gateway";
    public static final String ZYYPT_DOMAIN_TEST_API = "61.129.134.241:30304/api/service/gateway";
    public static final String ZYYPT_UPLOAD_DOMAIN_API = "61.129.134.231:30303/File/UploadFile";
    public static final String ZYYPT_UPLOAD_DOMAIN_TEST_API = "61.129.134.241:30304/File/UploadFile";



    public static final String FAIL     = "FAIL";
    public static final String SUCCESS  = "SUCCESS";
    public static final String HMACSHA256 = "HMAC-SHA256";
    public static final String MD5 = "MD5";

    public static final String FIELD_SIGN = "sign";
    public static final String FIELD_SIGN_TYPE = "sign_type";

    public static final String WXPAYSDK_VERSION = "WXPaySDK/3.0.9";
    public static final String USER_AGENT = WXPAYSDK_VERSION +
            " (" + System.getProperty("os.arch") + " " + System.getProperty("os.name") + " " + System.getProperty("os.version") +
            ") Java/" + System.getProperty("java.version") + " HttpClient/" + HttpClient.class.getPackage().getImplementationVersion();

    public static final String GETPRODUCT     = "/GetProduct";
    public static final String GETCUSTOMER    = "/GetCustomer";
    public static final String GETNCSALE      ="/nc/ncSale/list";
    public static final String GETCLEANSALES      ="/nc/cleanSales/list";
    public static final String GETEMPLOYEE      ="/GetEmployeeData";
    public static final String GETMONTHTARGET      ="/GetMonthTarget";
    public static final String GETCLEANINVENTORY      ="/phmdcloud-data/cleanInventory/list";
    public static final String GETEXPERT      ="/ExpertClean1173";

}
