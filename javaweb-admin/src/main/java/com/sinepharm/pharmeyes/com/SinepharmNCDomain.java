package com.sinepharm.pharmeyes.com;

import com.shyp.daodikeji.DDKJConfig;
import com.shyp.daodikeji.ISHYPDomain;

public class SinepharmNCDomain implements ISHYPDomain {
    @Override
    public DomainInfo getDomain(DDKJConfig config) {
        DomainInfo domainInfo = new DomainInfo(SinepharmConstants.DOMAIN_API_NOVERSION,true);
        return domainInfo;
    }
}