package com.sinepharm.pharmeyes.com;

import com.baomidou.mybatisplus.annotation.TableName;

@TableName("mdm_customer_nc_list")
public class NCList {
    private String dataCode;
    private String code;
    private long createTime;
    public void setCode(String code) {
        this.code = code;
    }
    public String getCode() {
        return code;
    }

    public void setCreateTime(long create_time) {
        this.createTime = create_time;
    }
    public long getCreateTime() {
        return createTime;
    }

    public String getDataCode() {
        return dataCode;
    }

    public void setDataCode(String dataCode) {
        this.dataCode = dataCode;
    }
}
