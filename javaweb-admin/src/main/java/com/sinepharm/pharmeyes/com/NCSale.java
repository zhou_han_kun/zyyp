package com.sinepharm.pharmeyes.com;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class NCSale {
        private BigInteger id;
        private String createUser;
        private String createDept;
        private Date createTime;
        private String updateUser;
        private Date updateTime;
        private Integer status;
        private Integer isDeleted;
        private String tenantId;
        private String csaleinvoicebid;
        private String orgCode;
        private String vgoldtaxcode;
        private Date kprq;
        private String khbm;
        private String khmc;
        private String wlbm;
        private String wlmc;
        private String gg;
        private String sccj;
        private String dw;
        private BigDecimal nnum;
        private String vlotno;
        private Date dproDate;
        private Date vinvaliddate;
        private BigDecimal norigtaxnetprice;
        private BigDecimal nqtorignetprice;
        private BigDecimal ntaxrate;
        private BigDecimal ntax;
        private BigDecimal norigmny;
        private BigDecimal norigtaxmny;
        private String addrname;
        private String psnname;
        private String deptname;
        private Date ts;
        private Integer dr;
        private Date createDate;
        private String businame;
        private String mdmCode;
        private String dataCode;
        private String registerName;
        private String employeeCode;
        private String mdmProductName;
        public void setId(BigInteger id) {
            this.id = id;
        }
        public BigInteger getId() {
            return id;
        }

        public void setCreateUser(String createUser) {
            this.createUser = createUser;
        }
        public String getCreateUser() {
            return createUser;
        }

        public void setCreateDept(String createDept) {
            this.createDept = createDept;
        }
        public String getCreateDept() {
            return createDept;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }
        public Date getCreateTime() {
            return createTime;
        }

        public void setUpdateUser(String updateUser) {
            this.updateUser = updateUser;
        }
        public String getUpdateUser() {
            return updateUser;
        }

        public void setUpdateTime(Date updateTime) {
            this.updateTime = updateTime;
        }
        public Date getUpdateTime() {
            return updateTime;
        }

        public void setStatus(int status) {
            this.status = status;
        }
        public int getStatus() {
            return status;
        }

        public void setIsDeleted(int isDeleted) {
            this.isDeleted = isDeleted;
        }
        public int getIsDeleted() {
            return isDeleted;
        }

        public void setTenantId(String tenantId) {
            this.tenantId = tenantId;
        }
        public String getTenantId() {
            return tenantId;
        }

        public void setCsaleinvoicebid(String csaleinvoicebid) {
            this.csaleinvoicebid = csaleinvoicebid;
        }
        public String getCsaleinvoicebid() {
            return csaleinvoicebid;
        }

        public void setOrgCode(String orgCode) {
            this.orgCode = orgCode;
        }
        public String getOrgCode() {
            return orgCode;
        }

        public void setVgoldtaxcode(String vgoldtaxcode) {
            this.vgoldtaxcode = vgoldtaxcode;
        }
        public String getVgoldtaxcode() {
            return vgoldtaxcode;
        }

        public void setKprq(Date kprq) {
            this.kprq = kprq;
        }
        public Date getKprq() {
            return kprq;
        }

        public void setKhbm(String khbm) {
            this.khbm = khbm;
        }
        public String getKhbm() {
            return khbm;
        }

        public void setKhmc(String khmc) {
            this.khmc = khmc;
        }
        public String getKhmc() {
            return khmc;
        }

        public void setWlbm(String wlbm) {
            this.wlbm = wlbm;
        }
        public String getWlbm() {
            return wlbm;
        }

        public void setWlmc(String wlmc) {
            this.wlmc = wlmc;
        }
        public String getWlmc() {
            return wlmc;
        }

        public void setGg(String gg) {
            this.gg = gg;
        }
        public String getGg() {
            return gg;
        }

        public void setSccj(String sccj) {
            this.sccj = sccj;
        }
        public String getSccj() {
            return sccj;
        }

        public void setDw(String dw) {
            this.dw = dw;
        }
        public String getDw() {
            return dw;
        }

        public void setNnum(BigDecimal nnum) {
            this.nnum = nnum;
        }
        public BigDecimal getNnum() {
            return nnum;
        }

        public void setVlotno(String vlotno) {
            this.vlotno = vlotno;
        }
        public String getVlotno() {
            return vlotno;
        }

        public void setDproDate(Date dproDate) {
            this.dproDate = dproDate;
        }
        public Date getDproDate() {
            return dproDate;
        }

        public void setVinvaliddate(Date vinvaliddate) {
            this.vinvaliddate = vinvaliddate;
        }
        public Date getVinvaliddate() {
            return vinvaliddate;
        }

        public void setNorigtaxnetprice(BigDecimal norigtaxnetprice) {
            this.norigtaxnetprice = norigtaxnetprice;
        }
        public BigDecimal getNorigtaxnetprice() {
            return norigtaxnetprice;
        }

        public void setNqtorignetprice(BigDecimal nqtorignetprice) {
            this.nqtorignetprice = nqtorignetprice;
        }
        public BigDecimal getNqtorignetprice() {
            return nqtorignetprice;
        }

        public void setNtaxrate(BigDecimal ntaxrate) {
            this.ntaxrate = ntaxrate;
        }
        public BigDecimal getNtaxrate() {
            return ntaxrate;
        }

        public void setNtax(BigDecimal ntax) {
            this.ntax = ntax;
        }
        public BigDecimal getNtax() {
            return ntax;
        }

        public void setNorigmny(BigDecimal norigmny) {
            this.norigmny = norigmny;
        }
        public BigDecimal getNorigmny() {
            return norigmny;
        }

        public void setNorigtaxmny(BigDecimal norigtaxmny) {
            this.norigtaxmny = norigtaxmny;
        }
        public BigDecimal getNorigtaxmny() {
            return norigtaxmny;
        }

        public void setAddrname(String addrname) {
            this.addrname = addrname;
        }
        public String getAddrname() {
            return addrname;
        }

        public void setPsnname(String psnname) {
            this.psnname = psnname;
        }
        public String getPsnname() {
            return psnname;
        }

        public void setDeptname(String deptname) {
            this.deptname = deptname;
        }
        public String getDeptname() {
            return deptname;
        }

        public void setTs(Date ts) {
            this.ts = ts;
        }
        public Date getTs() {
            return ts;
        }

        public void setDr(Integer dr) {
            this.dr = dr;
        }
        public Integer getDr() {
            return dr;
        }

        public void setCreateDate(Date createDate) {
            this.createDate = createDate;
        }
        public Date getCreateDate() {
            return createDate;
        }

        public void setBusiname(String businame) {
            this.businame = businame;
        }
        public String getBusiname() {
            return businame;
        }

        public void setMdmCode(String mdmCode) {
            this.mdmCode = mdmCode;
        }
        public String getMdmCode() {
            return mdmCode;
        }

        public void setDataCode(String dataCode) {
            this.dataCode = dataCode;
        }
        public String getDataCode() {
            return dataCode;
        }

        public void setRegisterName(String registerName) {
            this.registerName = registerName;
        }
        public String getRegisterName() {
            return registerName;
        }

        public void setEmployeeCode(String employeeCode) {
            this.employeeCode = employeeCode;
        }
        public String getEmployeeCode() {
            return employeeCode;
        }

        public void setMdmProductName(String mdmProductName) {
            this.mdmProductName = mdmProductName;
        }
        public String getMdmProductName() {
            return mdmProductName;
        }

    }