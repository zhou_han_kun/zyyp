package com.sinepharm.pharmeyes.com;

public class HongYingList {

    private String dataCode;
    private String code;
    private long create_time;
    public void setCode(String code) {
        this.code = code;
    }
    public String getCode() {
        return code;
    }

    public void setCreateTime(long create_time) {
        this.create_time = create_time;
    }
    public long getCreateTime() {
        return create_time;
    }

    public String getDataCode() {
        return dataCode;
    }

    public void setDataCode(String dataCode) {
        this.dataCode = dataCode;
    }
}
