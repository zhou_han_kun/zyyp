package com.sinepharm.pharmeyes.com;

import com.shyp.daodikeji.DDKJConfig;
import com.shyp.daodikeji.ISHYPDomain;

public class SinepharmTestDomain implements ISHYPDomain {
    @Override
    public DomainInfo getDomain(DDKJConfig config) {
        DomainInfo domainInfo = new DomainInfo(SinepharmConstants.DOMAIN_API_TEST,true);
        return domainInfo;
    }
}