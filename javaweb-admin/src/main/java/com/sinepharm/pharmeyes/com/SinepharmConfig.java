package com.sinepharm.pharmeyes.com;

import com.javaweb.common.config.CommonConfig;
import com.shyp.daodikeji.DDKJConfig;
import com.shyp.daodikeji.ISHYPDomain;
import com.shyp.daodikeji.SHYPDomain;

public class SinepharmConfig extends DDKJConfig{
    @Override
    public String getAppID() {
        return CommonConfig.appTenantId;
    }

    @Override
    public String getEnterpriseId() {
        return CommonConfig.enterpriseId;
    }

    @Override
    public Boolean useMock() {
        return CommonConfig.useMock;
    }

    @Override
    public String getSecret() {
        return CommonConfig.appTenantSecret;
    }


    @Override
    public ISHYPDomain getSHYPDomain() {
        return new SinepharmDomain();
    }
}
