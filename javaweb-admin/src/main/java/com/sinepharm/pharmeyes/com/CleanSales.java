package com.sinepharm.pharmeyes.com;

import java.math.BigDecimal;
import java.util.Date;

public class CleanSales {

    private String id;
    private String createUser;
    private String createDept;
    private Date createTime;
    private String updateUser;
    private Date updateTime;
    private String executionMonth;
    private String businessMonth;
    private Date dataUpdateTime;
    private int status;
    private int isDeleted;
    private String tenantId;
    private String saleDate;
    private String sellerCode;
    private String sellerName;
    private String originalSellerName;
    private String purchaserCode;
    private String purchaserName;
    private String productCode;
    private String productName;
    private String productSpecification;
    private String originalProductCode;
    private String originalProductName;
    private String originalProductSpecification;
    private String productUnit;
    private String saleQuantity;
    private String productPrice;
    private String purchaseAmount;
    private String manufacturer;
    private String productVlotno;
    private String expirationDate;
    private String purchaserAddress;
    private int isAdd;
    private String dairyName;
    private String uploadId;
    private String remark;
    private String isAddName;
    private String phnCode;
    private String salePhnCode;
    private String buyPhnCode;
    private String sealGeneralHospital;
    private String sealBranchHospital;
    private String buyGeneralHospital;
    private String buyBranchHospital;
    private String sealGeneralHospitalCode;
    private String sealBranchHospitalCode;
    private String buyGeneralHospitalCode;
    private String buyBranchHospitalCode;
    private String saleTotalSplitRelationship;
    private String saleTotalSplitRelationshipCode;
    private String purchaserLevelNature;
    private String purchaserOriginalName;
    private String purchaserTotalSplitRelationship;
    private String purchaserTotalSplitRelationshipCode;
    private String phnName;
    private String branchHospitalCode;
    private String branchHospitalName;
    private String mdmCode;
    private String mdmProductName;
    private String registerName;
    private String serialNumber;

    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }
    public String getCreateUser() {
        return createUser;
    }

    public void setCreateDept(String createDept) {
        this.createDept = createDept;
    }
    public String getCreateDept() {
        return createDept;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }
    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setExecutionMonth(String executionMonth) {
        this.executionMonth = executionMonth;
    }
    public String getExecutionMonth() {
        return executionMonth;
    }

    public void setBusinessMonth(String businessMonth) {
        this.businessMonth = businessMonth;
    }
    public String getBusinessMonth() {
        return businessMonth;
    }

    public void setDataUpdateTime(Date dataUpdateTime) {
        this.dataUpdateTime = dataUpdateTime;
    }
    public Date getDataUpdateTime() {
        return dataUpdateTime;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    public int getStatus() {
        return status;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }
    public int getIsDeleted() {
        return isDeleted;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }
    public String getTenantId() {
        return tenantId;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }
    public String getSaleDate() {
        return saleDate;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }
    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }
    public String getSellerName() {
        return sellerName;
    }

    public void setOriginalSellerName(String originalSellerName) {
        this.originalSellerName = originalSellerName;
    }
    public String getOriginalSellerName() {
        return originalSellerName;
    }

    public void setPurchaserCode(String purchaserCode) {
        this.purchaserCode = purchaserCode;
    }
    public String getPurchaserCode() {
        return purchaserCode;
    }

    public void setPurchaserName(String purchaserName) {
        this.purchaserName = purchaserName;
    }
    public String getPurchaserName() {
        return purchaserName;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    public String getProductCode() {
        return productCode;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
    public String getProductName() {
        return productName;
    }

    public void setProductSpecification(String productSpecification) {
        this.productSpecification = productSpecification;
    }
    public String getProductSpecification() {
        return productSpecification;
    }

    public void setOriginalProductCode(String originalProductCode) {
        this.originalProductCode = originalProductCode;
    }
    public String getOriginalProductCode() {
        return originalProductCode;
    }

    public void setOriginalProductName(String originalProductName) {
        this.originalProductName = originalProductName;
    }
    public String getOriginalProductName() {
        return originalProductName;
    }

    public void setOriginalProductSpecification(String originalProductSpecification) {
        this.originalProductSpecification = originalProductSpecification;
    }
    public String getOriginalProductSpecification() {
        return originalProductSpecification;
    }

    public void setProductUnit(String productUnit) {
        this.productUnit = productUnit;
    }
    public String getProductUnit() {
        return productUnit;
    }

    public void setSaleQuantity(String saleQuantity) {
        this.saleQuantity = saleQuantity;
    }
    public String getSaleQuantity() {
        return saleQuantity;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }
    public String getProductPrice() {
        return productPrice;
    }

    public void setPurchaseAmount(String purchaseAmount) {
        this.purchaseAmount = purchaseAmount;
    }
    public String getPurchaseAmount() {
        return purchaseAmount;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    public String getManufacturer() {
        return manufacturer;
    }

    public void setProductVlotno(String productVlotno) {
        this.productVlotno = productVlotno;
    }
    public String getProductVlotno() {
        return productVlotno;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }
    public String getExpirationDate() {
        return expirationDate;
    }

    public void setPurchaserAddress(String purchaserAddress) {
        this.purchaserAddress = purchaserAddress;
    }
    public String getPurchaserAddress() {
        return purchaserAddress;
    }

    public void setIsAdd(int isAdd) {
        this.isAdd = isAdd;
    }
    public int getIsAdd() {
        return isAdd;
    }

    public void setDairyName(String dairyName) {
        this.dairyName = dairyName;
    }
    public String getDairyName() {
        return dairyName;
    }

    public void setUploadId(String uploadId) {
        this.uploadId = uploadId;
    }
    public String getUploadId() {
        return uploadId;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getRemark() {
        return remark;
    }

    public void setIsAddName(String isAddName) {
        this.isAddName = isAddName;
    }
    public String getIsAddName() {
        return isAddName;
    }

    public String getPhnCode() {
        return phnCode;
    }

    public void setPhnCode(String phnCode) {
        this.phnCode = phnCode;
    }

    public String getSalePhnCode() {
        return salePhnCode;
    }

    public void setSalePhnCode(String salePhnCode) {
        this.salePhnCode = salePhnCode;
    }

    public String getBuyPhnCode() {
        return buyPhnCode;
    }

    public void setBuyPhnCode(String buyPhnCode) {
        this.buyPhnCode = buyPhnCode;
    }

    public String getSealGeneralHospital() {
        return sealGeneralHospital;
    }

    public void setSealGeneralHospital(String sealGeneralHospital) {
        this.sealGeneralHospital = sealGeneralHospital;
    }

    public String getSealBranchHospital() {
        return sealBranchHospital;
    }

    public void setSealBranchHospital(String sealBranchHospital) {
        this.sealBranchHospital = sealBranchHospital;
    }

    public String getBuyGeneralHospital() {
        return buyGeneralHospital;
    }

    public void setBuyGeneralHospital(String buyGeneralHospital) {
        this.buyGeneralHospital = buyGeneralHospital;
    }

    public String getBuyBranchHospital() {
        return buyBranchHospital;
    }

    public void setBuyBranchHospital(String buyBranchHospital) {
        this.buyBranchHospital = buyBranchHospital;
    }

    public String getSealGeneralHospitalCode() {
        return sealGeneralHospitalCode;
    }

    public void setSealGeneralHospitalCode(String sealGeneralHospitalCode) {
        this.sealGeneralHospitalCode = sealGeneralHospitalCode;
    }

    public String getSealBranchHospitalCode() {
        return sealBranchHospitalCode;
    }

    public void setSealBranchHospitalCode(String sealBranchHospitalCode) {
        this.sealBranchHospitalCode = sealBranchHospitalCode;
    }

    public String getBuyGeneralHospitalCode() {
        return buyGeneralHospitalCode;
    }

    public void setBuyGeneralHospitalCode(String buyGeneralHospitalCode) {
        this.buyGeneralHospitalCode = buyGeneralHospitalCode;
    }

    public String getBuyBranchHospitalCode() {
        return buyBranchHospitalCode;
    }

    public void setBuyBranchHospitalCode(String buyBranchHospitalCode) {
        this.buyBranchHospitalCode = buyBranchHospitalCode;
    }

    public String getSaleTotalSplitRelationship() {
        return saleTotalSplitRelationship;
    }

    public void setSaleTotalSplitRelationship(String saleTotalSplitRelationship) {
        this.saleTotalSplitRelationship = saleTotalSplitRelationship;
    }

    public String getSaleTotalSplitRelationshipCode() {
        return saleTotalSplitRelationshipCode;
    }

    public void setSaleTotalSplitRelationshipCode(String saleTotalSplitRelationshipCode) {
        this.saleTotalSplitRelationshipCode = saleTotalSplitRelationshipCode;
    }

    public String getPurchaserLevelNature() {
        return purchaserLevelNature;
    }

    public void setPurchaserLevelNature(String purchaserLevelNature) {
        this.purchaserLevelNature = purchaserLevelNature;
    }

    public String getPurchaserOriginalName() {
        return purchaserOriginalName;
    }

    public void setPurchaserOriginalName(String purchaserOriginalName) {
        this.purchaserOriginalName = purchaserOriginalName;
    }

    public String getPurchaserTotalSplitRelationship() {
        return purchaserTotalSplitRelationship;
    }

    public void setPurchaserTotalSplitRelationship(String purchaserTotalSplitRelationship) {
        this.purchaserTotalSplitRelationship = purchaserTotalSplitRelationship;
    }

    public String getPurchaserTotalSplitRelationshipCode() {
        return purchaserTotalSplitRelationshipCode;
    }

    public void setPurchaserTotalSplitRelationshipCode(String purchaserTotalSplitRelationshipCode) {
        this.purchaserTotalSplitRelationshipCode = purchaserTotalSplitRelationshipCode;
    }

    public String getPhnName() {
        return phnName;
    }

    public void setPhnName(String phnName) {
        this.phnName = phnName;
    }

    public String getBranchHospitalCode() {
        return branchHospitalCode;
    }

    public void setBranchHospitalCode(String branchHospitalCode) {
        this.branchHospitalCode = branchHospitalCode;
    }

    public String getBranchHospitalName() {
        return branchHospitalName;
    }

    public void setBranchHospitalName(String branchHospitalName) {
        this.branchHospitalName = branchHospitalName;
    }

    public String getMdmCode() {
        return mdmCode;
    }

    public void setMdmCode(String mdmCode) {
        this.mdmCode = mdmCode;
    }

    public String getMdmProductName() {
        return mdmProductName;
    }

    public void setMdmProductName(String mdmProductName) {
        this.mdmProductName = mdmProductName;
    }

    public String getRegisterName() {
        return registerName;
    }

    public void setRegisterName(String registerName) {
        this.registerName = registerName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}