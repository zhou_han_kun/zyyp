package com.sinepharm.pharmeyes.com;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class MonthTarget {
    @TableId(type = IdType.INPUT)
    /**
     * id
     */
    private String id;

    /**
     * vertionName
     */
    private String vertionName;

    /**
     * targetType
     */
    private String targetType;

    /**
     * month
     */
    private String month;

    /**
     * department1
     */
    private String department1;

    /**
     * managerCode1
     */
    private String managerCode1;

    /**
     * managerNumber1
     */
    private String managerNumber1;

    /**
     * managerName1
     */
    private String managerName1;

    /**
     * department2
     */
    private String department2;

    /**
     * managerCode2
     */
    private String managerCode2;

    /**
     * managerNumber2
     */
    private String managerNumber2;

    /**
     * managerName2
     */
    private String managerName2;

    /**
     * department3
     */
    private String department3;

    /**
     * managerCode3
     */
    private String managerCode3;

    /**
     * managerNumber3
     */
    private String managerNumber3;

    /**
     * managerName3
     */
    private String managerName3;

    /**
     * department4
     */
    private String department4;

    /**
     * managerCode4
     */
    private String managerCode4;

    /**
     * managerNumber4
     */
    private String managerNumber4;

    /**
     * managerName4
     */
    private String managerName4;

    /**
     * department5
     */
    private String department5;

    /**
     * managerCode5
     */
    private String managerCode5;

    /**
     * managerNumber5
     */
    private String managerNumber5;

    /**
     * managerName5
     */
    private String managerName5;

    /**
     * employeeCode
     */
    private String employeeCode;

    /**
     * employeeNumber
     */
    private String employeeNumber;

    /**
     * employeeName
     */
    private String employeeName;

    /**
     * juriCode
     */
    private String juriCode;

    /**
     * orgType
     */
    private String orgType;

    /**
     * orgCode
     */
    private String orgCode;

    /**
     * orgName
     */
    private String orgName;

    /**
     * orgAlias
     */
    private String orgAlias;

    /**
     * orgHeadquarters
     */
    private String orgHeadquarters;

    /**
     * prodAttribute
     */
    private String prodAttribute;

    /**
     * prodCategorize
     */
    private String prodCategorize;

    /**
     * prodCode
     */
    private String prodCode;

    /**
     * prodName
     */
    private String prodName;

    /**
     * prodSpec
     */
    private String prodSpec;

    /**
     * price
     */
    private String price;

    /**
     * quantity
     */
    private String quantity;

    /**
     * amount
     */
    private String amount;

    /**
     * quantityed
     */
    private String quantityed;

    /**
     * amounted
     */
    private String amounted;

    /**
     * confirmQuantityed
     */
    private String confirmQuantityed;

    /**
     * confirmAmounted
     */
    private String confirmAmounted;

    /**
     * createTime
     */
    private Date createTime;

    /**
     * updateTime
     */
    private Date updateTime;

    /**
     * year
     */
    private String year;

    /**
     * description
     */
    private String description;

    /**
     * saveTime
     */
    private Date saveTime;

    /**
     * clientAttribute
     */
    private String clientAttribute;

    /**
     * clientLevel
     */
    private String clientLevel;

    /**
     * clientClassify
     */
    private String clientClassify;

    /**
     * registerProvinceName
     */
    private String registerProvinceName;

    /**
     * registerCityName
     */
    private String registerCityName;

    /**
     * registerCountyName
     */
    private String registerCountyName;

    /**
     * writeOffDate
     */
    private Date writeOffDate;

    /**
     * upDateCode
     */
    private String upDateCode;

    /**
     * upRegisterName
     */
    private String upRegisterName;

    /**
     * upClientLevel
     */
    private String upClientLevel;

    /**
     * documentDate
     */
    private Date documentDate;

    /**
     * terminalSales
     */
    private String terminalSales;

    /**
     * entryPersonnel
     */
    private String entryPersonnel;

    /**
     * upImport
     */
    private String upImport;

    /**
     * downImport
     */
    private String downImport;

    /**
     * importQualitySpecifications
     */
    private String importQualitySpecifications;

    /**
     * variableCost
     */
    private String variableCost;

    /**
     * marginalRevenue
     */
    private String marginalRevenue;

    /**
     * preSettlementPrice
     */
    private String preSettlementPrice;

    /**
     * grossProfit
     */
    private String grossProfit;

    /**
     * tenantId
     */
    private String tenantId;

    /**
     * tenantName
     */
    private String tenantName;

    /**
     * entryMethod
     */
    private String entryMethod;
}
