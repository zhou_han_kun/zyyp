package com.sinepharm.pharmeyes.com;

import java.util.Date;

public class ErpRecords {

    private String flowOrgCode;
    private String flowOrgName;
    private String legacyItem;
    private String legacyItemDesc;
    private String legacyItemEnname;
    private String legacyItemModel;
    private String legacyItemName;
    private String legacyItemUom;
    private String legacyObjId;
    private String systemCode;
    private Date timeStamp;
    public void setFlowOrgCode(String flowOrgCode) {
        this.flowOrgCode = flowOrgCode;
    }
    public String getFlowOrgCode() {
        return flowOrgCode;
    }

    public void setFlowOrgName(String flowOrgName) {
        this.flowOrgName = flowOrgName;
    }
    public String getFlowOrgName() {
        return flowOrgName;
    }

    public void setLegacyItem(String legacyItem) {
        this.legacyItem = legacyItem;
    }
    public String getLegacyItem() {
        return legacyItem;
    }

    public void setLegacyItemDesc(String legacyItemDesc) {
        this.legacyItemDesc = legacyItemDesc;
    }
    public String getLegacyItemDesc() {
        return legacyItemDesc;
    }

    public void setLegacyItemEnname(String legacyItemEnname) {
        this.legacyItemEnname = legacyItemEnname;
    }
    public String getLegacyItemEnname() {
        return legacyItemEnname;
    }

    public void setLegacyItemModel(String legacyItemModel) {
        this.legacyItemModel = legacyItemModel;
    }
    public String getLegacyItemModel() {
        return legacyItemModel;
    }

    public void setLegacyItemName(String legacyItemName) {
        this.legacyItemName = legacyItemName;
    }
    public String getLegacyItemName() {
        return legacyItemName;
    }

    public void setLegacyItemUom(String legacyItemUom) {
        this.legacyItemUom = legacyItemUom;
    }
    public String getLegacyItemUom() {
        return legacyItemUom;
    }

    public void setLegacyObjId(String legacyObjId) {
        this.legacyObjId = legacyObjId;
    }
    public String getLegacyObjId() {
        return legacyObjId;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }
    public String getSystemCode() {
        return systemCode;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
    public Date getTimeStamp() {
        return timeStamp;
    }

}