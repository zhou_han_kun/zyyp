package com.javaweb.admin.config;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.javaweb.admin.entity.InvoiceSendingManagement;
import com.javaweb.admin.entity.Scheduled;
import com.javaweb.admin.mapper.InvoiceSendingManagementMapper;
import com.javaweb.admin.service.AbstractJob;
import com.javaweb.common.config.CommonConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.List;


@Configuration
@EnableScheduling
public class SendEmailConfig implements SchedulingConfigurer {
    @Autowired
    private ApplicationContext context;
    @Autowired
    private InvoiceSendingManagementMapper invoiceSendingManagementMapper;


    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        if(true){
            List<InvoiceSendingManagement> scheduledList = invoiceSendingManagementMapper.selectList(new QueryWrapper<>());
            for (InvoiceSendingManagement scheduled : scheduledList){
                AbstractJob task = null;
                try {
                    task = (AbstractJob) context.getBean(Class.forName("com.javaweb.admin.config.SendEmailJob"));
                    task.setCustomerCode(scheduled.getCustomerCode());
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                if (scheduled.getStatus() == 1) {
                    task.createJob(scheduled.getCron());

                }





            }





        }
    }


}
