package com.javaweb.admin.config;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.javaweb.admin.entity.C37InfRkScBillNew;
import com.javaweb.admin.entity.NcTaskLog;
import com.javaweb.admin.entity.Scheduled;
import com.javaweb.admin.entity.VSyOnhand;
import com.javaweb.admin.mapper.ScheduledMapper;
import com.javaweb.admin.mapper.VSyOnhandMapper;
import com.javaweb.admin.query.SySyxxQuery;
import com.javaweb.admin.service.AbstractJob;
import com.javaweb.admin.service.INcTaskLogService;
import com.javaweb.admin.service.ISySyxxService;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GetTraceCodeJob extends AbstractJob {
    @Autowired
    ScheduledMapper scheduledMapper;

    @Autowired
    VSyOnhandMapper onhandMapper;

    @Autowired
    ISySyxxService sySyxxService;

    @Autowired
    INcTaskLogService iNcTaskLogService;

    @Override
    public String getName() {
        QueryWrapper<Scheduled> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("classpath",this.getClass().getName());
        Scheduled entity = scheduledMapper.selectOne(queryWrapper);
        return entity.getName();
    }

    @Override
    public void execute() {
        List<VSyOnhand> list= onhandMapper.getInvcodeUploadList();
        logger.info(DateUtils.dateTimeNow() + "获取了" + list.size() + "个等待获取追溯码的物料");
        NcTaskLog taskLog = new NcTaskLog();
        taskLog.setTaskType("获取协会溯源码");
        taskLog.setBillCount(list.size());
        taskLog.setStartTime(DateUtils.now());
        String material_code = "";
        String batch_code="";
        String param = JSON.toJSONString(list);
        SySyxxQuery syxxQuery = new SySyxxQuery();
        if(list.size()>0) {
            for (int i = 0; i < list.size(); i++) {
                syxxQuery.setMaterialCode(list.get(i).getTbdm());
                syxxQuery.setBatchCode(list.get(i).getVlotno());
                JsonResult result = sySyxxService.SaveSyxx(syxxQuery);
                //logger.info(DateUtils.dateTimeNow() + "提交了采购入库单：" + djhs[0]+",处理结果:"+result.getData().toString());
                logger.info(DateUtils.dateTimeNow() + "提交了获取溯源码请求；" +JSON.toJSONString(syxxQuery));

            }
        }
        taskLog.setBillNo(param);
        taskLog.setEndTime(DateUtils.now());
        iNcTaskLogService.save(taskLog);

    }
}
