package com.javaweb.admin.config;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.javaweb.admin.entity.C37InfCkScBillNew;
import com.javaweb.admin.entity.C37InfRkScBillNew;
import com.javaweb.admin.entity.NcTaskLog;
import com.javaweb.admin.entity.Scheduled;
import com.javaweb.admin.mapper.ScheduledMapper;
import com.javaweb.admin.service.AbstractJob;
import com.javaweb.admin.service.IC37InfCkScBillNewService;
import com.javaweb.admin.service.IC37InfRkScBillNewService;
import com.javaweb.admin.service.INcTaskLogService;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import com.sphchina.esb.webservice.ISendToReviewLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UploadGJTCJob extends AbstractJob {
    @Autowired
    IC37InfCkScBillNewService c37InfCkScBillNewService;
    @Autowired
    IC37InfRkScBillNewService c37InfRkScBillNewService;
    @Autowired
    INcTaskLogService iNcTaskLogService;
    @Autowired
    ISendToReviewLog iSendToReviewLog;
    @Autowired
    ScheduledMapper scheduledMapper;
    @Override
    public void execute() {

        List<C37InfCkScBillNew> list= c37InfCkScBillNewService.GetCkScList("购进退出");
        logger.info(DateUtils.dateTimeNow() + "获取了" + list.size() + "个购进退出出库单");
        String billNo = "";
        NcTaskLog taskLog = new NcTaskLog();
        taskLog.setTaskType("购进退出");
        taskLog.setBillCount(list.size());
        taskLog.setStartTime(DateUtils.now());
        if(list.size()>0) {
            for (int i = 0; i < list.size(); i++) {
                String[] djhs = new String[1];
                djhs[0] = list.get(i).getChukNo();
                billNo+=list.size()>1?list.get(i).getChukNo()+",":list.get(i).getChukNo();
                JsonResult result = iSendToReviewLog.callGJTC(djhs, "E9BO01");
                //logger.info(DateUtils.dateTimeNow() + "提交了购进退出出库单：" + djhs[0]+",处理结果:"+result.getData().toString());
                logger.info(DateUtils.dateTimeNow() + "提交了购进退出出库单：" + djhs[0]);
            }
        }
        taskLog.setBillNo(billNo);
        taskLog.setEndTime(DateUtils.now());
        iNcTaskLogService.save(taskLog);

        //System.out.println(DateUtils.dateTimeNow()+":上传了一条数据");
        //this.cancel();
    }

    @Override
    public String getName() {
        QueryWrapper<Scheduled> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("classpath",this.getClass().getName());
        Scheduled entity = scheduledMapper.selectOne(queryWrapper);
        return entity.getName();
    }
}
