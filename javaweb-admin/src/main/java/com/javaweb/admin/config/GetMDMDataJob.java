package com.javaweb.admin.config;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.admin.entity.MdmProduct;
import com.javaweb.admin.entity.NcTaskLog;
import com.javaweb.admin.entity.Scheduled;
import com.javaweb.admin.entity.VSyOnhand;
import com.javaweb.admin.mapper.MdmProductMapper;
import com.javaweb.admin.mapper.ScheduledMapper;
import com.javaweb.admin.mapper.VSyOnhandMapper;
import com.javaweb.admin.query.MDMQueryParam;
import com.javaweb.admin.query.SySyxxQuery;
import com.javaweb.admin.service.AbstractJob;
import com.javaweb.admin.service.INcTaskLogService;
import com.javaweb.admin.service.ISySyxxService;
import com.javaweb.admin.service.impl.MDMServiceImpl;
import com.javaweb.common.config.DynamicDataSource;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class GetMDMDataJob extends AbstractJob {
    @Autowired
    ScheduledMapper scheduledMapper;

    @Autowired
    MDMServiceImpl mdmService;

    @Autowired
    MdmProductMapper mdmProductMapper;

    @Autowired
    INcTaskLogService iNcTaskLogService;

    @Override
    public String getName() {
        QueryWrapper<Scheduled> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("classpath",this.getClass().getName());
        Scheduled entity = scheduledMapper.selectOne(queryWrapper);
        return entity.getName();
    }

    @Override
    public void execute() {
        Map<String,Object> param = new HashMap<>();
        param.put("tenantId","1173");
        param.put("starttime",DateUtils.getDate()+" 00:00:00");
        param.put("endtime",DateUtils.dateTime(DateUtils.addDays(DateUtils.now(),1))+" 00:00:00");
        param.put("currentPage",1);
        param.put("size",500);


        logger.info(DateUtils.dateTimeNow() + "开始执行产品主数据同步，查询参数：" +JSON.toJSONString(param));
        //获取产品主数据最后更新时间
        IPage<MdmProduct> page = new Page<>(1,1);
        QueryWrapper<MdmProduct> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("update_time");
        queryWrapper.orderByDesc("update_time");
        DynamicDataSource.setDataSource("mdm");
        List<MdmProduct> list = mdmProductMapper.selectPage(page,queryWrapper).getRecords();
        if(list.size()>0)
        {
            MdmProduct entity = list.get(0);
            param.replace("starttime", DateUtil.format(entity.getUpdateTime(),DateUtils.YYYY_MM_DD_HH_MM_SS));
        }
        DynamicDataSource.clearDataSource();

        NcTaskLog taskLog = new NcTaskLog();
        taskLog.setTaskType("获取产品主数据");
        taskLog.setBillCount(0);
        taskLog.setStartTime(DateUtils.now());
        String material_code = "";
        String batch_code="";
        MDMQueryParam query = JSON.parseObject(JSON.toJSONString(param),MDMQueryParam.class);
        mdmService.SaveMaterial(query);
        taskLog.setBillNo("");
        taskLog.setEndTime(DateUtils.now());
        iNcTaskLogService.save(taskLog);

        logger.info(DateUtils.dateTimeNow() + "开始执行客商数据同步，查询参数：" +JSON.toJSONString(param));
        taskLog = new NcTaskLog();
        taskLog.setTaskType("获取客商数据");
        taskLog.setBillCount(0);
        taskLog.setStartTime(DateUtils.now());
        query = JSON.parseObject(JSON.toJSONString(param),MDMQueryParam.class);
        mdmService.SaveCustomer(query);
        taskLog.setBillNo("");
        taskLog.setEndTime(DateUtils.now());
        iNcTaskLogService.save(taskLog);

        param.replace("starttime", "2022-01-01 00:00:00");
        Date startDate = DateUtils.parseDate("2022-01-01");
        while(startDate.before(DateUtils.now())) {
            logger.info(DateUtils.dateTimeNow() + "开始执行NC销售流向同步，查询参数：" + JSON.toJSONString(param));
            taskLog = new NcTaskLog();
            taskLog.setTaskType("获取NC销售流向数据");
            taskLog.setBillCount(0);
            taskLog.setStartTime(DateUtils.now());
            query = JSON.parseObject(JSON.toJSONString(param), MDMQueryParam.class);
            mdmService.SaveNCSaleFlow(query);
            taskLog.setBillNo(JSON.toJSONString(param));
            taskLog.setEndTime(DateUtils.now());
            iNcTaskLogService.save(taskLog);
            DateUtils.addDays(startDate,1);
            param.replace("starttime", DateUtil.format(startDate,DateUtils.YYYY_MM_DD)+" 00:00:00");
            param.replace("endtime",DateUtils.dateTime(DateUtils.addDays(startDate,1))+" 00:00:00");

        }

    }
}