package com.javaweb.admin.config;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.javaweb.admin.entity.Scheduled;
import com.javaweb.admin.mapper.ScheduledMapper;
import com.javaweb.admin.service.AbstractJob;
import com.javaweb.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author liangxuelong
 * @version 1.0
 * @description: 测试定时任务
 * @date 2021/8/26 14:04
 */
@Component
public class SendMsgJob extends AbstractJob {
    @Autowired
    ScheduledMapper scheduledMapper;
    @Override
    public void execute() {
        logger.debug("发送了一条消息");
        System.out.println(DateUtils.dateTimeNow()+":发送了一条消息");
        //this.cancel();
    }

    @Override
    public String getName() {
        QueryWrapper<Scheduled> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("classpath",this.getClass().getName());
        Scheduled entity = scheduledMapper.selectOne(queryWrapper);
        return entity.getName();
    }
}



