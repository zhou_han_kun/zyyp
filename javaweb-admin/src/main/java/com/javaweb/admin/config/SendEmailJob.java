package com.javaweb.admin.config;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.javaweb.admin.entity.NcTaskLog;
import com.javaweb.admin.entity.Scheduled;
import com.javaweb.admin.mapper.ScheduledMapper;
import com.javaweb.admin.service.AbstractJob;
import com.javaweb.admin.service.IInvoiceFilenameRuleService;
import com.javaweb.admin.service.INcTaskLogService;
import com.javaweb.admin.service.impl.InvoiceFilenameRuleServiceImpl;
import com.javaweb.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SendEmailJob extends AbstractJob {
    @Autowired
    private ScheduledMapper scheduledMapper;
    @Autowired
    private IInvoiceFilenameRuleService invoiceFilenameRuleService;
    @Autowired
    private INcTaskLogService incTaskLogService;
    @Override
    public String getName() {
        QueryWrapper<Scheduled> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("classpath",this.getClass().getName());
        Scheduled entity = scheduledMapper.selectOne(queryWrapper);
        return entity.getName();
    }
    @Override
    public void execute() {
        NcTaskLog taskLog = new NcTaskLog();
        taskLog.setStartTime(DateUtils.now());
        invoiceFilenameRuleService.updateFile(this.getCustomerCode());
        taskLog.setEndTime(DateUtils.now());
        incTaskLogService.save(taskLog);
    }
}
