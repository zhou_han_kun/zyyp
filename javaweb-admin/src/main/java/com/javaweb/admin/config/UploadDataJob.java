package com.javaweb.admin.config;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.javaweb.admin.entity.C37InfCkScBillNew;
import com.javaweb.admin.entity.C37InfRkScBillNew;
import com.javaweb.admin.entity.NcTaskLog;
import com.javaweb.admin.entity.Scheduled;
import com.javaweb.admin.mapper.ScheduledMapper;
import com.javaweb.admin.mapper.VSyPoDetailMapper;
import com.javaweb.admin.mapper.VSySoDetailMapper;
import com.javaweb.admin.service.*;
import com.javaweb.admin.vo.vsypodetail.VSyPoDetailListVo;
import com.javaweb.admin.vo.vsysodetail.VSySoDetailListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import com.sphchina.esb.webservice.ISendToReviewLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.List;

@Component
public class UploadDataJob extends AbstractJob {

    @Autowired
    VSySoDetailMapper sySoDetailMapper;
    @Autowired
    VSyPoDetailMapper syPoDetailMapper;
    @Autowired
    ScheduledMapper scheduledMapper;
    @Autowired
    ISySyxxService sySyxxService;
    @Autowired
    INcTaskLogService iNcTaskLogService;
    @Override
    public void execute() {
        List<VSyPoDetailListVo> list= syPoDetailMapper.GetUploadList();
        logger.info(DateUtils.dateTimeNow() + "获取了" + list.size() + "个采购入库记录");
        NcTaskLog taskLog = new NcTaskLog();
        taskLog.setTaskType("上传采购入库/销售出库记录");
        taskLog.setBillCount(list.size());
        taskLog.setStartTime(DateUtils.now());
        StringBuilder sb = new StringBuilder();
        if(list.size()>0) {
            for (int i = 0; i < list.size(); i++) {
                String[] djhs = new String[1];
                djhs[0] = list.get(i).getPkInvoiceB();
                sb.append(djhs[0]);
                sySyxxService.ZyyptYprk(djhs);
                //JsonResult result = iSendToReviewLog.callGJTC(djhs, "E9BO01");
                //logger.info(DateUtils.dateTimeNow() + "提交了购进退出出库单：" + djhs[0]+",处理结果:"+result.getData().toString());

            }
        }
        List<VSySoDetailListVo> list2= sySoDetailMapper.GetUploadList();
        logger.info(DateUtils.dateTimeNow() + "获取了" + list2.size() + "个销售出库记录");
        if(list2.size()>0) {
            for (int i = 0; i < list2.size(); i++) {
                String[] djhs = new String[1];
                djhs[0] = list2.get(i).getCsaleinvoicebid();
                sb.append(djhs[0]);
                sySyxxService.ZyyptYpck(djhs);
                //JsonResult result = iSendToReviewLog.callXSCK(djhs, "E9BO01");
                //logger.info(DateUtils.dateTimeNow() + "提交了销售出库单：" + djhs[0]+",处理结果:"+result.getData().toString());

            }
        }
        taskLog.setBillNo(sb.toString());
        taskLog.setEndTime(DateUtils.now());
        iNcTaskLogService.save(taskLog);

    }

    @Override
    public String getName() {
        QueryWrapper<Scheduled> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("classpath",this.getClass().getName());
        Scheduled entity = scheduledMapper.selectOne(queryWrapper);
        return entity.getName();
    }
}