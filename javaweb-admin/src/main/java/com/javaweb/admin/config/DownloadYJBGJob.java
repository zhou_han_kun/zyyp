package com.javaweb.admin.config;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.javaweb.admin.entity.NcTaskLog;
import com.javaweb.admin.entity.Scheduled;
import com.javaweb.admin.entity.SysMsfxYjbg;
import com.javaweb.admin.mapper.ScheduledMapper;
import com.javaweb.admin.mapper.SysMsfxYjbgMapper;
import com.javaweb.admin.service.AbstractJob;
import com.javaweb.admin.service.INcTaskLogService;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.util.List;

@Component
public class DownloadYJBGJob extends AbstractJob {
    @Autowired
    ScheduledMapper scheduledMapper;

    @Autowired
    INcTaskLogService iNcTaskLogService;

    @Autowired
    SysMsfxYjbgMapper sysMsfxYjbgMapper;

    @Override
    public String getName() {
        QueryWrapper<Scheduled> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("classpath", this.getClass().getName());
        Scheduled entity = scheduledMapper.selectOne(queryWrapper);
        return entity.getName();
    }

    @Override
    public void execute() {
        NcTaskLog taskLog = new NcTaskLog();
        taskLog.setTaskType("下载药检报告");
        taskLog.setBillCount(0);
        taskLog.setStartTime(DateUtils.now());
        String material_code = "";
        String batch_code = "";
        try {
            List<SysMsfxYjbg> fileList = sysMsfxYjbgMapper.getDownloadYjbgList();
            for (SysMsfxYjbg item : fileList) {
                String url = item.getSealedReportUrl();
                String urlDecode = URLDecoder.decode(url,"utf-8");
                String fileurl = urlDecode.substring(0,urlDecode.indexOf("?"));
                String[] urlSec = fileurl.split("/");
                URL httpUrl = new URL(url);
                InputStream inputStream = httpUrl.openStream();

                byte[] bytes = new byte[1024];
                int len = 0;

                String fileNameAnPath = CommonConfig.taobaoYjbgPath + urlSec[urlSec.length-1];
                File f = new File(fileNameAnPath);
                if (f.exists()) {
                    f.delete();
                }
                FileOutputStream fileOutputStream = new FileOutputStream(f);
                while ((len = inputStream.read(bytes)) != -1) {
                    fileOutputStream.write(bytes, 0, len);

                }
                fileOutputStream.close();
                UpdateWrapper<SysMsfxYjbg> updateWrapper = new UpdateWrapper<>();
                updateWrapper.set("file_path",fileNameAnPath).set("down_file_status",1).eq("id",item.getId());
                sysMsfxYjbgMapper.update(new SysMsfxYjbg(),updateWrapper);
            }
            taskLog.setBillNo("");
            taskLog.setEndTime(DateUtils.now());
            iNcTaskLogService.save(taskLog);

        } catch(FileNotFoundException e){
                e.printStackTrace();
            } catch(IOException e){
                e.printStackTrace();
            }
    }
}

