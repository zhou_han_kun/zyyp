package com.javaweb.admin.entity;

import lombok.Data;

@Data
public class YYq004RsVo {
    /**
     *
     */
    private String csaleid;

    /**
     *
     */
    private String psdh;

    /**
     *
     */
    private String psdbh;

    private String fph;

    private String ddmxbh;

    private String psdz;
    /**
     *
     */
    private String cjrq;

    /**
     *
     */
    private String sdrq;

    /**
     *
     */
    private String zxs;

    /**
     *
     */
    private String jls;

    /**
     *
     */
    private String memo;

    /**
     *
     */
    private String ts;

    /**
     *
     */
    private String lastts;

    /**
     *
     */
    private String soucetype;

    /**
     *
     */
    private String status;

    /**
     *
     */
    private String psdbm;

    /**
     *
     */
    private String yybm;
    private String yymc;

    /**
     *
     */
    private String printflag;

    /**
     *
     */
    private String synflag;

    /**
     *
     */
    private String ygpsupflag;

    /**
     *
     */
    private String ygfpupflag;

    /**
     *
     */
    private String lastModifyTs;

}
