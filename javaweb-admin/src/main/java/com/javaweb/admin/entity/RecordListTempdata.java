package com.javaweb.admin.entity;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;

@Data
public class RecordListTempdata extends BaseEntity {
    private static final long serialVersionUID = 1L;
    private String uptDate;
    private String temperature1;
    private String temperature2;
    private String carNoname;
}
