package com.javaweb.admin.entity;

import lombok.Data;

@Data
public class MInvoicesrelationDto {
    private MInvoicesrelation invoicesrelation;
    private MInvoicesrelationline[] invoicesrelationLines;
}
