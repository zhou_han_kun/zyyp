package com.javaweb.admin.entity;

import lombok.Data;

@Data
public class YgptAddrContrast {
    private String id;
    private String dbilldate;
    private String yybm;
    private String yymc;
    private String psdbm;
    private String psdz;
    private String custaddrname;
    private String coperator;
    private String ywy;
    private String custcode;
    private String custname;
    private String pkcustaddr;
    private String erpwarehouse;
}
