package com.javaweb.admin.entity;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;


@Data
public class OrderForGoods extends BaseEntity {
    private static final long serialVersionUID = 1L;
    private String CgoldtaxDMCodeID;
    private String SendGoodsTaskID;
    private String Custcode;
    private String Custname;
    private String CustcodeSendAddr;
    private String Memo1;
    private String Memo2;
    private String Status;
    private String SendGoodsNum;
    private String RejectGoodsNum;
    private String statusName;
}
