package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
* <p>
    * 采购发票明细表
    * </p>
*
* @author 鲲鹏
* @since 2024-01-17
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("m_pruchinvoiceline")
public class MPruchinvoiceline extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private Integer pruchinvoiceid;

            /**
            * 
            */
                private String zxspbm;

            /**
            * 
            */
                private String scph;

            /**
            * 
            */
                private String companyid;

            /**
            * 
            */
                private String sentresult;

            /**
            * 
            */
                private String sentresultstr;

}