package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class SysMsfxYjbg {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String drugEntBaseInfoId;
    private String drugName;
    private String batchNo;
    private String drugReportV2Id;
    private String sealedReportUrl;
    private String filePath;
    private String pkgSpec;
    private String prepnSpec;
    private String prodCode;
    private String reportDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
}
