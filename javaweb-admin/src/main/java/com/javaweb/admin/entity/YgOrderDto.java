package com.javaweb.admin.entity;

import lombok.Data;

@Data
public class YgOrderDto {
    private String[] ddmxbhs;
    private String startDate;
    private String csaleid;
    //药事品台操作类型；新增/修改/作废
    private String czlx;
    //上传单据类型
    private String ygtype;

    private Boolean uploadPsd;
    private Boolean uploadFp;
    private Integer id;
}
