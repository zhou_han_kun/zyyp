package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "NC_custinfodoc")
public class NCCustInfoDoc {
    private String pkCustomer;
    private String custcode;
    private String custname;
    private String taxpayerid;
    private String memo;
    @TableField(exist = false)
    private String psdbm;
    @TableField(exist = false)
    private String custaddrname;
    @TableField(exist = false)
    private String yybm;


}
