package com.javaweb.admin.entity;

import lombok.Data;

@Data
public class SendMessageLog extends Smsevaluate{
    private static final long serialVersionUID = 1L;
    private String message  ;

    public SendMessageLog(Smsevaluate smsevaluate) {
        this.setSendGoodsTaskID(smsevaluate.getSendGoodsTaskID());
        this.setCustname(smsevaluate.getCustname());
        this.setCustcode(smsevaluate.getCustcode());
        this.setCustcodeSendAddr(smsevaluate.getCustcodeSendAddr());
    }
}
