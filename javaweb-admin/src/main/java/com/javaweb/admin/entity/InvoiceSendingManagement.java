package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
    * 
    * </p>
*
* @author 鲲鹏
* @since 2024-08-15
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("invoice_sending_management")
public class InvoiceSendingManagement extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private String customerCode;

            /**
            * 
            */
                private String customerName;

            /**
            * 
            */
                private String eMail;

            /**
            * 
            */
                private Integer status;

            /**
            * 
            */
                private String cron;

}