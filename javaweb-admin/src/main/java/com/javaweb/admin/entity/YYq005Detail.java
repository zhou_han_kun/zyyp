package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
* <p>
    * 订单代填明细表
    * </p>
*
* @author 鲲鹏
* @since 2023-12-31
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("y_yq005_detail")
public class YYq005Detail extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private String replaceno;

            /**
            * 
            */
                private String zxspbm;

                @TableField(exist = false)
                private String invcode;

                @TableField(exist = false)
                private String vlotno;
            /**
            * 
            */
                private String gjbm;

            /**
            * 
            */
                private String yqbm;

            /**
            * 
            */
                private BigDecimal cgdj;

            /**
            * 
            */
                private BigDecimal cgsl;

            /**
            * 
            */
                private String ggbz;

            /**
            * 
            */
                private String ypname;

            /**
            * 
            */
                private Integer sxh;

            /**
            * 
            */
                private String cglx;

            /**
            * 
            */
                private String cgjldw;

            /**
            * 
            */
                private BigDecimal allmoney;

            /**
            * 
            */
                private String medicinalunit;

            /**
            * 
            */
                private String medicinalprocuder;

            /**
            * 
            */
                private String measname;

            /**
            * 
            */
                private String medicinal;

            /**
            * 
            */
                private String sfjj;

            /**
            * 
            */
                private String cwyq;

            /**
            * 
            */
                private String psyq;

}