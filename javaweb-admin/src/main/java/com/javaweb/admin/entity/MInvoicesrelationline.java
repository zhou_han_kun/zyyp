package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
* <p>
    * 两票关系明细表
    * </p>
*
* @author 鲲鹏
* @since 2024-01-21
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("m_invoicesrelationline")
public class MInvoicesrelationline extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private String relationbusinessid;

            /**
            * 
            */
                private Integer pruchinvoiceid;

            /**
            * 
            */
                private String pruchfpdm;

            /**
            * 
            */
                private String pruchfph;

                private String pruchfprq;
            /**
            * 
            */
                private String companyid;

}