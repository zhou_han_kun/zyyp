package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
* <p>
    * 订单代填主表
    * </p>
*
* @author 鲲鹏
* @since 2023-12-31
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("y_yq005")
public class YYq005 extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private String replaceno;

            /**
            * 
            */
                @DateTimeFormat(pattern = "yyyy-MM-dd")
                @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
                private Date orderdate;

            /**
            * 
            */
                private Integer status;

            /**
            * 
            */
                private String empleeid;

            /**
            * 
            */
                private String salesman;

            /**
            * 
            */
                private String confirmid;

            /**
            * 
            */
                private String confirmareaid;

            /**
            * 
            */
                private String customername;

            /**
            * 
            */
                private String shaddress;

            /**
            * 
            */
                private String bzsm;

            /**
            * 
            */
                private String ddlx;

            /**
            * 
            */
                private String ddbh;

            /**
            * 
            */
                private String psdbm;

            /**
            * 
            */
                private String zwdhrq;

            /**
            * 
            */
                private String sfbtdd;

            /**
            * 
            */
                private String czlx;

            /**
            * 
            */
                private String sjcgrq;

            /**
            * 
            */
                private String dcpsbs;

            /**
            * 
            */
                private String yyjgdh;

            /**
            * 
            */
                private Integer jls;

            /**
            * 
            */
                private String splx;

            /**
            * 
            */
                private String yybm;

            /**
            * 
            */
                private String supplierId;

}