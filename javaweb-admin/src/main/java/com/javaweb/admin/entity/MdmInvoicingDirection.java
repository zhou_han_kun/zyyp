package com.javaweb.admin.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

@Data
public class MdmInvoicingDirection {
    private BigInteger id;
    private String createUser;
    private String createDept;
    private Date createTime;
    private String updateUser;
    private Date updateTime;
    private Integer status;
    private Integer isDeleted;
    private String tenantId;
    private String csaleinvoicebid;
    private String orgCode;
    private String vgoldtaxcode;
    private Date kprq;
    private String khbm;
    private String khmc;
    private String wlbm;
    private String wlmc;
    private String gg;
    private String sccj;
    private String dw;
    private BigDecimal nnum;
    private String vlotno;
    private Date dproDate;
    private Date vinvaliddate;
    private BigDecimal norigtaxnetprice;
    private BigDecimal nqtorignetprice;
    private BigDecimal ntaxrate;
    private BigDecimal ntax;
    private BigDecimal norigmny;
    private BigDecimal norigtaxmny;
    private String addrname;
    private String psnname;
    private String deptname;
    private Date ts;
    private Integer dr;
    private Date createDate;
    private String businame;
    private String mdmCode;
    private String dataCode;
    private String registerName;
    private String employeeCode;
    private String mdmProductName;
}
