package com.javaweb.admin.entity;

import lombok.Data;

@Data
public class ConverRate {
     private String ZXSPBM;
     //统编代码
     private String CPM ;
     //产品名称
     private String YYDWMC ;
     //最小使用单位
     private String BZNHSL ;
     //零售包装数据量
     private String CFGG ;
     //成分规格
     private String zmeasname ;
        //主计量单位ERP
    private String converRate ;
    //转换率
    private String pk_converrate ;
    //主键ID
    private String BZDWMC ;
    //阳光平台包装单位名称
    private String YPLX ;
    //药品类型
    private String converRateType ;
    //转换规则，乘法 or 除法

    private String invcode ;


    private String invname ;


    private String measname ;


    private String meascode ;


    private String taxratio ;
 //税率值

    private String taxratcode ;
//税率编码

    private String ts ;
//税率编码

    private Boolean invalidFlag;
 //作废标志

    private String invalidMemo ;
//作废说明

    private String goodsType ;
 //药品分类
}
