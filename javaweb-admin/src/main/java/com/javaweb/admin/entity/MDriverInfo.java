package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("m_driverinfo")
public class MDriverInfo {
    @TableId(type = IdType.AUTO)
    /**
     * id
     */
    private Integer id;

    /**
     * name
     */
    private String name;

    /**
     * driverid
     */
    private String driverid;

    /**
     * sex
     */
    private String sex;

    /**
     * old
     */
    private String old;

    /**
     * type
     */
    private String type;

    /**
     * driverold
     */
    private String driverold;

    /**
     * memo
     */
    private String memo;

    /**
     * memo1
     */
    private String memo1;

    /**
     * memo2
     */
    private String memo2;

    /**
     * memo3
     */
    private String memo3;

    /**
     * create_date
     */
    private Date createDate;

    /**
     * last_modify_date
     */
    private Date lastModifyDate;

    /**
     * delete_flag
     */
    private Integer deleteFlag;

    /**
     * create_usraccount
     */
    private String createUsraccount;

    /**
     * last_modify_usraccount
     */
    private String lastModifyUsraccount;
}
