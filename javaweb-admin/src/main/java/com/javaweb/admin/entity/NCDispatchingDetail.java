package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "v_ygpt_dispatching_b")
public class NCDispatchingDetail {
    private String psdh;
    private String psdtm;
    private String ddmxbh;
    private String invcode;
    private String zxspbm;
    private String scph;
    private String scrq;
    private String yxrq;
    private String xsddh;
    private String psl;
}