package com.javaweb.admin.entity;

import lombok.Data;

@Data
public class PhoneAndUser {
    private String linkPhone;
    private String linkUser;
}
