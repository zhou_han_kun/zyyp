package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
    * 
    * </p>
*
* @author 鲲鹏
* @since 2023-06-30
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("v_sy_po_detail")
public class VSyPoDetail extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private String custcode;

            /**
            * 
            */
                private String custname;

            /**
            * 
            */
                private String vbillcode;

            /**
            * 
            */
                private String cgy;

            /**
            * 
            */
                private String dbilldate;

            /**
            * 
            */
                private String invcode;

            /**
            * 
            */
                private String invname;

            /**
            * 
            */
                private String invspec;

            /**
            * 
            */
                private BigDecimal nastnum;

            /**
            * 
            */
                private String origin;

            /**
            * 
            */
                private String vlotno;

            /**
            * 
            */
                private String dprodate;

            /**
            * 
            */
                private String vinvaliddate;

            /**
            * 
            */
                private String crowno;

            /**
            * 
            */
                private String syflag;

            /**
            * 
            */
                private String tbdm;

            /**
            * 
            */
                private String ts;

            /**
            * 
            */
                private String pkInvoice;

            /**
            * 
            */
                private String pkInvoiceB;

                @TableField(exist = false)
                private String status;

    @TableField(exist = false)
    private BigDecimal convertNum;

}