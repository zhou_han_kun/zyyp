package com.javaweb.admin.entity;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;

@Data
public class Smsevaluate extends BaseEntity {
    private static final long serialVersionUID = 1L;
    private String TransitTime;
    private String sendGoodsTaskID;
    private String custname;
    private String linkUser;
    private String carCode;
    private String linkPhone;
    private String sendingTime;
    private String Name;
    private String WLBZ;
    private String wlbzpjmx;
    private String PSSJ;
    private String pssjpjmx;
    private String PSFW;
    private String psfwpjmx;
    private String JYXQ;
    private String dpts;
    private String custcode;
    private String CustcodeSendAddr;

}
