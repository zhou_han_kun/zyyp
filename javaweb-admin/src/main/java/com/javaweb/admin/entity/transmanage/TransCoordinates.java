package com.javaweb.admin.entity.transmanage;
import java.util.List;

public class TransCoordinates {
    private String type;
    private List<Features> features;
    public void setType(String type) {
        this.type = type;
    }
    public String getType() {
        return type;
    }

    public void setFeatures(List<Features> features) {
        this.features = features;
    }
    public List<Features> getFeatures() {
        return features;
    }

}
