package com.javaweb.admin.entity.querysealdrugreport;

public class Result {
    private Model model;
    private String msg_code;
    private String msg_info;
    private boolean response_success;
    public void setModel(Model model) {
        this.model = model;
    }
    public Model getModel() {
        return model;
    }

    public void setMsg_code(String msg_code) {
        this.msg_code = msg_code;
    }
    public String getMsg_code() {
        return msg_code;
    }

    public void setMsg_info(String msg_info) {
        this.msg_info = msg_info;
    }
    public String getMsg_info() {
        return msg_info;
    }

    public void setResponse_success(boolean response_success) {
        this.response_success = response_success;
    }
    public boolean getResponse_success() {
        return response_success;
    }
}
