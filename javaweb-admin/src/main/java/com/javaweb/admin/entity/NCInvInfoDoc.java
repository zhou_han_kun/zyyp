package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "NC_invinfodoc")
public class NCInvInfoDoc {

    private String pkInvmandoc;
    private String invcode ;


    private String invname ;


    private String measname ;


    private String invspec ;

    private String sccj ;

    private String taxratio ;

    @TableField(exist = false)
    private String zxspbm;
    @TableField(exist = false)
    private String cfgg;
    @TableField(exist = false)
    private String ypjx;
    @TableField(exist = false)
    private String bzdwmc;
    @TableField(exist = false)
    private String cgsl;
    @TableField(exist = false)
    private String cgdj;

}
