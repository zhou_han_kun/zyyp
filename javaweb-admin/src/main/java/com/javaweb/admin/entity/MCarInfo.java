package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("m_carinfo")
public class MCarInfo {
    @TableId(type = IdType.INPUT)
    /**
     * carid
     */
    private String carid;

    /**
     * carcode
     */
    private String carcode;

    /**
     * gpsdeviceid
     */
    private String gpsdeviceid;

    /**
     * hotdeviceid
     */
    private String hotdeviceid;

    /**
     * hotdeviceid2
     */
    private String hotdeviceid2;

    /**
     * type
     */
    private String type;

    /**
     * weight
     */
    private String weight;

    /**
     * memo
     */
    private String memo;

    /**
     * memo1
     */
    private String memo1;

    /**
     * memo2
     */
    private String memo2;

    /**
     * memo3
     */
    private String memo3;

    /**
     * create_date
     */
    private Date createDate;

    /**
     * last_modify_date
     */
    private Date lastModifyDate;

    /**
     * delete_flag
     */
    private Integer deleteFlag;

    /**
     * create_usraccount
     */
    private String createUsraccount;

    /**
     * last_modify_usraccount
     */
    private String lastModifyUsraccount;

    /**
     * carname
     */
    private String carname;

    /**
     * oilconsum
     */
    private String oilconsum;

    /**
     * oiltype
     */
    private String oiltype;
}
