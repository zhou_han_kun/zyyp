package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
* <p>
    * 采购发票主表
    * </p>
*
* @author 鲲鹏
* @since 2024-01-17
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("m_pruchinvoice")
public class MPruchinvoice extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private String fpdm;

            /**
            * 
            */
                private String fph;

            /**
            * 
            */
                private String fprq;

            /**
            * 
            */
                private String fpkjfmc;

            /**
            * 
            */
                private String fpjsfmc;

            /**
            * 
            */
                private String bzsm;

            /**
            * 
            */
                private String sentresult;

            /**
            * 
            */
                private String sentresultstr;

            /**
            * 
            */
                private String confirmresult;

            /**
            * 
            */
                private String confirmresultstr;

            /**
            * 
            */
                private String companyid;

            /**
            * 
            */
                private String status;

            /**
            * 
            */
                private String fpid;

}