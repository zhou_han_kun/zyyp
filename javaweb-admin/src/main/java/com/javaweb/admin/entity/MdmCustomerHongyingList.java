package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import java.math.BigInteger;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
    * 
    * </p>
*
* @author 鲲鹏
* @since 2023-07-25
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("mdm_customer_hongying_list")
public class MdmCustomerHongyingList extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private String dataCode;

            /**
            * 
            */
                private String code;

                @TableField(value = "create_time")
                private Long create_time;
}