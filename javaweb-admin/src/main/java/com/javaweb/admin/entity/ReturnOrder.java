package com.javaweb.admin.entity;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ReturnOrder extends BaseEntity {
    private String dbilldate;
    private String vbillcode;
    private String deptname;
    private String psnname;
    private String billmaker;
    private String custcode;
    private String custname;
    private String provinces;
    private BigDecimal ntotalnum;
    private BigDecimal ntotalorigmny;
    private String returnreason;
    private String addrname;
    private String billtypename;
    private String ts;
    private String csaleorderid;
    private String realreason;
    private String remark;
}
