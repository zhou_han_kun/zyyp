package com.javaweb.admin.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ReturnOrderDetail {
    private String csaleorderid;
    private String csaleorderbid;
    private String invcode;
    private String invname;
    private String gg;
    private BigDecimal nnum;
    private BigDecimal nprice;
    private BigDecimal ntaxprice;
    private BigDecimal ntaxmny;
    private String vlotno;
    private String vinvaliddate;
    private String vmanufacturer;
    private String shcyr;
    private String vbdef8;
    private String vbdef9;
    private String vbdef10;
    private String vbdef11;
    private String ts;
}
