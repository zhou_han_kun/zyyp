package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("syt_localbillmain")
public class SytLocalbillMain {
    @TableId(value = "push_msgid", type = IdType.INPUT)
    private Integer push_msgid;
    private String bill_out_id;
    private String bill_code;
    private String bill_time;
    private String bill_type;
    private String fromrefentid;
    private String fromentname;
    private String torefentid;
    private String toentname;
    private String ownerentname;
    private String ownerrefentid;
}
