package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName(value = "y_yq010_rs")
public class YYq010Rs{
    private String ddmxbh;
    private String jhdh;
    private String yqbm;
    private String yybm;
    private String psdbm;
    private String psdz;
    private String cglx;
    private String ddlx;
    private String splx;
    private String yplx;
    private String zxspbm;
    private String cpm;
    private String ypjx;
    private String sflp;
    private String cfgg;
    private Integer bznhsl;
    private String scqymc;
    private BigDecimal cgjg;
    private BigDecimal cgsl;
    private String dcpsbs;
    private String ddtjfs;
    private String ddclzt;
    private Date ddtjrq;
    private String bzsm;
    private String bzdwmc;
    private String imperpflag;
    private String gyLineid;
    private Date ts;

}
