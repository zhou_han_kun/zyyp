package com.javaweb.admin.entity.querysealdrugreport;

import lombok.Data;

@Data
public class Ocr_seal_drug_report_d_t_o {
    private String batch_no;
    private String drug_ent_base_info_id;
    private String drug_name;
    private String drug_report_v2_id;
    private String pkg_spec;
    private String prepn_spec;
    private String prod_code;
    private String report_date;
    private String sealed_report_url;
}
