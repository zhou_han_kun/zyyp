package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "v_ncinvoice_list")
public class NCInvoice {
    private String cjrq;
    @TableField(exist = false)
    private String cupsourcebillcode;
    private String psdh;
    private String custcode;
    private String custname;
    private String vreceiveaddress;
    private String psdbm;
    private String yybm;
    private String zxs;
    private String jls;
    private String printflag;
    private String memo;
    private String ygpsupflag;
    private String ygfpupflag;
    private String gpopsupflag;
    private String gpofpupflag;
    private String geyiupflag;
    private String soucetype;
    private String sdrq;
    private String invoicecustcode;
    private String invoicecustname;
    private String ordercustcode;
    private String ordercustname;
    private String cgoldtaxdm;
    private String scfph;
    private String csaleid;
    private String businame;
}
