package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName(value = "v_ygorder_list")
public class  YgOrder {
    private String ddtjrq;
    private String jhdh;
    private String ddmxbh;
    private String yybm;
    private String yymc;
    private String zxspbm;
    private String cpm;
    private String sflp;
    private String yplx;
    private String cfgg;
    private String ypjx;
    private String bzdwmc;
    private String scqymc;
    private BigDecimal cgsl;
    private String ordernum;
    private BigDecimal cgjg;
    private String psdbm;
    private String psdz;
    private String yydwmc;
    private String bznhsl;
    private String cgjldw;
    private String yqbm;
    private String splx;
    private String ddlx;
    private String cglx;
    private String ddtjfs;
    private String dcpsbs;
    private String bzsm;
    private String ddclzt;
    private String imperpflag;
    private String custaddrname;
    private String ywy;
    private String geyinflag;
    private String gyLineid;
    private String custcode;
    //阳光平台订单响应类型
    private String xylx;
    private String xylxmc;

}
