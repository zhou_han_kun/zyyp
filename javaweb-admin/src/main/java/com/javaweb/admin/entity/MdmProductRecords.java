package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
    * 
    * </p>
*
* @author 鲲鹏
* @since 2023-07-24
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("mdm_material_records")
public class MdmProductRecords extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private String flowOrgCode;

            /**
            * 
            */
                private String flowOrgName;

            /**
            * 
            */
                private String legacyItem;

            /**
            * 
            */
                private String legacyItemDesc;

            /**
            * 
            */
                private String legacyItemEnname;

            /**
            * 
            */
                private String legacyItemModel;

            /**
            * 
            */
                private String legacyItemName;

            /**
            * 
            */
                private String legacyItemUom;

            /**
            * 
            */
                private String legacyObjId;

            /**
            * 
            */
                private String systemCode;

            /**
            * 
            */
                private String timeStamp;

            /**
            * 
            */
                private String mdmCode;

}