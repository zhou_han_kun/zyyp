package com.javaweb.admin.entity.transmanage;

import lombok.Data;

@Data
public class Point {
    double lng;
    double lat;

    public void add(Double lng, Double lat) {
        this.lng = lng;
        this.lat = lat;
    }
}
