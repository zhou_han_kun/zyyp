package com.javaweb.admin.entity;

public class FDLResultData {
    private String recordId;

    public String getRecordId(){
        return recordId;
    }

    public void setRecordId(String recordId){
        this.recordId = recordId;
    }
}
