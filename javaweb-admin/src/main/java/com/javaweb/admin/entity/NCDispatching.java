package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "v_ygpt_dispatching")
public class NCDispatching {
    private String csaleid;
    private String ddmxbh;
    private String yqbm;
    private String yybm;
    private String psdbm;
    private String psdh;
    private String psdbh;
    private String cjrq;
    private String sdrq;
    private String zxs;
    private String jls;
    private String ts;
    private String ddh;
}
