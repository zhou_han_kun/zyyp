package com.javaweb.admin.entity;

import lombok.Data;

@Data
public class TransRecordList {
    private String sendgoodstaskid;
    private String cgoldtaxdmcodeid;
    private String fhtime;
    private String shtime;
    private String custcodesendaddr;
    private String custcode;
    private String statusname;
    private String drivername;
    private String sendname1;
    private String sendname2;
    private String sendname3;
    private String hotdeviceid;
    private String cartype;
    private String carcode;
    private String carid;
    private String ncustname;
    private String ctype;
    private String cmemo2;
    private String fhaddress;
}
