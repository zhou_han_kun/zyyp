package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
    * 
    * </p>
*
* @author 鲲鹏
* @since 2023-11-27
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_spbc_invset")
public class SpbcInvset extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private String bcname;


            /**
             *
             */
            private String invtype;

            private String availableCustomer;

            /**
            * 
            */
            @TableField(exist = false)
            private String invcode;

            /**
            * 
            */
                @DateTimeFormat(pattern = "yyyy-MM-dd")
                @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
                private Date begindate;

            /**
            * 
            */
                @DateTimeFormat(pattern = "yyyy-MM-dd")
                @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
                private Date enddate;

            /**
             *
             */
                private String createUserName;

                private Integer validity;

                @DateTimeFormat(pattern = "yyyy-MM-dd")
                @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
                private Date manageEnddate;

                private String uploadfile;
                private String approver;

                @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
                private Date approveTime;
}