package com.javaweb.admin.entity.filenameRule;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.javaweb.common.common.BaseEntity;
import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
@TableName("ods_nc_so_saleinvoice_download")
public class SaleinvoiceDownload{
    private static final long serialVersionUID = 1L;

    private String custcode;
    private String vbillcode;
    private String pdffilename;
    private String dbilldate;
    @TableField(exist = false)
    private String vnote;
    @TableField(exist = false)
    private String ntotalorigmny;

}
