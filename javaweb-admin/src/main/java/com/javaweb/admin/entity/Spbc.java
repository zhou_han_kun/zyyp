package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
    * 
    * </p>
*
* @author 鲲鹏
* @since 2023-11-27
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_spbc")
public class Spbc extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private String sqrq;

            /**
            * 
            */
                private String sqdh;

            /**
            * 
            */
                private String sqr;

            /**
            * 
            */
                private String custname;

            /**
            * 
            */
                private String custcode;

            /**
            * 
            */
                private String bcyjnf;

            /**
            * 
            */
                private String bcyjqs;

            /**
            * 
            */
                private String bclx;

                private String khlx;
            /**
            * 
            */
                @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
                private Date ts;

            /**
            * 
            */
                private String checkFlag;

    private String uploadfile;
    private Date submitTime;
    private String isOverdue;
    private String begindate;
    private String enddate;
    private String custaddress;
    private String pkaddress;
    private String gpoarea;

    @TableField(exist = false)
    private List<String> bcyjList;

    @TableField(exist = false)
    private List<String> custAddr;

    private boolean saleOrderSubstuted; //正票代填
    private boolean reOrderSubstuted; //负票代填
    private boolean saleInvoicePost;  //正票回传
    private boolean reInvoiceUnrelated; //负票无关联回传
}