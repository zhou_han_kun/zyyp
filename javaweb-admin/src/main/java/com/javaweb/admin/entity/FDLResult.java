package com.javaweb.admin.entity;

public class FDLResult {
    private int status;
    private FDLResultData data;

    public int getStatus(){
        return status;
    }

    public void setStatus(int status){
        this.status = status;
    }

    public FDLResultData getData(){
        return data;
    }

    public void setData(FDLResultData data){
        this.data = data;
    }
}
