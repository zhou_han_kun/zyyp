package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "Y_ConverRate")
public class YConverRate {
    private String zxspbm;
    //统编代码
    private String cpm ;
    //产品名称
    private String invcode ;
    private String invname ;
    //生产企业
    @TableField(exist = false)
    private String scqymc;

    //最小使用单位
    private String yydwmc ;
    //零售包装数据量
    private String bznhsl ;
    //成分规格
    private String cfgg ;
    private String zmeasname ;
    //主计量单位ERP
    private String converrate ;
    //转换率
    private String pkConverrate ;
    //主键ID
    private String bzdwmc ;
    //阳光平台包装单位名称
    //药品类型
    private String yplx ;
    @TableField(exist = false)
    private String defname;
    private String converratetype ;
    //转换规则，乘法 or 除法

    private String integermul ;


    private String coperator ;


    private String dbilldate ;

    private String ts ;

    private Boolean invalidflag;
    //作废标志

    private String invalidmemo ;
}
