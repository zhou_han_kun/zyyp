package com.javaweb.admin.entity.querysealdrugreport;

import java.util.List;

public class Result_list {
        private List<Ocr_seal_drug_report_d_t_o> ocr_seal_drug_report_d_t_o;
        public void setOcr_seal_drug_report_d_t_o(List<Ocr_seal_drug_report_d_t_o> ocr_seal_drug_report_d_t_o) {
            this.ocr_seal_drug_report_d_t_o = ocr_seal_drug_report_d_t_o;
        }
        public List<Ocr_seal_drug_report_d_t_o> getOcr_seal_drug_report_d_t_o() {
            return ocr_seal_drug_report_d_t_o;
        }
}
