package com.javaweb.admin.entity.filenameRule;

import lombok.Data;

@Data
public class InvoiceDownloadView {
    private String csaleinvoiceid;
    private String vgoldtaxcode;
    private String dbilldate;
    private String custcode;
}
