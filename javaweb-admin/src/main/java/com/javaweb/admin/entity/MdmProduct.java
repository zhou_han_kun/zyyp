package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
    * 
    * </p>
*
* @author 鲲鹏
* @since 2023-07-24
*/
@Data
@TableName("mdm_material")
public class MdmProduct {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
    private String atc;
    private String atcName;


    /**
    *
    */
    private String standardCode;

    /**
    *
    */
    private String casNo;

    /**
    *
    */
    private String placeOfOrigin;

    /**
    *
    */
    private String productOrMaterial;

    /**
    *
    */
    private String country;
    private String countryName;

    /**
    *
    */
    private String domesticImports;

    /**
    *
    */
    private String specifications;

    /**
    *
    */
    private String managementType;

    /**
    *
    */
    private String managementTypeName;

    /**
    *
    */
    private String measuringUnit;
    private String measuringUnitName;

    /**
    *
    */
    private String dosageForm;

    /**
    *
    */
    private String dosageFormName;

    /**
    *
    */
    private String mdmCode;

    /**
    *
    */
    private String distributingResultInformation;

    /**
    *
    */
    private String detailCategory;

    /**
    *
    */
    private String detailCategoryName;

    /**
    *
    */
    private String productName;

    /**
    *
    */
    private String otc;
    private String otcName;

    /**
    *
    */
    private String generalSpecialDrug;
    private String generalSpecialDrugName;

    /**
    *
    */
    private String approvalNumber;

    /**
    *
    */
    private String productionEnterprise;

    /**
    *
    */
    private String whetherExport;

    /**
    *
    */
    private String whetherEssentialMedicine;

    /**
    *
    */
    private String whetherInsuranceDirectory;

    /**
    *
    */
    private String tradeName;

    /**
    *
    */
    private Integer mdmCodeStatus;

    /**
    *
    */
    private String timeStamp;

    /**
    *
    */
    private String categories;

    /**
    *
    */
    private String categoriesName;

    /**
    *
    */
    private String foreignNames;

    /**
    *
    */
    private String outsourcing;

    /**
    *
    */
    private String minimumPackingUnit;

    /**
    *
    */
    private String minimumPackingUnitName;

    /**
    *
    */
    private String minimumPackingSpecification;

    private String benchmarkingProduct;
    private String productCategories;
    private String unitPrice;
    private String minUnitPrice;
    private String disease1;
    private String disease1Ratio;
    private String disease2;
    private String disease2Ratio;
    private String disease3;
    private String disease3Ratio;
    private String disease4;
    private String disease4Ratio;
    private String startDose;
    private String maxDose;
    private String patientsCoefficient;
    private String drugSers;
    private String drugSpec;
    private String kpiPrice;
    private String specificationsPillBox;


    /**
    *
    */
    private Integer isDeleted;
    private String dataType;
    private Date updateTime;
    private String hongYingCode;
    private String assessmentUnitPrice;
    private String bidPrice;
    private String averagePrice;
    private String productClassification;
    private String productAttribute;
    private String productPackingNumber;


    /**
    *
    */
    private Integer addType;

    /**
    *
    */
    private String entityMdmCode;

    /**
    *
    */
    private String entityProductName;

    private Date ts;
}