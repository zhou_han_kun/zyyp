package com.javaweb.admin.entity;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;


@Data
public class PurchaseRecord extends BaseEntity {
    private static final long serialVersionUID = 1L;
    private String vinvoicecode;
    private String vinvoicedm;
    private String custname;
    private String invcode;
    private String invname;
    private String ninvoicenum;



}
