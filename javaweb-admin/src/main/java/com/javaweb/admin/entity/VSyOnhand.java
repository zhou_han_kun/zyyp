package com.javaweb.admin.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class VSyOnhand {
    private String tbdm;
    private String vlotno;
    private String invcode;
    private String invname;
    private String storname;
    private String proddate;
    private BigDecimal nonhandnum;
    private String vinvaliddate;
}
