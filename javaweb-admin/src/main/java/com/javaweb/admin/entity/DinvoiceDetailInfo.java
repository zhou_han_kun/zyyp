package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
    * 
    * </p>
*
* @author 鲲鹏
* @since 2024-07-12
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("D_PoinvoiceDetailInfo")
public class DinvoiceDetailInfo extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private String cinvoiceBid;

            /**
            * 
            */
                private String cinvoiceid;

            /**
            * 
            */
                private Integer sxh;

            /**
            * 
            */
                private String invcode;

                @TableField(exist = false)
                private String zxspbm;

            /**
            * 
            */
                private String invname;

            /**
            * 
            */
                private Integer ninvoicenum;

            /**
            * 
            */
                private Integer sonum;

            /**
            * 
            */
                private String vproducenum;

            /**
            * 
            */
                private BigDecimal nmoney;

            /**
            * 
            */
                private BigDecimal nsummny;

            /**
            * 
            */
                private BigDecimal ntaxmny;

            /**
            * 
            */
                private BigDecimal norgnettaxprice;

            /**
            * 
            */
                private BigDecimal noriginalcurprice;

            /**
            * 
            */
                @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
                private Date createDate;

            /**
            * 
            */
                @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
                private Date lastModifyDate;

            /**
            * 
            */
                private String createUsraccount;

            /**
            * 
            */
                private String lastModifyUsraccount;

            /**
            * 
            */
                private String status;

            /**
            * 
            */
                private String cljg;

            /**
            * 
            */
                private String clqkms;

            /**
            * 
            */
            /**
            * 
            */
            /**
            * 
            */
                @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
                private Date ts;

}