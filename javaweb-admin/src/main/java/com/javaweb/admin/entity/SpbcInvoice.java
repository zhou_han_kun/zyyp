package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.javaweb.common.common.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("sys_spbc_invoice")
public class SpbcInvoice extends BaseEntity {
    private Integer pid;
    private String did;
    private String csaleinvoicebid;
    private BigDecimal quantity;
    private BigDecimal origsaleprice;
    private BigDecimal nowsaleprice;
    private BigDecimal diffprice;
    private BigDecimal balanceamount;
}
