package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
    * 
    * </p>
*
* @author 鲲鹏
* @since 2024-08-09
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("D_SendMessageLog2")
public class D3endMessageLog2 extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
                private Date ts;

            /**
            * 
            */
                private String message;

            /**
            * 
            */
                private String linkUser;

            /**
            * 
            */
                private String linkPhone;

            /**
            * 
            */
                private String sentRult;

            /**
            * 
            */
                private String messagetime;

            /**
            * 
            */
                private String custcode;

}