package com.javaweb.admin.entity;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;

@Data
public class InvoiceDetail extends BaseEntity {
    private String CinvoiceBid;
    private String Csaleid;
    private String SXH;
    private String invcode;
    private String invname;
    private String cbatchid;
    private String nnumber;
    private String nmny;
    private String nsummny;
    private String ntaxmny;
    private String ntaxprice;
    private String nprice;
    private String DDMXBH;
    private String createDate;
    private String lastModifyDate;
    private String createUsraccount;
    private String lastModifyUsraccount;
    private String CLJG;
    private String CLQKMS;
    private String ntaxrate;
    private String salecode;
    private String npacknumber;
    private String invspec;
    private String graphid;
    private String dproducedate;
    private String dvalidate;
    private String  lsjprice;
    private String storcode;
    private String storname;
    private String zmeasname;
    private String fmeasname;
    private String jx;
    private String invoicerowno;
    private String vdef14;
    private String salerowno;
    private String bzgg;
    private String prodarea;
    private String hsl;
    private String vdef11;
    private String vdef12;
    private String ts2;
    private String cupsourcebillcode;
    private String coriginalbillcode;
    private String vdef13;
    private String vdef20;
    private String ts;

}
