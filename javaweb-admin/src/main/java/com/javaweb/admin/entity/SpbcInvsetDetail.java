package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
    * 
    * </p>
*
* @author 鲲鹏
* @since 2023-12-04
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_spbc_invset_detail")
public class SpbcInvsetDetail extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private Integer pid;

            /**
            * 
            */
                private String cmaterialvid;

                private BigDecimal balanceamountback;

                private BigDecimal nowsaleprice;

}