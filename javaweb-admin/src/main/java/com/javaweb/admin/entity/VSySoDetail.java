package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
    * 
    * </p>
*
* @author 鲲鹏
* @since 2023-06-30
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("v_sy_so_detail")
public class VSySoDetail extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private String csaleinvoicebid;

            /**
            * 
            */
                private String csaleinvoiceid;

            /**
            * 
            */
                private String dbilldate;

            /**
            * 
            */
                private String custcode;

            /**
            * 
            */
                private String custname;

            /**
            * 
            */
                private String invcode;

            /**
            * 
            */
                private String invname;

            /**
            * 
            */
                private String invspec;

            /**
            * 
            */
                private String measname;

            /**
            * 
            */
                private String crowno;

            /**
            * 
            */
                private String vlotno;

            /**
            * 
            */
                private String dprodate;

            /**
            * 
            */
                private String vinvaliddate;

            /**
            * 
            */
                private BigDecimal nnum;

            /**
            * 
            */
                private String ts;

            /**
            * 
            */
                private String vbdef13;

            /**
            * 
            */
                private String tbdm;

            /**
            * 
            */
                private String vsrccode;

            /**
            * 
            */
                private String hsl;

}