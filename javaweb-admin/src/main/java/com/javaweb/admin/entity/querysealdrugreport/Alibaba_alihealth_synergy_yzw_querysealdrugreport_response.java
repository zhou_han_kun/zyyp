package com.javaweb.admin.entity.querysealdrugreport;

public class Alibaba_alihealth_synergy_yzw_querysealdrugreport_response {
    private Result result;
    private String request_id;
    public void setResult(Result result) {
        this.result = result;
    }
    public Result getResult() {
        return result;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }
    public String getRequest_id() {
        return request_id;
    }
}
