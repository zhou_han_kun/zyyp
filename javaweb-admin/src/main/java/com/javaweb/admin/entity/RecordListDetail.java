package com.javaweb.admin.entity;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;

@Data
public class RecordListDetail extends BaseEntity {
    private static final long serialVersionUID = 1L;
    private String invcode;
    private String invname;
    private String SendGoodsNum;
    private String cbatchid;
    private String RejectGoodsNum;
}
