package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
* <p>
    * 配送单主表
    * </p>
*
* @author 鲲鹏
* @since 2024-01-17
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("y_yq004_rs")
public class YYq004Rs extends BaseEntity{

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private String csaleid;

            /**
            * 
            */
                private String psdh;

            /**
            * 
            */
                private String psdbh;

            /**
            * 
            */
                private String cjrq;

            /**
            * 
            */
                private String sdrq;

            /**
            * 
            */
                private String zxs;

            /**
            * 
            */
                private String jls;

            /**
            * 
            */
                private String memo;

            /**
            * 
            */
                private String ts;

            /**
            * 
            */
                private String lastts;

            /**
            * 
            */
                private String soucetype;

            /**
            * 
            */
                private String status;

            /**
            * 
            */
                private String psdbm;

            /**
            * 
            */
                private String yybm;

            /**
            * 
            */
                private String printflag;

            /**
            * 
            */
                private String synflag;

            /**
            * 
            */
                private String ygpsupflag;

            /**
            * 
            */
                private String gpopsupflag;

            /**
            * 
            */
                private String gpoflag;

            /**
            * 
            */
                private String ygfpupflag;

            /**
            * 
            */
                private String gpofpupflag;

            /**
            * 
            */
                private String lastModifyTs;

            /**
            * 
            */
                private String geyiupglag;

}