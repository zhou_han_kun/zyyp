package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
    * 
    * </p>
*
* @author 鲲鹏
* @since 2024-07-11
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_invprice_detail")
public class InvpriceDetail extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private Integer pid;

            /**
            * 
            */
                private BigDecimal stockQty;

            /**
            * 
            */
                private String billtime;

            /**
            * 
            */
                private String controlBeginTime;

            /**
            * 
            */
                private String controlEndTime;

            /**
            * 
            */
                private String convertRule;

            /**
            * 
            */
                private BigDecimal convertRate;

            /**
            * 
            */
                private BigDecimal controlPrice;

            /**
            * 
            */
                private BigDecimal convertedPrice;

            /**
            * 
            */
                private BigDecimal lowestPrice;

            /**
            * 
            */
                private String controlMode;

            /**
            * 
            */
                @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
                private Date syncTime;

            /**
            * 
            */
                private String syncStatus;

            /**
            * 
            */
                private String priceAttach;

}