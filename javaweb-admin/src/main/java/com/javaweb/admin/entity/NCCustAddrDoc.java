package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "NC_custaddrdoc")
public class NCCustAddrDoc {
    private String pkCustaddr;
    private String custcode;
    private String custname;
    private String addrname;
    private String bankno;
    private String bankname;
    private String lineaddr;
    private String expectstart;
    private String expectend;
    private String notreceitime;
    private String linkuser;
    private String linkphone;

}
