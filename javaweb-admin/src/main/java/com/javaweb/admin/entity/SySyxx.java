package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
    * 
    * </p>
*
* @author 鲲鹏
* @since 2023-03-28
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sy_syxx")
public class SySyxx extends BaseEntity {

private static final long serialVersionUID = 1L;

/**
*
*/
    private String saleDate;

/**
*
*/
    private String materialName;

/**
*
*/
    private String xhTraceCode;

/**
*
*/
    private String packageSpecification;

/**
*
*/
    private String customerName;

/**
*
*/
    private String batchCode;

/**
*
*/
    private String filingNo;

/**
*
*/
    private String materialCode;

/**
*
*/
    private String specification;

/**
*
*/
    private String materialCategoryName;

/**
*
*/
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date ts;

}