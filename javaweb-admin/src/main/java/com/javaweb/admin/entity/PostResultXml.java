package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@TableName("d_postresultxml")
public class PostResultXml {
    private Integer id;
    private String postid;
    private String yqid;
    private String postxml;
    private String retpostxml;
    private String ztcljg;
    private String retmemo;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createDate;
    private String createUsraccount;
    private String ts;

}
