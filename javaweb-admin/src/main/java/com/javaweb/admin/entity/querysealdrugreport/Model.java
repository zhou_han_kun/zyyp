package com.javaweb.admin.entity.querysealdrugreport;

public class Model {
    private int page;
    private int page_size;
    private Result_list result_list;
    private int total_num;
    public void setPage(int page) {
        this.page = page;
    }
    public int getPage() {
        return page;
    }

    public void setPage_size(int page_size) {
        this.page_size = page_size;
    }
    public int getPage_size() {
        return page_size;
    }

    public void setResult_list(Result_list result_list) {
        this.result_list = result_list;
    }
    public Result_list getResult_list() {
        return result_list;
    }

    public void setTotal_num(int total_num) {
        this.total_num = total_num;
    }
    public int getTotal_num() {
        return total_num;
    }
}
