package com.javaweb.admin.entity;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;

@Data
public class GpsData extends BaseEntity {
    private static final long serialVersionUID = 1L;
    private String devid;
    private String devno;
    private String devname;
    private double lat;
    private double lng;
    private String uptDate;
    private String speed;
    private String direction;
    private String altitude;
}
