package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("mdm_month_target")
public class MdmMonthTarget {
    @TableId(type = IdType.INPUT)
    /**
     * id
     */
    private String id;

    /**
     * vertionName
     */
    @TableField(value = "vertionName")
    private String vertionName;

    /**
     * targetType
     */
    @TableField(value = "targetType")
    private String targetType;

    /**
     * month
     */
    private String month;

    /**
     * department1
     */
    private String department1;

    /**
     * managerCode1
     */
    @TableField(value = "managerCode1")
    private String managerCode1;

    /**
     * managerNumber1
     */
    @TableField(value = "managerNumber1")
    private String managerNumber1;

    /**
     * managerName1
     */
    @TableField(value = "managerName1")
    private String managerName1;

    /**
     * department2
     */
    private String department2;

    /**
     * managerCode2
     */
    @TableField(value = "managerCode2")
    private String managerCode2;

    /**
     * managerNumber2
     */
    @TableField(value = "managerNumber2")
    private String managerNumber2;

    /**
     * managerName2
     */
    @TableField(value = "managerName2")
    private String managerName2;

    /**
     * department3
     */
    private String department3;

    /**
     * managerCode3
     */
    @TableField(value = "managerCode3")
    private String managerCode3;

    /**
     * managerNumber3
     */
    @TableField(value = "managerNumber3")
    private String managerNumber3;

    /**
     * managerName3
     */
    @TableField(value = "managerName3")
    private String managerName3;

    /**
     * department4
     */
    private String department4;

    /**
     * managerCode4
     */
    @TableField(value = "managerCode4")
    private String managerCode4;

    /**
     * managerNumber4
     */
    @TableField(value = "managerNumber4")
    private String managerNumber4;

    /**
     * managerName4
     */
    @TableField(value = "managerName4")
    private String managerName4;

    /**
     * department5
     */
    private String department5;

    /**
     * managerCode5
     */
    @TableField(value = "managerCode5")
    private String managerCode5;

    /**
     * managerNumber5
     */
    @TableField(value = "managerNumber5")
    private String managerNumber5;

    /**
     * managerName5
     */
    @TableField(value = "managerName5")
    private String managerName5;

    /**
     * employeeCode
     */
    @TableField(value = "employeeCode")
    private String employeeCode;

    /**
     * employeeNumber
     */
    @TableField(value = "employeeNumber")
    private String employeeNumber;

    /**
     * employeeName
     */
    @TableField(value = "employeeName")
    private String employeeName;

    /**
     * juriCode
     */
    @TableField(value = "juriCode")
    private String juriCode;

    /**
     * orgType
     */
    @TableField(value = "orgType")
    private String orgType;

    /**
     * orgCode
     */
    @TableField(value = "orgCode")
    private String orgCode;

    /**
     * orgName
     */
    @TableField(value = "orgName")
    private String orgName;

    /**
     * orgAlias
     */
    @TableField(value = "orgAlias")
    private String orgAlias;

    /**
     * orgHeadquarters
     */
    @TableField(value = "orgHeadquarters")
    private String orgHeadquarters;

    /**
     * prodAttribute
     */
    @TableField(value = "prodAttribute")
    private String prodAttribute;

    /**
     * prodCategorize
     */
    @TableField(value = "prodCategorize")
    private String prodCategorize;

    /**
     * prodCode
     */
    @TableField(value = "prodCode")
    private String prodCode;

    /**
     * prodName
     */
    @TableField(value = "prodName")
    private String prodName;

    /**
     * prodSpec
     */
    @TableField(value = "prodSpec")
    private String prodSpec;

    /**
     * price
     */
    private String price;

    /**
     * quantity
     */
    private String quantity;

    /**
     * amount
     */
    private String amount;

    /**
     * quantityed
     */
    private String quantityed;

    /**
     * amounted
     */
    private String amounted;

    /**
     * confirmQuantityed
     */
    @TableField(value = "confirmQuantityed")
    private String confirmQuantityed;

    /**
     * confirmAmounted
     */
    @TableField(value = "confirmAmounted")
    private String confirmAmounted;

    /**
     * createTime
     */
    @TableField(value = "createTime")
    private Date createTime;

    /**
     * updateTime
     */
    @TableField(value = "updateTime")
    private Date updateTime;

    /**
     * year
     */
    private String year;

    /**
     * description
     */
    private String description;

    /**
     * saveTime
     */
    @TableField(value = "saveTime")
    private Date saveTime;

    /**
     * clientAttribute
     */
    @TableField(value = "clientAttribute")
    private String clientAttribute;

    /**
     * clientLevel
     */
    @TableField(value = "clientLevel")
    private String clientLevel;

    /**
     * clientClassify
     */
    @TableField(value = "clientClassify")
    private String clientClassify;

    /**
     * registerProvinceName
     */
    @TableField(value = "registerProvinceName")
    private String registerProvinceName;

    /**
     * registerCityName
     */
    @TableField(value = "registerCityName")
    private String registerCityName;

    /**
     * registerCountyName
     */
    @TableField(value = "registerCountyName")
    private String registerCountyName;

    /**
     * writeOffDate
     */
    @TableField(value = "writeOffDate")
    private Date writeOffDate;

    /**
     * upDateCode
     */
    @TableField(value = "upDateCode")
    private String upDateCode;

    /**
     * upRegisterName
     */
    @TableField(value = "upRegisterName")
    private String upRegisterName;

    /**
     * upClientLevel
     */
    @TableField(value = "upClientLevel")
    private String upClientLevel;

    /**
     * documentDate
     */
    @TableField(value = "documentDate")
    private Date documentDate;

    /**
     * terminalSales
     */
    @TableField(value = "terminalSales")
    private String terminalSales;

    /**
     * entryPersonnel
     */
    @TableField(value = "entryPersonnel")
    private String entryPersonnel;

    /**
     * upImport
     */
    @TableField(value = "upImport")
    private String upImport;

    /**
     * downImport
     */
    @TableField(value = "downImport")
    private String downImport;

    /**
     * importQualitySpecifications
     */
    @TableField(value = "importQualitySpecifications")
    private String importQualitySpecifications;

    /**
     * variableCost
     */
    @TableField(value = "variableCost")
    private String variableCost;

    /**
     * marginalRevenue
     */
    @TableField(value = "marginalRevenue")
    private String marginalRevenue;

    /**
     * preSettlementPrice
     */
    @TableField(value = "preSettlementPrice")
    private String preSettlementPrice;

    /**
     * grossProfit
     */
    @TableField(value = "grossProfit")
    private String grossProfit;

    /**
     * tenantId
     */
    @TableField(value = "tenantId")
    private String tenantId;

    /**
     * tenantName
     */
    @TableField(value = "tenantName")
    private String tenantName;

    /**
     * entryMethod
     */
    @TableField(value = "entryMethod")
    private String entryMethod;
}
