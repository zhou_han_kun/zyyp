package com.javaweb.admin.entity;

import lombok.Data;

@Data
public class YYq005Dto {
    private YYq005 yq005;
    private YYq005Detail[] yq005Details;
}
