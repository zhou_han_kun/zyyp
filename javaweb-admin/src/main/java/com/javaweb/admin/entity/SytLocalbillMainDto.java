package com.javaweb.admin.entity;

import lombok.Data;

@Data
public class SytLocalbillMainDto {
    private String dataitem_methodname;
    private String dataitem_content;
}
