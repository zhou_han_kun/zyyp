package com.javaweb.admin.entity;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;

@Data
public class TempData extends BaseEntity {
    private static final long serialVersionUID = 1L;
    private String devno;
    private String devnoname;
    private String temperature1;
    private String uptDate;
}
