package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
* <p>
    * 配送单明细表
    * </p>
*
* @author 鲲鹏
* @since 2024-01-17
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("y_yq004_rs_detail")
public class YYq004RsDetail extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private String cinvoiceBid;

            /**
            * 
            */
                private String csaleid;

            /**
            * 
            */
                private String psmxbh;

            /**
            * 
            */
                private String psdtm;

            /**
            * 
            */
                private String zxlx;

            /**
            * 
            */
                private String splx;

            /**
            * 
            */
                private String zxspbm;

            /**
            * 
            */
                private String ddmxbh;

            /**
            * 
            */
                private String invcode;

            /**
            * 
            */
                private String invname;

}