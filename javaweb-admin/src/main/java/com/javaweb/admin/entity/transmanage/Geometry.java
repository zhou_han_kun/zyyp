package com.javaweb.admin.entity.transmanage;

import java.util.List;

public class Geometry {

    private String type;
    private List<Point> coordinates;
    public void setType(String type) {
        this.type = type;
    }
    public String getType() {
        return type;
    }

    public void setCoordinates(List<Point> coordinates) {
        this.coordinates = coordinates;
    }
    public List<Point> getCoordinates() {
        return coordinates;
    }

}