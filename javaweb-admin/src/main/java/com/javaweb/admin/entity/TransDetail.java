package com.javaweb.admin.entity;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;

@Data
public class TransDetail extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private String pqh;
    private String fhdz;
    private String shzd;
    private String shcs;
    private String shqx;
    private String shdz;
    private String qwsdBegin;
    private String qwsdEnd;
    private String bshsj;
    private String spsl;
    private String sptj;
    private  String spzzl;
    private String ccfs;
    private String spmc;
}
