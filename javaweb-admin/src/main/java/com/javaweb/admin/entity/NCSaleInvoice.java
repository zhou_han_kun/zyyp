package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;


@Data
@TableName(value = "v_ygpt_saleinvoice")
public class NCSaleInvoice {
    private String csaleid;
    private String yqbm;
    private String yybm;
    private String psdbm;
    private String fph;
    private String fprq;
    private String fphszje;
    private String fpwszje;
    private String dlcgbz;
    private String fpbz;
    private String glmxbh;
    private String xsddh;
    private String splx;
    private String sfch;
    private String glbz;
    private String wfglsm;
    private String zxspbm;
    private String scph;
    private String pzwh;
    private BigDecimal spsl;
    private String cgjldw;
    private String scrq;
    private String yxrq;
    private BigDecimal wsdj;
    private BigDecimal hsdj;
    private BigDecimal sl;
    private BigDecimal je;
    private BigDecimal wsje;
    private BigDecimal hsje;
    private BigDecimal pfj;
    private BigDecimal lsj;
    private String dbilltime;
}

