package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
    * 
    * </p>
*
* @author 鲲鹏
* @since 2024-07-12
*/
@Data
@TableName("D_PoinvoiceInfo")
public class DinvoiceInfo {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                @TableId(type = IdType.INPUT)
                private String cinvoiceid;

            /**
            * 
            */
                private String vinvoicecode;

            /**
            * 
            */
                private String vinvoicedm;

            /**
            * 
            */
                @DateTimeFormat(pattern = "yyyy-MM-dd")
                @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
                private Date dinvoicedate;

            /**
            * 
            */
                private String custcode;

            /**
            * 
            */
                private String custname;

            /**
            * 
            */
                private Integer allnum;

            /**
            * 
            */
                private BigDecimal vinvoiceallmoney;

            /**
            * 
            */
                @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
                private Date createDate;

            /**
            * 
            */
                @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
                private Date lastModifyDate;

            /**
            * 
            */
                private String createUsraccount;

            /**
            * 
            */
                private String lastModifyUsraccount;

            /**
            * 
            */
                private String status;

                @TableField(exist = false)
                private String statusdesc;
            /**
            * 
            */
                private String fileupLoadflg;

            /**
            * 
            */
                private String fpid;

            /**
            * 
            */
                private String deptname;

            /**
            * 
            */
                private String cemployee;

            /**
            * 
            */
                @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
                private Date ts;

            /**
            * 
            */
                private String retmemo;

            /**
            * 
            */
                private Integer isclose;

                @TableField(exist = false)
                private String iscloseDesc;
            /**
            * 
            */
                private String dr;

            /**
            * 
            */
                private String memo;

}