package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 两票关系主表
 * </p>
 *
 * @author 鲲鹏
 * @since 2024-01-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("m_invoicesrelation")
public class MInvoicesrelation extends BaseEntity {

    private static final long serialVersionUID = 1L;
    private String Custname;
    private String Custcode;
    private String AllNum;
    private String ntotalsummny;
    private String vdef11;
    private String Status;
    private String ConnectFlg;
    private String csaleid;
    private String Vreceiptcode;
    private String CgoldtaxDM;
    private String Cgoldtaxcode;
    private String dbilldate;
    private String cemployee;
    private String RetMemo;
    private String synflag;
    private String IsFPFlag;
    private String IsPoSoFlag;
    private String statusName;
    private String CinvoiceBid;


    private String businessid;

    /**
     *
     */
    private String salesfph;

    /**
     *
     */
    private String salesfprq;

    /**
     *
     */
    private String salesfphsje;
    private String saleszxspbm;
    private String salesscph;
    private String salesspsl;


    /**
     *
     */
    private Integer result;

    /**
     *
     */
    private String resultstr;

    /**
     *
     */
    private String companyid;

    /**
     *
     */
    private String status;

    /**
     *
     */
    private String glid;

    @TableField(exist = false)
    private String pruchfph;

}
