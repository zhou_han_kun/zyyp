package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
    * 
    * </p>
*
* @author 鲲鹏
* @since 2024-02-01
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("y_yq007")
public class YYq007 extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private String yybm;

                @TableField(exist = false)
                private String yymc;

                @TableField(exist = false)
                private String custaddrname;

                @TableField(exist = false)
                private String czlx;
                /**
            * 
            */
                private String psdbm;

            /**
            * 
            */
                private String yqbm;

            /**
            * 
            */
                private String thdbh;

            /**
            * 
            */
                private String dlcgbz;

            /**
            * 
            */
                private String sjthrq;

            /**
            * 
            */
                private String splx;

            /**
            * 
            */
                private String zxspbm;

            /**
            * 
            */
                private String ggbz;

            /**
            * 
            */
                private String cgjldw;

            /**
            * 
            */
                private String scph;

            /**
            * 
            */
                private BigDecimal thsl;

            /**
            * 
            */
                private BigDecimal thdj;

            /**
            * 
            */
                private BigDecimal thzj;

            /**
            * 
            */
                private String thyy;

            /**
            * 
            */
                private Integer status;

}