package com.javaweb.admin.entity;

import lombok.Data;

@Data
public class YQ018InvList {
    private String zxspbm;
    private String cpm;
    private String ypjx;
    private String cfgg;
    private String sccj;
    private String invcode;
    private String invname;
    private String onhandnum;
    private String sfjj;
}
