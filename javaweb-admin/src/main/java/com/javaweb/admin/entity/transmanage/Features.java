package com.javaweb.admin.entity.transmanage;

public class Features {

    private String type;
    private Properties properties;
    private Geometry geometry;
    public void setType(String type) {
        this.type = type;
    }
    public String getType() {
        return type;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }
    public Properties getProperties() {
        return properties;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }
    public Geometry getGeometry() {
        return geometry;
    }

}
