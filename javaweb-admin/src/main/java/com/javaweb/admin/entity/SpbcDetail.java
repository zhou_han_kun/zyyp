package com.javaweb.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
    * 
    * </p>
*
* @author 鲲鹏
* @since 2023-11-27
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_spbc_detail")
public class SpbcDetail extends BaseEntity {

private static final long serialVersionUID = 1L;

            /**
            * 
            */
                private Integer pid;

            /**
            * 
            */
                private String invcode;

            /**
            * 
            */
                private String invname;

            /**
            * 
            */
                private String vlotno;

            /**
            * 
            */
                private BigDecimal nqtorigtaxprice;

            /**
            * 
            */
            private BigDecimal nqtorigprice;

            /**
            * 
            */
            private BigDecimal norigtaxmny;

            /**
            * 
            */
            private BigDecimal norigmny;

            /**
            * 
            */
            private BigDecimal nastnum;

            /**
            * 
            */
            private BigDecimal balancenum;

            /**
            * 
            */
            private BigDecimal remainnum;

            /**
            * 
            */
            private BigDecimal nowsaleprice;

            /**
            * 
            */
            private BigDecimal diffprice;

            /**
            * 
            */
            private BigDecimal balanceamount;

            /**
            * 
            */
                private String csaleid;

            /**
            * 
            */
                private String cinvoicebid;

            /**
            * 
            */
                private String bcCsaleid;

            /**
            * 
            */
                @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
                private Date ts;

            /**
            * 
            */
                private String bcCinvoiceBid;

                private String gg;

                private String manufacturer;

}