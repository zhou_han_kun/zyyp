// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.controller;

import com.javaweb.admin.query.SpbcCustomerQuery;
import com.javaweb.admin.query.SpbcSummaryQuery;
import com.javaweb.admin.vo.spbc.SpbcDto;
import com.javaweb.common.enums.LogType;
import com.javaweb.common.common.BaseController;
import com.javaweb.admin.entity.Spbc;
import com.javaweb.admin.query.SpbcQuery;
import com.javaweb.admin.service.ISpbcService;
import com.javaweb.common.annotation.Log;
import com.javaweb.common.utils.JsonResult;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2023-11-27
 */
@RestController
@RequestMapping("/spbc")
public class SpbcController extends BaseController {

    @Autowired
    private ISpbcService spbcService;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @RequiresPermissions("sys:bctb:index")
    @GetMapping("/index")
    public JsonResult index(SpbcQuery query) {
        return spbcService.getList(query);
    }

    @GetMapping("/selectcustomer")
    public JsonResult selectcustomer(SpbcCustomerQuery query) {
        return spbcService.getSpbcCustomerList(query);
    }

    @PostMapping("/summarylist")
    public JsonResult summarylist(@RequestBody SpbcSummaryQuery query) {
        return spbcService.getSpbcSummaryList(query);
    }

    @PostMapping("/detaillist")
    public JsonResult detaillist(@RequestBody SpbcSummaryQuery query) {
        return spbcService.getSpbcDetailList(query);
    }

    @PostMapping("/invoicelist")
    public JsonResult invoicelist(@RequestBody SpbcSummaryQuery query) {
        return spbcService.getSpbcInvoiceList(query);
    }
    /**
     * 添加记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "补差管理", logType = LogType.INSERT)
    @RequiresPermissions("sys:bctb:add")
    @PostMapping("/add")
    public JsonResult add(@RequestBody Spbc entity) {
        return spbcService.edit(entity);
    }

    @Log(title = "补差管理", logType = LogType.INSERT)
    @PostMapping("/create")
    public JsonResult create(@RequestBody SpbcDto entity) {
        return spbcService.saveSpbc(entity);
    }
    /**
     * 获取详情
     *
     * @param file 上传文件
     * @return
     */

    @PostMapping("/uploadfile")
    public JsonResult uploadfile(@RequestParam("file") MultipartFile file)  {
        return spbcService.uploadFile(file);
    }

    @GetMapping("/info/{spbcId}")
    public JsonResult info(@PathVariable("spbcId") Integer spbcId) {
        return spbcService.info(spbcId);
    }

    @PostMapping("/updateflag/{id}/{flag}")
    public JsonResult updateflag(@PathVariable("id") Integer id,@PathVariable("flag") String flag) {
        return spbcService.updateCheckFlag(id,flag);
    }
    /**
     * 更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "补差管理", logType = LogType.UPDATE)
    @RequiresPermissions("sys:bctb:edit")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody Spbc entity) {
        return spbcService.edit(entity);
    }

    @Log(title = "补差管理", logType = LogType.UPDATE)
    @PutMapping("/update")
    public JsonResult update(@RequestBody SpbcDto entity) {
        return spbcService.saveSpbc(entity);
    }

    /**
     * 删除记录
     *
     * @param spbcId 记录ID
     * @return
     */
    @Log(title = "补差管理", logType = LogType.DELETE)
    @RequiresPermissions("sys:bctb:delete")
    @DeleteMapping("/delete/{spbcId}")
    public JsonResult delete(@PathVariable("spbcId") Integer spbcId) {
        spbcService.deleteSpbcDetailByPid(spbcId);
        spbcService.deleteSpbcInvoiceByPid(spbcId);
        return spbcService.deleteById(spbcId);
    }

    @GetMapping("/detail")
    public JsonResult detail(SpbcSummaryQuery query) {
        return spbcService.getSpbcDetail(query);
    }

    @GetMapping("/invoicedetail")
    public JsonResult invoicedetail(SpbcSummaryQuery query) {
        return spbcService.getSpbcInvoiceDetail(query);
    }

    @GetMapping("/getCustAddrList")
    public JsonResult getCustAddrList(String custcode) {
        return spbcService.getCustAddrList(custcode);
    }

}