package com.javaweb.admin.controller;

import com.javaweb.admin.mapper.ReturnOrderMapper;
import com.javaweb.admin.query.ReturnQuery;
import com.javaweb.admin.query.SySyxxQuery;
import com.javaweb.admin.service.IReturnOrderService;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/return")
public class ReturnController {

    @Autowired
    private IReturnOrderService returnOrderService;
    @GetMapping("/index")
    public JsonResult index(ReturnQuery query) {
        try {
            return returnOrderService.getReturnOrderList(query);
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResult.error(e.getMessage());
        }



    }

    @GetMapping("/orderdetail/{csaleorderid}")
    public JsonResult orderdetail(@PathVariable("csaleorderid") String csaleorderid){
        return returnOrderService.getOrderDetail(csaleorderid);
    }
}
