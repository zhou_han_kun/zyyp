// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.controller;

import com.javaweb.common.enums.LogType;
import com.javaweb.common.common.BaseController;
import com.javaweb.admin.entity.MdmProductRecords;
import com.javaweb.admin.query.MdmProductRecordsQuery;
import com.javaweb.admin.service.IMdmProductRecordsService;
import com.javaweb.common.annotation.Log;
import com.javaweb.common.utils.JsonResult;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2023-07-24
 */
@RestController
@RequestMapping("/mdmproductrecords")
public class MdmProductRecordsController extends BaseController {

    @Autowired
    private IMdmProductRecordsService mdmProductRecordsService;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @RequiresPermissions("sys:mdmproductrecords:index")
    @GetMapping("/index")
    public JsonResult index(MdmProductRecordsQuery query) {
        return mdmProductRecordsService.getList(query);
    }

    /**
     * 添加记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "", logType = LogType.INSERT)
    @RequiresPermissions("sys:mdmproductrecords:add")
    @PostMapping("/add")
    public JsonResult add(@RequestBody MdmProductRecords entity) {
        return mdmProductRecordsService.edit(entity);
    }

    /**
     * 获取详情
     *
     * @param mdmproductrecordsId 记录ID
     * @return
     */
    @GetMapping("/info/{mdmproductrecordsId}")
    public JsonResult info(@PathVariable("mdmproductrecordsId") Integer mdmproductrecordsId) {
        return mdmProductRecordsService.info(mdmproductrecordsId);
    }

    /**
     * 更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "", logType = LogType.UPDATE)
    @RequiresPermissions("sys:mdmproductrecords:edit")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody MdmProductRecords entity) {
        return mdmProductRecordsService.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param mdmproductrecordsIds 记录ID
     * @return
     */
    @Log(title = "", logType = LogType.DELETE)
    @RequiresPermissions("sys:mdmproductrecords:delete")
    @DeleteMapping("/delete/{mdmproductrecordsIds}")
    public JsonResult delete(@PathVariable("mdmproductrecordsIds") Integer[] mdmproductrecordsIds) {
        return mdmProductRecordsService.deleteByIds(mdmproductrecordsIds);
    }

}