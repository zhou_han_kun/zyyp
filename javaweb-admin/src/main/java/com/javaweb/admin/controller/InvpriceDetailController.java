// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.controller;

import com.javaweb.common.enums.LogType;
import com.javaweb.common.common.BaseController;
import com.javaweb.admin.entity.InvpriceDetail;
import com.javaweb.admin.query.InvpriceDetailQuery;
import com.javaweb.admin.service.IInvpriceDetailService;
import com.javaweb.common.annotation.Log;
import com.javaweb.common.utils.JsonResult;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2024-07-11
 */
@RestController
@RequestMapping("/invpricedetail")
public class InvpriceDetailController extends BaseController {

    @Autowired
    private IInvpriceDetailService invpriceDetailService;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    //@RequiresPermissions("sys:invpricedetail:index")
    @GetMapping("/index")
    public JsonResult index(InvpriceDetailQuery query) {
        return invpriceDetailService.getList(query);
    }

    /**
     * 添加记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "", logType = LogType.INSERT)
    //@RequiresPermissions("sys:invpricedetail:add")
    @PostMapping("/add")
    public JsonResult add(@RequestBody InvpriceDetail entity) {
        return invpriceDetailService.edit(entity);
    }

    /**
     * 获取详情
     *
     * @param invpricedetailId 记录ID
     * @return
     */
    @GetMapping("/info/{invpricedetailId}")
    public JsonResult info(@PathVariable("invpricedetailId") Integer invpricedetailId) {
        return invpriceDetailService.info(invpricedetailId);
    }

    /**
     * 更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "", logType = LogType.UPDATE)
    //@RequiresPermissions("sys:invpricedetail:edit")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody InvpriceDetail entity) {
        return invpriceDetailService.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param invpricedetailIds 记录ID
     * @return
     */
    @Log(title = "", logType = LogType.DELETE)
    //@RequiresPermissions("sys:invpricedetail:delete")
    @DeleteMapping("/delete/{invpricedetailIds}")
    public JsonResult delete(@PathVariable("invpricedetailIds") Integer[] invpricedetailIds) {
        return invpriceDetailService.deleteByIds(invpricedetailIds);
    }

}