package com.javaweb.admin.controller;

import com.javaweb.admin.entity.SytLocalbillMain;
import com.javaweb.admin.entity.SytLocalbillMainDto;
import com.javaweb.admin.query.MDMQueryParam;
import com.javaweb.admin.service.ISytLocalbillMainService;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.JsonResultSYT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/localbillmain")
public class LocalbillMainController {
    @Autowired
    private ISytLocalbillMainService sytLocalbillMainService;

    @PostMapping("/FLDataCallBack")
    public JsonResultSYT FLDataCallBack(@RequestBody SytLocalbillMainDto dto) {
        return sytLocalbillMainService.saveLocalbillMain(dto);
    }

}
