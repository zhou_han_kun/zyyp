// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.controller;

import com.javaweb.admin.entity.*;
import com.javaweb.admin.query.NCCustQuery;
import com.javaweb.admin.query.NCInvQuery;
import com.javaweb.admin.query.YYq005Query;
import com.javaweb.admin.service.IYYq005Service;
import com.javaweb.common.annotation.Log;
import com.javaweb.common.common.BaseController;
import com.javaweb.common.enums.LogType;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 订单代填主表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2023-12-31
 */
@RestController
@RequestMapping("/yyq005")
public class YYq005Controller extends BaseController {

    @Autowired
    private IYYq005Service yYq005Service;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    //@RequiresPermissions("sys:yyq005:index")
    @GetMapping("/index")
    public JsonResult index(YYq005Query query) {
        return yYq005Service.getList(query);
    }

    @GetMapping("/detail")
    public JsonResult detail(YYq005Query query) {
        return JsonResult.success(yYq005Service.getDetailList(query.getReplaceno()));
    }
    /**
     * 添加记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "订单代填主表", logType = LogType.INSERT)
    @RequiresPermissions("sys:yyq005:add")
    @PostMapping("/add")
    public JsonResult add(@RequestBody YYq005 entity) {
        return yYq005Service.edit(entity);
    }

    /**
     * 获取详情
     *
     * @param yyq005Id 记录ID
     * @return
     */
    @GetMapping("/info/{yyq005Id}")
    public JsonResult info(@PathVariable("yyq005Id") Integer yyq005Id) {
        return yYq005Service.info(yyq005Id);
    }

    @GetMapping("/custlist")
    public JsonResult custlist(NCCustQuery custQuery) {
        String code = StringUtils.isNull(custQuery.getCustcode())?"":custQuery.getCustcode();
        String name = StringUtils.isNull(custQuery.getCustname())?"":custQuery.getCustname();
        List<NCCustInfoDoc> list=  yYq005Service.getCustomerList(code,name);
        return JsonResult.success(list);
    }

    @GetMapping("/custaddrlist")
    public JsonResult custaddrlist(NCCustQuery custQuery) {
        String code = StringUtils.isNull(custQuery.getCustcode())?"":custQuery.getCustcode();
        List<NCCustAddrDoc> list=  yYq005Service.getCustomerAddressList(code);
        return JsonResult.success(list);
    }

    @GetMapping("/custinvlist")
    public JsonResult custinvlist(NCInvQuery invQuery) {
        String code = StringUtils.isNull(invQuery.getInvcode())?"":invQuery.getInvcode();
        String name = StringUtils.isNull(invQuery.getInvname())?"":invQuery.getInvname();
        String yybm = StringUtils.isNull(invQuery.getYybm())?"00@00":invQuery.getYybm();
        List<YYq005Detail> list=  yYq005Service.getInvCustList(yybm,code,name);
        return JsonResult.success(list);
    }

    @GetMapping("/postlog")
    public JsonResult postlog(PostResultXml postQuery) {
        String postid = StringUtils.isNull(postQuery.getPostid())?"":postQuery.getPostid();
        return  yYq005Service.getLogList(postid);
    }

    @PostMapping("/create")
    public JsonResult create(@RequestBody YYq005Dto dto) {
        return yYq005Service.create(dto);
    }

    @PostMapping("/dddtupload")
    public JsonResult dddtupload(@RequestBody YYq005Dto dto) {
        return yYq005Service.DDDTToYGPT(dto);
    }

    @PostMapping("/dddtconfirm")
    public JsonResult dddtconfirm(@RequestBody YYq005Dto dto) {
        return yYq005Service.DDDTConfirm(dto);
    }


    /**
     * 更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "订单代填主表", logType = LogType.UPDATE)
    @RequiresPermissions("sys:yyq005:edit")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody YYq005 entity) {
        return yYq005Service.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param yyq005Ids 记录ID
     * @return
     */
    @Log(title = "订单代填主表", logType = LogType.DELETE)
    //@RequiresPermissions("sys:yyq005:delete")
    @DeleteMapping("/delete/{yyq005Ids}")
    public JsonResult delete(@PathVariable("yyq005Ids") String[] yyq005Ids) {
        return yYq005Service.deleteByIds(yyq005Ids);
    }

}