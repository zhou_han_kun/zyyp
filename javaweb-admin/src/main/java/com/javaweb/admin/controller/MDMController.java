package com.javaweb.admin.controller;

import com.javaweb.admin.query.MDMQueryParam;
import com.javaweb.admin.query.SySyxxQuery;
import com.javaweb.admin.service.IMDMService;
import com.javaweb.admin.service.ISySyxxService;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mdm")
public class MDMController {
    @Autowired
    private IMDMService imdmService;
    @PostMapping("/material")
    public JsonResult material(@RequestBody MDMQueryParam query) {
            return imdmService.SaveMaterial(query);
    }

    @PostMapping("/customer")
    public JsonResult customer(@RequestBody MDMQueryParam query) {
        return imdmService.SaveCustomer(query);
    }

    @PostMapping("/ncsale")
    public JsonResult ncsale(@RequestBody MDMQueryParam query) {
        return imdmService.SaveNCSaleFlow(query);
    }

    @PostMapping("/employee")
    public JsonResult employee(@RequestBody MDMQueryParam query) {
        return imdmService.SaveEmployee(query);
    }

    @PostMapping("/expert")
    public JsonResult expert(@RequestBody MDMQueryParam query) {
        return imdmService.SaveExpert(query);
    }

    @PostMapping("/cleansales")
    public JsonResult cleansales(@RequestBody MDMQueryParam query) {
        return imdmService.SaveCleanSales(query);
    }

    @PostMapping("/cleaninventory")
    public JsonResult cleaninventory(@RequestBody MDMQueryParam query) {
        return imdmService.SaveCleanInventory(query);
    }

    @PostMapping("/monthtarget")
    public JsonResult monthtarget(@RequestBody MDMQueryParam query) {
        return imdmService.SaveMonthTarget(query);
    }
}
