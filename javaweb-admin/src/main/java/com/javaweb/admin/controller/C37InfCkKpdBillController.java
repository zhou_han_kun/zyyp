// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.controller;

import com.javaweb.common.enums.LogType;
import com.javaweb.common.common.BaseController;
import com.javaweb.admin.entity.C37InfCkKpdBill;
import com.javaweb.admin.query.C37InfCkKpdBillQuery;
import com.javaweb.admin.service.IC37InfCkKpdBillService;
import com.javaweb.common.annotation.Log;
import com.javaweb.common.utils.JsonResult;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 销售订单表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2022-11-26
 */
@RestController
@RequestMapping("/c37infckkpdbill")
public class C37InfCkKpdBillController extends BaseController {

    @Autowired
    private IC37InfCkKpdBillService c37InfCkKpdBillService;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    //@RequiresPermissions("sys:c37infckkpdbill:index")
    @GetMapping("/index")
    public JsonResult index(C37InfCkKpdBillQuery query) {
        return c37InfCkKpdBillService.GetXsddList(query);
    }

    @GetMapping("/detail")
    public JsonResult detail(C37InfCkKpdBillQuery query) {
        return c37InfCkKpdBillService.GetXsddDetailList(query);
    }
    /**
     * 添加记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "销售订单表", logType = LogType.INSERT)
    @RequiresPermissions("sys:c37infckkpdbill:add")
    @PostMapping("/add")
    public JsonResult add(@RequestBody C37InfCkKpdBill entity) {
        return c37InfCkKpdBillService.edit(entity);
    }

    /**
     * 获取详情
     *
     * @param c37infckkpdbillId 记录ID
     * @return
     */
    @GetMapping("/info/{c37infckkpdbillId}")
    public JsonResult info(@PathVariable("c37infckkpdbillId") Integer c37infckkpdbillId) {
        return c37InfCkKpdBillService.info(c37infckkpdbillId);
    }

    /**
     * 更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "销售订单表", logType = LogType.UPDATE)
    @RequiresPermissions("sys:c37infckkpdbill:edit")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody C37InfCkKpdBill entity) {
        return c37InfCkKpdBillService.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param c37infckkpdbillIds 记录ID
     * @return
     */
    @Log(title = "销售订单表", logType = LogType.DELETE)
    @RequiresPermissions("sys:c37infckkpdbill:delete")
    @DeleteMapping("/delete/{c37infckkpdbillIds}")
    public JsonResult delete(@PathVariable("c37infckkpdbillIds") Integer[] c37infckkpdbillIds) {
        return c37InfCkKpdBillService.deleteByIds(c37infckkpdbillIds);
    }

}