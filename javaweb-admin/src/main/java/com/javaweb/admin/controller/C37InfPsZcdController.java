// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.controller;

import com.javaweb.common.enums.LogType;
import com.javaweb.common.common.BaseController;
import com.javaweb.admin.entity.C37InfPsZcd;
import com.javaweb.admin.query.C37InfPsZcdQuery;
import com.javaweb.admin.service.IC37InfPsZcdService;
import com.javaweb.common.annotation.Log;
import com.javaweb.common.utils.JsonResult;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 运输记录 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2022-11-26
 */
@RestController
@RequestMapping("/c37infpszcd")
public class C37InfPsZcdController extends BaseController {

    @Autowired
    private IC37InfPsZcdService c37InfPsZcdService;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    //@RequiresPermissions("sys:c37infpszcd:index")
    @GetMapping("/index")
    public JsonResult index(C37InfPsZcdQuery query) {
        return c37InfPsZcdService.getList(query);
    }

    /**
     * 添加记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "运输记录", logType = LogType.INSERT)
    @RequiresPermissions("sys:c37infpszcd:add")
    @PostMapping("/add")
    public JsonResult add(@RequestBody C37InfPsZcd entity) {
        return c37InfPsZcdService.edit(entity);
    }

    /**
     * 获取详情
     *
     * @param c37infpszcdId 记录ID
     * @return
     */
    @GetMapping("/info/{c37infpszcdId}")
    public JsonResult info(@PathVariable("c37infpszcdId") Integer c37infpszcdId) {
        return c37InfPsZcdService.info(c37infpszcdId);
    }

    /**
     * 更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "运输记录", logType = LogType.UPDATE)
    @RequiresPermissions("sys:c37infpszcd:edit")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody C37InfPsZcd entity) {
        return c37InfPsZcdService.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param c37infpszcdIds 记录ID
     * @return
     */
    @Log(title = "运输记录", logType = LogType.DELETE)
    @RequiresPermissions("sys:c37infpszcd:delete")
    @DeleteMapping("/delete/{c37infpszcdIds}")
    public JsonResult delete(@PathVariable("c37infpszcdIds") Integer[] c37infpszcdIds) {
        return c37InfPsZcdService.deleteByIds(c37infpszcdIds);
    }

}