// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.controller;

import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.enums.LogType;
import com.javaweb.common.common.BaseController;
import com.javaweb.admin.entity.C37InfJcDwzl;
import com.javaweb.admin.query.C37InfJcDwzlQuery;
import com.javaweb.admin.service.IC37InfJcDwzlService;
import com.javaweb.common.annotation.Log;
import com.javaweb.common.utils.JsonResult;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 单位资料 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2022-11-24
 */
@RestController
@RequestMapping("/c37infjcdwzl")
public class C37InfJcDwzlController extends BaseController {

    @Autowired
    private IC37InfJcDwzlService c37InfJcDwzlService;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    //@RequiresPermissions("sys:c37infjcdwzl:index")
    @GetMapping("/index")
    public JsonResult index(C37InfJcDwzlQuery query) {
        return c37InfJcDwzlService.getList(query);
    }

    /**
     * 添加记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "单位资料", logType = LogType.INSERT)
    @RequiresPermissions("sys:c37infjcdwzl:add")
    @PostMapping("/add")
    public JsonResult add(@RequestBody C37InfJcDwzl entity) {
        return c37InfJcDwzlService.edit(entity);
    }

    /**
     * 获取详情
     *
     * @param c37infjcdwzlId 记录ID
     * @return
     */
    @GetMapping("/info/{c37infjcdwzlId}")
    public JsonResult info(@PathVariable("c37infjcdwzlId") Integer c37infjcdwzlId) {
        return c37InfJcDwzlService.info(c37infjcdwzlId);
    }

    /**
     * 更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "单位资料", logType = LogType.UPDATE)
    @RequiresPermissions("sys:c37infjcdwzl:edit")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody C37InfJcDwzl entity) {
        return c37InfJcDwzlService.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param c37infjcdwzlIds 记录ID
     * @return
     */
    @Log(title = "单位资料", logType = LogType.DELETE)
    @RequiresPermissions("sys:c37infjcdwzl:delete")
    @DeleteMapping("/delete/{c37infjcdwzlIds}")
    public JsonResult delete(@PathVariable("c37infjcdwzlIds") Integer[] c37infjcdwzlIds) {
        return c37InfJcDwzlService.deleteByIds(c37infjcdwzlIds);
    }

}