// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.controller;

import com.javaweb.admin.entity.MPruchinvoice;
import com.javaweb.admin.entity.MPruchinvoiceDto;
import com.javaweb.admin.query.MPruchinvoiceQuery;
import com.javaweb.admin.service.IMPruchinvoiceService;
import com.javaweb.common.annotation.Log;
import com.javaweb.common.common.BaseController;
import com.javaweb.common.enums.LogType;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 采购发票主表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2024-01-17
 */
@RestController
@RequestMapping("/mpruchinvoice")
public class MPruchinvoiceController extends BaseController {

    @Autowired
    private IMPruchinvoiceService mPruchinvoiceService;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    //@RequiresPermissions("sys:mpruchinvoice:index")
    @GetMapping("/index")
    public JsonResult index(MPruchinvoiceQuery query) {
        return mPruchinvoiceService.getList(query);
    }

    @GetMapping("/pruchinvoice")
    public JsonResult pruchinvoice(MPruchinvoiceQuery query) {
        return mPruchinvoiceService.getPruchinvoiceList(query);
    }
    /**
     * 添加记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "采购发票主表", logType = LogType.INSERT)
    //@RequiresPermissions("sys:mpruchinvoice:add")
    @PostMapping("/add")
    public JsonResult add(@RequestBody MPruchinvoice entity) {
        return mPruchinvoiceService.edit(entity);
    }


    @Log(title = "采购发票主表", logType = LogType.INSERT)
    //@RequiresPermissions("sys:mpruchinvoice:add")
    @PostMapping("/create")
    public JsonResult create(@RequestBody MPruchinvoiceDto dto) {
        return mPruchinvoiceService.create(dto);
    }

    @Log(title = "采购发票主表", logType = LogType.UPDATE)
    //@RequiresPermissions("sys:mpruchinvoice:add")
    @PostMapping("/update")
    public JsonResult update(@RequestBody MPruchinvoiceDto dto) {
        return mPruchinvoiceService.update(dto);
    }
    /**
     * 获取详情
     *
     * @param mpruchinvoiceId 记录ID
     * @return
     */
    @GetMapping("/info/{mpruchinvoiceId}")
    public JsonResult info(@PathVariable("mpruchinvoiceId") Integer mpruchinvoiceId) {
        return mPruchinvoiceService.info(mpruchinvoiceId);
    }

    /**
     * 更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "采购发票主表", logType = LogType.UPDATE)
    //@RequiresPermissions("sys:mpruchinvoice:edit")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody MPruchinvoice entity) {
        return mPruchinvoiceService.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param mpruchinvoiceIds 记录ID
     * @return
     */
    @Log(title = "采购发票主表", logType = LogType.DELETE)
    //@RequiresPermissions("sys:mpruchinvoice:delete")
    @DeleteMapping("/delete/{mpruchinvoiceIds}")
    public JsonResult delete(@PathVariable("mpruchinvoiceIds") Integer[] mpruchinvoiceIds) {
        return mPruchinvoiceService.deleteByIds(mpruchinvoiceIds);
    }

}