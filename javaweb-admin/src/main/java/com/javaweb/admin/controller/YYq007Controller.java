// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.controller;

import com.javaweb.admin.entity.YYq005Detail;
import com.javaweb.admin.entity.YYq005Dto;
import com.javaweb.admin.query.NCInvQuery;
import com.javaweb.common.enums.LogType;
import com.javaweb.common.common.BaseController;
import com.javaweb.admin.entity.YYq007;
import com.javaweb.admin.query.YYq007Query;
import com.javaweb.admin.service.IYYq007Service;
import com.javaweb.common.annotation.Log;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2024-02-01
 */
@RestController
@RequestMapping("/yyq007")
public class YYq007Controller extends BaseController {

    @Autowired
    private IYYq007Service yYq007Service;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    //@RequiresPermissions("sys:yyq007:index")
    @GetMapping("/index")
    public JsonResult index(YYq007Query query) {
        return yYq007Service.GetYQ007List(query);
    }

    /**
     * 添加记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "", logType = LogType.INSERT)
    //@RequiresPermissions("sys:yyq007:add")
    @PostMapping("/add")
    public JsonResult add(@RequestBody YYq007 entity) {
        return yYq007Service.edit(entity);
    }


    @PostMapping("/dddtupload")
    public JsonResult dddtupload(@RequestBody YYq007 dto) {
        return yYq007Service.THDDTToYGPT(dto);
    }

    @PostMapping("/dddtconfirm")
    public JsonResult dddtconfirm(@RequestBody YYq007 dto) {
        return yYq007Service.THDDTConfirm(dto);
    }

    /**
     * 获取详情
     *
     * @param yyq007Id 记录ID
     * @return
     */
    @GetMapping("/info/{yyq007Id}")
    public JsonResult info(@PathVariable("yyq007Id") Integer yyq007Id) {
        return yYq007Service.info(yyq007Id);
    }


    @GetMapping("/invlist")
    public JsonResult invlist(NCInvQuery invQuery) {
        String code = StringUtils.isNull(invQuery.getInvcode())?"":invQuery.getInvcode();
        String name = StringUtils.isNull(invQuery.getInvname())?"":invQuery.getInvname();
        String yybm = StringUtils.isNull(invQuery.getYybm())?"00@00":invQuery.getYybm();
        String zxspbm = StringUtils.isNull(invQuery.getZxspbm())?"":invQuery.getZxspbm();
        List<YYq005Detail> list=  yYq007Service.getInvCustList(yybm,code,name,zxspbm);
        return JsonResult.success(list);
    }
    /**
     * 更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "", logType = LogType.UPDATE)
    @RequiresPermissions("sys:yyq007:edit")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody YYq007 entity) {
        return yYq007Service.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param yyq007Ids 记录ID
     * @return
     */
    @Log(title = "", logType = LogType.DELETE)
    //@RequiresPermissions("sys:yyq007:delete")
    @DeleteMapping("/delete/{yyq007Ids}")
    public JsonResult delete(@PathVariable("yyq007Ids") Integer[] yyq007Ids) {
        return yYq007Service.deleteByIds(yyq007Ids);
    }

}