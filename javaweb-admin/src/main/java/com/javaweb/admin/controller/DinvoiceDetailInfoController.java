// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.controller;

import com.javaweb.common.enums.LogType;
import com.javaweb.common.common.BaseController;
import com.javaweb.admin.entity.DinvoiceDetailInfo;
import com.javaweb.admin.query.DinvoiceDetailInfoQuery;
import com.javaweb.admin.service.IDinvoiceDetailInfoService;
import com.javaweb.common.annotation.Log;
import com.javaweb.common.utils.JsonResult;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2024-07-12
 */
@RestController
@RequestMapping("/d0oinvoicedetailinfo")
public class DinvoiceDetailInfoController extends BaseController {

    @Autowired
    private IDinvoiceDetailInfoService d0oinvoiceDetailInfoService;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @RequiresPermissions("sys:d0oinvoicedetailinfo:index")
    @GetMapping("/index")
    public JsonResult index(DinvoiceDetailInfoQuery query) {
        return d0oinvoiceDetailInfoService.getList(query);
    }

    /**
     * 添加记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "", logType = LogType.INSERT)
    @RequiresPermissions("sys:d0oinvoicedetailinfo:add")
    @PostMapping("/add")
    public JsonResult add(@RequestBody DinvoiceDetailInfo entity) {
        return d0oinvoiceDetailInfoService.edit(entity);
    }

    /**
     * 获取详情
     *
     * @param d0oinvoicedetailinfoId 记录ID
     * @return
     */
    @GetMapping("/info/{d0oinvoicedetailinfoId}")
    public JsonResult info(@PathVariable("d0oinvoicedetailinfoId") Integer d0oinvoicedetailinfoId) {
        return d0oinvoiceDetailInfoService.info(d0oinvoicedetailinfoId);
    }

    /**
     * 更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "", logType = LogType.UPDATE)
    @RequiresPermissions("sys:d0oinvoicedetailinfo:edit")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody DinvoiceDetailInfo entity) {
        return d0oinvoiceDetailInfoService.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param d0oinvoicedetailinfoIds 记录ID
     * @return
     */
    @Log(title = "", logType = LogType.DELETE)
    @RequiresPermissions("sys:d0oinvoicedetailinfo:delete")
    @DeleteMapping("/delete/{d0oinvoicedetailinfoIds}")
    public JsonResult delete(@PathVariable("d0oinvoicedetailinfoIds") Integer[] d0oinvoicedetailinfoIds) {
        return d0oinvoiceDetailInfoService.deleteByIds(d0oinvoicedetailinfoIds);
    }

}