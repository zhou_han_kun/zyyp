// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.controller;

import com.javaweb.admin.query.SpbcInvcodeQuery;
import com.javaweb.admin.service.ISpbcInvsetDetailService;
import com.javaweb.common.enums.LogType;
import com.javaweb.common.common.BaseController;
import com.javaweb.admin.entity.SpbcInvset;
import com.javaweb.admin.query.SpbcInvsetQuery;
import com.javaweb.admin.service.ISpbcInvsetService;
import com.javaweb.common.annotation.Log;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.system.utils.ShiroUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2023-11-27
 */
@RestController
@RequestMapping("/spbcinvset")
public class SpbcInvsetController extends BaseController {

    @Autowired
    private ISpbcInvsetService spbcInvsetService;

    @Autowired
    private ISpbcInvsetDetailService spbcInvsetDetailService;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @RequiresPermissions("sys:spbcinvset:index")
    @GetMapping("/index")
    public JsonResult index(SpbcInvsetQuery query) {
        return spbcInvsetService.getList(query);
    }

    @GetMapping("/getValidatedList")
    public JsonResult getValidatedList(SpbcInvsetQuery query) {
        return spbcInvsetService.getValidatedList(query);
    }
    @GetMapping("/getAllList")
    public JsonResult getAllList(SpbcInvsetQuery query) { return spbcInvsetService.getAllList(query); }
    @GetMapping("/selectinvcode")
    public JsonResult selectinvcode(SpbcInvcodeQuery query) {
        return spbcInvsetService.GetSpbcInvcodeList(query);
    }

    @GetMapping("/invsetdetail")
    public JsonResult invsetdetail(SpbcInvcodeQuery query) {
        return spbcInvsetService.GetSpbcInvsetDetailList(query);
    }

    /**
     * 添加记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "补差管理", logType = LogType.INSERT)
    //@RequiresPermissions("sys:spbcinvset:add")
    @PostMapping("/add")
    public JsonResult add(@RequestBody SpbcInvset entity) {
        entity.setCreateUserName(ShiroUtils.getUserInfo().getRealname());
        return spbcInvsetService.edit(entity);
    }

    /**
     * 获取详情
     *
     * @param spbcinvsetId 记录ID
     * @return
     */
    @GetMapping("/info/{spbcinvsetId}")
    public JsonResult info(@PathVariable("spbcinvsetId") Integer spbcinvsetId) {
        return spbcInvsetService.info(spbcinvsetId);
    }

    /**
     * 更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "补差管理", logType = LogType.UPDATE)
    @RequiresPermissions("sys:spbcinvset:edit")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody SpbcInvset entity) {
        return spbcInvsetService.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param spbcinvsetIds 记录ID
     * @return
     */
    @Log(title = "补差管理", logType = LogType.DELETE)
    @RequiresPermissions("sys:spbcinvset:delete")
    @DeleteMapping("/delete/{spbcinvsetIds}")
    public JsonResult delete(@PathVariable("spbcinvsetIds") Integer[] spbcinvsetIds) {
        for(Integer pid : spbcinvsetIds){
            spbcInvsetDetailService.deleteSpbcInvsetDetailByPid(pid);
        }
        return spbcInvsetService.deleteByIds(spbcinvsetIds);
    }

    @PostMapping("/uploadfile")
    public JsonResult uploadfile(@RequestParam("file") MultipartFile file){
        return spbcInvsetService.uploadFile(file);
    }

    @PostMapping("/updateMark/{id}/{mark}")
    public JsonResult updateMark(@PathVariable("id") Integer id,@PathVariable("mark") String mark) {
        if(mark.equals("1")) {
            int num = spbcInvsetService.getSpbcNum(id);
            if (num > 0) {
                return JsonResult.error("该补差规则已关联补差申请，无法取消审核");
            }
        }
        return spbcInvsetService.updateMark(id, mark);
    }

    @PostMapping("/copy/{id}")
    public JsonResult copyById(@PathVariable("id") Integer id) {
        return spbcInvsetService.copyById(id);
    }

}