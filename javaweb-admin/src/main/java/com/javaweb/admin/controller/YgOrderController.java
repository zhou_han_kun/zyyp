package com.javaweb.admin.controller;

import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.XmlUtil;
import com.javaweb.admin.entity.YConverRate;
import com.javaweb.admin.entity.YgOrderDto;
import com.javaweb.admin.entity.YgptAddrContrast;
import com.javaweb.admin.query.*;
import com.javaweb.admin.service.IYgOrderService;
import com.javaweb.common.utils.DBUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/ygorder")
public class YgOrderController {
    @Autowired
    private IYgOrderService ygOrderService;

    @GetMapping("/index")
    public JsonResult index(YgOrderQuery query) {
        return ygOrderService.GetYgOrderList(query);
    }

    @GetMapping("/ncinvoice")
    public JsonResult ncinvoice(NCInvoiceQuery query) {
        return ygOrderService.GetNCInvoiceList(query);
    }
    /*
    * 根据销售发票号获取发票信息
    * */
    @GetMapping("/ncsaleinvoice")
    public JsonResult ncsaleinvoice(NCInvoiceQuery query) {
        return ygOrderService.GetNCSaleInvoice(query.getInvoiceNo());
    }

    @GetMapping("/nc56invoice")
    public JsonResult nc56invoice(NCInvoiceQuery query) {
        return ygOrderService.GetNC56InvoiceList(query);
    }

    @GetMapping("/ncdispatching")
    public JsonResult ncdispatching(NCDispatchingQuery query) {
        return ygOrderService.GetNCDispatchingList(query);
    }

    @GetMapping("/converrate")
    public JsonResult converrate(YConverRateQuery query) {
        return ygOrderService.GetYConverRateList(query);
    }

    @GetMapping("/ygptaddr")
    public JsonResult ygptaddr(YgptAddrContrastQuery query) {
        return ygOrderService.GetYgptAddrContrastList(query);
    }

    @GetMapping("/ncinvlist")
    public JsonResult ncinvlist(NCInvQuery query) {
        return ygOrderService.GetNCInvList(query);
    }

    @GetMapping("/nc56invlist")
    public JsonResult nc56invlist(NCInvQuery query) {
        return ygOrderService.GetNC56InvList(query);
    }


    @GetMapping("/ncinvinfolist")
    public JsonResult ncinvinfolist(NCInvQuery query) {
        return ygOrderService.GetNCInvInfoList(query);
    }

    @GetMapping("/nccustinfolist")
    public JsonResult nccustinfolist(NCCustQuery query) {
        return ygOrderService.GetNCCustInfoList(query);
    }

    @GetMapping("/nccustaddrlist")
    public JsonResult nccustaddrlist(NCCustQuery query) {
        return ygOrderService.GetNCCustAddrList(query);
    }

    @GetMapping("/nccustlist")
    public JsonResult nccustlist(NCCustQuery query) {
        return ygOrderService.GetNCCustList(query);
    }

    @GetMapping("/nc56custlist")
    public JsonResult nc56custlist(NCCustQuery query) {
        return ygOrderService.GetNC56CustList(query);
    }

    @GetMapping("/dispatchdetail")
    public JsonResult dispatchdetail(NCDispatchingQuery query) {
        return ygOrderService.GetDispatchDetail(query.getPsdh());
    }

    @PostMapping("/yq003")
    public  JsonResult yq003()
    {
        ClassPathResource classPathResource = new ClassPathResource("nc/" + "YQ003.xml");
        InputStream inputStream = classPathResource.getStream();
        //File yq003 = classPathResource.getFile();
        Document document= XmlUtil.readXML(inputStream);
        Element elementG=XmlUtil.getRootElement(document);
        Element element =XmlUtil.getElement(elementG,"HEAD");
        NodeList YsPsdHead = element.getElementsByTagName("IP");
        String ip = NetUtil.getLocalhostStr();
        String mac = "";
        try {
            mac = NetUtil.getMacAddress(InetAddress.getLocalHost());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        YsPsdHead.item(0).setTextContent(ip);
        NodeList YsPsdHead1 = element.getElementsByTagName("MAC");
        YsPsdHead1.item(0).setTextContent(mac);
        NodeList YsPsdHead2 = element.getElementsByTagName("BZXX");
        YsPsdHead2.item(0).setTextContent("");
        List list = new ArrayList();
        try {
            list = DBUtils.convertList(DBUtils.GetResultSet("  select * from v_lyskl_prm_tarifflist where custcode ='20276487' and dr=0  and invcode = '180174002004'"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        return JsonResult.success(list);
    }

    @PostMapping("/saleordernc63")
    public  JsonResult saleordernc63(@RequestBody YgOrderDto dto)
    {
        return ygOrderService.ExportToNC(dto);
    }

    @PostMapping("/orderresponse")
    public  JsonResult orderresponse(@RequestBody YgOrderDto dto)
    {
        return ygOrderService.OrderResponseToYGPT(dto);
    }

    @PostMapping("/uploadygpt")
    public  JsonResult uploadygpt(@RequestBody YgOrderDto dto)
    {
        return ygOrderService.ExportToYGPT(dto);
    }

    @PostMapping("/uploadygptpsd")
    public  JsonResult uploadygptpsd(@RequestBody YgOrderDto dto)
    {
        return ygOrderService.ExportPSDToYGPT(dto);
    }

    @PostMapping("/yq029")
    public  JsonResult yq029(@RequestBody YgOrderDto dto)
    {
        return ygOrderService.YQ029ToYGPT(dto);
    }

    @PostMapping("/yq030")
    public  JsonResult yq030(@RequestBody YgOrderDto dto)
    {
        return ygOrderService.YQ030ToYGPT(dto);
    }

    @PostMapping("/yq031")
    public  JsonResult yq031(@RequestBody YgOrderDto dto)
    {
        return ygOrderService.YQ031ToYGPT(dto);
    }

    @PostMapping("/yq032")
    public  JsonResult yq032(@RequestBody YgOrderDto dto)
    {
        return ygOrderService.YQ032ToYGPT(dto);
    }

    @PostMapping("/confirmygptpsd")
    public  JsonResult confirmygptpsd(@RequestBody YgOrderDto dto)
    {
        if(dto.getUploadPsd()){
        return ygOrderService.PsdConfirmToYGPT(dto);
        }
        else if(dto.getUploadFp()){
            return ygOrderService.FpConfirmToYGPT(dto);
        }
        else
        {
            return JsonResult.error("非配送单/发票确认操作");
        }
    }


    @PostMapping("/updaterate")
    public  JsonResult updaterate(@RequestBody YConverRate entity)
    {
        return ygOrderService.UpdateInvCode(entity);
    }

    @PostMapping("/updatecustaddr")
    public  JsonResult updatecustaddr(@RequestBody YgptAddrContrast entity)
    {
        return ygOrderService.UpdateCustAddr(entity);
    }
}
