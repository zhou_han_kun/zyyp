package com.javaweb.admin.controller;

import com.alibaba.fastjson.JSONObject;
import com.javaweb.admin.query.SySyxxQuery;
import com.javaweb.admin.service.ISySyxxService;
import com.javaweb.common.common.BaseController;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.utils.JsonResult;
import com.shyp.daodikeji.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.TreeMap;


@RestController
@RequestMapping("/shyp")
public class SHYPController extends BaseController {

    @Autowired
    private ISySyxxService iSySyxxService;
    @GetMapping("/index")
    public JsonResult index(@RequestBody SySyxxQuery query) {
        try {
            return iSySyxxService.SaveSyxx(query);
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResult.error(e.getMessage());
        }


    }

    @PostMapping("/zyyptrk")
    public JsonResult zyyptrk(@RequestBody String[] query) {
        try {
            return iSySyxxService.ZyyptYprk(query);
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResult.error(e.getMessage());
        }


    }
    @PostMapping("/zyyptck")
    public JsonResult zyyptck(@RequestBody String[] query) {
        try {
            return iSySyxxService.ZyyptYpck(query);
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResult.error(e.getMessage());
        }


    }

    @PostMapping("zsmzf")
    public JsonResult zsmzf(@RequestBody String traceCode) {
        try {
            return iSySyxxService.ZyyptZsmzf(traceCode);
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResult.error(e.getMessage());
        }


    }


}
