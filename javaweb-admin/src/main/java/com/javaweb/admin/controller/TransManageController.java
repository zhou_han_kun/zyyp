package com.javaweb.admin.controller;

import com.javaweb.admin.entity.MCarInfo;
import com.javaweb.admin.entity.MDriverInfo;
import com.javaweb.admin.entity.NCCustAddrDoc;
import com.javaweb.admin.query.*;
import com.javaweb.admin.service.ITransManageService;
import com.javaweb.common.common.BaseController;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transmanage")
public class TransManageController extends BaseController {
    @Autowired
    ITransManageService transManageService;

    /**
     * 获取运输人员列表
     * @param query
     * @return
     */
    @GetMapping("/transuser")
    public JsonResult transuser(TransUserQuery query) {
        return transManageService.getTransUserList(query);
    }

    /**
     * 更新运输人员信息
     * @param entity
     * @return
     */
    @PostMapping("/savetransuser")
    public JsonResult savetransuser(@RequestBody MDriverInfo entity) {
        return transManageService.saveTransUser(entity);
    }

    /**
     * 获取车辆信息列表
     * @param query
     * @return
     */
    @GetMapping("/carinfo")
    public JsonResult carinfo(MCarInfoQuery query) {
        return transManageService.getCarInfoList(query);
    }

    /**
     * 更新运输车辆信息
     * @param entity
     * @return
     */
    @PostMapping("/savecarinfo")
    public JsonResult savecarinfo(@RequestBody MCarInfo entity) {
        return transManageService.saveCarInfo(entity);
    }

    /**
     * 获取客户地址信息列表
     * @param query
     * @return
     */
    @GetMapping("/custaddr")
    public JsonResult custaddr(NCCustAddrDocQuery query) {
        return transManageService.getCustAddrList(query);
    }

    /**
     * 更新客户地址信息
     * @param entity
     * @return
     */
    @PostMapping("/savecustaddr")
    public JsonResult savecustaddr(@RequestBody NCCustAddrDoc entity) {
        return transManageService.saveCustAddr(entity);
    }

    @GetMapping("/gpsdata")
    public JsonResult gpsdata(GpsDataQuery query) {
        return  transManageService.getGpsDataList(query);
    }

    /**
     * 获取运输车辆行驶轨迹
     * @param query
     * @return
     */
    @GetMapping("/transcoordinate")
    public JsonResult transcoordinate(TransCoordinatesQuery query) {
        return  transManageService.getTransCoordinates(query);
    }

    /**
     * 车辆温度获取
     * @param query
     * @return
     */
    @GetMapping("/tempdata")
    public JsonResult tempdata(TempdataQuery query) {
        return transManageService.getTempdataList(query);
    }
    @GetMapping("/recordlist")
    public JsonResult recordlist(TransRecordListQuery query){
        return transManageService.getRecordList(query);
    }
    @GetMapping("/detail")
    public JsonResult detail(RecordListDetailQuery query){
        return transManageService.getDetail(query);
    }
    @GetMapping("/recordlistTempdata")
    public JsonResult recordlistTempdata(RecordListTempdataQuery query){
        return transManageService.getTempdata1and2List(query);
    }

    /**
     * 地图查询下订单查询
     * @param query
     * @return
     */
    @GetMapping("/OrderForGoods")
    public JsonResult OrderForGoods(OrderForGoodsQuery query){
        return transManageService.getOrderForGoods(query);
    }
    @GetMapping("/smsevaluate")
    public JsonResult smsevaluate(SmsevaluateQuery query){
        return transManageService.getSmsevaluate(query);
    }
    @GetMapping("/transdetail")
    public JsonResult transdetail(TransDetailQuery query){

        return transManageService.getTransDetail(query);

    }
    @PostMapping("/smsevaluate/reissue")
    public JsonResult smsevaluateReissue(@RequestBody SmsevaluateReissueQuery query){
        return transManageService.smsevaluateReissue(query);
    }
    @GetMapping("/custevaluate")
    public JsonResult custevaluate(CustevaluateQuery query){
        return transManageService.getCustevaluateList(query);
    };
    @PostMapping("/custevaluate/reissue")
    public JsonResult custevaluateReissue(@RequestBody CustevaluateReissueQuery query){
        return transManageService.custevaluateReissue(query);
    }
}
