package com.javaweb.admin.controller;

import com.alibaba.fastjson.JSON;
import com.javaweb.admin.entity.Example2;
import com.javaweb.admin.service.IExample2Service;
import com.javaweb.common.annotation.Log;
import com.javaweb.common.common.BaseController;
import com.javaweb.common.enums.LogType;
import com.javaweb.common.utils.JsonResult;
import com.sphchina.esb.webservice.ESBDto;
import com.sphchina.esb.webservice.ISendToReviewLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/esbservice")
public class ESBController extends BaseController {

    @Autowired
    private ISendToReviewLog sendToReviewLog;

    @Log(title = "", logType = LogType.INSERT)
    @PostMapping("/sendorder")
    public JsonResult sendorder(@RequestBody String body) {
        //String body = JSON.toJSONString(entity);
        return sendToReviewLog.callEsb(body,"E9BO01");
    }

    @PostMapping("/sendorder2")
    public JsonResult sendorder2(@RequestBody String body) {
        //String body = JSON.toJSONString(entity);
        return sendToReviewLog.callEsbKdl("","",body);
    }

    @Log(title = "", logType = LogType.INSERT)
    @PostMapping("/cgrk/{Djhs}")
    public JsonResult cgrk(@PathVariable("Djhs") String[] Djhs) {
        //String body = JSON.toJSONString(entity);
        return sendToReviewLog.callCGRK(Djhs,"E9BO01");
    }

    @Log(title = "", logType = LogType.INSERT)
    @PostMapping("/gjtc/{Djhs}")
    public JsonResult gjtc(@PathVariable("Djhs") String[] body) {
        //String body = JSON.toJSONString(entity);
        return sendToReviewLog.callGJTC(body,"E9BO01");
    }

    @Log(title = "", logType = LogType.INSERT)
    @PostMapping("/xsck/{Djhs}")
    public JsonResult xsck(@PathVariable("Djhs") String[] body) {
        //String body = JSON.toJSONString(entity);
        return sendToReviewLog.callXSCK(body,"E9BO01");
    }

    @Log(title = "", logType = LogType.INSERT)
    @PostMapping("/xsth/{Djhs}")
    public JsonResult xsth(@PathVariable("Djhs") String[] body) {
        //String body = JSON.toJSONString(entity);
        return sendToReviewLog.callXSTH(body,"E9BO01");
    }

}
