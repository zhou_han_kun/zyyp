package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class MCarInfoQuery extends BaseQuery {
    private String carid;
    private String carcode;
    private String type;
}
