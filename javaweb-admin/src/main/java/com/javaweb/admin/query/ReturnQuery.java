package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class ReturnQuery extends BaseQuery {
    private String vbillcode;
    private String riqiBegin;
    private String riqiEnd;
    private String custname;
}
