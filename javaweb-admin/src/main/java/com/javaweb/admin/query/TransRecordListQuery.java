package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class TransRecordListQuery extends BaseQuery {
    private String invcode; //产品
    private String custcode; //客户
    private String vlotno; //批号
    private String custaddr; //地址
    private String status; //状态
    private String cgoldtaxdmcodeid; //发票号
    private String carid; //车牌
    private String taskid; //任务单号
    private String carrier; //承运单位
    private String beginTime;
    private String endTime;
}
