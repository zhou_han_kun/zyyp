package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class CustevaluateQuery extends BaseQuery {
    /*送货月份*/
    private String dbilldate;
    /*发送状态*/
    private  String SelectedValue;
    /*关键词*/
    private  String txtKey;
    private String messagetime;
}
