package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class SpbcInvcodeQuery extends BaseQuery {
    private String invcode;
    private String gg;
    private String vmanufacturer;
    private String pid;
}
