package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class YQ018InvListQuery extends BaseQuery {
    private String invcode;
}
