package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class TransUserQuery extends BaseQuery {
    private String name;
    private String type;
    private String driverid;

}
