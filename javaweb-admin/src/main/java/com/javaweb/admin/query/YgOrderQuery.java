package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class YgOrderQuery extends BaseQuery {
    private String yymc;
    private String riqiBegin;
    private String riqiEnd;
    private String imperpflag; //导入标记
    private String jhdh;
    private String xyzt; //响应状态
    private String xylx; //响应类型
}
