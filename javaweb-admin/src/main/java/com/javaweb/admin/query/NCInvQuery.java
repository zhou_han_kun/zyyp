package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class NCInvQuery extends BaseQuery {
    private String invcode;
    private String invname;
    private String yybm;
    private String zxspbm;
}
