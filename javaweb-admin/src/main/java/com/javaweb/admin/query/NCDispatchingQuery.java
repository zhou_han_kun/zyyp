package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class NCDispatchingQuery extends BaseQuery {
    private String custCode;
    private String yybm;
    private String custName;
    private String yymc;
    private String invName;
    private String soucetype;
    private String importType;
    private String ygType;
    private String czlx;
    private String printflag;
    private String riqiBegin;
    private String riqiEnd;
    private String psdh;

}
