package com.javaweb.admin.query;

import com.javaweb.admin.entity.Smsevaluate;
import lombok.Data;

@Data
public class SmsevaluateReissueQuery {
    private Smsevaluate[] smsevaluates;
}
