// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

/**
 * <p>
 * 商品资料查询条件
 * </p>
 *
 * @author 鲲鹏
 * @since 2022-11-24
 */
@Data
public class C37InfJcSpzlQuery extends BaseQuery {
    /**
     * 商品编号
     */
    private String shangpNo;

    /**
     * 商品名称
     */
    private String chineseName;

    private String riqiBegin;
    private String riqiEnd;

    /**
     * 药品规格
     */
    private String yaopGuig;

    /**
     * 生产厂家
     */
    private String maker;

    /**
     * 产地
     */
    private String chandi;

    /**
     * 包装数量
     */
    private Integer baozNum;

    /**
     * 包装单位
     */
    private String baozDanw;

    /**
     * 批准文号
     */
    private String pizNo;

    /**
     * 是否活动
     */
    private String beactive;

    /**
     * 更新时间
     */
    private String gengxTime;

    /**
     * 剂型
     */
    private String jixing;

    /**
     * 客户单位内码
     */
    private String yezId;

    /**
     * 下传状态
     */
    private String zt;
}
