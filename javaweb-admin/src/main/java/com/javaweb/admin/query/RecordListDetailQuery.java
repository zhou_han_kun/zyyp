package com.javaweb.admin.query;

import com.javaweb.common.common.BaseEntity;
import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class RecordListDetailQuery extends BaseQuery {
    private String cgoldtaxdmcodeid;
}
