package com.javaweb.admin.query;

import com.javaweb.admin.entity.Custevaluate;
import lombok.Data;

@Data
public class CustevaluateReissueQuery {
    Custevaluate[] custevaluate;
}
