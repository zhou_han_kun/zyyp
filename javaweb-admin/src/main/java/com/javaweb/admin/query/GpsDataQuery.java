package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class GpsDataQuery extends BaseQuery {
    private String beginTime;
    private String endTime;
    private String devno;
    private Integer minuteInterval;

}
