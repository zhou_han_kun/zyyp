package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class SmsevaluateQuery extends BaseQuery {
    private String beginTime;
    private String endTime;
    private String txtCode;
    private String txtName;
    private String txtKey;


}
