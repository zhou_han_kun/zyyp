package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class SpbcSummaryQuery extends BaseQuery {
    private String custcode;
    private String invcode;
    private String vlotno;
    private List<String> bcyjList;
    private String begindate;
    private String enddate;
    private String pid;
    private String did;
    private BigDecimal nqtorigtaxprice;
    private String gpoarea;
    private List<String> custaddrList;
}
