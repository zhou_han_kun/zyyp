package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class RecordListTempdataQuery extends BaseQuery {
    private String beginTime;
    private String endTime;
    private String devname;
    private String hotdeviceid1;
    private String hotdeviceid2;

}
