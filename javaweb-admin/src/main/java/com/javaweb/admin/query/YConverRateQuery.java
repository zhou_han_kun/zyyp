package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class YConverRateQuery extends BaseQuery {
    private String zxspbm;
    private String cpm;
    private boolean nosetflag;

}
