package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class OrderForGoodsQuery extends BaseQuery {
    private String beginTime;
    private String endTime;
    private String[] devname;

}
