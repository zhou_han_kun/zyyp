package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class YgptAddrContrastQuery extends BaseQuery {
    private String yybm;
    private String yymc;
    private String psdbm;
    private String psdz;
    private boolean nosetflag;
}
