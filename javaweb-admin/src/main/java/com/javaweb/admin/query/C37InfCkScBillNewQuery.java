// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

/**
 * <p>
 * 销售出库/购进退出上传单表查询条件
 * </p>
 *
 * @author 鲲鹏
 * @since 2022-11-26
 */
@Data
public class C37InfCkScBillNewQuery extends BaseQuery {
    /**
     * 单据编号
     */
    private String danjNo;

    /**
     * 订单日期
     */
    private String riqiChar;
    private String riqiBegin;
    private String riqiEnd;

    /**
     * 开票日期
     */
    private String kaipTime;

    /**
     * 单位编号
     */
    private String danwNo;

    /**
     * 出库单据
     */
    private String chukNo;


    /**
     * 商品编号
     */
    private String shangpNo;

    /**
     * 批号
     */
    private String lot;

    /**
     * 退货类型
     */
    private String yewType;

    /**
     * 下传状态
     */
    private String zt;
}
