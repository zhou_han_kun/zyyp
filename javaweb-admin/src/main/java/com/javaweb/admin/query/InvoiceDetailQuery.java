package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class InvoiceDetailQuery extends BaseQuery {
    private String csaleid;
    private int num;
}
