package com.javaweb.admin.query;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class MDMQueryParam {

    private String tenant_key;
    private String nonce_str;
    private String sign;
    private String dataTypeIgnore;
    private String tenantIdIgnore;
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String starttimeIgnore;
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String endtimeIgnore;

    private String executionMonthIgnore;
    private String businessMonthIgnore;
    private String startMonthIgnore;
    private String endMonthIgnore;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date saleStartDateIgnore;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date saleEndDateIgnore;

    private String dataUpdateStartDateIgnore;
    private String dataUpdateEndDateIgnore;

    private int pageCurrentPage;
    private int pageSize;
    private String timestamp;
    private String cleanStatusIgnore;
    public void setTenant_key(String tenant_key) {
        this.tenant_key = tenant_key;
    }
    public String getTenant_key() {
        return tenant_key;
    }

    public void setNonce_str(String nonce_str) {
        this.nonce_str = nonce_str;
    }
    public String getNonce_str() {
        return nonce_str;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
    public String getSign() {
        return sign;
    }

    public void setTenantIdIgnore(String tenantId) {
        this.tenantIdIgnore = tenantId;
    }
    public String getTenantIdIgnore() {
        return tenantIdIgnore;
    }

    public void setStarttimeIgnore(String starttime) {
        this.starttimeIgnore = starttime;
    }
    public String getStarttimeIgnore() {
        return starttimeIgnore;
    }

    public void setEndtimeIgnore(String endtime) {
        this.endtimeIgnore = endtime;
    }
    public String getEndtimeIgnore() {
        return endtimeIgnore;
    }

    public void setPageCurrentPage(int currentPage) {
        this.pageCurrentPage = currentPage;
    }
    public int getPageCurrentPage() {
        return pageCurrentPage;
    }

    public void setPageSize(int size) {
        this.pageSize = size;
    }
    public int getPageSize() {
        return pageSize;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    public String getTimestamp() {
        return timestamp;
    }

    public String getCleanStatusIgnore() {
        return cleanStatusIgnore;
    }

    public void setCleanStatusIgnore(String cleanStatus) {
        this.cleanStatusIgnore = cleanStatus;
    }

    public String getDataTypeIgnore() {
        return dataTypeIgnore;
    }

    public void setDataTypeIgnore(String dataType) {
        this.dataTypeIgnore = dataType;
    }

    public String getExecutionMonthIgnore() {
        return executionMonthIgnore;
    }

    public void setExecutionMonthIgnore(String executionMonth) {
        this.executionMonthIgnore = executionMonth;
    }

    public String getBusinessMonthIgnore() {
        return businessMonthIgnore;
    }

    public void setBusinessMonthIgnore(String businessMonth) {
        this.businessMonthIgnore = businessMonth;
    }

    public Date getSaleStartDateIgnore() {
        return saleStartDateIgnore;
    }

    public void setSaleStartDateIgnore(Date saleStartDate) {
        this.saleStartDateIgnore = saleStartDate;
    }

    public Date getSaleEndDateIgnore() {
        return saleEndDateIgnore;
    }

    public void setSaleEndDateIgnore(Date saleEndDate) {
        this.saleEndDateIgnore = saleEndDate;
    }

    public String getDataUpdateStartDateIgnore() {
        return dataUpdateStartDateIgnore;
    }

    public void setDataUpdateStartDateIgnore(String dataUpdateStartDate) {
        this.dataUpdateStartDateIgnore = dataUpdateStartDate;
    }

    public String getDataUpdateEndDateIgnore() {
        return dataUpdateEndDateIgnore;
    }

    public void setDataUpdateEndDateIgnore(String dataUpdateEndDate) {
        this.dataUpdateEndDateIgnore = dataUpdateEndDate;
    }

    public String getStartMonthIgnore() {
        return startMonthIgnore;
    }

    public void setStartMonthIgnore(String startMonthIgnore) {
        this.startMonthIgnore = startMonthIgnore;
    }

    public String getEndMonthIgnore() {
        return endMonthIgnore;
    }

    public void setEndMonthIgnore(String endMonthIgnore) {
        this.endMonthIgnore = endMonthIgnore;
    }
}
