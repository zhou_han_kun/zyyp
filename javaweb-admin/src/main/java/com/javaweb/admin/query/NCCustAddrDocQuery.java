package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class NCCustAddrDocQuery extends BaseQuery {
    private String custcode;
    private String custname;
    private String keyword;

}
