package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class NCInvoiceQuery extends BaseQuery {
    private String invoiceNo;
    private String pruchfph;
    private String custCode;
    private String custName;
    private String invName;
    private String soucetype;
    private String importType;
    private String ygType;
    private String czlx;
    private String printflag;
    private String riqiBegin;
    private String riqiEnd;
}
