// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.admin.vo.c37infckscbillnew.C37InfCkScBillNewListVo;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.constant.C37InfRkCgddBillConstant;
import com.javaweb.admin.entity.C37InfRkCgddBill;
import com.javaweb.admin.mapper.C37InfRkCgddBillMapper;
import com.javaweb.admin.query.C37InfRkCgddBillQuery;
import com.javaweb.admin.service.IC37InfRkCgddBillService;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.c37infrkcgddbill.C37InfRkCgddBillInfoVo;
import com.javaweb.admin.vo.c37infrkcgddbill.C37InfRkCgddBillListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  * 采购订单 服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2022-11-26
  */
@Service
public class C37InfRkCgddBillServiceImpl extends BaseServiceImpl<C37InfRkCgddBillMapper, C37InfRkCgddBill> implements IC37InfRkCgddBillService {

    @Autowired
    private C37InfRkCgddBillMapper c37InfRkCgddBillMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult getList(BaseQuery query) {
        C37InfRkCgddBillQuery c37InfRkCgddBillQuery = (C37InfRkCgddBillQuery) query;
        // 查询条件
        QueryWrapper<C37InfRkCgddBill> queryWrapper = new QueryWrapper<>();
//        queryWrapper.select( "danj_no","riqi_char","danw_no","lianx_staff","lianxr_phone","caigou_staff","hanghao","shangp_no","lot","shengc_char","youx_char","num","price","keh_notes","zt","kh_zzbm","yez_id");
//        if(!StringUtils.isEmpty(c37InfRkCgddBillQuery.getDanwNo()))
//        {
//            queryWrapper.like("danw_no",c37InfRkCgddBillQuery.getDanwNo());
//        }
//        if(!StringUtils.isEmpty(c37InfRkCgddBillQuery.getDanjNo()))
//        {
//            queryWrapper.like("danj_no",c37InfRkCgddBillQuery.getDanjNo());
//        }
//        if(!StringUtils.isEmpty(c37InfRkCgddBillQuery.getLot()))
//        {
//            queryWrapper.like("lot",c37InfRkCgddBillQuery.getLot());
//        }
//        if(!StringUtils.isEmpty(c37InfRkCgddBillQuery.getShangpNo()))
//        {
//            queryWrapper.like("shangp_no",c37InfRkCgddBillQuery.getShangpNo());
//        }
//        if(!StringUtils.isEmpty(c37InfRkCgddBillQuery.getZt()))
//        {
//            queryWrapper.eq("zt",c37InfRkCgddBillQuery.getZt());
//        }
        // queryWrapper.orderByDesc("id");

        // 获取数据列表
        IPage<C37InfRkCgddBill> page = new Page<>(c37InfRkCgddBillQuery.getPage(), c37InfRkCgddBillQuery.getLimit());
        IPage<C37InfRkCgddBill> pageData = c37InfRkCgddBillMapper.selectPage(page, queryWrapper);
        return JsonResult.success(pageData);
    }

    /**
     * 获取采购订单列表
     * */
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult GetCgddList(BaseQuery query) {
        C37InfRkCgddBillQuery c37InfRkCgddBillQuery = (C37InfRkCgddBillQuery) query;
        // 查询条件
//        QueryWrapper<C37InfRkCgddBill> queryWrapper = new QueryWrapper<>();
//        queryWrapper.select( "danj_no","riqi_char","danw_no","lianx_staff","lianxr_phone","caigou_staff","hanghao","shangp_no","lot","shengc_char","youx_char","num","price","keh_notes","zt","kh_zzbm","yez_id");
//        if(!StringUtils.isEmpty(c37InfRkCgddBillQuery.getDanwNo()))
//        {
//            queryWrapper.like("danw_no",c37InfRkCgddBillQuery.getDanwNo());
//        }
//        if(!StringUtils.isEmpty(c37InfRkCgddBillQuery.getDanjNo()))
//        {
//            queryWrapper.like("danj_no",c37InfRkCgddBillQuery.getDanjNo());
//        }
//        if(!StringUtils.isEmpty(c37InfRkCgddBillQuery.getLot()))
//        {
//            queryWrapper.like("lot",c37InfRkCgddBillQuery.getLot());
//        }
//        if(!StringUtils.isEmpty(c37InfRkCgddBillQuery.getShangpNo()))
//        {
//            queryWrapper.like("shangp_no",c37InfRkCgddBillQuery.getShangpNo());
//        }
//        if(!StringUtils.isEmpty(c37InfRkCgddBillQuery.getZt()))
//        {
//            queryWrapper.eq("zt",c37InfRkCgddBillQuery.getZt());
//        }
        // queryWrapper.orderByDesc("id");

        // 获取数据列表
        if(c37InfRkCgddBillQuery.getPage()==null)
        {
            c37InfRkCgddBillQuery.setPage(1);
            c37InfRkCgddBillQuery.setLimit(100000);
        }
        IPage<C37InfRkCgddBillListVo> page = new Page<>(c37InfRkCgddBillQuery.getPage(), c37InfRkCgddBillQuery.getLimit());
        IPage<C37InfRkCgddBillListVo> pageData = c37InfRkCgddBillMapper.GetCgddList(page, c37InfRkCgddBillQuery);
//        pageData.convert(x -> {
//            C37InfRkCgddBillListVo c37InfRkCgddBillListVo = Convert.convert(C37InfRkCgddBillListVo.class, x);
//            return c37InfRkCgddBillListVo;
//        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取采购订单明细
     * */
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult GetCgddDetailList(BaseQuery query) {
        C37InfRkCgddBillQuery c37InfRkCgddBillQuery = (C37InfRkCgddBillQuery) query;

        if(c37InfRkCgddBillQuery.getPage()==null)
        {
            c37InfRkCgddBillQuery.setPage(1);
            c37InfRkCgddBillQuery.setLimit(100000);
        }
        // 获取数据列表
        IPage<C37InfRkCgddBillListVo> page = new Page<>(c37InfRkCgddBillQuery.getPage(), c37InfRkCgddBillQuery.getLimit());
        IPage<C37InfRkCgddBillListVo> pageData = c37InfRkCgddBillMapper.GetCgddDetailList(page, c37InfRkCgddBillQuery);
//        pageData.convert(x -> {
//            C37InfRkCgddBillListVo c37InfRkCgddBillListVo = Convert.convert(C37InfRkCgddBillListVo.class, x);
//            return c37InfRkCgddBillListVo;
//        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        C37InfRkCgddBill entity = (C37InfRkCgddBill) super.getInfo(id);
        // 返回视图Vo
        C37InfRkCgddBillInfoVo c37InfRkCgddBillInfoVo = new C37InfRkCgddBillInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, c37InfRkCgddBillInfoVo);
        return c37InfRkCgddBillInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(C37InfRkCgddBill entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
        } else {
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(C37InfRkCgddBill entity) {
        entity.setMark(0);
        return super.delete(entity);
    }

}