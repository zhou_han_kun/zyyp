// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.constant.C37InfDsfRkzyjlConstant;
import com.javaweb.admin.entity.C37InfDsfRkzyjl;
import com.javaweb.admin.mapper.C37InfDsfRkzyjlMapper;
import com.javaweb.admin.query.C37InfDsfRkzyjlQuery;
import com.javaweb.admin.service.IC37InfDsfRkzyjlService;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.c37infdsfrkzyjl.C37InfDsfRkzyjlInfoVo;
import com.javaweb.admin.vo.c37infdsfrkzyjl.C37InfDsfRkzyjlListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  *  服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2023-03-23
  */
@Service
public class C37InfDsfRkzyjlServiceImpl extends BaseServiceImpl<C37InfDsfRkzyjlMapper, C37InfDsfRkzyjl> implements IC37InfDsfRkzyjlService {

    @Autowired
    private C37InfDsfRkzyjlMapper c37InfDsfRkzyjlMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult getList(BaseQuery query) {
        C37InfDsfRkzyjlQuery c37InfDsfRkzyjlQuery = (C37InfDsfRkzyjlQuery) query;
        // 查询条件
        QueryWrapper<C37InfDsfRkzyjl> queryWrapper = new QueryWrapper<>();
//        List<String> excludeFields = new ArrayList<>();
//        excludeFields.add("id");
//        queryWrapper.lambda().select(C37InfDsfRkzyjl.class, i -> !excludeFields.contains(i.getProperty()));
        queryWrapper.select("chandi,caig_staff,num,price,yez_no,cunc_condition,zhongy_haitat,piz_no,yans_temp,shengchan_date,jily_type,baoz_danw,youx_date,yew_type,hanghao,danw_name,chul_fanga,shij_num,riqi_date,yewdj_no,danj_no,shij_js,qiy_date,scrq,zt,shangp_no,bhg_num,baoz_num,jixing,danw_no,jushou_reason,lot,yuns_danw,fay_didian,amount,yuns_type,chep_no,maker,shij_lss,xians_loc,yez_id,kh_zzbm,yaop_guig,shouh_staff,yans_rlt,chinese_name,wenk_way,ssxkcyr,zhij_staff");
        // queryWrapper.orderByDesc("id");
        if(!StringUtils.isEmpty(c37InfDsfRkzyjlQuery.getDanjNo()))
        {
            queryWrapper.like("danj_no",c37InfDsfRkzyjlQuery.getDanjNo());
        }
        if(!StringUtils.isEmpty(c37InfDsfRkzyjlQuery.getChineseName()))
        {
            queryWrapper.like("chinese_name",c37InfDsfRkzyjlQuery.getChineseName());
        }
        if(!StringUtils.isEmpty(c37InfDsfRkzyjlQuery.getLot()))
        {
            queryWrapper.like("lot",c37InfDsfRkzyjlQuery.getLot());
        }
        if(!StringUtils.isEmpty(c37InfDsfRkzyjlQuery.getRiqiBegin()))
        {
            queryWrapper.ge("riqi_date",c37InfDsfRkzyjlQuery.getRiqiBegin());
        }
        if(!StringUtils.isEmpty(c37InfDsfRkzyjlQuery.getRiqiEnd()))
        {
            queryWrapper.le("riqi_date",c37InfDsfRkzyjlQuery.getRiqiEnd());
        }
        queryWrapper.orderByDesc("riqi_date");
        // 获取数据列表
        IPage<C37InfDsfRkzyjl> page = new Page<>(c37InfDsfRkzyjlQuery.getPage(), c37InfDsfRkzyjlQuery.getLimit());
        IPage<C37InfDsfRkzyjl> pageData = c37InfDsfRkzyjlMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            C37InfDsfRkzyjlListVo c37InfDsfRkzyjlListVo = Convert.convert(C37InfDsfRkzyjlListVo.class, x);
            return c37InfDsfRkzyjlListVo;
        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        C37InfDsfRkzyjl entity = (C37InfDsfRkzyjl) super.getInfo(id);
        // 返回视图Vo
        C37InfDsfRkzyjlInfoVo c37InfDsfRkzyjlInfoVo = new C37InfDsfRkzyjlInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, c37InfDsfRkzyjlInfoVo);
        return c37InfDsfRkzyjlInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(C37InfDsfRkzyjl entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
        } else {
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(C37InfDsfRkzyjl entity) {
        entity.setMark(0);
        return super.delete(entity);
    }

}