// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.admin.entity.C37InfJcZhiydoc;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.constant.C37InfPsZcdConstant;
import com.javaweb.admin.entity.C37InfPsZcd;
import com.javaweb.admin.mapper.C37InfPsZcdMapper;
import com.javaweb.admin.query.C37InfPsZcdQuery;
import com.javaweb.admin.service.IC37InfPsZcdService;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.c37infpszcd.C37InfPsZcdInfoVo;
import com.javaweb.admin.vo.c37infpszcd.C37InfPsZcdListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  * 运输记录 服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2022-11-26
  */
@Service
public class C37InfPsZcdServiceImpl extends BaseServiceImpl<C37InfPsZcdMapper, C37InfPsZcd> implements IC37InfPsZcdService {

    @Autowired
    private C37InfPsZcdMapper c37InfPsZcdMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult getList(BaseQuery query) {
        C37InfPsZcdQuery c37InfPsZcdQuery = (C37InfPsZcdQuery) query;
        // 查询条件
        QueryWrapper<C37InfPsZcd> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("yez_id","danj_no","hanghao","fac_time","fah_add","danw_no","danw_name","diz_phone","ping_num","zhengx_number","pingx_num","yuns_way","jingbr","tygs","chepai_no","scrq","zt","kh_zzbm");
        if(!StringUtils.isEmpty(c37InfPsZcdQuery.getDanwNo()))
        {
            queryWrapper.like("danw_no",c37InfPsZcdQuery.getDanwNo());
        }
        if(!StringUtils.isEmpty(c37InfPsZcdQuery.getRiqiBegin()))
        {
            queryWrapper.ge("fac_time",c37InfPsZcdQuery.getRiqiBegin());
        }
        if(!StringUtils.isEmpty(c37InfPsZcdQuery.getRiqiEnd()))
        {
            queryWrapper.le("fac_time",c37InfPsZcdQuery.getRiqiEnd());
        }
        if(!StringUtils.isEmpty(c37InfPsZcdQuery.getScrqBegin()))
        {
            queryWrapper.ge("scrq",c37InfPsZcdQuery.getScrqBegin());
        }
        if(!StringUtils.isEmpty(c37InfPsZcdQuery.getScrqEnd()))
        {
            queryWrapper.le("scrq",c37InfPsZcdQuery.getScrqEnd());
        }

        if(!StringUtils.isEmpty(c37InfPsZcdQuery.getDanjNo()))
        {
            queryWrapper.like("danj_no",c37InfPsZcdQuery.getDanjNo());
        }
        if(!StringUtils.isEmpty(c37InfPsZcdQuery.getDanwName()))
        {
            queryWrapper.like("danw_name",c37InfPsZcdQuery.getDanwName());
        }
        if(!StringUtils.isEmpty(c37InfPsZcdQuery.getChepaiNo()))
        {
            queryWrapper.like("chepai_no",c37InfPsZcdQuery.getChepaiNo());
        }
        if(!StringUtils.isEmpty(c37InfPsZcdQuery.getTygs()))
        {
            queryWrapper.like("tygs",c37InfPsZcdQuery.getTygs());
        }
        if(!StringUtils.isEmpty(c37InfPsZcdQuery.getYunsWay()))
        {
            queryWrapper.like("yuns_way",c37InfPsZcdQuery.getYunsWay());
        }
        if(!StringUtils.isEmpty(c37InfPsZcdQuery.getZt()))
        {
            queryWrapper.eq("zt",c37InfPsZcdQuery.getZt());
        }
        // queryWrapper.orderByDesc("id");

        // 获取数据列表
        IPage<C37InfPsZcd> page = new Page<>(c37InfPsZcdQuery.getPage(), c37InfPsZcdQuery.getLimit());
        IPage<C37InfPsZcd> pageData = c37InfPsZcdMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            C37InfPsZcdListVo c37InfPsZcdListVo = Convert.convert(C37InfPsZcdListVo.class, x);
            return c37InfPsZcdListVo;
        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        C37InfPsZcd entity = (C37InfPsZcd) super.getInfo(id);
        // 返回视图Vo
        C37InfPsZcdInfoVo c37InfPsZcdInfoVo = new C37InfPsZcdInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, c37InfPsZcdInfoVo);
        return c37InfPsZcdInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(C37InfPsZcd entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
        } else {
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(C37InfPsZcd entity) {
        entity.setMark(0);
        return super.delete(entity);
    }

}