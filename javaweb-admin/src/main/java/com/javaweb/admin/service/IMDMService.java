package com.javaweb.admin.service;

import com.javaweb.admin.query.MDMQueryParam;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.utils.JsonResult;

public interface IMDMService {
    JsonResult SaveMaterial(MDMQueryParam query);
    JsonResult SaveCustomer(MDMQueryParam query);
    JsonResult SaveNCSaleFlow(MDMQueryParam query);
    JsonResult SaveEmployee(MDMQueryParam query);
    JsonResult SaveCleanSales(MDMQueryParam query);
    JsonResult SaveCleanInventory(MDMQueryParam query);
    JsonResult SaveMonthTarget(MDMQueryParam query);

    JsonResult SaveExpert(MDMQueryParam query);
}
