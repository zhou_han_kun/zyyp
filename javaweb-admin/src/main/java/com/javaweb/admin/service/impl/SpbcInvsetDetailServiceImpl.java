// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.constant.SpbcInvsetDetailConstant;
import com.javaweb.admin.entity.SpbcInvsetDetail;
import com.javaweb.admin.mapper.SpbcInvsetDetailMapper;
import com.javaweb.admin.query.SpbcInvsetDetailQuery;
import com.javaweb.admin.service.ISpbcInvsetDetailService;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.spbcinvsetdetail.SpbcInvsetDetailInfoVo;
import com.javaweb.admin.vo.spbcinvsetdetail.SpbcInvsetDetailListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  *  服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2023-12-04
  */
@Service
public class SpbcInvsetDetailServiceImpl extends BaseServiceImpl<SpbcInvsetDetailMapper, SpbcInvsetDetail> implements ISpbcInvsetDetailService {

    @Autowired
    private SpbcInvsetDetailMapper spbcInvsetDetailMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        SpbcInvsetDetailQuery spbcInvsetDetailQuery = (SpbcInvsetDetailQuery) query;
        // 查询条件
        QueryWrapper<SpbcInvsetDetail> queryWrapper = new QueryWrapper<>();
        // queryWrapper.orderByDesc("id");

        // 获取数据列表
        IPage<SpbcInvsetDetail> page = new Page<>(spbcInvsetDetailQuery.getPage(), spbcInvsetDetailQuery.getLimit());
        IPage<SpbcInvsetDetail> pageData = spbcInvsetDetailMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            SpbcInvsetDetailListVo spbcInvsetDetailListVo = Convert.convert(SpbcInvsetDetailListVo.class, x);
            return spbcInvsetDetailListVo;
        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        SpbcInvsetDetail entity = (SpbcInvsetDetail) super.getInfo(id);
        // 返回视图Vo
        SpbcInvsetDetailInfoVo spbcInvsetDetailInfoVo = new SpbcInvsetDetailInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, spbcInvsetDetailInfoVo);
        return spbcInvsetDetailInfoVo;
    }

    @Override
    public JsonResult saveInvcode(SpbcInvsetDetail[] invsetDetails) {
        try {
            for (SpbcInvsetDetail entity : invsetDetails) {
                if(spbcInvsetDetailMapper.getInvcodeNum(entity.getPid(), entity.getCmaterialvid())>0){
                    return JsonResult.error("该药品编码已存在，请勿重复添加");
                }else {
                    entity.setCreateUser(ShiroUtils.getUserId());
                    entity.setCreateTime(DateUtils.now());
                    this.add(entity);
                }

            }
            return JsonResult.success();
        }
        catch(Exception ex)
        {
            return JsonResult.error(ex.getMessage());
        }
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(SpbcInvsetDetail entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
            entity.setUpdateUser(ShiroUtils.getUserId());
            entity.setUpdateTime(DateUtils.now());
        } else {
            entity.setCreateUser(ShiroUtils.getUserId());
            entity.setCreateTime(DateUtils.now());
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(SpbcInvsetDetail entity) {
        entity.setUpdateUser(1);
        entity.setUpdateTime(DateUtils.now());
        entity.setMark(0);
        return super.delete(entity);
    }

    @Override
    public JsonResult deleteByIds(Integer[] spbcinvsetdetailIds) {
        try {
            for(Integer id : spbcinvsetdetailIds) {
                spbcInvsetDetailMapper.deleteById(id,ShiroUtils.getUserId());
            }
            return JsonResult.success();
        }
        catch(Exception ex)
        {
            return JsonResult.error(ex.getMessage());
        }
    }

    @Override
    public void deleteSpbcInvsetDetailByPid(Integer pid){
        spbcInvsetDetailMapper.deleteSpbcInvsetDetailByPid(pid, ShiroUtils.getUserId());
    }

    @Override
    public JsonResult saveBalanceAmount(SpbcInvsetDetail[] invsetDetails) {
        try {
            for (SpbcInvsetDetail entity : invsetDetails) {
                UpdateWrapper<SpbcInvsetDetail> updateWrapper = new UpdateWrapper<>();
                updateWrapper.set("balanceamountback", entity.getBalanceamountback()).eq("id", entity.getId());
                spbcInvsetDetailMapper.update(new SpbcInvsetDetail(), updateWrapper);
            }
            return JsonResult.success();
        }
        catch(Exception ex)
        {
            return JsonResult.error(ex.getMessage());
        }
    }

    @Override
    public JsonResult saveNowSalePrice(SpbcInvsetDetail[] invsetDetails) {
        try {
            for (SpbcInvsetDetail entity : invsetDetails) {
                UpdateWrapper<SpbcInvsetDetail> updateWrapper = new UpdateWrapper<>();
                updateWrapper.set("nowsaleprice", entity.getNowsaleprice()).eq("id", entity.getId());
                spbcInvsetDetailMapper.update(new SpbcInvsetDetail(), updateWrapper);
            }
            return JsonResult.success();
        }
        catch(Exception ex)
        {
            return JsonResult.error(ex.getMessage());
        }
    }
}