// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.constant.InvoiceSendingManagementConstant;
import com.javaweb.admin.entity.InvoiceSendingManagement;
import com.javaweb.admin.mapper.InvoiceSendingManagementMapper;
import com.javaweb.admin.query.InvoiceSendingManagementQuery;
import com.javaweb.admin.service.IInvoiceSendingManagementService;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.invoicesendingmanagement.InvoiceSendingManagementInfoVo;
import com.javaweb.admin.vo.invoicesendingmanagement.InvoiceSendingManagementListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  *  服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2024-08-15
  */
@Service
public class InvoiceSendingManagementServiceImpl extends BaseServiceImpl<InvoiceSendingManagementMapper, InvoiceSendingManagement> implements IInvoiceSendingManagementService {

    @Autowired
    private InvoiceSendingManagementMapper invoiceSendingManagementMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        InvoiceSendingManagementQuery invoiceSendingManagementQuery = (InvoiceSendingManagementQuery) query;
        // 查询条件
        QueryWrapper<InvoiceSendingManagement> queryWrapper = new QueryWrapper<>();
        // queryWrapper.orderByDesc("id");

        // 获取数据列表
        IPage<InvoiceSendingManagement> page = new Page<>(invoiceSendingManagementQuery.getPage(), invoiceSendingManagementQuery.getLimit());
        IPage<InvoiceSendingManagement> pageData = invoiceSendingManagementMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            InvoiceSendingManagementListVo invoiceSendingManagementListVo = Convert.convert(InvoiceSendingManagementListVo.class, x);
            return invoiceSendingManagementListVo;
        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        InvoiceSendingManagement entity = (InvoiceSendingManagement) super.getInfo(id);
        // 返回视图Vo
        InvoiceSendingManagementInfoVo invoiceSendingManagementInfoVo = new InvoiceSendingManagementInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, invoiceSendingManagementInfoVo);
        return invoiceSendingManagementInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(InvoiceSendingManagement entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
            entity.setUpdateUser(ShiroUtils.getUserId());
            entity.setUpdateTime(DateUtils.now());
        } else {
            entity.setCreateUser(ShiroUtils.getUserId());
            entity.setCreateTime(DateUtils.now());
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(InvoiceSendingManagement entity) {
        entity.setUpdateUser(1);
        entity.setUpdateTime(DateUtils.now());
        entity.setMark(0);
        return super.delete(entity);
    }

}