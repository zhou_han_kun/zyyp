// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.admin.query.C37InfRkCgddBillQuery;
import com.javaweb.admin.vo.c37infrkcgddbill.C37InfRkCgddBillListVo;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.constant.C37InfCkGttzdBillConstant;
import com.javaweb.admin.entity.C37InfCkGttzdBill;
import com.javaweb.admin.mapper.C37InfCkGttzdBillMapper;
import com.javaweb.admin.query.C37InfCkGttzdBillQuery;
import com.javaweb.admin.service.IC37InfCkGttzdBillService;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.c37infckgttzdbill.C37InfCkGttzdBillInfoVo;
import com.javaweb.admin.vo.c37infckgttzdbill.C37InfCkGttzdBillListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  * 购进退出通知单表 服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2022-11-26
  */
@Service
public class C37InfCkGttzdBillServiceImpl extends BaseServiceImpl<C37InfCkGttzdBillMapper, C37InfCkGttzdBill> implements IC37InfCkGttzdBillService {

    @Autowired
    private C37InfCkGttzdBillMapper c37InfCkGttzdBillMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult getList(BaseQuery query) {
        C37InfCkGttzdBillQuery c37InfCkGttzdBillQuery = (C37InfCkGttzdBillQuery) query;
        // 查询条件
        QueryWrapper<C37InfCkGttzdBill> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("danj_no","riqi_char","kaip_time","danw_no","caigou_staff","tuih_staff","caoz_staff","notes","hanghao","shangp_no"
                ,"lot","num","price","tuih_catagory","tuih_reason","zt","kh_zzbm","yez_id");
        // queryWrapper.orderByDesc("id");
        if(!StringUtils.isEmpty(c37InfCkGttzdBillQuery.getDanwNo()))
        {
            queryWrapper.like("danw_no",c37InfCkGttzdBillQuery.getDanwNo());
        }
        if(!StringUtils.isEmpty(c37InfCkGttzdBillQuery.getDanjNo()))
        {
            queryWrapper.like("danj_no",c37InfCkGttzdBillQuery.getDanjNo());
        }
        if(!StringUtils.isEmpty(c37InfCkGttzdBillQuery.getCaigouStaff()))
        {
            queryWrapper.like("caigou_staff",c37InfCkGttzdBillQuery.getCaigouStaff());
        }
        if(!StringUtils.isEmpty(c37InfCkGttzdBillQuery.getShangpNo()))
        {
            queryWrapper.like("shangp_no",c37InfCkGttzdBillQuery.getShangpNo());
        }
        if(!StringUtils.isEmpty(c37InfCkGttzdBillQuery.getLot()))
        {
            queryWrapper.like("lot",c37InfCkGttzdBillQuery.getLot());
        }
        if(!StringUtils.isEmpty(c37InfCkGttzdBillQuery.getTuihCatagory()))
        {
            queryWrapper.like("tuih_catagory",c37InfCkGttzdBillQuery.getTuihCatagory());
        }
        if(!StringUtils.isEmpty(c37InfCkGttzdBillQuery.getZt()))
        {
            queryWrapper.eq("zt",c37InfCkGttzdBillQuery.getZt());
        }
        // 获取数据列表
        IPage<C37InfCkGttzdBill> page = new Page<>(c37InfCkGttzdBillQuery.getPage(), c37InfCkGttzdBillQuery.getLimit());
        IPage<C37InfCkGttzdBill> pageData = c37InfCkGttzdBillMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            C37InfCkGttzdBillListVo c37InfCkGttzdBillListVo = Convert.convert(C37InfCkGttzdBillListVo.class, x);
            return c37InfCkGttzdBillListVo;
        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取采购退货订单列表
     * */
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult GetCgddList(BaseQuery query) {
        C37InfCkGttzdBillQuery c37InfCkGttzdBillQuery = (C37InfCkGttzdBillQuery) query;

        // 获取数据列表
        if(c37InfCkGttzdBillQuery.getPage()==null)
        {
            c37InfCkGttzdBillQuery.setPage(1);
            c37InfCkGttzdBillQuery.setLimit(100000);
        }
        IPage<C37InfCkGttzdBillListVo> page = new Page<>(c37InfCkGttzdBillQuery.getPage(), c37InfCkGttzdBillQuery.getLimit());
        IPage<C37InfCkGttzdBillListVo> pageData = c37InfCkGttzdBillMapper.GetCgddList(page, c37InfCkGttzdBillQuery);
//        pageData.convert(x -> {
//            C37InfRkCgddBillListVo c37InfRkCgddBillListVo = Convert.convert(C37InfRkCgddBillListVo.class, x);
//            return c37InfRkCgddBillListVo;
//        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取采购订单明细
     * */
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult GetCgddDetailList(BaseQuery query) {
        C37InfCkGttzdBillQuery c37InfCkGttzdBillQuery = (C37InfCkGttzdBillQuery) query;

        if(c37InfCkGttzdBillQuery.getPage()==null)
        {
            c37InfCkGttzdBillQuery.setPage(1);
            c37InfCkGttzdBillQuery.setLimit(100000);
        }
        // 获取数据列表
        IPage<C37InfCkGttzdBillListVo> page = new Page<>(c37InfCkGttzdBillQuery.getPage(), c37InfCkGttzdBillQuery.getLimit());
        IPage<C37InfCkGttzdBillListVo> pageData = c37InfCkGttzdBillMapper.GetCgddDetailList(page, c37InfCkGttzdBillQuery);
//        pageData.convert(x -> {
//            C37InfRkCgddBillListVo c37InfRkCgddBillListVo = Convert.convert(C37InfRkCgddBillListVo.class, x);
//            return c37InfRkCgddBillListVo;
//        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        C37InfCkGttzdBill entity = (C37InfCkGttzdBill) super.getInfo(id);
        // 返回视图Vo
        C37InfCkGttzdBillInfoVo c37InfCkGttzdBillInfoVo = new C37InfCkGttzdBillInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, c37InfCkGttzdBillInfoVo);
        return c37InfCkGttzdBillInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(C37InfCkGttzdBill entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
        } else {
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(C37InfCkGttzdBill entity) {
        entity.setMark(0);
        return super.delete(entity);
    }

}