package com.javaweb.admin.service;

import com.javaweb.admin.vo.cgfp.CGFPInfo;
import com.javaweb.common.utils.JsonResult;

import java.io.IOException;
import java.util.List;


public interface ICGFPService {
    List<CGFPInfo> getCGFPList();

    JsonResult cgfpToBIP(CGFPInfo fp, String token) throws IOException;
}
