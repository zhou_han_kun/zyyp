// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.admin.entity.YYq005Detail;
import com.javaweb.admin.mapper.YYq005DetailMapper;
import com.javaweb.admin.query.YYq005DetailQuery;
import com.javaweb.admin.service.IYYq005DetailService;
import com.javaweb.admin.vo.yyq005detail.YYq005DetailInfoVo;
import com.javaweb.admin.vo.yyq005detail.YYq005DetailListVo;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
  * <p>
  * 订单代填明细表 服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2023-12-31
  */
@Service
public class YYq005DetailServiceImpl extends BaseServiceImpl<YYq005DetailMapper, YYq005Detail> implements IYYq005DetailService {

    @Autowired
    private YYq005DetailMapper yYq005DetailMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        YYq005DetailQuery yYq005DetailQuery = (YYq005DetailQuery) query;
        // 查询条件
        QueryWrapper<YYq005Detail> queryWrapper = new QueryWrapper<>();
        // queryWrapper.orderByDesc("id");

        // 获取数据列表
        IPage<YYq005Detail> page = new Page<>(yYq005DetailQuery.getPage(), yYq005DetailQuery.getLimit());
        IPage<YYq005Detail> pageData = yYq005DetailMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            YYq005DetailListVo yYq005DetailListVo = Convert.convert(YYq005DetailListVo.class, x);
            return yYq005DetailListVo;
        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        YYq005Detail entity = (YYq005Detail) super.getInfo(id);
        // 返回视图Vo
        YYq005DetailInfoVo yYq005DetailInfoVo = new YYq005DetailInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, yYq005DetailInfoVo);
        return yYq005DetailInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(YYq005Detail entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
        } else {
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(YYq005Detail entity) {
        entity.setMark(0);
        return super.delete(entity);
    }

}