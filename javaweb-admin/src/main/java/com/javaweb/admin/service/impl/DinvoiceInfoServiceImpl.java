// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.entity.DinvoiceInfo;
import com.javaweb.admin.mapper.DinvoiceInfoMapper;
import com.javaweb.admin.query.DinvoiceInfoQuery;
import com.javaweb.admin.service.IDinvoiceInfoService;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.d0oinvoiceinfo.D0oinvoiceInfoInfoVo;
import com.javaweb.admin.vo.d0oinvoiceinfo.D0oinvoiceInfoListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
  * <p>
  *  服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2024-07-12
  */
@Service
public class DinvoiceInfoServiceImpl  implements IDinvoiceInfoService {

    @Autowired
    private DinvoiceInfoMapper dinvoiceInfoMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getPOinvoiceList(BaseQuery query) {
        DinvoiceInfoQuery dinvoiceInfoQuery = (DinvoiceInfoQuery) query;
        // 获取数据列表
        IPage<DinvoiceInfo> page = new Page<>(dinvoiceInfoQuery.getPage(), dinvoiceInfoQuery.getLimit());
        IPage<DinvoiceInfo> pageData = dinvoiceInfoMapper.getPOInvoiceList(page, dinvoiceInfoQuery);
//        pageData.convert(x -> {
//            D0oinvoiceInfoListVo d0oinvoiceInfoListVo = Convert.convert(D0oinvoiceInfoListVo.class, x);
//            return d0oinvoiceInfoListVo;
//        });
        return JsonResult.success(pageData);
    }

    @Override
    public JsonResult getPOinvoiceDetailList(BaseQuery query) {
        DinvoiceInfoQuery dinvoiceInfoQuery = (DinvoiceInfoQuery) query;
        return JsonResult.success(dinvoiceInfoMapper.getPOInvoiceDetailList(((DinvoiceInfoQuery) query).getCinvoiceid()));
    }
}