package com.javaweb.admin.service;

import java.util.Hashtable;

public class Const {

    //本地测试{true-本地测试; false-正式系统}
    public static boolean LOCAL_TEST = true;
    //SQL 数据库地址 生产环境
    public static String MS_SQL_ADDR = "127.0.0.1,1533";
    public static String MS_SQL_USER = "sa";
    public static String MS_SQL_PASS = "sz502@123";


    /** localtest/
     *
     public static string MS_SQL_ADDR = ".";
     public static string MS_SQL_USER = "sa";

     /**/
    //RPA数据库地址
    public static String MS_SQL_ADDR_RPA = "127.0.0.1";

    //public static string MS_SQL_DATA_CONNECTION = "server=10.62.0.4;database=SPSunPlatfromDb;uid=tianyi01;pwd=abcd.1234";
    public static String MS_SQL_DATA_CONNECTION = "server=127.0.0.1,1533;database=SunPlatform;uid=sa;pwd=sz502@123";
    //public static string MS_SQL_ADDR = "DESKTOP-CYH2019";
    //SQL  数据库名称
    //public static string MS_SQL_DBNAME = "SPSunPlatfromDb_NC63";
    public static String MS_SQL_DBNAME = "SunPlatform";

    public static String MS_SQL_DBNAME_RPA = "RPADB";

    public static String MS_SQL_DBNAME_BPM = "bpmdb";

    //public static String CORP_NAME = "上海信谊雷允上药业有限公司";
    public static String CORP_NAME = "上海信谊天一药业有限公司";

    //阳光平台账号信息 Start
//    public static String YGPT_CORPCODE = "shlysyy001"; //阳光平台 雷允上药企编码
//    public static String YGPT_PASSWORD = "kabfuehkqp"; //阳光平台 雷允上药企编码
//    public static String YGPT_JGDM = "91310000631291406P"; //阳光平台 机构代码
//    public static String YGPT_CORPCODE = "yymy0001"; //阳光平台 国际医药药企编码
//    public static String YGPT_PASSWORD = "123456"; //阳光平台 国际医药企密码
//    public static String YGPT_PASSWORD_TEST = "sidfnmnfrz"; //阳光平台 国际医药企密码
//    public static String YGPT_JGDM = "YYMY0001"; //阳光平台 机构代码
    public static String YGPT_CORPCODE = "xyty001"; //阳光平台 国际医药药企编码
    public static String YGPT_PASSWORD = "zsxyty001001"; //阳光平台 国际医药企密码
    public static String YGPT_JGDM = "XYTY0001"; //阳光平台 机构代码

    public static String YGPT_VERSION = "1.0.0.0"; //阳光平台 机构代码

    //阳光平台账号信息 End
    public static boolean bServiceThreadRun = false;

    public static Hashtable al_thread = new Hashtable();//线程管理工具。

    //阳光医院采购网，议价信息采集间隔 =10分钟
    public static Integer YgYiJiaInterval = 5;
    //阳光平台订单采集时间间隔 = 1 分钟
    public static Integer YgOrderInterval = 1;
    //NC现存量信息采集时间间隔 = 5 分钟
    public static Integer NC_OnHand_Num_Interval = 5;
    //NC存货信息采集时间间隔 = 5 分钟
    public static Integer NC_InvCode_Interval = 5;
    //NC客户信息采集时间间隔 = 5 分钟
    public static Integer NC_CustInfo_Interval = 5;
    //NC客户地址数据采集时间间隔 = 5 分钟
    public static Integer NC_CustAddr_Interval = 5;
    //销售发票数据修正间隔
    public static Integer NC_Sale_Modify_Interval = 5;
    //销售发票主子表数据同步间隔 = 5 分钟
    public static Integer NC_SALEDATA_INTERVAL = 2;
    //格银订单同步时间间隔 = 10 分钟
    public static Integer GEYING_SALEDATA_INTERVAL = 10;
    //运输记录抓取时间间隔 = 10 分钟
    public static Integer NC_TRANSE_INTERVAL = 10;
    //运输温度数据抓取间隔
    public static Integer NC_TRANSE_TEMP_INTERVAL = 1;
    //运输GPS位置数据抓取间隔
    public static Integer NC_TRANSE_GPS_INTERVAL = 2;
    //运输里程的处理时间间隔 = 120分钟,两小时一次
    public static Integer NC_TRANSE_MILEAGEL_INTERVAL = 120;
    //新华冷链项目传报的时间间隔,2分钟一次
    public static Integer XINHUA_MED_INTERVAL = 2;
    //现存量日数据= 10 分钟
    public static Integer NC_OnHand_DayNum_Interval = 10;
    //NC63销售价格数据同步间隔 = 2 分钟
    public static Integer NC_SALE_PRICE_INTERVAL = 2;
    //订单跟踪数据处理 = 2 分钟
    public static Integer NC_ORDER_TRACK_INTERVAL = 2;
    //补差数据同步至ERP = 2 分钟
    public static Integer BC_TO_ERP_INTERVAL = 2;

    //打印多久后进行阳光平台配送和发票的传报(阳光配送及发票传报后即开始进行配送和发票、GPO的传报) 分钟
    public static Integer NC_UPTO_YANGGUANG_FOR_PRINT_INTERVAL = 10;

    //销售数据采集的开始时间
    public static String NC_SALE_INVOICE_START_TIME = "2021-12-01 00:00:00";

    //订单响应类型
    public static String ORDER_RESPONSE_ALL = "全量响应";
    public static String ORDER_RESPONSE_SOME = "部分响应";
    public static String ORDER_RESPONSE_NO = "不予响应";


    //打印状态
    public static String PRINT_OK = "已打印";
    public static String PRINT_NO = "未打印";

    //阳光配送及发票传报状态
    public static String YG_FP_AND_PS_OK = "已传报";
    public static String YG_FP_AND_PS_NO = "未传报";
    public static String YG_FP_AND_PS_CONFIRM = "已确认";


    //阳光传报接口
    public static String YQ003_TXT = "YQ003(阳光配送单传报)";
    public static String YQ004_TXT = "YQ004(阳光发票传报)";
    public static String YQ005_TXT = "YQ005(订单代为填报)";
    public static String YQ006_TXT = "YQ006(订单代填确认)";
    public static String YQ007_TXT = "YQ007(退货单代为填报)";
    public static String YQ008_TXT = "YQ008(退货单代填确认)";
    public static String YQ029_TXT = "YQ029(进货发票信息传报)";
    public static String YQ030_TXT = "YQ030(进货发票信息确认)";
    public static String YQ031_TXT = "YQ031(配送发票与进货发票关联关系传报传报)";
    public static String YQ032_TXT = "YQ032(配送发票与进货发票关联关系传报确认)";
    public static String YQ039_TXT = "YQ039(阳光订单响应)";
    public static String YQ040_TXT = "YQ040(阳光配送单数据提交)";
    public static String YQ041_TXT = "YQ041(阳光发票数据提交)";

    //GPO传报接口
    public static String GPO_GS101_TXT = "GS101(GPO配送单传报)";
    public static String GPO_GS102_TXT = "GS102(GPO发票传报)";
    //GPO标志
    public static String GPO_FLAG = "GPO";

    //客户生成阳光平台条码打印规则NC_custinfodoc.ygPrintType
    public static String YG_PRINT_TYPE_1 = "一品一码";
    public static String YG_PRINT_TYPE_2 = "多品一码";

    //主子表同步完成状态synchronous
    public static String ALL_SYN_FLAG_OK = "0";
    public static String ALL_SYN_FLAG_NO = "1";


    //格银账号信息START
    public static String GEYIN_SID = "076";
    public static String GEYIN_UID = "1212fs";
    public static String GEYIN_PWD = "l^4kasdfj7";

    public static String GEYIN_UID_TEST = "XyJzx8709";
    public static String GEYIN_PWD_TEST = "jool*5RF9i";
    //格银账号信息END


    //订单类型 NC63对照 START
    //30-Cxx-1173-01	雷允上普通销售
    public static String SALES_ORDER_TYPE_PUTONG = "30-Cxx-1233-01";
    //30-Cxx-1173-02	信谊雷允上GPO销售
    public static String SALES_ORDER_TYPE_GPO = "30-Cxx-1233-01";
    //30-Cxx-1173-03	信谊雷允上折扣
    public static String SALES_ORDER_TYPE_ZHEKOU = "30-Cxx-1173-03";
    //30-Cxx-1173-20	23年新的GPO 信谊雷允上GPO销售2
    public static String SALES_ORDER_TYPE_GPO_2 = "30-Cxx-1173-20";

    //业务流程 NC63对照
    //信谊雷允上GPO销售
    //select * from bd_busitype where pk_group = '0001V1100000000007S7' and businame like  '%雷允上%'
    public static String SALE_ORDER_BUSITYPE_GPO = "Cxx-1173-02";
    public static String SALE_ORDER_BUSITYPE_GPO_NAME = "信谊雷允上GPO销售";
    //信谊雷允上普通销售
    public static String SALE_ORDER_BUSITYPE_PUTONG = "Cxx-1233-002";
    public static String SALE_ORDER_BUSITYPE_PUTONG_NAME = "信谊雷允上普通销售";
    //信谊雷允上GPO销售2， 2023年新版本GPO
    public static String SALE_ORDER_BUSITYPE_GPO_2 = "Cxx-1173-20";
    public static String SALE_ORDER_BUSITYPE_GPO_NAME_2 = "信谊雷允上GPO销售2";



    //订单来源-药事
    public static String SALE_ORDER_SOURCE_YS = "Yaoshi";
    //订单来源-格银
    public static String SALE_ORDER_SOURCE_GY = "Geyin";
    //订单来源-上药控股分销
    public static String SALE_ORDER_SOURCE_SY = "Fenxiao";
    //订单来源
    public static String SALE_ORDER_SOURCE_SG = "自制";
    //NC63对照 END

    //public static string NC63OrderPostUrl = "http://10.11.0.109:80/service/XChangeServlet?account=0001&groupcode=0001";
    public static String NC63OrderPostUrl = "http://10.11.100.123:80/service/XChangeServlet?account=01&groupcode=0001"; // 测试系统NC63订单推送接口


    //上药控股webservice相关
    public static String KONGGU_ID = "SYXYTY";
    public static String KONGGU_KEY = "9d75319380bfa5dd84c284eacdad370d";
    //上药控股订单回传状态
    public static String KONGGU_ORDER_UP_OK = "已回传";


    //log file
    public static String LOG_KONGGU_DOWN = "上药控股采集日志";
    public static String LOG_KONGGU_UP = "上药控股上传日志";
    public static String LOG_BC_TOERP = "补差同步ERP";

    public static String SYN_ING = "同步中";


    //订单的处理类别
    public static String ORDER_01 = "订单处理";

    public static String ORDER_01_01 = "已下单";
    public static String ORDER_01_02 = "导入ERP";
    public static String ORDER_01_03 = "出库单生成";
    public static String ORDER_01_04 = "开具开票";

    public static String ORDER_02 = "仓库处理中";
    public static String ORDER_02_01 = "条码打印";

    public static String ORDER_03 = "数据处理";
    public static String ORDER_03_01 = "阳光配送上传";
    public static String ORDER_03_02 = "阳光发票上传";
    public static String ORDER_03_04 = "GPO配送上传";
    public static String ORDER_03_05 = "GPO发票上传";
    public static String ORDER_03_06 = "两票上传";
    public static String ORDER_03_07 = "阳光订单响应";
    public static String ORDER_03_08 = "配送单数据提交";
    public static String ORDER_03_09 = "发票数据提交";
    public static String ORDER_03_10 = "进货发票信息传报";
    public static String ORDER_03_11 = "进货发票信息确认";
    public static String ORDER_03_12 = "配送发票与进货发票关联关系传报传报";
    public static String ORDER_03_13 = "配送发票与进货发票关联关系传报确认";

    public static String ORDER_05_01 = "阳光订单代填上传";
    public static String ORDER_05_02 = "阳光订单代填确认";

    public static String ORDER_07_01 = "阳光退货单代填上传";
    public static String ORDER_07_02 = "阳光退货单代填确认";

    public static String ORDER_04 = "运输";


    //医药两票制 code =yylpz, PK_DEFDOC=1001V11000000083B16Y
    public static String CONST_PK_YYLPZ = "yylpz";
}

