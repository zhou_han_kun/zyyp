// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.admin.entity.C37InfRkScBillNew;
import com.javaweb.admin.query.C37InfRkScBillNewQuery;
import com.javaweb.admin.vo.c37infrkscbillnew.C37InfRkScBillNewListVo;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.constant.C37InfCkScBillNewConstant;
import com.javaweb.admin.entity.C37InfCkScBillNew;
import com.javaweb.admin.mapper.C37InfCkScBillNewMapper;
import com.javaweb.admin.query.C37InfCkScBillNewQuery;
import com.javaweb.admin.service.IC37InfCkScBillNewService;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.c37infckscbillnew.C37InfCkScBillNewInfoVo;
import com.javaweb.admin.vo.c37infckscbillnew.C37InfCkScBillNewListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  * 销售出库/购进退出上传单表 服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2022-11-26
  */
@Service
public class C37InfCkScBillNewServiceImpl extends BaseServiceImpl<C37InfCkScBillNewMapper, C37InfCkScBillNew> implements IC37InfCkScBillNewService {

    @Autowired
    private C37InfCkScBillNewMapper c37InfCkScBillNewMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult getList(BaseQuery query) {
        C37InfCkScBillNewQuery c37InfCkScBillNewQuery = (C37InfCkScBillNewQuery) query;
        // 查询条件
        QueryWrapper<C37InfCkScBillNew> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("danj_no","chuk_no","danw_no","riqi_char","yew_staff","tih_way","yew_type","danjxc_side","hanghao","shangp_no"
                ,"jih_num","num","lot","shengc_char","youx_char","chongh_flg","tuih_reason","zt","kub","kh_zzbm","yez_id");
        // queryWrapper.orderByDesc("id");
        if(!StringUtils.isEmpty(c37InfCkScBillNewQuery.getDanwNo()))
        {
            queryWrapper.like("danw_no",c37InfCkScBillNewQuery.getDanwNo());
        }
        if(!StringUtils.isEmpty(c37InfCkScBillNewQuery.getDanjNo()))
        {
            queryWrapper.like("danj_no",c37InfCkScBillNewQuery.getDanjNo());
        }
        if(!StringUtils.isEmpty(c37InfCkScBillNewQuery.getChukNo()))
        {
            queryWrapper.like("chuk_no",c37InfCkScBillNewQuery.getChukNo());
        }
        if(!StringUtils.isEmpty(c37InfCkScBillNewQuery.getShangpNo()))
        {
            queryWrapper.like("shangp_no",c37InfCkScBillNewQuery.getShangpNo());
        }
        if(!StringUtils.isEmpty(c37InfCkScBillNewQuery.getLot()))
        {
            queryWrapper.like("lot",c37InfCkScBillNewQuery.getLot());
        }
        if(!StringUtils.isEmpty(c37InfCkScBillNewQuery.getYewType()))
        {
            queryWrapper.like("yew_type",c37InfCkScBillNewQuery.getYewType());
        }
        if(!StringUtils.isEmpty(c37InfCkScBillNewQuery.getZt()))
        {
            queryWrapper.eq("zt",c37InfCkScBillNewQuery.getZt());
        }

        // 获取数据列表
        IPage<C37InfCkScBillNew> page = new Page<>(c37InfCkScBillNewQuery.getPage(), c37InfCkScBillNewQuery.getLimit());
        IPage<C37InfCkScBillNew> pageData = c37InfCkScBillNewMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            C37InfCkScBillNewListVo c37InfCkScBillNewListVo = Convert.convert(C37InfCkScBillNewListVo.class, x);
            return c37InfCkScBillNewListVo;
        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        C37InfCkScBillNew entity = (C37InfCkScBillNew) super.getInfo(id);
        // 返回视图Vo
        C37InfCkScBillNewInfoVo c37InfCkScBillNewInfoVo = new C37InfCkScBillNewInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, c37InfCkScBillNewInfoVo);
        return c37InfCkScBillNewInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(C37InfCkScBillNew entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
        } else {
        }
        return super.edit(entity);
    }

    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public List<C37InfCkScBillNew> DetailList(String danj_no) {
        QueryWrapper<C37InfCkScBillNew> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("danj_no","chuk_no","danw_no","riqi_char","yew_staff","tih_way","yew_type","danjxc_side","hanghao","shangp_no"
                ,"jih_num","num","lot","shengc_char","youx_char","chongh_flg","tuih_reason","zt","kub","kh_zzbm","yez_id");
        // queryWrapper.orderByDesc("id");
        if(!StringUtils.isEmpty(danj_no))
        {
            queryWrapper.eq("chuk_no",danj_no);
        }

        return super.list(queryWrapper);
    }

    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public List<C37InfCkScBillNew> GetCkScList(String yew_type) {
        QueryWrapper<C37InfCkScBillNew> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("distinct  chuk_no");
        // queryWrapper.orderByDesc("id");
        if(!StringUtils.isEmpty(yew_type))
        {
            queryWrapper.eq("yew_type",yew_type);
        }
        queryWrapper.eq("zt","Y");

        return super.list(queryWrapper);
    }

    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult GetCkdList(BaseQuery query) {
        C37InfCkScBillNewQuery c37InfCkScBillNewQuery = (C37InfCkScBillNewQuery) query;
        // 查询条件
//        QueryWrapper<C37InfCkScBillNew> queryWrapper = new QueryWrapper<>();
//        queryWrapper.select("DISTINCT danj_no,riqi_char,danw_no,chuk_no,zt");
//        // queryWrapper.orderByDesc("id");
//        if(!StringUtils.isEmpty(c37InfCkScBillNewQuery.getYewType()))
//        {
//            queryWrapper.like("yew_type",c37InfCkScBillNewQuery.getYewType());
//        }
//        if(!StringUtils.isEmpty(c37InfCkScBillNewQuery.getZt()))
//        {
//            queryWrapper.eq("zt",c37InfCkScBillNewQuery.getZt());
//        }
//        IPage<C37InfCkScBillNew> page = new Page<>(c37InfCkScBillNewQuery.getPage(), c37InfCkScBillNewQuery.getLimit());
//        IPage<C37InfCkScBillNew> pageData = c37InfCkScBillNewMapper.selectPage(page, queryWrapper);
//        pageData.convert(x -> {
//            C37InfCkScBillNewListVo c37InfCkScBillNewListVo = Convert.convert(C37InfCkScBillNewListVo.class, x);
//            return c37InfCkScBillNewListVo;
//        });
//        return JsonResult.success(pageData);

        // 获取数据列表
        IPage<C37InfCkScBillNew> page = new Page<>(c37InfCkScBillNewQuery.getPage(), c37InfCkScBillNewQuery.getLimit());
        IPage<C37InfCkScBillNew> pageData = c37InfCkScBillNewMapper.GetCkdList(page, c37InfCkScBillNewQuery);
        pageData.convert(x -> {
            C37InfCkScBillNewListVo c37InfCkScBillNewListVo = Convert.convert(C37InfCkScBillNewListVo.class, x);
            return c37InfCkScBillNewListVo;
        });
        return JsonResult.success(pageData);
    }


    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public boolean UpdateZT(String danj_no, String zt) {
        try {
            C37InfCkScBillNew entity = new C37InfCkScBillNew();
            //entity.setDanjNo(danj_no);
            UpdateWrapper<C37InfCkScBillNew> updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("ZT", zt).eq("chuk_no", danj_no).eq("ZT","Y");
            return this.update(entity, updateWrapper);
        }
        catch(Exception ex)
        {
            return false;
        }
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(C37InfCkScBillNew entity) {
        entity.setMark(0);
        return super.delete(entity);
    }

}