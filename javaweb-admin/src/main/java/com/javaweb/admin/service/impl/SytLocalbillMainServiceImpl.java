package com.javaweb.admin.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonObject;
import com.javaweb.admin.entity.SytLocalbillMain;
import com.javaweb.admin.entity.SytLocalbillMainDto;
import com.javaweb.admin.mapper.SytLocalbillMainMapper;
import com.javaweb.admin.service.ISytLocalbillMainService;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.JsonResultSYT;
import com.javaweb.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SytLocalbillMainServiceImpl implements ISytLocalbillMainService {
    @Autowired
    private SytLocalbillMainMapper sytLocalbillMainMapper;

    @Override
    public JsonResultSYT saveLocalbillMain(SytLocalbillMainDto dto) {
        try {
            String content = dto.getDataitem_content().replace("'","\"");

            String methodname = dto.getDataitem_methodname();
            if(methodname.toLowerCase().equals("push_download_localbill")) {

                JSONObject jsonObject = JSON.parseObject(content);
                String data = jsonObject.getString("result");
                SytLocalbillMain entity;
                if(!StringUtils.isNull(data)) {
                    JSONObject dataObject = JSON.parseObject(data);
                    entity = JSON.parseObject(dataObject.getString("bill"), SytLocalbillMain.class);
                }else{
                    entity = JSON.parseObject(jsonObject.getString("bill"), SytLocalbillMain.class);
                }
                SytLocalbillMain localbillMain = sytLocalbillMainMapper.selectById(entity.getPush_msgid());
                if (StringUtils.isNull(localbillMain)) {
                    sytLocalbillMainMapper.insert(entity);
                } else {
                    sytLocalbillMainMapper.updateById(entity);
                }
            }
            return JsonResultSYT.success();
        }
        catch (Exception ex)
        {
            return JsonResultSYT.error(ex.getMessage());
        }
    }
}
