// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.admin.entity.C37InfCkScBillNew;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.constant.C37InfRkScBillNewConstant;
import com.javaweb.admin.entity.C37InfRkScBillNew;
import com.javaweb.admin.mapper.C37InfRkScBillNewMapper;
import com.javaweb.admin.query.C37InfRkScBillNewQuery;
import com.javaweb.admin.service.IC37InfRkScBillNewService;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.c37infrkscbillnew.C37InfRkScBillNewInfoVo;
import com.javaweb.admin.vo.c37infrkscbillnew.C37InfRkScBillNewListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  * 采购入库/销售退回上传单表 服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2022-11-26
  */
@Service
public class C37InfRkScBillNewServiceImpl extends BaseServiceImpl<C37InfRkScBillNewMapper, C37InfRkScBillNew> implements IC37InfRkScBillNewService {

    @Autowired
    private C37InfRkScBillNewMapper c37InfRkScBillNewMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult getList(BaseQuery query) {
        C37InfRkScBillNewQuery c37InfRkScBillNewQuery = (C37InfRkScBillNewQuery) query;
        // 查询条件
        QueryWrapper<C37InfRkScBillNew> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("danj_no","riqi_char","danw_no","caigou_staff","shouh_staff","hanghao","shangp_no","lot","shengc_char","youx_char","ruk_type","jih_num","num","price","ruk_no","hangh_cgd","zhij_staff","zt","pingx_no","yans_rlt","kh_zzbm","yez_id");
        // queryWrapper.orderByDesc("id");
        if(!StringUtils.isEmpty(c37InfRkScBillNewQuery.getDanwNo()))
        {
            queryWrapper.like("danw_no",c37InfRkScBillNewQuery.getDanwNo());
        }
        if(!StringUtils.isEmpty(c37InfRkScBillNewQuery.getDanjNo()))
        {
            queryWrapper.like("danj_no",c37InfRkScBillNewQuery.getDanjNo());
        }
        if(!StringUtils.isEmpty(c37InfRkScBillNewQuery.getRukNo()))
        {
            queryWrapper.like("ruk_no",c37InfRkScBillNewQuery.getRukNo());
        }
        if(!StringUtils.isEmpty(c37InfRkScBillNewQuery.getCaigouStaff()))
        {
            queryWrapper.like("caigou_staff",c37InfRkScBillNewQuery.getCaigouStaff());
        }
        if(!StringUtils.isEmpty(c37InfRkScBillNewQuery.getShangpNo()))
        {
            queryWrapper.like("shangp_no",c37InfRkScBillNewQuery.getShangpNo());
        }
        if(!StringUtils.isEmpty(c37InfRkScBillNewQuery.getLot()))
        {
            queryWrapper.like("lot",c37InfRkScBillNewQuery.getLot());
        }
        if(!StringUtils.isEmpty(c37InfRkScBillNewQuery.getRukType()))
        {
            queryWrapper.like("ruk_type",c37InfRkScBillNewQuery.getRukType());
        }
        if(!StringUtils.isEmpty(c37InfRkScBillNewQuery.getZt()))
        {
            queryWrapper.eq("zt",c37InfRkScBillNewQuery.getZt());
        }

        // 获取数据列表
        IPage<C37InfRkScBillNew> page = new Page<>(c37InfRkScBillNewQuery.getPage(), c37InfRkScBillNewQuery.getLimit());
        IPage<C37InfRkScBillNew> pageData = c37InfRkScBillNewMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            C37InfRkScBillNewListVo c37InfRkScBillNewListVo = Convert.convert(C37InfRkScBillNewListVo.class, x);
            return c37InfRkScBillNewListVo;
        });
        return JsonResult.success(pageData);
    }

    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult GetRkdList(BaseQuery query) {
        C37InfRkScBillNewQuery c37InfRkScBillNewQuery = (C37InfRkScBillNewQuery) query;
        // 查询条件
//        QueryWrapper<C37InfRkScBillNew> queryWrapper = new QueryWrapper<>();
//        queryWrapper.select("distinct danj_no,riqi_char,danw_no,ruk_no,zt");
//        // queryWrapper.orderByDesc("id");
//        if(!StringUtils.isEmpty(c37InfRkScBillNewQuery.getRukType()))
//        {
//            queryWrapper.like("ruk_type",c37InfRkScBillNewQuery.getRukType());
//        }
//        if(!StringUtils.isEmpty(c37InfRkScBillNewQuery.getZt()))
//        {
//            queryWrapper.eq("zt",c37InfRkScBillNewQuery.getZt());
//        }

        // 获取数据列表
        IPage<C37InfRkScBillNew> page = new Page<>(c37InfRkScBillNewQuery.getPage(), c37InfRkScBillNewQuery.getLimit());
        IPage<C37InfRkScBillNew> pageData = c37InfRkScBillNewMapper.GetRkdList(page, c37InfRkScBillNewQuery);
        pageData.convert(x -> {
            C37InfRkScBillNewListVo c37InfRkScBillNewListVo = Convert.convert(C37InfRkScBillNewListVo.class, x);
            return c37InfRkScBillNewListVo;
        });
        return JsonResult.success(pageData);
    }

    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public List<C37InfRkScBillNew> DetailList(String danj_no) {
        QueryWrapper<C37InfRkScBillNew> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("danj_no","riqi_char","danw_no","caigou_staff","shouh_staff","hanghao","shangp_no","lot","shengc_char","youx_char","ruk_type","jih_num","num","price","ruk_no","hangh_cgd","zhij_staff","zt","pingx_no","yans_rlt","kh_zzbm","yez_id");
        // queryWrapper.orderByDesc("id");
        if(!StringUtils.isEmpty(danj_no))
        {
            queryWrapper.eq("ruk_no",danj_no);
        }

        return super.list(queryWrapper);
    }
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public List<C37InfRkScBillNew> GetRkScList(String ruk_type) {
        QueryWrapper<C37InfRkScBillNew> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("distinct  ruk_no");
        // queryWrapper.orderByDesc("id");
        if(!StringUtils.isEmpty(ruk_type))
        {
            queryWrapper.eq("ruk_type",ruk_type);
        }
        queryWrapper.eq("zt","Y");

        return super.list(queryWrapper);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        C37InfRkScBillNew entity = (C37InfRkScBillNew) super.getInfo(id);
        // 返回视图Vo
        C37InfRkScBillNewInfoVo c37InfRkScBillNewInfoVo = new C37InfRkScBillNewInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, c37InfRkScBillNewInfoVo);
        return c37InfRkScBillNewInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(C37InfRkScBillNew entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
        } else {
        }
        return super.edit(entity);
    }

    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public boolean UpdateZT(String danj_no, String zt) {
        try {
            C37InfRkScBillNew entity = new C37InfRkScBillNew();
            //entity.setDanjNo(danj_no);
            UpdateWrapper<C37InfRkScBillNew> updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("ZT", zt).eq("ruk_no", danj_no).eq("zt","Y");
            return this.update(entity, updateWrapper);
        }
        catch(Exception ex)
        {
            return false;
        }
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(C37InfRkScBillNew entity) {
        entity.setMark(0);
        return super.delete(entity);
    }

}