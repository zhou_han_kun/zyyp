// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.admin.query.C37InfCkGttzdBillQuery;
import com.javaweb.admin.vo.c37infckgttzdbill.C37InfCkGttzdBillListVo;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.constant.C37InfCkKpdBillConstant;
import com.javaweb.admin.entity.C37InfCkKpdBill;
import com.javaweb.admin.mapper.C37InfCkKpdBillMapper;
import com.javaweb.admin.query.C37InfCkKpdBillQuery;
import com.javaweb.admin.service.IC37InfCkKpdBillService;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.c37infckkpdbill.C37InfCkKpdBillInfoVo;
import com.javaweb.admin.vo.c37infckkpdbill.C37InfCkKpdBillListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  * 销售订单表 服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2022-11-26
  */
@Service
public class C37InfCkKpdBillServiceImpl extends BaseServiceImpl<C37InfCkKpdBillMapper, C37InfCkKpdBill> implements IC37InfCkKpdBillService {

    @Autowired
    private C37InfCkKpdBillMapper c37InfCkKpdBillMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult getList(BaseQuery query) {
        C37InfCkKpdBillQuery c37InfCkKpdBillQuery = (C37InfCkKpdBillQuery) query;
        // 查询条件
        QueryWrapper<C37InfCkKpdBill> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("danj_no","danw_no","yew_staff","erji_danw_no","kaip_time","keh_notes","hanghao","shangp_no","num","price","lot_request","zt","tih_way","fap_no","kh_zzbm","yez_id","diz_id");
        // queryWrapper.orderByDesc("id");
        if(!StringUtils.isEmpty(c37InfCkKpdBillQuery.getDanwNo()))
        {
            queryWrapper.like("danw_no",c37InfCkKpdBillQuery.getDanwNo());
        }
        if(!StringUtils.isEmpty(c37InfCkKpdBillQuery.getDanjNo()))
        {
            queryWrapper.like("danj_no",c37InfCkKpdBillQuery.getDanjNo());
        }
        if(!StringUtils.isEmpty(c37InfCkKpdBillQuery.getYewStaff()))
        {
            queryWrapper.like("yew_staff",c37InfCkKpdBillQuery.getYewStaff());
        }
        if(!StringUtils.isEmpty(c37InfCkKpdBillQuery.getShangpNo()))
        {
            queryWrapper.like("shangp_no",c37InfCkKpdBillQuery.getShangpNo());
        }
        if(!StringUtils.isEmpty(c37InfCkKpdBillQuery.getLotRequest()))
        {
            queryWrapper.like("lot_request",c37InfCkKpdBillQuery.getLotRequest());
        }
        if(!StringUtils.isEmpty(c37InfCkKpdBillQuery.getTihWay()))
        {
            queryWrapper.like("tih_way",c37InfCkKpdBillQuery.getTihWay());
        }
        if(!StringUtils.isEmpty(c37InfCkKpdBillQuery.getZt()))
        {
            queryWrapper.eq("zt",c37InfCkKpdBillQuery.getZt());
        }

        // 获取数据列表
        IPage<C37InfCkKpdBill> page = new Page<>(c37InfCkKpdBillQuery.getPage(), c37InfCkKpdBillQuery.getLimit());
        IPage<C37InfCkKpdBill> pageData = c37InfCkKpdBillMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            C37InfCkKpdBillListVo c37InfCkKpdBillListVo = Convert.convert(C37InfCkKpdBillListVo.class, x);
            return c37InfCkKpdBillListVo;
        });
        return JsonResult.success(pageData);
    }

    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult GetXsddList(BaseQuery query) {
        C37InfCkKpdBillQuery c37InfCkKpdBillQuery = (C37InfCkKpdBillQuery) query;

        // 获取数据列表
        if(c37InfCkKpdBillQuery.getPage()==null)
        {
            c37InfCkKpdBillQuery.setPage(1);
            c37InfCkKpdBillQuery.setLimit(100000);
        }
        IPage<C37InfCkKpdBillListVo> page = new Page<>(c37InfCkKpdBillQuery.getPage(), c37InfCkKpdBillQuery.getLimit());
        IPage<C37InfCkKpdBillListVo> pageData = c37InfCkKpdBillMapper.GetXsddList(page, c37InfCkKpdBillQuery);
        return JsonResult.success(pageData);
    }

    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult GetXsddDetailList(BaseQuery query) {
        C37InfCkKpdBillQuery c37InfCkKpdBillQuery = (C37InfCkKpdBillQuery) query;

        // 获取数据列表
        if(c37InfCkKpdBillQuery.getPage()==null)
        {
            c37InfCkKpdBillQuery.setPage(1);
            c37InfCkKpdBillQuery.setLimit(100000);
        }
        IPage<C37InfCkKpdBillListVo> page = new Page<>(c37InfCkKpdBillQuery.getPage(), c37InfCkKpdBillQuery.getLimit());
        IPage<C37InfCkKpdBillListVo> pageData = c37InfCkKpdBillMapper.GetXsddDetailList(page, c37InfCkKpdBillQuery);
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        C37InfCkKpdBill entity = (C37InfCkKpdBill) super.getInfo(id);
        // 返回视图Vo
        C37InfCkKpdBillInfoVo c37InfCkKpdBillInfoVo = new C37InfCkKpdBillInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, c37InfCkKpdBillInfoVo);
        return c37InfCkKpdBillInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(C37InfCkKpdBill entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
        } else {
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(C37InfCkKpdBill entity) {
        entity.setMark(0);
        return super.delete(entity);
    }

}