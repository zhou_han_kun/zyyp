// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.admin.entity.*;
import com.javaweb.admin.mapper.MInvoicesrelationMapper;
import com.javaweb.admin.mapper.MInvoicesrelationlineMapper;
import com.javaweb.admin.query.InvoiceDetailQuery;
import com.javaweb.admin.query.InvpriceQuery;
import com.javaweb.admin.query.MInvoicesrelationQuery;
import com.javaweb.admin.service.IMInvoicesrelationService;
import com.javaweb.admin.vo.minvoicesrelation.MInvoicesrelationInfoVo;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  * 两票关系主表 服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2024-01-21
  */
@Service
public class MInvoicesrelationServiceImpl implements IMInvoicesrelationService {

    @Autowired
    private MInvoicesrelationMapper mInvoicesrelationMapper;

    @Autowired
    private MInvoicesrelationlineMapper mInvoicesrelationlineMapper;




    @Override
    public JsonResult getList(BaseQuery query) {
        MInvoicesrelationQuery mInvoicesrelationQuery = (MInvoicesrelationQuery) query;
        mInvoicesrelationQuery.setRiqiBegin(mInvoicesrelationQuery.getRiqiBegin().substring(0, 10));
        mInvoicesrelationQuery.setRiqiEnd(mInvoicesrelationQuery.getRiqiEnd().substring(0, 10));
        // 获取数据列表
        IPage<MInvoicesrelation> page = new Page<>(mInvoicesrelationQuery.getPage(), mInvoicesrelationQuery.getLimit());
        IPage<MInvoicesrelation> pageData = mInvoicesrelationMapper.getList(page, mInvoicesrelationQuery);
        return JsonResult.success(pageData);
    }

    @Override
    public JsonResult getInvoiceDetailList(BaseQuery query) {
        InvoiceDetailQuery invoiceDetailQuery=(InvoiceDetailQuery) query;
        invoiceDetailQuery.setPage(1);
        invoiceDetailQuery.setLimit(-1);
        IPage<InvoiceDetail> page = new Page<>(invoiceDetailQuery.getPage(), invoiceDetailQuery.getLimit());
        IPage<InvoiceDetail> pageData = mInvoicesrelationMapper.getInvoiceDetailList(page, invoiceDetailQuery);
        if(invoiceDetailQuery.getNum()==0){
            return JsonResult.success(pageData);
        }
        List<InvoiceDetail> records = pageData.getRecords();
        List<InvoiceDetail> InvoiceDetailList=new ArrayList<>();
        int sum=0;
        for (InvoiceDetail invoiceDetail : records) {
            if(sum>invoiceDetailQuery.getNum()){
                break;
            }
            InvoiceDetailList.add(invoiceDetail);
            sum+=Integer.parseInt(invoiceDetail.getNnumber());
        }
        pageData.setRecords(InvoiceDetailList);
        return JsonResult.success(pageData);
    }

    @Override
    public JsonResult getPurchaseRecordList(BaseQuery query) {
        InvoiceDetailQuery invoiceDetailQuery=(InvoiceDetailQuery) query;
        invoiceDetailQuery.setPage(1);
        invoiceDetailQuery.setLimit(-1);
        IPage<PurchaseRecord> page = new Page<>(invoiceDetailQuery.getPage(), invoiceDetailQuery.getLimit());
        IPage<PurchaseRecord> pageData = mInvoicesrelationMapper.getPurchaseRecordList(page, invoiceDetailQuery);
        return JsonResult.success(pageData);
    }
}
