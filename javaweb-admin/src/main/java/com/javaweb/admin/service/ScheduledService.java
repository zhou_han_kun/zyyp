package com.javaweb.admin.service;

import com.javaweb.admin.entity.Scheduled;

import java.util.List;

public interface ScheduledService {
    List<Scheduled> findAllScheduled();
    void createJob(Scheduled scheduled);
    void updateJob(Scheduled scheduled);
    void executeJob(Scheduled scheduled);
    void deleteJob(Scheduled scheduled);
    void stopJob(Scheduled scheduled);
    void startJob(Scheduled scheduled);
}
