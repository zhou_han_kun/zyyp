// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.constant.C37InfJcSpzlConstant;
import com.javaweb.admin.entity.C37InfJcSpzl;
import com.javaweb.admin.mapper.C37InfJcSpzlMapper;
import com.javaweb.admin.query.C37InfJcSpzlQuery;
import com.javaweb.admin.service.IC37InfJcSpzlService;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.c37infjcspzl.C37InfJcSpzlInfoVo;
import com.javaweb.admin.vo.c37infjcspzl.C37InfJcSpzlListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  * 商品资料 服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2022-11-24
  */
@Service
public class C37InfJcSpzlServiceImpl extends BaseServiceImpl<C37InfJcSpzlMapper, C37InfJcSpzl> implements IC37InfJcSpzlService {

    @Autowired
    private C37InfJcSpzlMapper c37InfJcSpzlMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult getList(BaseQuery query) {
        C37InfJcSpzlQuery c37InfJcSpzlQuery = (C37InfJcSpzlQuery) query;
        // 查询条件
        QueryWrapper<C37InfJcSpzl> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("lius_no","shangp_no","chinese_name","yaop_guig","maker","chandi","baoz_num","baoz_danw","piz_no","beactive","gengx_time","jixing","yez_id","zt");
        if(!StringUtils.isEmpty(c37InfJcSpzlQuery.getShangpNo()))
        {
            queryWrapper.like("shangp_no",c37InfJcSpzlQuery.getShangpNo());
        }
        if(!StringUtils.isEmpty(c37InfJcSpzlQuery.getChineseName()))
        {
            queryWrapper.like("chinese_name",c37InfJcSpzlQuery.getChineseName());
        }
        if(!StringUtils.isEmpty(c37InfJcSpzlQuery.getRiqiBegin()))
        {
            queryWrapper.ge("gengx_time",c37InfJcSpzlQuery.getRiqiBegin());
        }
        if(!StringUtils.isEmpty(c37InfJcSpzlQuery.getRiqiEnd()))
        {
            queryWrapper.le("gengx_time",c37InfJcSpzlQuery.getRiqiEnd()+" 23:59:59");
        }
        queryWrapper.orderByAsc("shangp_no").orderByDesc("lius_no");
        // queryWrapper.orderByDesc("id");

        // 获取数据列表
        IPage<C37InfJcSpzl> page = new Page<>(c37InfJcSpzlQuery.getPage(), c37InfJcSpzlQuery.getLimit());
        IPage<C37InfJcSpzl> pageData = c37InfJcSpzlMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            C37InfJcSpzlListVo c37InfJcSpzlListVo = Convert.convert(C37InfJcSpzlListVo.class, x);
            return c37InfJcSpzlListVo;
        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        C37InfJcSpzl entity = (C37InfJcSpzl) super.getInfo(id);
        // 返回视图Vo
        C37InfJcSpzlInfoVo c37InfJcSpzlInfoVo = new C37InfJcSpzlInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, c37InfJcSpzlInfoVo);
        return c37InfJcSpzlInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(C37InfJcSpzl entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
        } else {
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(C37InfJcSpzl entity) {
        entity.setMark(0);
        return super.delete(entity);
    }

}