package com.javaweb.admin.service;

import com.javaweb.admin.entity.ReturnOrder;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.IBaseService;
import com.javaweb.common.utils.JsonResult;

public interface IReturnOrderService extends IBaseService<ReturnOrder> {
    JsonResult getReturnOrderList(BaseQuery query);

    JsonResult getOrderDetail(String csaleorderid);

}
