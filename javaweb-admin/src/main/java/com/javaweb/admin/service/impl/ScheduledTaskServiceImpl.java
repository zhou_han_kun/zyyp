package com.javaweb.admin.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.javaweb.admin.config.ScheduledConfig;
import com.javaweb.admin.entity.Scheduled;
import com.javaweb.admin.mapper.ScheduledMapper;
import com.javaweb.admin.service.AbstractJob;
import com.javaweb.admin.service.ScheduledService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.ScheduledFuture;

/**
 * @author liangxuelong
 * @version 1.0
 * @description: Scheduled service
 * @date 2021/8/16 9:07
 */
@Service
public class ScheduledTaskServiceImpl extends ServiceImpl<ScheduledMapper, Scheduled> implements ScheduledService {

    @Autowired
    private ApplicationContext context;


    @Override
    public List<Scheduled> findAllScheduled() {
        return list();
    }

    /**
     * @description: 创建定时任务
     * @param: scheduled
     * @return: void
     * @author liangxuelong
     * @date: 2021/8/27
     */
    @Override
    @Transactional
    public void createJob(Scheduled scheduled) {
        AbstractJob job = null;
        try {
            job = (AbstractJob) context.getBean(Class.forName(scheduled.getClasspath()));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if(job!=null){
            job.createJob(scheduled.getCron());
        }
        save(scheduled);
    }

    /**
     * @description: 修改定时任务
     * @param: scheduled
     * @return: void
     * @author liangxuelong
     * @date: 2021/8/27
     */
    @Override
    public void updateJob(Scheduled scheduled) {
        AbstractJob job = null;
        try {
            job = (AbstractJob) context.getBean(Class.forName(scheduled.getClasspath()));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if(job!=null){
            job.cancel(); //先删除
            job.createJob(scheduled.getCron());//再创建
        }
        updateById(scheduled);
    }

    /**
     * @description: 立刻执行定时任务
     * @param: scheduled
     * @return: void
     * @author liangxuelong
     * @date: 2021/8/27
     */
    @Override
    public void executeJob(Scheduled scheduled) {
        AbstractJob job = null;
        try {
            job = (AbstractJob) context.getBean(Class.forName(scheduled.getClasspath()));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if(job!=null){
            job.execute();
        }
    }

    /**
     * @description: 删除定时任务
     * @param: scheduled
     * @return: void
     * @author liangxuelong
     * @date: 2021/8/27
     */
    @Override
    public void deleteJob(Scheduled scheduled) {
        AbstractJob job = null;
        try {
            job = (AbstractJob) context.getBean(Class.forName(scheduled.getClasspath()));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if(job!=null){
            job.cancel(); //删除
            removeById(scheduled.getId());
        }
    }

    @Override
    public void stopJob(Scheduled scheduled) {
        if (ScheduledConfig.cache.isEmpty()) return;
        if (ScheduledConfig.cache.get(scheduled.getName()) == null) return;

        ScheduledFuture scheduledFuture = ScheduledConfig.cache.get(scheduled.getName());

        if (scheduledFuture != null) {
            scheduledFuture.cancel(true);   // 这里需要使用指定的 scheduledFuture 来停止当前的线程
            ScheduledConfig.cache.remove(scheduled.getName());        // 移除缓存
        }
    }

    @Override
    public void startJob(Scheduled scheduled) {

    }
}


