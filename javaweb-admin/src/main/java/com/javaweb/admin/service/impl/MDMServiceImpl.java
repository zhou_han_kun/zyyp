package com.javaweb.admin.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.javaweb.admin.entity.*;
import com.javaweb.admin.mapper.*;
import com.javaweb.admin.query.MDMQueryParam;
import com.javaweb.admin.service.*;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.*;
import com.shyp.daodikeji.*;
import com.sinepharm.pharmeyes.com.*;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.logging.log4j.core.util.JsonUtils;
import org.apache.logging.log4j.util.PropertiesUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.cglib.core.Converter;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.net.URLEncoder;
import java.util.*;

@Service
public class MDMServiceImpl implements IMDMService {

    @Autowired
    private MdmProductMapper mdmProductMapper;
    @Autowired
    private IMdmProductRecordsService mdmProductRecordsService;
    @Autowired
    private MdmCustomerMapper mdmCustomerMapper;
    @Autowired
    private IMdmCustomerHongyingListService mdmCustomerHongyingListService;

    @Autowired
    private MdmInvoicingDirectionMapper mdmInvoicingDirectionMapper;

    @Autowired
    private MdmCustomerHongyingListMapper mdmCustomerHongyingListMapper;

    @Autowired
    private MdmCustomerNCListMapper mdmCustomerNCListMapper;

    @Autowired
    private MdmProductRecordsMapper mdmProductRecordsMapper;

    @Autowired
    private MdmEmployeeMapper employeeMapper;
    @Autowired
    private MdmCleanSalesMapper cleanSalesMapper;

    @Autowired
    private MdmMonthTargetMapper monthTargetMapper;

    @Autowired
    SqlSessionFactory sqlSessionFactory;

    @Override
    @SpecifyDataSource(value = DataSourceType.MDM)
    public JsonResult SaveMaterial(MDMQueryParam query) {
        try {
            Map<String, String> reqOrder = new TreeMap<>();
            Map<String, String> reqSign = new TreeMap<>();
            reqOrder.put("tenantIdIgnore", query.getTenantIdIgnore());
            reqOrder.put("tenant_key", URLEncoder.encode(CommonConfig.appTenantId,"UTF-8"));
            //reqOrder.put("tenant_secret", CommonConfig.appTenantSecret);
//            reqOrder.put("starttime", URLEncoder.encode(DateUtil.format(query.getStarttime(),DateUtils.YYYY_MM_DD_HH_MM_SS),"UTF-8"));
//            reqOrder.put("endtime",URLEncoder.encode(DateUtil.format(query.getEndtime(),DateUtils.YYYY_MM_DD_HH_MM_SS),"UTF-8"));
            reqOrder.put("starttimeIgnore", URLEncoder.encode(query.getStarttimeIgnore(),"UTF-8"));
            reqOrder.put("endtimeIgnore",URLEncoder.encode(query.getEndtimeIgnore(),"UTF-8"));
            reqOrder.put("nonce_str",SHYPUtil.generateNonceStr());
            reqOrder.put("pageCurrentPage", String.valueOf(query.getPageCurrentPage()));
            reqOrder.put("pageSize", String.valueOf(query.getPageSize()));
            Long ts = SHYPUtil.getCurrentTimestamp();
            reqOrder.put("timestamp", ts.toString());
            String sign = SHYPUtil.generateSignature(reqOrder, CommonConfig.appTenantSecret, SHYPConstants.SignType.MD5);
            reqOrder.put("sign", sign);
            DDKJConfig config = new SinepharmConfig();
            MDMRequest request = new MDMRequest(config);
            String result = request.requestWithoutCert(SinepharmConstants.GETPRODUCT, ts.toString(), reqOrder, false, SHYPConstants.RequestMethod.POST);
            if(!Boolean.valueOf(JSONUtil.getByPath(JSONUtil.parse(result),"success").toString()))
            {
                return JsonResult.error(result);
            }
            ProductResult productResult = JSONObject.parseObject(result,ProductResult.class);
            MdmProduct mdmProduct;
            MdmProduct entity;
            Map<String,Object> param = new HashMap<>();
            //处理返回的第一页数据
            for(Product product :productResult.getData().getRecords()) {
                mdmProduct = new MdmProduct();
                //PropertyUtils.copyProperties(mdmProduct, product);
                BeanUtils.copyProperties(product,mdmProduct);
                entity = mdmProductMapper.selectOne(new QueryWrapper<MdmProduct>().eq("mdm_code",mdmProduct.getMdmCode()));
                if(entity!=null) {
                    param.clear();
                    param.put("mdm_code",entity.getMdmCode());
                    mdmProductRecordsMapper.deleteByMap(param);
                    mdmProduct.setMdmCode(entity.getMdmCode());
                    mdmProduct.setTs(DateUtils.now());
                    mdmProductMapper.update(mdmProduct,new UpdateWrapper<MdmProduct>().eq("mdm_code",entity.getMdmCode()));
                }
                else
                {
                    mdmProductMapper.insert(mdmProduct);
                }
                MdmProductRecords productRecords;
                for (ErpRecords record : product.getErpRecords()) {
                    productRecords = new MdmProductRecords();
                    BeanUtils.copyProperties(record,productRecords);
                    productRecords.setMdmCode(mdmProduct.getMdmCode());
                    mdmProductRecordsService.save(productRecords);
                }
            }

            //获取返回的总页数
            Integer totalPage = productResult.getData().getPages();
            //循环获取剩余数据
            for (int i = 2; i <=totalPage ; i++) {
                reqOrder.replace("pageCurrentPage",String.valueOf(i));
                ts = SHYPUtil.getCurrentTimestamp();
                reqOrder.replace("timestamp", ts.toString());
                sign = SHYPUtil.generateSignature(reqOrder, CommonConfig.appTenantSecret, SHYPConstants.SignType.MD5);
                reqOrder.replace("sign", sign);
                result = request.requestWithoutCert(SinepharmConstants.GETPRODUCT, ts.toString(), reqOrder, false, SHYPConstants.RequestMethod.POST);
                productResult = JSONObject.parseObject(result,ProductResult.class);
                for(Product product :productResult.getData().getRecords()) {
                    mdmProduct = new MdmProduct();
                    //PropertyUtils.copyProperties(mdmProduct, product);
                    BeanUtils.copyProperties(product,mdmProduct);
                    entity = mdmProductMapper.selectOne(new QueryWrapper<MdmProduct>().eq("mdm_code",mdmProduct.getMdmCode()));
                    if(entity!=null) {
                        param.clear();
                        param.put("mdm_code",entity.getMdmCode());
                        mdmProductRecordsMapper.deleteByMap(param);
                        mdmProduct.setMdmCode(entity.getMdmCode());
                        mdmProduct.setTs(DateUtils.now());
                        mdmProductMapper.update(mdmProduct,new UpdateWrapper<MdmProduct>().eq("mdm_code",entity.getMdmCode()));
                    }
                    else
                    {
                        mdmProductMapper.insert(mdmProduct);
                    }
                    MdmProductRecords productRecords;
                    for (ErpRecords record : product.getErpRecords()) {
                        productRecords = new MdmProductRecords();
                        BeanUtils.copyProperties(record,productRecords);
                        productRecords.setMdmCode(mdmProduct.getMdmCode());
                        mdmProductRecordsService.save(productRecords);
                    }
                }
            }

            return JsonResult.success(result);
        }
        catch(Exception ex)
        {
            return JsonResult.error(ex.getMessage());
        }
    }

    @Override
    @SpecifyDataSource(value = DataSourceType.MDM)
    public JsonResult SaveCustomer(MDMQueryParam query) {
        try {
            Map<String, String> reqOrder = new TreeMap<>();
            reqOrder.put("tenantIdIgnore", query.getTenantIdIgnore());
            reqOrder.put("tenant_key", URLEncoder.encode(CommonConfig.appTenantId,"UTF-8"));
            //reqOrder.put("tenant_secret", CommonConfig.appTenantSecret);
            //String temp = DateUtil.format(query.getEndtime(),DateUtils.YYYY_MM_DD_HH_MM_SS);
//            reqOrder.put("starttime", URLEncoder.encode(DateUtil.format(query.getStarttime(),DateUtils.YYYY_MM_DD_HH_MM_SS),"UTF-8"));
//            reqOrder.put("endtime",URLEncoder.encode(DateUtil.format(query.getEndtime(),DateUtils.YYYY_MM_DD_HH_MM_SS),"UTF-8"));
//            reqOrder.put("starttime", DateUtil.format(query.getStarttime(),DateUtils.YYYY_MM_DD_HH_MM_SS));
//            reqOrder.put("endtime",DateUtil.format(query.getEndtime(),DateUtils.YYYY_MM_DD_HH_MM_SS));
            reqOrder.put("starttimeIgnore", URLEncoder.encode(query.getStarttimeIgnore(),"UTF-8"));
            reqOrder.put("endtimeIgnore",URLEncoder.encode(query.getEndtimeIgnore(),"UTF-8"));
            reqOrder.put("nonce_str",SHYPUtil.generateNonceStr());
            reqOrder.put("pageCurrentPage", String.valueOf(query.getPageCurrentPage()));
            reqOrder.put("pageSize", String.valueOf(query.getPageSize()));
            Long ts = SHYPUtil.getCurrentTimestamp();
            reqOrder.put("timestamp", ts.toString());
            String sign = SHYPUtil.generateSignature(reqOrder, CommonConfig.appTenantSecret, SHYPConstants.SignType.MD5);
            reqOrder.put("sign", sign);
            DDKJConfig config = new SinepharmConfig();
            MDMRequest request = new MDMRequest(config);
            String result = request.requestWithoutCert(SinepharmConstants.GETCUSTOMER, ts.toString(), reqOrder, false, SHYPConstants.RequestMethod.POST);
            //result="{\"code\":200,\"success\":true,\"data\":{\"records\":[{\"id\":null,\"createUser\":\"1586592284594151426\",\"createDept\":null,\"createTime\":\"2023-06-21 18:04:32\",\"updateUser\":\"1586592284594151426\",\"updateTime\":\"2022-12-30 16:29:31\",\"status\":null,\"isDeleted\":null,\"groupMdmInvoiceDealer\":null,\"processInstanceInfo\":null,\"dataCode\":\"PHN00269784\",\"dataSource\":\"\",\"registerName\":\"国药控股湖北有限公司\",\"formerName\":\"\",\"registerProvinceName\":\"湖北省\",\"registerProvinceCode\":\"420000\",\"registerCityName\":\"武汉市\",\"registerCityCode\":\"420100\",\"registerCountyName\":\"洪山区\",\"registerCountyCode\":\"420111\",\"registerAddress\":\"武汉市东湖新技术开发区高新大道666号CRO办公区A19栋4-7层\",\"registerAddressLongitudeBaidu\":\"114.48142220123173\",\"registerAddressLatitudeBaidu\":\"30.490879843415514\",\"registerAddressLongitudeGaode\":\"114.472353\",\"registerAddressLatitudeGaode\":\"30.483658\",\"registerAddressMapRunError\":\"百度经纬度填充成功!,高德经纬度填充成功!\",\"registerAddressMapRunStatus\":\"success\",\"uniformSocialCreditCode\":\"9142010072612386XR\",\"attributeLevelFirst\":\"经销商\",\"attributeLevelSecond\":\"批发商\",\"attributeLevelThird\":\"批发商\",\"operationStatus\":\"valid\",\"humanSocialLevel\":\"\",\"humanSocialRank\":\"\",\"healthCommissionLevel\":\"\",\"healthCommissionCode\":\"\",\"clientAttribute\":\"商业\",\"clientLevel\":\"一级商\",\"clientClassify\":\"BP03\",\"remark\":\"update\",\"discountAgreementType\":\"\",\"ncSysCode\":\"10017425\",\"hongYingSysCode\":\"W002700164\",\"crmSysCode\":\"\",\"areaManagementSysCode\":\"\",\"mergeDestinationName\":\"\",\"mergeDestinationCode\":\"\",\"mergeTime\":null,\"isHis\":0,\"version\":1,\"mergeUser\":null,\"isValid\":0,\"isElectronicCommerce\":1,\"byProductIsElectronicJson\":null,\"isFlowOpen\":0,\"byProductIsFlowOpenJson\":null,\"chainHeadQuarterName\":\"国药控股股份有限公司\",\"chainHeadQuarterCode\":\"\",\"mergeUserName\":\"\",\"businessStatus\":\"\",\"operation\":\"导入\",\"addTime\":\"2022-06-30 20:29:40\",\"clientLevelUpdateTime\":null,\"addType\":3,\"businessPerson\":\"\",\"businessDept\":\"\",\"entityHongYingSysCode\":\"\",\"entityRegisterName\":\"\",\"generalHeadquarters\":\"\",\"branchOffice\":\"\",\"remarks\":\"\",\"generalHospital\":null,\"branchHospital\":null,\"industryCode\":null,\"industryName\":null,\"operationStatusName\":\"有效\",\"humanSocialLevelName\":null,\"humanSocialRankName\":null,\"healthCommissionLevelName\":null,\"clientClassifyName\":null,\"discountAgreementTypeName\":null,\"values\":null,\"invoiceNo\":null,\"processInstanceId\":null,\"isElectronicCommerceName\":\"否\",\"isFlowOpenName\":\"是\",\"businessStatusName\":null,\"customerName\":\"国药控股湖北有限公司\",\"memoricCode\":null,\"companyCode\":null,\"applyCompanyKey\":null,\"tenantName\":\"上海信谊联合医药药材有限公司信谊医药事业部（1529）/上海信谊天一药业有限公司（1173）\",\"tenantId\":\"1298/1529/1173\",\"updateUserName\":\"lzb\",\"hongYingList\":[{\"code\":\"W002700164\",\"createTime\":1656593905000}],\"ncList\":[{\"code\":\"10017425\",\"createTime\":1657697189000}],\"hospitalClassification\":null,\"mthHospMedicineIncome\":\"\",\"mthHospClinicAmount\":\"\",\"mthHospBunkAmount\":\"\",\"mthHospMicroEcologyQuantity\":\"\",\"mthDigestiveDeptClinicAmount\":\"\",\"mthPaediatricsDeptClinicAmount\":\"\",\"mthPaediatricsDeptInpatientAmount\":\"\",\"mthNeonatologyDeptClinicAmount\":\"\",\"mthNeonatologyDeptInpatientAmount\":\"\",\"mthDermatologyDeptClinicAmount\":\"\",\"existingScaleGrade\":null,\"distributionMark\":null,\"creditPositionGrade\":null,\"hasSaleTeamName\":null,\"groupProvincialPlatMark\":null,\"clientRelationGrade\":null,\"blazeSpecHospital\":null,\"mileStoneBificoCapsHosp\":null,\"mileStoneBificoPowderHosp\":null,\"mileStoneTaiersiHosp\":null,\"mileStoneRibavirinHosp\":null,\"mthDigestiveDeptInpatientAmount\":\"\",\"distributorLabel\":\"\",\"electronicCommerceLabel\":\"\",\"isExistInvoice\":null,\"isAddTerminal\":null,\"alias\":null,\"hisName\":\"\"},{\"id\":null,\"createUser\":\"1586592284594151426\",\"createDept\":null,\"createTime\":\"2023-06-21 18:04:42\",\"updateUser\":\"1586592284594151426\",\"updateTime\":\"2022-12-30 16:29:31\",\"status\":null,\"isDeleted\":null,\"groupMdmInvoiceDealer\":null,\"processInstanceInfo\":null,\"dataCode\":\"PHN00270265\",\"dataSource\":\"\",\"registerName\":\"云南省医药有限公司\",\"formerName\":\"\",\"registerProvinceName\":\"云南省\",\"registerProvinceCode\":\"530000\",\"registerCityName\":\"昆明市\",\"registerCityCode\":\"530100\",\"registerCountyName\":\"呈贡区\",\"registerCountyCode\":\"530114\",\"registerAddress\":\"云南省昆明市呈贡区月华街1819号\",\"registerAddressLongitudeBaidu\":\"102.84689000983312\",\"registerAddressLatitudeBaidu\":\"24.841213740463695\",\"registerAddressLongitudeGaode\":\"102.840463\",\"registerAddressLatitudeGaode\":\"24.834822\",\"registerAddressMapRunError\":\"百度经纬度填充成功!,高德经纬度填充成功!\",\"registerAddressMapRunStatus\":\"success\",\"uniformSocialCreditCode\":\"91530000734312206U\",\"attributeLevelFirst\":\"经销商\",\"attributeLevelSecond\":\"批发商\",\"attributeLevelThird\":\"批发商\",\"operationStatus\":\"valid\",\"humanSocialLevel\":\"\",\"humanSocialRank\":\"\",\"healthCommissionLevel\":\"\",\"healthCommissionCode\":\"\",\"clientAttribute\":\"商业\",\"clientLevel\":\"一级商\",\"clientClassify\":\"JX01\",\"remark\":\"update\",\"discountAgreementType\":\"\",\"ncSysCode\":\"10002160\",\"hongYingSysCode\":\"W087100001\",\"crmSysCode\":\"\",\"areaManagementSysCode\":\"\",\"mergeDestinationName\":\"\",\"mergeDestinationCode\":\"\",\"mergeTime\":null,\"isHis\":0,\"version\":1,\"mergeUser\":null,\"isValid\":0,\"isElectronicCommerce\":1,\"byProductIsElectronicJson\":null,\"isFlowOpen\":0,\"byProductIsFlowOpenJson\":null,\"chainHeadQuarterName\":\"云南白药集团股份有限公司\",\"chainHeadQuarterCode\":\"\",\"mergeUserName\":\"\",\"businessStatus\":\"\",\"operation\":\"导入\",\"addTime\":\"2022-06-30 20:29:41\",\"clientLevelUpdateTime\":null,\"addType\":3,\"businessPerson\":\"\",\"businessDept\":\"\",\"entityHongYingSysCode\":\"\",\"entityRegisterName\":\"\",\"generalHeadquarters\":\"\",\"branchOffice\":\"\",\"remarks\":\"\",\"generalHospital\":null,\"branchHospital\":null,\"industryCode\":null,\"industryName\":null,\"operationStatusName\":\"有效\",\"humanSocialLevelName\":null,\"humanSocialRankName\":null,\"healthCommissionLevelName\":null,\"clientClassifyName\":null,\"discountAgreementTypeName\":null,\"values\":null,\"invoiceNo\":null,\"processInstanceId\":null,\"isElectronicCommerceName\":\"否\",\"isFlowOpenName\":\"是\",\"businessStatusName\":null,\"customerName\":\"云南省医药有限公司\",\"memoricCode\":\"\",\"companyCode\":\"1528\",\"applyCompanyKey\":\"1297\",\"tenantName\":\"上海信谊联合医药药材有限公司信谊联合事业部（1528）/上海信谊联合医药药材有限公司信谊医药事业部（1529）/上海信谊天一药业有限公司（1173）\",\"tenantId\":\"1298/1529/1528/1173\",\"updateUserName\":\"lzb\",\"hongYingList\":[{\"code\":\"W087100001\",\"createTime\":1656593905000}],\"ncList\":[{\"code\":\"10002160\",\"createTime\":1657697198000}],\"hospitalClassification\":null,\"mthHospMedicineIncome\":\"\",\"mthHospClinicAmount\":\"\",\"mthHospBunkAmount\":\"\",\"mthHospMicroEcologyQuantity\":\"\",\"mthDigestiveDeptClinicAmount\":\"\",\"mthPaediatricsDeptClinicAmount\":\"\",\"mthPaediatricsDeptInpatientAmount\":\"\",\"mthNeonatologyDeptClinicAmount\":\"\",\"mthNeonatologyDeptInpatientAmount\":\"\",\"mthDermatologyDeptClinicAmount\":\"\",\"existingScaleGrade\":null,\"distributionMark\":null,\"creditPositionGrade\":null,\"hasSaleTeamName\":null,\"groupProvincialPlatMark\":null,\"clientRelationGrade\":null,\"blazeSpecHospital\":null,\"mileStoneBificoCapsHosp\":null,\"mileStoneBificoPowderHosp\":null,\"mileStoneTaiersiHosp\":null,\"mileStoneRibavirinHosp\":null,\"mthDigestiveDeptInpatientAmount\":\"\",\"distributorLabel\":\"\",\"electronicCommerceLabel\":\"\",\"isExistInvoice\":null,\"isAddTerminal\":null,\"alias\":null,\"hisName\":\"\"},{\"id\":null,\"createUser\":\"1586592284594151426\",\"createDept\":null,\"createTime\":\"2023-06-21 18:04:42\",\"updateUser\":\"1586592284594151426\",\"updateTime\":\"2022-12-30 16:29:31\",\"status\":null,\"isDeleted\":null,\"groupMdmInvoiceDealer\":null,\"processInstanceInfo\":null,\"dataCode\":\"PHN0048790\",\"dataSource\":\"\",\"registerName\":\"浙江震元股份有限公司\",\"formerName\":\"\",\"registerProvinceName\":\"浙江省\",\"registerProvinceCode\":\"330000\",\"registerCityName\":\"绍兴市\",\"registerCityCode\":\"330600\",\"registerCountyName\":\"越城区\",\"registerCountyCode\":\"330602\",\"registerAddress\":\"浙江省绍兴市稽山街道延安东路558号\",\"registerAddressLongitudeBaidu\":\"120.61893791674273\",\"registerAddressLatitudeBaidu\":\"29.994865725248748\",\"registerAddressLongitudeGaode\":\"120.612367\",\"registerAddressLatitudeGaode\":\"29.988568\",\"registerAddressMapRunError\":\"百度经纬度填充成功!,高德经纬度填充成功!\",\"registerAddressMapRunStatus\":\"success\",\"uniformSocialCreditCode\":\"\",\"attributeLevelFirst\":\"经销商\",\"attributeLevelSecond\":\"批发商\",\"attributeLevelThird\":\"批发商\",\"operationStatus\":\"valid\",\"humanSocialLevel\":\"\",\"humanSocialRank\":\"\",\"healthCommissionLevel\":\"\",\"healthCommissionCode\":\"\",\"clientAttribute\":\"商业\",\"clientLevel\":\"一级商\",\"clientClassify\":\"\",\"remark\":\"update\",\"discountAgreementType\":\"\",\"ncSysCode\":\"10002216\",\"hongYingSysCode\":\"W057500002\",\"crmSysCode\":\"\",\"areaManagementSysCode\":\"\",\"mergeDestinationName\":\"\",\"mergeDestinationCode\":\"\",\"mergeTime\":null,\"isHis\":0,\"version\":1,\"mergeUser\":null,\"isValid\":0,\"isElectronicCommerce\":1,\"byProductIsElectronicJson\":null,\"isFlowOpen\":0,\"byProductIsFlowOpenJson\":null,\"chainHeadQuarterName\":\"绍兴震元健康产业集团有限公司\",\"chainHeadQuarterCode\":\"\",\"mergeUserName\":\"\",\"businessStatus\":\"\",\"operation\":\"导入\",\"addTime\":\"2022-06-30 20:29:41\",\"clientLevelUpdateTime\":null,\"addType\":3,\"businessPerson\":\"\",\"businessDept\":\"\",\"entityHongYingSysCode\":\"\",\"entityRegisterName\":\"\",\"generalHeadquarters\":\"\",\"branchOffice\":\"\",\"remarks\":\"\",\"generalHospital\":null,\"branchHospital\":null,\"industryCode\":null,\"industryName\":null,\"operationStatusName\":\"有效\",\"humanSocialLevelName\":null,\"humanSocialRankName\":null,\"healthCommissionLevelName\":null,\"clientClassifyName\":null,\"discountAgreementTypeName\":null,\"values\":null,\"invoiceNo\":null,\"processInstanceId\":null,\"isElectronicCommerceName\":\"否\",\"isFlowOpenName\":\"是\",\"businessStatusName\":null,\"customerName\":\"浙江震元股份有限公司\",\"memoricCode\":\"130411071\",\"companyCode\":null,\"applyCompanyKey\":null,\"tenantName\":\"上海信谊联合医药药材有限公司信谊医药事业部（1529）/上海信谊天一药业有限公司（1173）\",\"tenantId\":\"1298/1529/1173\",\"updateUserName\":\"lzb\",\"hongYingList\":[{\"code\":\"W057500002\",\"createTime\":1663828538000}],\"ncList\":[{\"code\":\"10002216\",\"createTime\":1657697157000}],\"hospitalClassification\":null,\"mthHospMedicineIncome\":\"\",\"mthHospClinicAmount\":\"\",\"mthHospBunkAmount\":\"\",\"mthHospMicroEcologyQuantity\":\"\",\"mthDigestiveDeptClinicAmount\":\"\",\"mthPaediatricsDeptClinicAmount\":\"\",\"mthPaediatricsDeptInpatientAmount\":\"\",\"mthNeonatologyDeptClinicAmount\":\"\",\"mthNeonatologyDeptInpatientAmount\":\"\",\"mthDermatologyDeptClinicAmount\":\"\",\"existingScaleGrade\":null,\"distributionMark\":null,\"creditPositionGrade\":null,\"hasSaleTeamName\":null,\"groupProvincialPlatMark\":null,\"clientRelationGrade\":null,\"blazeSpecHospital\":null,\"mileStoneBificoCapsHosp\":null,\"mileStoneBificoPowderHosp\":null,\"mileStoneTaiersiHosp\":null,\"mileStoneRibavirinHosp\":null,\"mthDigestiveDeptInpatientAmount\":\"\",\"distributorLabel\":\"\",\"electronicCommerceLabel\":\"\",\"isExistInvoice\":null,\"isAddTerminal\":null,\"alias\":null,\"hisName\":\"\"},{\"id\":null,\"createUser\":\"1586592284594151426\",\"createDept\":null,\"createTime\":\"2023-06-21 18:04:42\",\"updateUser\":\"1586592284594151426\",\"updateTime\":\"2022-12-30 16:29:31\",\"status\":null,\"isDeleted\":null,\"groupMdmInvoiceDealer\":null,\"processInstanceInfo\":null,\"dataCode\":\"PHN0051164\",\"dataSource\":\"\",\"registerName\":\"浙江恩泽医药有限公司\",\"formerName\":\"\",\"registerProvinceName\":\"浙江省\",\"registerProvinceCode\":\"330000\",\"registerCityName\":\"台州市\",\"registerCityCode\":\"331000\",\"registerCountyName\":\"临海市\",\"registerCountyCode\":\"331082\",\"registerAddress\":\"浙江省台州市临海市古城街道回浦路92号\",\"registerAddressLongitudeBaidu\":\"121.12511339855743\",\"registerAddressLatitudeBaidu\":\"28.851339928452636\",\"registerAddressLongitudeGaode\":\"121.118865\",\"registerAddressLatitudeGaode\":\"28.848588\",\"registerAddressMapRunError\":\"百度经纬度填充成功!,高德经纬度填充成功!\",\"registerAddressMapRunStatus\":\"success\",\"uniformSocialCreditCode\":\"91331082771905908L\",\"attributeLevelFirst\":\"经销商\",\"attributeLevelSecond\":\"批发商\",\"attributeLevelThird\":\"批发商\",\"operationStatus\":\"valid\",\"humanSocialLevel\":\"\",\"humanSocialRank\":\"\",\"healthCommissionLevel\":\"\",\"healthCommissionCode\":\"\",\"clientAttribute\":\"商业\",\"clientLevel\":\"一级商\",\"clientClassify\":\"BP02\",\"remark\":\"update\",\"discountAgreementType\":\"\",\"ncSysCode\":\"10012404\",\"hongYingSysCode\":\"W057600030\",\"crmSysCode\":\"\",\"areaManagementSysCode\":\"\",\"mergeDestinationName\":\"\",\"mergeDestinationCode\":\"\",\"mergeTime\":null,\"isHis\":0,\"version\":1,\"mergeUser\":null,\"isValid\":0,\"isElectronicCommerce\":1,\"byProductIsElectronicJson\":null,\"isFlowOpen\":0,\"byProductIsFlowOpenJson\":null,\"chainHeadQuarterName\":\"台州恩泽医疗中心（集团）\",\"chainHeadQuarterCode\":\"\",\"mergeUserName\":\"\",\"businessStatus\":\"\",\"operation\":\"导入\",\"addTime\":\"2022-06-30 20:29:41\",\"clientLevelUpdateTime\":null,\"addType\":3,\"businessPerson\":\"\",\"businessDept\":\"\",\"entityHongYingSysCode\":\"\",\"entityRegisterName\":\"\",\"generalHeadquarters\":\"\",\"branchOffice\":\"\",\"remarks\":\"\",\"generalHospital\":null,\"branchHospital\":null,\"industryCode\":null,\"industryName\":null,\"operationStatusName\":\"有效\",\"humanSocialLevelName\":null,\"humanSocialRankName\":null,\"healthCommissionLevelName\":null,\"clientClassifyName\":null,\"discountAgreementTypeName\":null,\"values\":null,\"invoiceNo\":null,\"processInstanceId\":null,\"isElectronicCommerceName\":\"否\",\"isFlowOpenName\":\"是\",\"businessStatusName\":null,\"customerName\":\"浙江恩泽医药有限公司\",\"memoricCode\":null,\"companyCode\":null,\"applyCompanyKey\":null,\"tenantName\":\"上海信谊联合医药药材有限公司信谊医药事业部（1529）/上海信谊天一药业有限公司（1173）\",\"tenantId\":\"1298/1529/1173\",\"updateUserName\":\"lzb\",\"hongYingList\":[{\"code\":\"W057600030\",\"createTime\":1656593905000}],\"ncList\":[{\"code\":\"10012404\",\"createTime\":1657697167000}],\"hospitalClassification\":null,\"mthHospMedicineIncome\":\"\",\"mthHospClinicAmount\":\"\",\"mthHospBunkAmount\":\"\",\"mthHospMicroEcologyQuantity\":\"\",\"mthDigestiveDeptClinicAmount\":\"\",\"mthPaediatricsDeptClinicAmount\":\"\",\"mthPaediatricsDeptInpatientAmount\":\"\",\"mthNeonatologyDeptClinicAmount\":\"\",\"mthNeonatologyDeptInpatientAmount\":\"\",\"mthDermatologyDeptClinicAmount\":\"\",\"existingScaleGrade\":null,\"distributionMark\":null,\"creditPositionGrade\":null,\"hasSaleTeamName\":null,\"groupProvincialPlatMark\":null,\"clientRelationGrade\":null,\"blazeSpecHospital\":null,\"mileStoneBificoCapsHosp\":null,\"mileStoneBificoPowderHosp\":null,\"mileStoneTaiersiHosp\":null,\"mileStoneRibavirinHosp\":null,\"mthDigestiveDeptInpatientAmount\":\"\",\"distributorLabel\":\"\",\"electronicCommerceLabel\":\"\",\"isExistInvoice\":null,\"isAddTerminal\":null,\"alias\":null,\"hisName\":\"\"},{\"id\":null,\"createUser\":\"1586592284594151426\",\"createDept\":null,\"createTime\":\"2023-06-21 18:04:32\",\"updateUser\":\"1586592284594151426\",\"updateTime\":\"2023-03-23 10:02:12\",\"status\":null,\"isDeleted\":null,\"groupMdmInvoiceDealer\":null,\"processInstanceInfo\":null,\"dataCode\":\"PHN0079466\",\"dataSource\":\"\",\"registerName\":\"京东医药（北京）有限公司\",\"formerName\":\"北京佳康医药有限责任公司\",\"registerProvinceName\":\"北京市\",\"registerProvinceCode\":\"110000\",\"registerCityName\":\"北京市\",\"registerCityCode\":\"110000\",\"registerCountyName\":\"大兴区\",\"registerCountyCode\":\"110115\",\"registerAddress\":\"北京市北京经济技术开发区科创十一街20号院3号楼11层1102室（北京自贸试验区高端产业片区亦庄组团）\",\"registerAddressLongitudeBaidu\":\"116.56140809600717\",\"registerAddressLatitudeBaidu\":\"39.78335744917434\",\"registerAddressLongitudeGaode\":\"116.503556\",\"registerAddressLatitudeGaode\":\"39.803790\",\"registerAddressMapRunError\":\"百度经纬度填充成功!,高德经纬度填充成功!\",\"registerAddressMapRunStatus\":\"success\",\"uniformSocialCreditCode\":\"91110101101338211U\",\"attributeLevelFirst\":\"经销商\",\"attributeLevelSecond\":\"批发商\",\"attributeLevelThird\":\"批发商\",\"operationStatus\":\"valid\",\"humanSocialLevel\":\"\",\"humanSocialRank\":\"\",\"healthCommissionLevel\":\"\",\"healthCommissionCode\":\"\",\"clientAttribute\":\"商业\",\"clientLevel\":\"二级商及以下\",\"clientClassify\":\"\",\"remark\":\"update\",\"discountAgreementType\":\"\",\"ncSysCode\":\"10009528\",\"hongYingSysCode\":\"P001009335/P001008428/T001009251\",\"crmSysCode\":\"\",\"areaManagementSysCode\":\"\",\"mergeDestinationName\":\"\",\"mergeDestinationCode\":\"\",\"mergeTime\":null,\"isHis\":0,\"version\":1,\"mergeUser\":null,\"isValid\":0,\"isElectronicCommerce\":1,\"byProductIsElectronicJson\":null,\"isFlowOpen\":0,\"byProductIsFlowOpenJson\":null,\"chainHeadQuarterName\":\"北京京东健康有限公司\",\"chainHeadQuarterCode\":\"\",\"mergeUserName\":\"\",\"businessStatus\":\"\",\"operation\":\"导入\",\"addTime\":\"2022-06-30 20:29:42\",\"clientLevelUpdateTime\":\"2023-03-23 10:02:11\",\"addType\":3,\"businessPerson\":\"\",\"businessDept\":\"\",\"entityHongYingSysCode\":\"\",\"entityRegisterName\":\"\",\"generalHeadquarters\":\"\",\"branchOffice\":\"\",\"remarks\":\"\",\"generalHospital\":null,\"branchHospital\":null,\"industryCode\":null,\"industryName\":null,\"operationStatusName\":\"有效\",\"humanSocialLevelName\":null,\"humanSocialRankName\":null,\"healthCommissionLevelName\":null,\"clientClassifyName\":null,\"discountAgreementTypeName\":null,\"values\":null,\"invoiceNo\":null,\"processInstanceId\":null,\"isElectronicCommerceName\":\"否\",\"isFlowOpenName\":\"是\",\"businessStatusName\":null,\"customerName\":\"京东医药（北京）有限公司\",\"memoricCode\":\"\",\"companyCode\":\"1734\",\"applyCompanyKey\":\"1373\",\"tenantName\":\"上海信谊联合医药药材有限公司信谊联合事业部（1528）/上海信谊联合医药药材有限公司信谊医药事业部（1529）/上海信谊联合医药药材有限公司新零售事业部（1734）/上海信谊天一药业有限公司（1173）\",\"tenantId\":\"1298/1297/1529/1528/1173/1734\",\"updateUserName\":\"lzb\",\"hongYingList\":[{\"code\":\"P001009335\",\"createTime\":1656593905000},{\"code\":\"P001008428\",\"createTime\":1656593905000},{\"code\":\"T001009251\",\"createTime\":1656593905000}],\"ncList\":[{\"code\":\"10009528\",\"createTime\":1657697137000}],\"hospitalClassification\":null,\"mthHospMedicineIncome\":\"\",\"mthHospClinicAmount\":\"\",\"mthHospBunkAmount\":\"\",\"mthHospMicroEcologyQuantity\":\"\",\"mthDigestiveDeptClinicAmount\":\"\",\"mthPaediatricsDeptClinicAmount\":\"\",\"mthPaediatricsDeptInpatientAmount\":\"\",\"mthNeonatologyDeptClinicAmount\":\"\",\"mthNeonatologyDeptInpatientAmount\":\"\",\"mthDermatologyDeptClinicAmount\":\"\",\"existingScaleGrade\":null,\"distributionMark\":null,\"creditPositionGrade\":null,\"hasSaleTeamName\":null,\"groupProvincialPlatMark\":null,\"clientRelationGrade\":null,\"blazeSpecHospital\":null,\"mileStoneBificoCapsHosp\":null,\"mileStoneBificoPowderHosp\":null,\"mileStoneTaiersiHosp\":null,\"mileStoneRibavirinHosp\":null,\"mthDigestiveDeptInpatientAmount\":\"\",\"distributorLabel\":\"\",\"electronicCommerceLabel\":\"\",\"isExistInvoice\":null,\"isAddTerminal\":null,\"alias\":null,\"hisName\":\"\"},{\"id\":null,\"createUser\":\"1546338837845499906\",\"createDept\":null,\"createTime\":\"2022-11-10 15:34:59\",\"updateUser\":\"1546338837845499906\",\"updateTime\":\"2023-05-22 00:00:00\",\"status\":null,\"isDeleted\":null,\"groupMdmInvoiceDealer\":null,\"processInstanceInfo\":null,\"dataCode\":\"PHN0144841\",\"dataSource\":null,\"registerName\":\"扎兰屯市北方医药有限责任公司\",\"formerName\":\"\",\"registerProvinceName\":\"内蒙古自治区\",\"registerProvinceCode\":\"150000\",\"registerCityName\":\"呼伦贝尔市\",\"registerCityCode\":\"150700\",\"registerCountyName\":\"扎兰屯市\",\"registerCountyCode\":\"150783\",\"registerAddress\":\"扎兰屯市葛根路113—114号\",\"registerAddressLongitudeBaidu\":\"122.74558268725775\",\"registerAddressLatitudeBaidu\":\"48.01387332748418\",\"registerAddressLongitudeGaode\":\"122.739579\",\"registerAddressLatitudeGaode\":\"48.007736\",\"registerAddressMapRunError\":\"百度经纬度填充成功!,高德经纬度填充成功!\",\"registerAddressMapRunStatus\":\"success\",\"uniformSocialCreditCode\":\"91150783747902336R\",\"attributeLevelFirst\":\"经销商\",\"attributeLevelSecond\":\"批发商\",\"attributeLevelThird\":\"批发商\",\"operationStatus\":\"valid\",\"humanSocialLevel\":\"\",\"humanSocialRank\":\"\",\"healthCommissionLevel\":\"\",\"healthCommissionCode\":\"\",\"clientAttribute\":\"商业\",\"clientLevel\":\"一级商\",\"clientClassify\":\"\",\"remark\":\"\",\"discountAgreementType\":\"\",\"ncSysCode\":\"10105525\",\"hongYingSysCode\":\"D047004001\",\"crmSysCode\":\"\",\"areaManagementSysCode\":\"\",\"mergeDestinationName\":\"\",\"mergeDestinationCode\":\"\",\"mergeTime\":null,\"isHis\":0,\"version\":1,\"mergeUser\":null,\"isValid\":0,\"isElectronicCommerce\":1,\"byProductIsElectronicJson\":null,\"isFlowOpen\":0,\"byProductIsFlowOpenJson\":null,\"chainHeadQuarterName\":\"\",\"chainHeadQuarterCode\":\"\",\"mergeUserName\":\"\",\"businessStatus\":\"\",\"operation\":\"导入\",\"addTime\":\"2022-06-30 20:29:41\",\"clientLevelUpdateTime\":null,\"addType\":3,\"businessPerson\":null,\"businessDept\":null,\"entityHongYingSysCode\":null,\"entityRegisterName\":null,\"generalHeadquarters\":null,\"branchOffice\":null,\"remarks\":null,\"generalHospital\":null,\"branchHospital\":null,\"industryCode\":null,\"industryName\":null,\"operationStatusName\":\"有效\",\"humanSocialLevelName\":null,\"humanSocialRankName\":null,\"healthCommissionLevelName\":null,\"clientClassifyName\":null,\"discountAgreementTypeName\":null,\"values\":null,\"invoiceNo\":null,\"processInstanceId\":null,\"isElectronicCommerceName\":\"否\",\"isFlowOpenName\":\"是\",\"businessStatusName\":null,\"customerName\":\"扎兰屯市北方医药有限责任公司\",\"memoricCode\":\"\",\"companyCode\":\"1173\",\"applyCompanyKey\":\"1008\",\"tenantName\":\"上海信谊联合医药药材有限公司信谊医药事业部（1529）/上海信谊天一药业有限公司（1173）\",\"tenantId\":\"1298/1529/1173\",\"updateUserName\":\"潘亚男\",\"hongYingList\":[{\"code\":\"D047004001\",\"createTime\":1656593905000}],\"ncList\":[{\"code\":\"10105525\",\"createTime\":1657697143000}],\"hospitalClassification\":null,\"mthHospMedicineIncome\":\"\",\"mthHospClinicAmount\":\"\",\"mthHospBunkAmount\":\"\",\"mthHospMicroEcologyQuantity\":\"\",\"mthDigestiveDeptClinicAmount\":\"\",\"mthPaediatricsDeptClinicAmount\":\"\",\"mthPaediatricsDeptInpatientAmount\":\"\",\"mthNeonatologyDeptClinicAmount\":\"\",\"mthNeonatologyDeptInpatientAmount\":\"\",\"mthDermatologyDeptClinicAmount\":\"\",\"existingScaleGrade\":null,\"distributionMark\":null,\"creditPositionGrade\":null,\"hasSaleTeamName\":null,\"groupProvincialPlatMark\":null,\"clientRelationGrade\":null,\"blazeSpecHospital\":null,\"mileStoneBificoCapsHosp\":null,\"mileStoneBificoPowderHosp\":null,\"mileStoneTaiersiHosp\":null,\"mileStoneRibavirinHosp\":null,\"mthDigestiveDeptInpatientAmount\":\"\",\"distributorLabel\":\"\",\"electronicCommerceLabel\":\"\",\"isExistInvoice\":null,\"isAddTerminal\":null,\"alias\":null,\"hisName\":\"\"},{\"id\":null,\"createUser\":\"1586592284594151426\",\"createDept\":null,\"createTime\":\"2023-06-21 18:04:32\",\"updateUser\":\"1586592284594151426\",\"updateTime\":\"2022-12-30 16:29:31\",\"status\":null,\"isDeleted\":null,\"groupMdmInvoiceDealer\":null,\"processInstanceInfo\":null,\"dataCode\":\"PHN0165705\",\"dataSource\":\"\",\"registerName\":\"山东瑞朗医药股份有限公司\",\"formerName\":\"山东瑞朗医药经营有限公司/利群集团青岛医药经营有限公司\",\"registerProvinceName\":\"山东省\",\"registerProvinceCode\":\"370000\",\"registerCityName\":\"青岛市\",\"registerCityCode\":\"370200\",\"registerCountyName\":\"市北区\",\"registerCountyCode\":\"370203\",\"registerAddress\":\"山东省青岛市市北区合肥路686号甲\",\"registerAddressLongitudeBaidu\":\"120.42086255686108\",\"registerAddressLatitudeBaidu\":\"36.118579716626758\",\"registerAddressLongitudeGaode\":\"120.414595\",\"registerAddressLatitudeGaode\":\"36.113476\",\"registerAddressMapRunError\":\"百度经纬度填充成功!,高德经纬度填充成功!\",\"registerAddressMapRunStatus\":\"success\",\"uniformSocialCreditCode\":\"913702006612609186\",\"attributeLevelFirst\":\"经销商\",\"attributeLevelSecond\":\"批发商\",\"attributeLevelThird\":\"批发商\",\"operationStatus\":\"valid\",\"humanSocialLevel\":\"\",\"humanSocialRank\":\"\",\"healthCommissionLevel\":\"\",\"healthCommissionCode\":\"\",\"clientAttribute\":\"商业\",\"clientLevel\":\"一级商\",\"clientClassify\":\"\",\"remark\":\"update\",\"discountAgreementType\":\"\",\"ncSysCode\":\"10091717\",\"hongYingSysCode\":\"W053100050\",\"crmSysCode\":\"\",\"areaManagementSysCode\":\"\",\"mergeDestinationName\":\"\",\"mergeDestinationCode\":\"\",\"mergeTime\":null,\"isHis\":0,\"version\":1,\"mergeUser\":null,\"isValid\":0,\"isElectronicCommerce\":1,\"byProductIsElectronicJson\":null,\"isFlowOpen\":0,\"byProductIsFlowOpenJson\":null,\"chainHeadQuarterName\":\"利群集团股份有限公司\",\"chainHeadQuarterCode\":\"\",\"mergeUserName\":\"\",\"businessStatus\":\"\",\"operation\":\"导入\",\"addTime\":\"2022-06-30 20:29:41\",\"clientLevelUpdateTime\":null,\"addType\":3,\"businessPerson\":\"\",\"businessDept\":\"\",\"entityHongYingSysCode\":\"\",\"entityRegisterName\":\"\",\"generalHeadquarters\":\"\",\"branchOffice\":\"\",\"remarks\":\"\",\"generalHospital\":null,\"branchHospital\":null,\"industryCode\":null,\"industryName\":null,\"operationStatusName\":\"有效\",\"humanSocialLevelName\":null,\"humanSocialRankName\":null,\"healthCommissionLevelName\":null,\"clientClassifyName\":null,\"discountAgreementTypeName\":null,\"values\":null,\"invoiceNo\":null,\"processInstanceId\":null,\"isElectronicCommerceName\":\"否\",\"isFlowOpenName\":\"是\",\"businessStatusName\":null,\"customerName\":\"山东瑞朗医药股份有限公司\",\"memoricCode\":null,\"companyCode\":null,\"applyCompanyKey\":null,\"tenantName\":\"上海信谊联合医药药材有限公司信谊医药事业部（1529）/上海信谊天一药业有限公司（1173）\",\"tenantId\":\"1298/1529/1173\",\"updateUserName\":\"lzb\",\"hongYingList\":[{\"code\":\"W053100050\",\"createTime\":1656593905000}],\"ncList\":[{\"code\":\"10091717\",\"createTime\":1657697176000}],\"hospitalClassification\":null,\"mthHospMedicineIncome\":\"\",\"mthHospClinicAmount\":\"\",\"mthHospBunkAmount\":\"\",\"mthHospMicroEcologyQuantity\":\"\",\"mthDigestiveDeptClinicAmount\":\"\",\"mthPaediatricsDeptClinicAmount\":\"\",\"mthPaediatricsDeptInpatientAmount\":\"\",\"mthNeonatologyDeptClinicAmount\":\"\",\"mthNeonatologyDeptInpatientAmount\":\"\",\"mthDermatologyDeptClinicAmount\":\"\",\"existingScaleGrade\":null,\"distributionMark\":null,\"creditPositionGrade\":null,\"hasSaleTeamName\":null,\"groupProvincialPlatMark\":null,\"clientRelationGrade\":null,\"blazeSpecHospital\":null,\"mileStoneBificoCapsHosp\":null,\"mileStoneBificoPowderHosp\":null,\"mileStoneTaiersiHosp\":null,\"mileStoneRibavirinHosp\":null,\"mthDigestiveDeptInpatientAmount\":\"\",\"distributorLabel\":\"\",\"electronicCommerceLabel\":\"\",\"isExistInvoice\":null,\"isAddTerminal\":null,\"alias\":null,\"hisName\":\"\"},{\"id\":null,\"createUser\":\"1586592284594151426\",\"createDept\":null,\"createTime\":\"2023-06-21 18:04:32\",\"updateUser\":\"1586592284594151426\",\"updateTime\":\"2022-12-30 16:29:31\",\"status\":null,\"isDeleted\":null,\"groupMdmInvoiceDealer\":null,\"processInstanceInfo\":null,\"dataCode\":\"PHN0216589\",\"dataSource\":\"\",\"registerName\":\"湖南津湘药业有限公司\",\"formerName\":\"\",\"registerProvinceName\":\"湖南省\",\"registerProvinceCode\":\"430000\",\"registerCityName\":\"益阳市\",\"registerCityCode\":\"430900\",\"registerCountyName\":\"赫山区\",\"registerCountyCode\":\"430903\",\"registerAddress\":\"益阳市赫山区迎宾东路269号\",\"registerAddressLongitudeBaidu\":\"112.40797743626908\",\"registerAddressLatitudeBaidu\":\"28.53937044214738\",\"registerAddressLongitudeGaode\":\"112.391334\",\"registerAddressLatitudeGaode\":\"28.535186\",\"registerAddressMapRunError\":\"百度经纬度填充成功!,高德经纬度填充成功!\",\"registerAddressMapRunStatus\":\"success\",\"uniformSocialCreditCode\":\"91430900593298075J\",\"attributeLevelFirst\":\"经销商\",\"attributeLevelSecond\":\"批发商\",\"attributeLevelThird\":\"批发商\",\"operationStatus\":\"valid\",\"humanSocialLevel\":\"\",\"humanSocialRank\":\"\",\"healthCommissionLevel\":\"\",\"healthCommissionCode\":\"\",\"clientAttribute\":\"商业\",\"clientLevel\":\"一级商\",\"clientClassify\":\"\",\"remark\":\"update\",\"discountAgreementType\":\"\",\"ncSysCode\":\"10021395\",\"hongYingSysCode\":\"W073700003\",\"crmSysCode\":\"\",\"areaManagementSysCode\":\"\",\"mergeDestinationName\":\"\",\"mergeDestinationCode\":\"\",\"mergeTime\":null,\"isHis\":0,\"version\":1,\"mergeUser\":null,\"isValid\":0,\"isElectronicCommerce\":1,\"byProductIsElectronicJson\":null,\"isFlowOpen\":0,\"byProductIsFlowOpenJson\":null,\"chainHeadQuarterName\":\"湖南千津湘韵实业发展有限公司\",\"chainHeadQuarterCode\":\"\",\"mergeUserName\":\"\",\"businessStatus\":\"\",\"operation\":\"导入\",\"addTime\":\"2022-06-30 20:29:41\",\"clientLevelUpdateTime\":null,\"addType\":3,\"businessPerson\":\"\",\"businessDept\":\"\",\"entityHongYingSysCode\":\"\",\"entityRegisterName\":\"\",\"generalHeadquarters\":\"\",\"branchOffice\":\"\",\"remarks\":\"\",\"generalHospital\":null,\"branchHospital\":null,\"industryCode\":null,\"industryName\":null,\"operationStatusName\":\"有效\",\"humanSocialLevelName\":null,\"humanSocialRankName\":null,\"healthCommissionLevelName\":null,\"clientClassifyName\":null,\"discountAgreementTypeName\":null,\"values\":null,\"invoiceNo\":null,\"processInstanceId\":null,\"isElectronicCommerceName\":\"否\",\"isFlowOpenName\":\"是\",\"businessStatusName\":null,\"customerName\":\"湖南津湘药业有限公司\",\"memoricCode\":null,\"companyCode\":null,\"applyCompanyKey\":null,\"tenantName\":\"上海信谊联合医药药材有限公司信谊医药事业部（1529）/上海信谊天一药业有限公司（1173）\",\"tenantId\":\"1298/1529/1173\",\"updateUserName\":\"lzb\",\"hongYingList\":[{\"code\":\"W073700003\",\"createTime\":1656593905000}],\"ncList\":[{\"code\":\"10021395\",\"createTime\":1657697188000}],\"hospitalClassification\":null,\"mthHospMedicineIncome\":\"\",\"mthHospClinicAmount\":\"\",\"mthHospBunkAmount\":\"\",\"mthHospMicroEcologyQuantity\":\"\",\"mthDigestiveDeptClinicAmount\":\"\",\"mthPaediatricsDeptClinicAmount\":\"\",\"mthPaediatricsDeptInpatientAmount\":\"\",\"mthNeonatologyDeptClinicAmount\":\"\",\"mthNeonatologyDeptInpatientAmount\":\"\",\"mthDermatologyDeptClinicAmount\":\"\",\"existingScaleGrade\":null,\"distributionMark\":null,\"creditPositionGrade\":null,\"hasSaleTeamName\":null,\"groupProvincialPlatMark\":null,\"clientRelationGrade\":null,\"blazeSpecHospital\":null,\"mileStoneBificoCapsHosp\":null,\"mileStoneBificoPowderHosp\":null,\"mileStoneTaiersiHosp\":null,\"mileStoneRibavirinHosp\":null,\"mthDigestiveDeptInpatientAmount\":\"\",\"distributorLabel\":\"\",\"electronicCommerceLabel\":\"\",\"isExistInvoice\":null,\"isAddTerminal\":null,\"alias\":null,\"hisName\":\"\"},{\"id\":null,\"createUser\":\"1586592284594151426\",\"createDept\":null,\"createTime\":\"2023-06-21 17:47:18\",\"updateUser\":\"1586592284594151426\",\"updateTime\":\"2022-12-06 21:27:49\",\"status\":null,\"isDeleted\":null,\"groupMdmInvoiceDealer\":null,\"processInstanceInfo\":null,\"dataCode\":\"PHN0273254\",\"dataSource\":\"\",\"registerName\":\"国药控股鄂尔多斯市有限公司\",\"formerName\":\"\",\"registerProvinceName\":\"内蒙古自治区\",\"registerProvinceCode\":\"150000\",\"registerCityName\":\"鄂尔多斯市\",\"registerCityCode\":\"150600\",\"registerCountyName\":\"达拉特旗\",\"registerCountyCode\":\"150621\",\"registerAddress\":\"鄂尔多斯市达拉特旗三垧梁工业园区纬二路以南\",\"registerAddressLongitudeBaidu\":\"110.0523194283497\",\"registerAddressLatitudeBaidu\":\"40.320672351087\",\"registerAddressLongitudeGaode\":\"110.032737\",\"registerAddressLatitudeGaode\":\"40.309257\",\"registerAddressMapRunError\":\"百度经纬度填充成功!,高德经纬度填充成功!\",\"registerAddressMapRunStatus\":\"success\",\"uniformSocialCreditCode\":\"911506213530435752\",\"attributeLevelFirst\":\"经销商\",\"attributeLevelSecond\":\"批发商\",\"attributeLevelThird\":\"批发商\",\"operationStatus\":\"valid\",\"humanSocialLevel\":\"\",\"humanSocialRank\":\"\",\"healthCommissionLevel\":\"\",\"healthCommissionCode\":\"\",\"clientAttribute\":\"商业\",\"clientLevel\":\"一级商\",\"clientClassify\":\"\",\"remark\":\"update\",\"discountAgreementType\":\"\",\"ncSysCode\":\"20067348\",\"hongYingSysCode\":\"D047704013\",\"crmSysCode\":\"\",\"areaManagementSysCode\":\"\",\"mergeDestinationName\":\"\",\"mergeDestinationCode\":\"\",\"mergeTime\":null,\"isHis\":0,\"version\":1,\"mergeUser\":null,\"isValid\":0,\"isElectronicCommerce\":1,\"byProductIsElectronicJson\":null,\"isFlowOpen\":0,\"byProductIsFlowOpenJson\":null,\"chainHeadQuarterName\":\"国药控股内蒙古有限公司\",\"chainHeadQuarterCode\":\"\",\"mergeUserName\":\"\",\"businessStatus\":\"\",\"operation\":\"导入\",\"addTime\":\"2022-06-30 20:29:41\",\"clientLevelUpdateTime\":null,\"addType\":3,\"businessPerson\":\"\",\"businessDept\":\"\",\"entityHongYingSysCode\":\"\",\"entityRegisterName\":\"\",\"generalHeadquarters\":\"\",\"branchOffice\":\"\",\"remarks\":\"\",\"generalHospital\":null,\"branchHospital\":null,\"industryCode\":null,\"industryName\":null,\"operationStatusName\":\"有效\",\"humanSocialLevelName\":null,\"humanSocialRankName\":null,\"healthCommissionLevelName\":null,\"clientClassifyName\":null,\"discountAgreementTypeName\":null,\"values\":null,\"invoiceNo\":null,\"processInstanceId\":null,\"isElectronicCommerceName\":\"否\",\"isFlowOpenName\":\"是\",\"businessStatusName\":null,\"customerName\":\"国药控股鄂尔多斯市有限公司\",\"memoricCode\":\"\",\"companyCode\":\"1173\",\"applyCompanyKey\":\"1008\",\"tenantName\":\"上海信谊联合医药药材有限公司信谊医药事业部（1529）/上海信谊天一药业有限公司（1173）\",\"tenantId\":\"1298/1529/1173\",\"updateUserName\":\"lzb\",\"hongYingList\":[{\"code\":\"D047704013\",\"createTime\":1656593905000}],\"ncList\":[{\"code\":\"20067348\",\"createTime\":1657697144000}],\"hospitalClassification\":null,\"mthHospMedicineIncome\":\"\",\"mthHospClinicAmount\":\"\",\"mthHospBunkAmount\":\"\",\"mthHospMicroEcologyQuantity\":\"\",\"mthDigestiveDeptClinicAmount\":\"\",\"mthPaediatricsDeptClinicAmount\":\"\",\"mthPaediatricsDeptInpatientAmount\":\"\",\"mthNeonatologyDeptClinicAmount\":\"\",\"mthNeonatologyDeptInpatientAmount\":\"\",\"mthDermatologyDeptClinicAmount\":\"\",\"existingScaleGrade\":null,\"distributionMark\":null,\"creditPositionGrade\":null,\"hasSaleTeamName\":null,\"groupProvincialPlatMark\":null,\"clientRelationGrade\":null,\"blazeSpecHospital\":null,\"mileStoneBificoCapsHosp\":null,\"mileStoneBificoPowderHosp\":null,\"mileStoneTaiersiHosp\":null,\"mileStoneRibavirinHosp\":null,\"mthDigestiveDeptInpatientAmount\":\"\",\"distributorLabel\":\"\",\"electronicCommerceLabel\":\"\",\"isExistInvoice\":null,\"isAddTerminal\":null,\"alias\":null,\"hisName\":\"\"},{\"id\":null,\"createUser\":\"1586592284594151426\",\"createDept\":null,\"createTime\":\"2023-06-21 18:04:42\",\"updateUser\":\"1586592284594151426\",\"updateTime\":\"2023-02-14 17:48:52\",\"status\":null,\"isDeleted\":null,\"groupMdmInvoiceDealer\":null,\"processInstanceInfo\":null,\"dataCode\":\"PHN0985580\",\"dataSource\":\"\",\"registerName\":\"泰州医药集团有限公司\",\"formerName\":\"泰州医药有限公司/江苏省泰州医药总公司\",\"registerProvinceName\":\"江苏省\",\"registerProvinceCode\":\"320000\",\"registerCityName\":\"泰州市\",\"registerCityCode\":\"321200\",\"registerCountyName\":\"海陵区\",\"registerCountyCode\":\"321202\",\"registerAddress\":\"泰州市新仓街66号\",\"registerAddressLongitudeBaidu\":\"\",\"registerAddressLatitudeBaidu\":\"\",\"registerAddressLongitudeGaode\":\"119.909414\",\"registerAddressLatitudeGaode\":\"32.496515\",\"registerAddressMapRunError\":\"百度地理坐标转换失败，错误信息[null],status[201]\\n[ErrorCode]: null\\n[RequestId]: null\\n[HostId]: null,高德经纬度填充成功!\",\"registerAddressMapRunStatus\":\"fail\",\"uniformSocialCreditCode\":\"913212001418613224\",\"attributeLevelFirst\":\"经销商\",\"attributeLevelSecond\":\"批发商\",\"attributeLevelThird\":\"批发商\",\"operationStatus\":\"valid\",\"humanSocialLevel\":\"\",\"humanSocialRank\":\"\",\"healthCommissionLevel\":\"\",\"healthCommissionCode\":\"\",\"clientAttribute\":\"商业\",\"clientLevel\":\"一级商\",\"clientClassify\":\"\",\"remark\":\"update\",\"discountAgreementType\":\"\",\"ncSysCode\":\"10002328\",\"hongYingSysCode\":\"D052300001/D052306381\",\"crmSysCode\":\"\",\"areaManagementSysCode\":\"\",\"mergeDestinationName\":\"\",\"mergeDestinationCode\":\"\",\"mergeTime\":null,\"isHis\":0,\"version\":1,\"mergeUser\":null,\"isValid\":0,\"isElectronicCommerce\":1,\"byProductIsElectronicJson\":null,\"isFlowOpen\":0,\"byProductIsFlowOpenJson\":null,\"chainHeadQuarterName\":\"苏中药业集团股份有限公司\",\"chainHeadQuarterCode\":\"\",\"mergeUserName\":\"\",\"businessStatus\":\"\",\"operation\":\"导入\",\"addTime\":\"2022-06-30 20:29:42\",\"clientLevelUpdateTime\":null,\"addType\":3,\"businessPerson\":\"\",\"businessDept\":\"\",\"entityHongYingSysCode\":\"\",\"entityRegisterName\":\"\",\"generalHeadquarters\":\"\",\"branchOffice\":\"\",\"remarks\":\"\",\"generalHospital\":null,\"branchHospital\":null,\"industryCode\":null,\"industryName\":null,\"operationStatusName\":\"有效\",\"humanSocialLevelName\":null,\"humanSocialRankName\":null,\"healthCommissionLevelName\":null,\"clientClassifyName\":null,\"discountAgreementTypeName\":null,\"values\":null,\"invoiceNo\":null,\"processInstanceId\":null,\"isElectronicCommerceName\":\"否\",\"isFlowOpenName\":\"是\",\"businessStatusName\":null,\"customerName\":\"泰州医药集团有限公司\",\"memoricCode\":\"\",\"companyCode\":null,\"applyCompanyKey\":\"1297\",\"tenantName\":\"上海信谊联合医药药材有限公司信谊医药事业部（1529）/上海信谊天一药业有限公司（1173）\",\"tenantId\":\"1298/1297/1529/1173\",\"updateUserName\":\"lzb\",\"hongYingList\":[{\"code\":\"D052300001\",\"createTime\":1656593905000},{\"code\":\"D052306381\",\"createTime\":1676368131000}],\"ncList\":[{\"code\":\"10002328\",\"createTime\":1657697157000}],\"hospitalClassification\":null,\"mthHospMedicineIncome\":\"\",\"mthHospClinicAmount\":\"\",\"mthHospBunkAmount\":\"\",\"mthHospMicroEcologyQuantity\":\"\",\"mthDigestiveDeptClinicAmount\":\"\",\"mthPaediatricsDeptClinicAmount\":\"\",\"mthPaediatricsDeptInpatientAmount\":\"\",\"mthNeonatologyDeptClinicAmount\":\"\",\"mthNeonatologyDeptInpatientAmount\":\"\",\"mthDermatologyDeptClinicAmount\":\"\",\"existingScaleGrade\":null,\"distributionMark\":null,\"creditPositionGrade\":null,\"hasSaleTeamName\":null,\"groupProvincialPlatMark\":null,\"clientRelationGrade\":null,\"blazeSpecHospital\":null,\"mileStoneBificoCapsHosp\":null,\"mileStoneBificoPowderHosp\":null,\"mileStoneTaiersiHosp\":null,\"mileStoneRibavirinHosp\":null,\"mthDigestiveDeptInpatientAmount\":\"\",\"distributorLabel\":\"\",\"electronicCommerceLabel\":\"\",\"isExistInvoice\":null,\"isAddTerminal\":null,\"alias\":null,\"hisName\":\"\"}],\"total\":10905,\"size\":10,\"current\":2,\"orders\":[],\"optimizeCountSql\":true,\"hitCount\":false,\"countId\":null,\"maxLimit\":null,\"searchCount\":true,\"pages\":1091},\"msg\":\"操作成功\"}";
            if(!Boolean.valueOf(JSONUtil.getByPath(JSONUtil.parse(result),"success").toString()))
            {
                //
                FileUtils.writeLog("error",result);
                return JsonResult.error(result);
            }
            FileUtils.writeLog("Customer",result);
            CustomerResult customerResult = JSONObject.parseObject(result,CustomerResult.class);
            if(ObjectUtil.isNull(customerResult.getData()))
            {
                //
                FileUtils.writeLog("error",result);
                return JsonResult.success(result);
            }
            MdmCustomer mdmCustomer;
            Map<String,Object> param = new HashMap<>();
            Customer entity;
            //处理返回的第一页数据
            for(Customer customer :customerResult.getData().getRecords()) {
                mdmCustomer = new MdmCustomer();
                //PropertyUtils.copyProperties(mdmProduct, product);
                try {
                    BeanUtils.copyProperties(customer, mdmCustomer);
                    entity = mdmCustomerMapper.selectOne(new QueryWrapper<Customer>().select("data_code").eq("data_code",customer.getDataCode()));
                    if(entity!=null) {
                        param.clear();
                        param.put("data_code",entity.getDataCode());
                        mdmCustomerHongyingListMapper.deleteByMap(param);
                        mdmCustomerNCListMapper.deleteByMap(param);
                        customer.setTs(DateUtils.now());
                        mdmCustomerMapper.update(customer,new UpdateWrapper<Customer>().eq("data_code",entity.getDataCode()));

                    }
                    else
                    {
                        mdmCustomerMapper.insert(customer);
                    }
                    MdmCustomerHongyingList mdmHongyingList;
                    for (HongYingList record : customer.getHongYingList()) {
                        mdmHongyingList = new MdmCustomerHongyingList();
                        BeanUtils.copyProperties(record, mdmHongyingList);
                        mdmHongyingList.setDataCode(entity.getDataCode());
                        mdmHongyingList.setCreate_time(record.getCreateTime());
                        mdmCustomerHongyingListService.save(mdmHongyingList);
                    }
                    for (NCList record : customer.getNCList()) {
                        record.setDataCode(entity.getDataCode());
                        mdmCustomerNCListMapper.insert(record);
                    }
                }
                catch(Exception ex)
                {
                    //记录log
                    //continue;;
                    FileUtils.writeLog("error",ex.getMessage());
                }
            }

            //获取返回的总页数
            Integer totalPage = customerResult.getData().getPages();
            //循环获取剩余数据
            if(totalPage>1) {
                for (int i = 2; i <= totalPage; i++) {
                    reqOrder.replace("pageCurrentPage", String.valueOf(i));
                    ts = SHYPUtil.getCurrentTimestamp();
                    reqOrder.replace("timestamp", ts.toString());
                    reqOrder.replace("nonce_str", SHYPUtil.generateNonceStr());
                    sign = SHYPUtil.generateSignature(reqOrder, CommonConfig.appTenantSecret, SHYPConstants.SignType.MD5);
                    reqOrder.replace("sign", sign);
                    result = request.requestWithoutCert(SinepharmConstants.GETCUSTOMER, ts.toString(), reqOrder, false, SHYPConstants.RequestMethod.POST);
                    FileUtils.writeLog("Customer",result);
                    customerResult = JSONObject.parseObject(result, CustomerResult.class);
                    for (Customer customer : customerResult.getData().getRecords()) {
                        mdmCustomer = new MdmCustomer();
                        //PropertyUtils.copyProperties(mdmProduct, product);
                        try {
                            BeanUtils.copyProperties(customer, mdmCustomer);
                            entity = mdmCustomerMapper.selectOne(new QueryWrapper<Customer>().select("data_code").eq("data_code", customer.getDataCode()));
                            if (entity != null) {
                                param.clear();
                                param.put("data_code", entity.getDataCode());
                                mdmCustomerHongyingListMapper.deleteByMap(param);
                                mdmCustomerNCListMapper.deleteByMap(param);
                                customer.setTs(DateUtils.now());
                                mdmCustomerMapper.update(customer, new UpdateWrapper<Customer>().eq("data_code", entity.getDataCode()));
                            } else {
                                mdmCustomerMapper.insert(customer);
                            }
                            MdmCustomerHongyingList mdmHongyingList;
                            for (HongYingList record : customer.getHongYingList()) {
                                mdmHongyingList = new MdmCustomerHongyingList();
                                BeanUtils.copyProperties(record, mdmHongyingList);
                                mdmHongyingList.setDataCode(entity.getDataCode());
                                mdmHongyingList.setCreate_time(record.getCreateTime());
                                mdmCustomerHongyingListService.save(mdmHongyingList);
                            }
                            for (NCList record : customer.getNCList()) {
                                record.setDataCode(entity.getDataCode());
                                mdmCustomerNCListMapper.insert(record);
                            }
                        } catch (Exception ex) {
                            continue;
                        }
                    }
                }
            }

            return JsonResult.success(result);
        }
        catch(Exception ex)
        {
            return JsonResult.error(ex.getMessage());
        }
    }

    @Override
    @SpecifyDataSource(value = DataSourceType.MDM)
    public JsonResult SaveNCSaleFlow(MDMQueryParam query) {
        try {
            Map<String, String> reqOrder = new TreeMap<>();
            reqOrder.put("tenantIdIgnore", query.getTenantIdIgnore());
            reqOrder.put("tenant_key", URLEncoder.encode(CommonConfig.appTenantId,"UTF-8"));
            //reqOrder.put("tenant_secret", CommonConfig.appTenantSecret);

//            reqOrder.put("starttime", URLEncoder.encode(DateUtil.format(query.getStarttime(),DateUtils.YYYY_MM_DD_HH_MM_SS),"UTF-8"));
//            reqOrder.put("endtime",URLEncoder.encode(DateUtil.format(query.getEndtime(),DateUtils.YYYY_MM_DD_HH_MM_SS),"UTF-8"));
            reqOrder.put("starttimeIgnore", URLEncoder.encode(query.getStarttimeIgnore(),"UTF-8"));
            reqOrder.put("endtimeIgnore",URLEncoder.encode(query.getEndtimeIgnore(),"UTF-8"));
            reqOrder.put("nonce_str",SHYPUtil.generateNonceStr());
            reqOrder.put("pageCurrentPage", String.valueOf(query.getPageCurrentPage()));
            reqOrder.put("pageSize", String.valueOf(query.getPageSize()));
            Long ts = SHYPUtil.getCurrentTimestamp();
            reqOrder.put("timestamp", ts.toString());
            String sign = SHYPUtil.generateSignature(reqOrder, CommonConfig.appTenantSecret, SHYPConstants.SignType.MD5);
            reqOrder.put("sign", sign);
            DDKJConfig config = new SinepharmNCConfig();
            MDMRequest request = new MDMRequest(config);
            String result = request.requestWithoutCert(SinepharmConstants.GETNCSALE, ts.toString(), reqOrder, false, SHYPConstants.RequestMethod.POST);
            if(!Boolean.valueOf(JSONUtil.getByPath(JSONUtil.parse(result),"success").toString()))
            {
                return JsonResult.error(result);
            }
            NCSaleResult ncSaleResult = JSONObject.parseObject(result,NCSaleResult.class);
            MdmInvoicingDirection mdmInvoicingDirection;
            MdmInvoicingDirection entity;
            List<MdmInvoicingDirection> directionList;
            List<MdmInvoicingDirection> ncSaleList = new ArrayList<>();
            Map<String,Object> param = new HashMap<>();
            //处理返回的第一页数据
            for(NCSale ncSale :ncSaleResult.getData().getRecords()) {
                mdmInvoicingDirection = new MdmInvoicingDirection();
                //PropertyUtils.copyProperties(mdmProduct, product);
                //try {
                BeanUtils.copyProperties(ncSale, mdmInvoicingDirection);
                param.clear();
                //ncSaleList.add(mdmInvoicingDirection);
                param.put("csaleinvoicebid",ncSale.getCsaleinvoicebid());
                directionList = mdmInvoicingDirectionMapper.selectByMap(param);
                if (directionList.size()>0)
                {
                    entity = directionList.get(0);
                    mdmInvoicingDirectionMapper.update(mdmInvoicingDirection,new QueryWrapper<MdmInvoicingDirection>().eq("csaleinvoicebid",ncSale.getCsaleinvoicebid()));
                }
                else {
                    mdmInvoicingDirectionMapper.insert(mdmInvoicingDirection);
                }
                //}
                //catch(Exception ex)
                //{
                //记录log
                //}
            }
//            SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH,false);
//            MdmInvoicingDirectionMapper studentMapperNew = sqlSession.getMapper(MdmInvoicingDirectionMapper.class);
//            ncSaleList.stream().forEach(student -> studentMapperNew.insert(student));
//            sqlSession.commit();
//            sqlSession.clearCache();

            //获取返回的总页数
            Integer totalPage = ncSaleResult.getData().getPages();
            //循环获取剩余数据
            for (int i = 2; i <=totalPage ; i++) {
                reqOrder.replace("currentPage",String.valueOf(i));
                ts = SHYPUtil.getCurrentTimestamp();
                reqOrder.replace("timestamp", ts.toString());
                reqOrder.replace("nonce_str",SHYPUtil.generateNonceStr());
                sign = SHYPUtil.generateSignature(reqOrder, CommonConfig.appTenantSecret, SHYPConstants.SignType.MD5);
                reqOrder.replace("sign", sign);
                result = request.requestWithoutCert(SinepharmConstants.GETNCSALE, ts.toString(), reqOrder, false, SHYPConstants.RequestMethod.POST);
                ncSaleResult = JSONObject.parseObject(result,NCSaleResult.class);
                ncSaleList.clear();
                for(NCSale ncSale :ncSaleResult.getData().getRecords()) {
                    mdmInvoicingDirection = new MdmInvoicingDirection();
                    //PropertyUtils.copyProperties(mdmProduct, product);
                    BeanUtils.copyProperties(ncSale,mdmInvoicingDirection);
                    param.clear();
                    param.put("csaleinvoicebid",ncSale.getCsaleinvoicebid());
                    directionList = mdmInvoicingDirectionMapper.selectByMap(param);
                    if (directionList.size()>0)
                    {
                        entity = directionList.get(0);
                        mdmInvoicingDirectionMapper.update(mdmInvoicingDirection,new QueryWrapper<MdmInvoicingDirection>().eq("csaleinvoicebid",ncSale.getCsaleinvoicebid()));
                    }
                    else {
                        mdmInvoicingDirectionMapper.insert(mdmInvoicingDirection);
                    }
                    //ncSaleList.add(mdmInvoicingDirection);
                    //mdmInvoicingDirectionMapper.insert(mdmInvoicingDirection);
                }
//                sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH,false);
//                MdmInvoicingDirectionMapper studentMapperNew2 = sqlSession.getMapper(MdmInvoicingDirectionMapper.class);
//                ncSaleList.stream().forEach(student -> studentMapperNew2.insert(student));
//                sqlSession.commit();
//                sqlSession.clearCache();
            }

            return JsonResult.success(result);
        }
        catch(Exception ex)
        {
            return JsonResult.error(ex.getMessage());
        }
    }

    @Override
    @SpecifyDataSource(value = DataSourceType.MDM)
    public JsonResult SaveEmployee(MDMQueryParam query) {
        try {
            Map<String, String> reqOrder = new TreeMap<>();
            reqOrder.put("tenantIdIgnore", query.getTenantIdIgnore());
            reqOrder.put("tenant_key", URLEncoder.encode(CommonConfig.appTenantId,"UTF-8"));
            reqOrder.put("dataTypeIgnore",query.getDataTypeIgnore());
//            reqOrder.put("starttime", URLEncoder.encode(DateUtil.format(query.getStarttime(),DateUtils.YYYY_MM_DD_HH_MM_SS),"UTF-8"));
//            reqOrder.put("endtime",URLEncoder.encode(DateUtil.format(query.getEndtime(),DateUtils.YYYY_MM_DD_HH_MM_SS),"UTF-8"));
            reqOrder.put("starttimeIgnore", URLEncoder.encode(query.getStarttimeIgnore(),"UTF-8"));
            reqOrder.put("endtimeIgnore",URLEncoder.encode(query.getEndtimeIgnore(),"UTF-8"));
            reqOrder.put("nonce_str",SHYPUtil.generateNonceStr());
            reqOrder.put("pageCurrentPage", String.valueOf(query.getPageCurrentPage()));
            reqOrder.put("pageSize", String.valueOf(query.getPageSize()));
            Long ts = SHYPUtil.getCurrentTimestamp();
            reqOrder.put("timestamp", ts.toString());
            String sign = SHYPUtil.generateSignature(reqOrder, CommonConfig.appTenantSecret, SHYPConstants.SignType.MD5);
            reqOrder.put("sign", sign);
            DDKJConfig config = new SinepharmConfig();
            MDMRequest request = new MDMRequest(config);
            String result = request.requestWithoutCert(SinepharmConstants.GETEMPLOYEE, ts.toString(), reqOrder, false, SHYPConstants.RequestMethod.POST);
            FileUtils.writeLog("NCEmployee",result);
            if(!Boolean.valueOf(JSONUtil.getByPath(JSONUtil.parse(result),"success").toString()))
            {
                return JsonResult.error(result);
            }
            EmployeeResult ncSaleResult = JSONObject.parseObject(result,EmployeeResult.class);
            List<MdmEmployee> employeeList;
            List<Employee> ncSaleList = new ArrayList<>();
            MdmEmployee mdmEmployee;
            Map<String,Object> param = new HashMap<>();
            //处理返回的第一页数据
            for(Employee emp :ncSaleResult.getData().getRecords()) {
                mdmEmployee = new MdmEmployee();
                //PropertyUtils.copyProperties(mdmProduct, product);
                //try {
                BeanUtils.copyProperties(emp, mdmEmployee);
                param.clear();
                //ncSaleList.add(mdmInvoicingDirection);
                param.put("ehr_key",emp.getEhrKey());
                employeeList = employeeMapper.selectByMap(param);
                if (employeeList.size()>0)
                {
                    employeeMapper.update(mdmEmployee,new QueryWrapper<MdmEmployee>().eq("ehr_key",emp.getEhrKey()));
                }
                else {
                    employeeMapper.insert(mdmEmployee);
                }
                //}
                //catch(Exception ex)
                //{
                //记录log
                //}
            }
//            SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH,false);
//            MdmInvoicingDirectionMapper studentMapperNew = sqlSession.getMapper(MdmInvoicingDirectionMapper.class);
//            ncSaleList.stream().forEach(student -> studentMapperNew.insert(student));
//            sqlSession.commit();
//            sqlSession.clearCache();

            //获取返回的总页数
            Integer totalPage = ncSaleResult.getData().getPages();
            //循环获取剩余数据
            for (int i = 2; i <=totalPage ; i++) {
                reqOrder.replace("pageCurrentPage",String.valueOf(i));
                ts = SHYPUtil.getCurrentTimestamp();
                reqOrder.replace("timestamp", ts.toString());
                reqOrder.replace("nonce_str",SHYPUtil.generateNonceStr());
                sign = SHYPUtil.generateSignature(reqOrder, CommonConfig.appTenantSecret, SHYPConstants.SignType.MD5);
                reqOrder.replace("sign", sign);
                result = request.requestWithoutCert(SinepharmConstants.GETNCSALE, ts.toString(), reqOrder, false, SHYPConstants.RequestMethod.POST);
                ncSaleResult = JSONObject.parseObject(result,EmployeeResult.class);
                //Map<String,Object> param = new HashMap<>();
                param.clear();
                //处理返回的第一页数据
                for(Employee emp :ncSaleResult.getData().getRecords()) {
                    mdmEmployee = new MdmEmployee();
                    //PropertyUtils.copyProperties(mdmProduct, product);
                    //try {
                    BeanUtils.copyProperties(emp, mdmEmployee);
                    param.clear();
                    //ncSaleList.add(mdmInvoicingDirection);
                    param.put("ehr_key",emp.getEhrKey());
                    employeeList = employeeMapper.selectByMap(param);
                    if (employeeList.size()>0)
                    {
                        employeeMapper.update(mdmEmployee,new QueryWrapper<MdmEmployee>().eq("ehr_key",emp.getEhrKey()));
                    }
                    else {
                        employeeMapper.insert(mdmEmployee);
                    }
                    //}
                    //catch(Exception ex)
                    //{
                    //记录log
                    //}
                }
//                sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH,false);
//                MdmInvoicingDirectionMapper studentMapperNew2 = sqlSession.getMapper(MdmInvoicingDirectionMapper.class);
//                ncSaleList.stream().forEach(student -> studentMapperNew2.insert(student));
//                sqlSession.commit();
//                sqlSession.clearCache();
            }

            return JsonResult.success(result);
        }
        catch(Exception ex)
        {
            return JsonResult.error(ex.getMessage());
        }
    }

    @Override
    @SpecifyDataSource(value = DataSourceType.MDM)
    public JsonResult SaveExpert(MDMQueryParam query) {
        try {
            //String temp="businessMonth=2023-06&currentPage=1&dataUpdateEndDate=&dataUpdateStartDate=&executionMonth=2023-07&nonce_str=IkEDFIcwsq4j7ZReJZniHFDodnRay5WnuOx3&saleEndDate=2023-12-31 23:59:59&saleStartDate=2022-01-01 00:00:00&size=500&tenantId=000000&tenant_key=84dd048e-fca6-47ae-8023-9313aefe8593&timestamp=1694586112&tenant_secret=6d5d95c8-9c45-48b8-9427-499e1ea8ad0e";
            //temp = DigestUtils.md5DigestAsHex(temp.getBytes());
            Map<String, String> reqOrder = new TreeMap<>();
            Map<String, String> reqSign = new TreeMap<>();
            reqOrder.put("tenantIdIgnore", query.getTenantIdIgnore());
            reqOrder.put("tenant_key", URLEncoder.encode(CommonConfig.appTenantId,"UTF-8"));
            reqSign.put("tenant_key", URLEncoder.encode(CommonConfig.appTenantId,"UTF-8"));

            //reqSign.put("tenant_secret", URLEncoder.encode(CommonConfig.appTenantSecret,"UTF-8"));
            reqOrder.put("cleanStatusIgnore", query.getCleanStatusIgnore());
            reqOrder.put("createStartTimeIgnore",query.getStarttimeIgnore());
            reqOrder.put("createEndTimeIgnore",query.getEndtimeIgnore());
            String nonceStr = SHYPUtil.generateNonceStr();
            reqOrder.put("nonce_str",nonceStr);
            reqSign.put("nonce_str",nonceStr);
            if(query.getPageCurrentPage()>0) {
                reqOrder.put("pageCurrentPage", String.valueOf(query.getPageCurrentPage()));
            }
            if(query.getPageSize()>0) {
                reqOrder.put("pageSize", String.valueOf(query.getPageSize()));
            }
            Long ts = SHYPUtil.getCurrentTimestamp();
            reqOrder.put("timestamp", ts.toString());
            reqSign.put("timestamp", ts.toString());
            String sign = SHYPUtil.generateSignature(reqOrder, CommonConfig.appTenantSecret, SHYPConstants.SignType.MD5);
            reqOrder.put("sign", sign);
            reqSign.put("sign", sign);
            FileUtils.writeLog("MD5",sign);
            DDKJConfig config = new SinepharmConfig();
            MDMRequest request = new MDMRequest(config);
            String result = request.requestWithoutCert(SinepharmConstants.GETEXPERT, ts.toString(), reqSign,reqOrder, false, SHYPConstants.RequestMethod.POST);
            FileUtils.writeLog("Expert",result);
            if(!Boolean.valueOf(JSONUtil.getByPath(JSONUtil.parse(result),"success").toString()))
            {
                return JsonResult.error(result);
            }
            CleanSalesResult ncSaleResult = JSONObject.parseObject(result,CleanSalesResult.class);
            MdmCleanSales mdmCleanSales;
            MdmInvoicingDirection entity;
            List<MdmCleanSales> directionList;
            List<MdmCleanSales> ncSaleList = new ArrayList<>();
            Map<String,Object> param = new HashMap<>();
            //处理返回的第一页数据
            for(CleanSales ncSale :ncSaleResult.getData().getRecords()) {
                mdmCleanSales = new MdmCleanSales();
                //PropertyUtils.copyProperties(mdmProduct, product);
                //try {
                BeanUtils.copyProperties(ncSale, mdmCleanSales);
                mdmCleanSales.setTs(DateUtils.now());
                param.clear();
                //ncSaleList.add(mdmInvoicingDirection);
                param.put("id",ncSale.getId());
                directionList = cleanSalesMapper.selectByMap(param);
                if (directionList.size()>0)
                {
                    //entity = directionList.get(0);
                    cleanSalesMapper.update(mdmCleanSales,new QueryWrapper<MdmCleanSales>().eq("id",ncSale.getId()));
                }
                else {
                    cleanSalesMapper.insert(mdmCleanSales);
                }
                //}
                //catch(Exception ex)
                //{
                //记录log
                //}
            }
//            SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH,false);
//            MdmInvoicingDirectionMapper studentMapperNew = sqlSession.getMapper(MdmInvoicingDirectionMapper.class);
//            ncSaleList.stream().forEach(student -> studentMapperNew.insert(student));
//            sqlSession.commit();
//            sqlSession.clearCache();

            //获取返回的总页数
            Integer totalPage = ncSaleResult.getData().getPages();
            //循环获取剩余数据
            for (int i = query.getPageCurrentPage()+1; i <=totalPage ; i++) {
                if(reqOrder.containsKey("pageCurrentPage")) {
                    reqOrder.replace("pageCurrentPage", String.valueOf(i));
                }
                else
                {
                    reqOrder.put("pageCurrentPage", String.valueOf(i));
                }
                ts = SHYPUtil.getCurrentTimestamp();
                reqOrder.replace("timestamp", ts.toString());
                reqSign.replace("timestamp", ts.toString());
                nonceStr = SHYPUtil.generateNonceStr();
                reqOrder.replace("nonce_str",nonceStr);
                reqSign.replace("nonce_str",nonceStr);
                sign = SHYPUtil.generateNCSignature(reqOrder, CommonConfig.appTenantSecret, SHYPConstants.SignType.MD5);
                reqOrder.replace("sign", sign);
                reqSign.replace("sign", sign);
                FileUtils.writeLog("MD5",sign);
                result = request.requestWithoutCert(SinepharmConstants.GETEXPERT, ts.toString(), reqSign,reqOrder, false, SHYPConstants.RequestMethod.POST);
                ncSaleResult = JSONObject.parseObject(result,CleanSalesResult.class);
                if(!Boolean.valueOf(JSONUtil.getByPath(JSONUtil.parse(result),"success").toString()))
                {
                    FileUtils.writeLog("error",reqOrder.toString());
                    FileUtils.writeLog("error",result);
                    continue;
                }
                param.clear();
                //处理返回的第一页数据
                for(CleanSales ncSale :ncSaleResult.getData().getRecords()) {
                    mdmCleanSales = new MdmCleanSales();
                    //PropertyUtils.copyProperties(mdmProduct, product);
                    //try {
                    BeanUtils.copyProperties(ncSale, mdmCleanSales);
                    param.clear();
                    //ncSaleList.add(mdmInvoicingDirection);
                    param.put("id",ncSale.getId());
                    directionList = cleanSalesMapper.selectByMap(param);
                    if (directionList.size()>0)
                    {
                        //entity = directionList.get(0);
                        cleanSalesMapper.update(mdmCleanSales,new QueryWrapper<MdmCleanSales>().eq("id",ncSale.getId()));
                    }
                    else {
                        cleanSalesMapper.insert(mdmCleanSales);
                    }
                    //}
                    //catch(Exception ex)
                    //{
                    //记录log
                    //}
                }
//                sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH,false);
//                MdmInvoicingDirectionMapper studentMapperNew2 = sqlSession.getMapper(MdmInvoicingDirectionMapper.class);
//                ncSaleList.stream().forEach(student -> studentMapperNew2.insert(student));
//                sqlSession.commit();
//                sqlSession.clearCache();
            }

            return JsonResult.success(result);
        }
        catch(Exception ex)
        {
            return JsonResult.error(ex.getMessage());
        }
    }

    @Override
    @SpecifyDataSource(value = DataSourceType.MDM)
    public JsonResult SaveCleanSales(MDMQueryParam query) {
        try {
            //String temp="businessMonth=2023-06&currentPage=1&dataUpdateEndDate=&dataUpdateStartDate=&executionMonth=2023-07&nonce_str=IkEDFIcwsq4j7ZReJZniHFDodnRay5WnuOx3&saleEndDate=2023-12-31 23:59:59&saleStartDate=2022-01-01 00:00:00&size=500&tenantId=000000&tenant_key=84dd048e-fca6-47ae-8023-9313aefe8593&timestamp=1694586112&tenant_secret=6d5d95c8-9c45-48b8-9427-499e1ea8ad0e";
            //temp = DigestUtils.md5DigestAsHex(temp.getBytes());
            Map<String, String> reqOrder = new TreeMap<>();
            Map<String, String> reqSign = new TreeMap<>();
            reqOrder.put("tenantIdIgnore", query.getTenantIdIgnore());
            //reqOrder.put("tenant_key", URLEncoder.encode(CommonConfig.appTenantId,"UTF-8"));
            reqSign.put("tenant_key", URLEncoder.encode(CommonConfig.appTenantId,"UTF-8"));

            //reqSign.put("tenant_secret", URLEncoder.encode(CommonConfig.appTenantSecret,"UTF-8"));
            reqOrder.put("businessMonthIgnore", query.getBusinessMonthIgnore());
            if(!StringUtils.isEmpty(query.getExecutionMonthIgnore())) {
                reqOrder.put("executionMonthIgnore", query.getExecutionMonthIgnore());
            }else
            {
                reqOrder.put("executionMonthIgnore", "");
            }
            reqOrder.put("saleStartDateIgnore",DateUtil.format(query.getSaleStartDateIgnore(),DateUtils.YYYY_MM_DD_HH_MM_SS));
            reqOrder.put("saleEndDateIgnore",DateUtil.format(query.getSaleEndDateIgnore(),DateUtils.YYYY_MM_DD_HH_MM_SS));
            if(!StringUtils.isEmpty(query.getDataUpdateStartDateIgnore())) {
                reqOrder.put("dataUpdateStartDateIgnore", URLEncoder.encode(query.getDataUpdateStartDateIgnore(), "UTF-8"));
            }
            else
            {
                reqOrder.put("dataUpdateStartDateIgnore","");
            }
            if(!StringUtils.isEmpty(query.getDataUpdateStartDateIgnore())) {
                reqOrder.put("dataUpdateEndDateIgnore", URLEncoder.encode(query.getDataUpdateStartDateIgnore(),"UTF-8"));
            }
            else
            {
                reqOrder.put("dataUpdateEndDateIgnore","");
            }
            String nonceStr = SHYPUtil.generateNonceStr();
            //reqOrder.put("nonce_str",nonceStr);
            reqSign.put("nonce_str",nonceStr);
            if(query.getPageCurrentPage()>0) {
                reqOrder.put("pageCurrentPage", String.valueOf(query.getPageCurrentPage()));
            }
            if(query.getPageSize()>0) {
                reqOrder.put("pageSize", String.valueOf(query.getPageSize()));
            }
            Long ts = SHYPUtil.getCurrentTimestamp();
            //reqOrder.put("timestamp", ts.toString());
            reqSign.put("timestamp", ts.toString());
            reqSign.put("tenant_secret",URLEncoder.encode(CommonConfig.appTenantSecret,"UTF-8"));
            String sign = SHYPUtil.generateNCSignature(reqSign, CommonConfig.appTenantSecret, SHYPConstants.SignType.MD5);
            //reqOrder.put("sign", sign);
            reqSign.put("sign", sign);
            FileUtils.writeLog("MD5",sign);
            DDKJConfig config = new SinepharmNCConfig();
            MDMRequest request = new MDMRequest(config);
            String result = request.requestWithoutCert(SinepharmConstants.GETCLEANSALES, ts.toString(), reqSign,reqOrder, false, SHYPConstants.RequestMethod.POST);
//            String result="{\"code\":200,\"success\":true,\"data\":{\n" +
//                    "  \"records\": [\n" +
//                    "    {\n" +
//                    "      \"buyBranchHospitalCode\": \"\",\n" +
//                    "      \"productSpecification\": \"90mg*10片\",\n" +
//                    "      \"purchaserTotalSplitRelationship\": null,\n" +
//                    "      \"mdmCode\": \"P00306390\",\n" +
//                    "      \"simpleDataMonthTargetVo\": {\n" +
//                    "        \"managerNumber4\": \"TY0398\",\n" +
//                    "        \"managerNumber5\": \"TY0398\",\n" +
//                    "        \"managerNumber2\": \"TY0398\",\n" +
//                    "        \"managerNumber3\": \"TY0398\",\n" +
//                    "        \"managerNumber1\": null,\n" +
//                    "        \"description\": \"2024年指标\",\n" +
//                    "        \"employeeCode\": null,\n" +
//                    "        \"employeeNumber\": null,\n" +
//                    "        \"managerCode1\": \"127007\",\n" +
//                    "        \"orgCode\": \"PHN01796597\",\n" +
//                    "        \"department4\": \"医院推广部大区自留\",\n" +
//                    "        \"managerCode4\": \"145777\",\n" +
//                    "        \"department5\": \"医院推广部大区自留\",\n" +
//                    "        \"managerCode5\": \"145777\",\n" +
//                    "        \"department2\": \"C板块\",\n" +
//                    "        \"managerCode2\": \"145777\",\n" +
//                    "        \"department3\": \"医院推广部\",\n" +
//                    "        \"managerCode3\": \"145777\",\n" +
//                    "        \"employeeName\": null,\n" +
//                    "        \"department1\": \"上海信谊天一药业有限公司\",\n" +
//                    "        \"targetType\": null,\n" +
//                    "        \"managerName5\": \"移传萍\",\n" +
//                    "        \"managerName4\": \"移传萍\",\n" +
//                    "        \"managerName3\": \"移传萍\",\n" +
//                    "        \"managerName2\": \"移传萍\",\n" +
//                    "        \"managerName1\": \"周伟\",\n" +
//                    "        \"prodCode\": \"P00306390\",\n" +
//                    "        \"month\": \"202405\"\n" +
//                    "      },\n" +
//                    "      \"productUnit\": \"盒\",\n" +
//                    "      \"sealGeneralHospital\": \"\",\n" +
//                    "      \"productName\": \"盐酸地尔硫卓缓释片90mg*10片\",\n" +
//                    "      \"executionMonth\": \"2024-05\",\n" +
//                    "      \"purchaserCode\": \"PHN01715566\",\n" +
//                    "      \"clientAttribute\": \"三终端\",\n" +
//                    "      \"id\": \"950845447229288493\",\n" +
//                    "      \"dataUpdateTime\": null,\n" +
//                    "      \"originalProductName\": \"盐酸地尔硫卓缓释片\",\n" +
//                    "      \"originalSellerName\": \"\",\n" +
//                    "      \"buyGeneralHospitalCode\": \"\",\n" +
//                    "      \"serialNumber\": \"359261\",\n" +
//                    "      \"drugSers\": \"恬尔新系列\",\n" +
//                    "      \"sealBranchHospitalCode\": \"\",\n" +
//                    "      \"sealBranchHospital\": \"\",\n" +
//                    "      \"buyGeneralHospital\": \"\",\n" +
//                    "      \"dairyName\": null,\n" +
//                    "      \"businessMonth\": \"2024-04\",\n" +
//                    "      \"purchaserName\": \"上海市嘉定区安亭镇社区卫生服务中心\",\n" +
//                    "      \"productVlotno\": \"02231105\",\n" +
//                    "      \"saleTotalSplitRelationshipCode\": null,\n" +
//                    "      \"purchaserOriginalName\": \"上海市嘉定区安亭镇社区卫生服务中心\",\n" +
//                    "      \"isAddName\": \"是\",\n" +
//                    "      \"mdmProductName\": \"盐酸地尔硫卓缓释片\",\n" +
//                    "      \"productPrice\": null,\n" +
//                    "      \"status\": 1,\n" +
//                    "      \"saleQuantity\": \"20\",\n" +
//                    "      \"uploadId\": null,\n" +
//                    "      \"branchHospitalName\": null,\n" +
//                    "      \"phnName\": \"上海市嘉定区安亭镇社区卫生服务中心\",\n" +
//                    "      \"sellerName\": \"上海上药新亚医药有限公司\",\n" +
//                    "      \"remark\": null,\n" +
//                    "      \"saleDate\": \"2024-04-16\",\n" +
//                    "      \"purchaserLevelNature\": \"基层医疗卫生机构\",\n" +
//                    "      \"purchaserAddress\": \"中国上海市嘉定区安亭镇博园路7132号-7144号(双号)、7148号\",\n" +
//                    "      \"manufacturer\": \"上海信谊万象药业股份有限公司\",\n" +
//                    "      \"sellerCode\": \"PHN01786869\",\n" +
//                    "      \"isDeleted\": \"0\",\n" +
//                    "      \"originalProductSpecification\": \"90mg*10片\",\n" +
//                    "      \"buyBranchHospital\": \"\",\n" +
//                    "      \"branchHospitalCode\": null,\n" +
//                    "      \"expirationDate\": null,\n" +
//                    "      \"salePhnCode\": \"PHN01786869\",\n" +
//                    "      \"phnCode\": \"PHN01715566\",\n" +
//                    "      \"originalProductCode\": \"\",\n" +
//                    "      \"updateUser\": \"\",\n" +
//                    "      \"updateTime\": \"2024-02-04\",\n" +
//                    "      \"clientLevel\": \"\",\n" +
//                    "      \"statMonth\": \"2024-04\",\n" +
//                    "      \"isAdd\": \"1\",\n" +
//                    "      \"sealGeneralHospitalCode\": \"\",\n" +
//                    "      \"standardShippingAddress\": \"上海市嘉定区安亭镇民丰路1200号\",\n" +
//                    "      \"purchaseAmount\": null,\n" +
//                    "      \"saleTotalSplitRelationship\": null,\n" +
//                    "      \"productCode\": \"P00306390\",\n" +
//                    "      \"createDept\": null,\n" +
//                    "      \"createTime\": \"2024-02-04\",\n" +
//                    "      \"purchaserTotalSplitRelationshipCode\": null,\n" +
//                    "      \"buyPhnCode\": \"PHN01715566\",\n" +
//                    "      \"tenantId\": \"000000\",\n" +
//                    "      \"createUser\": \"\",\n" +
//                    "      \"registerName\": \"上海市嘉定区安亭镇社区卫生服务中心\"\n" +
//                    "    },\n" +
//                    "    {\n" +
//                    "      \"buyBranchHospitalCode\": \"\",\n" +
//                    "      \"productSpecification\": \"90mg*10片\",\n" +
//                    "      \"purchaserTotalSplitRelationship\": null,\n" +
//                    "      \"mdmCode\": \"P00306390\",\n" +
//                    "      \"simpleDataMonthTargetVo\": {\n" +
//                    "        \"managerNumber4\": \"TY0398\",\n" +
//                    "        \"managerNumber5\": \"TY0398\",\n" +
//                    "        \"managerNumber2\": \"TY0398\",\n" +
//                    "        \"managerNumber3\": \"TY0398\",\n" +
//                    "        \"managerNumber1\": null,\n" +
//                    "        \"description\": \"2024年指标\",\n" +
//                    "        \"employeeCode\": null,\n" +
//                    "        \"employeeNumber\": null,\n" +
//                    "        \"managerCode1\": \"127007\",\n" +
//                    "        \"orgCode\": \"PHN01796597\",\n" +
//                    "        \"department4\": \"医院推广部大区自留\",\n" +
//                    "        \"managerCode4\": \"145777\",\n" +
//                    "        \"department5\": \"医院推广部大区自留\",\n" +
//                    "        \"managerCode5\": \"145777\",\n" +
//                    "        \"department2\": \"C板块\",\n" +
//                    "        \"managerCode2\": \"145777\",\n" +
//                    "        \"department3\": \"医院推广部\",\n" +
//                    "        \"managerCode3\": \"145777\",\n" +
//                    "        \"employeeName\": null,\n" +
//                    "        \"department1\": \"上海信谊天一药业有限公司\",\n" +
//                    "        \"targetType\": null,\n" +
//                    "        \"managerName5\": \"移传萍\",\n" +
//                    "        \"managerName4\": \"移传萍\",\n" +
//                    "        \"managerName3\": \"移传萍\",\n" +
//                    "        \"managerName2\": \"移传萍\",\n" +
//                    "        \"managerName1\": \"周伟\",\n" +
//                    "        \"prodCode\": \"P00306390\",\n" +
//                    "        \"month\": \"202405\"\n" +
//                    "      },\n" +
//                    "      \"productUnit\": \"瓶\",\n" +
//                    "      \"sealGeneralHospital\": \"\",\n" +
//                    "      \"productName\": \"盐酸地尔硫卓缓释片90mg*10片\",\n" +
//                    "      \"executionMonth\": \"2024-05\",\n" +
//                    "      \"purchaserCode\": \"PHN01796597\",\n" +
//                    "      \"clientAttribute\": \"三终端\",\n" +
//                    "      \"id\": \"950845447229288493\",\n" +
//                    "      \"dataUpdateTime\": null,\n" +
//                    "      \"originalProductName\": \"盐酸地尔硫卓缓释片\",\n" +
//                    "      \"originalSellerName\": \"\",\n" +
//                    "      \"buyGeneralHospitalCode\": \"\",\n" +
//                    "      \"serialNumber\": \"359260\",\n" +
//                    "      \"drugSers\": \"恬尔新系列\",\n" +
//                    "      \"sealBranchHospitalCode\": \"\",\n" +
//                    "      \"sealBranchHospital\": \"\",\n" +
//                    "      \"buyGeneralHospital\": \"\",\n" +
//                    "      \"dairyName\": null,\n" +
//                    "      \"businessMonth\": \"2024-04\",\n" +
//                    "      \"purchaserName\": \"上海市宝山区大场镇第三社区卫生服务中心\",\n" +
//                    "      \"productVlotno\": null,\n" +
//                    "      \"saleTotalSplitRelationshipCode\": \"PHN0034346\",\n" +
//                    "      \"purchaserOriginalName\": \"上海市宝山区大场镇第三社区卫生服务中心\",\n" +
//                    "      \"isAddName\": \"是\",\n" +
//                    "      \"mdmProductName\": \"盐酸地尔硫卓缓释片\",\n" +
//                    "      \"productPrice\": null,\n" +
//                    "      \"status\": 1,\n" +
//                    "      \"saleQuantity\": \"30\",\n" +
//                    "      \"uploadId\": null,\n" +
//                    "      \"branchHospitalName\": null,\n" +
//                    "      \"phnName\": \"上海市宝山区大场镇第三社区卫生服务中心\",\n" +
//                    "      \"sellerName\": \"上药宝山(上海)医药有限公司\",\n" +
//                    "      \"remark\": null,\n" +
//                    "      \"saleDate\": \"2024-04-12\",\n" +
//                    "      \"purchaserLevelNature\": \"基层医疗卫生机构\",\n" +
//                    "      \"purchaserAddress\": null,\n" +
//                    "      \"manufacturer\": \"上海信谊万象药业\",\n" +
//                    "      \"sellerCode\": \"PHN01782873\",\n" +
//                    "      \"isDeleted\": \"0\",\n" +
//                    "      \"originalProductSpecification\": \"90mg*10片*1板\",\n" +
//                    "      \"buyBranchHospital\": \"\",\n" +
//                    "      \"branchHospitalCode\": null,\n" +
//                    "      \"expirationDate\": null,\n" +
//                    "      \"salePhnCode\": \"PHN01782873\",\n" +
//                    "      \"phnCode\": \"PHN01796597\",\n" +
//                    "      \"originalProductCode\": \"\",\n" +
//                    "      \"updateUser\": \"\",\n" +
//                    "      \"updateTime\": \"2024-02-04\",\n" +
//                    "      \"clientLevel\": \"\",\n" +
//                    "      \"statMonth\": \"2024-04\",\n" +
//                    "      \"isAdd\": \"1\",\n" +
//                    "      \"sealGeneralHospitalCode\": \"\",\n" +
//                    "      \"standardShippingAddress\": \"上海市宝山区少年村路218号\",\n" +
//                    "      \"purchaseAmount\": null,\n" +
//                    "      \"saleTotalSplitRelationship\": \"上药控股有限公司\",\n" +
//                    "      \"productCode\": \"P00306390\",\n" +
//                    "      \"createDept\": null,\n" +
//                    "      \"createTime\": \"2024-02-04\",\n" +
//                    "      \"purchaserTotalSplitRelationshipCode\": null,\n" +
//                    "      \"buyPhnCode\": \"PHN01796597\",\n" +
//                    "      \"tenantId\": \"000000\",\n" +
//                    "      \"createUser\": \"\",\n" +
//                    "      \"registerName\": \"上海市宝山区大场镇第三社区卫生服务中心\"\n" +
//                    "    },\n" +
//                    "    {\n" +
//                    "      \"buyBranchHospitalCode\": \"\",\n" +
//                    "      \"productSpecification\": \"0.5g\",\n" +
//                    "      \"purchaserTotalSplitRelationship\": null,\n" +
//                    "      \"mdmCode\": \"P00000435\",\n" +
//                    "      \"simpleDataMonthTargetVo\": {\n" +
//                    "        \"managerNumber4\": null,\n" +
//                    "        \"managerNumber5\": null,\n" +
//                    "        \"managerNumber2\": null,\n" +
//                    "        \"managerNumber3\": null,\n" +
//                    "        \"managerNumber1\": null,\n" +
//                    "        \"description\": null,\n" +
//                    "        \"employeeCode\": null,\n" +
//                    "        \"employeeNumber\": null,\n" +
//                    "        \"managerCode1\": null,\n" +
//                    "        \"orgCode\": null,\n" +
//                    "        \"department4\": null,\n" +
//                    "        \"managerCode4\": null,\n" +
//                    "        \"department5\": null,\n" +
//                    "        \"managerCode5\": null,\n" +
//                    "        \"department2\": null,\n" +
//                    "        \"managerCode2\": null,\n" +
//                    "        \"department3\": null,\n" +
//                    "        \"managerCode3\": null,\n" +
//                    "        \"employeeName\": null,\n" +
//                    "        \"department1\": null,\n" +
//                    "        \"targetType\": null,\n" +
//                    "        \"managerName5\": null,\n" +
//                    "        \"managerName4\": null,\n" +
//                    "        \"managerName3\": null,\n" +
//                    "        \"managerName2\": null,\n" +
//                    "        \"managerName1\": null,\n" +
//                    "        \"prodCode\": null,\n" +
//                    "        \"month\": null\n" +
//                    "      },\n" +
//                    "      \"productUnit\": \"盒\",\n" +
//                    "      \"sealGeneralHospital\": \"\",\n" +
//                    "      \"productName\": \"盐酸二甲双胍缓释片0.5g*30片\",\n" +
//                    "      \"executionMonth\": \"2024-05\",\n" +
//                    "      \"purchaserCode\": \"PHN01203266\",\n" +
//                    "      \"clientAttribute\": \"二终端\",\n" +
//                    "      \"id\": \"988125438411390978\",\n" +
//                    "      \"dataUpdateTime\": null,\n" +
//                    "      \"originalProductName\": \"盐酸二甲双胍缓释片(美哒灵) 0.5克*30片 上海上药信谊药厂有限公司(原上海信谊药厂有限公司)\",\n" +
//                    "      \"originalSellerName\": \"\",\n" +
//                    "      \"buyGeneralHospitalCode\": \"\",\n" +
//                    "      \"serialNumber\": \"359259\",\n" +
//                    "      \"drugSers\": \"美哒灵系列\",\n" +
//                    "      \"sealBranchHospitalCode\": \"\",\n" +
//                    "      \"sealBranchHospital\": \"\",\n" +
//                    "      \"buyGeneralHospital\": \"\",\n" +
//                    "      \"dairyName\": \"上虹-鹤庆路二店\",\n" +
//                    "      \"businessMonth\": \"2024-04\",\n" +
//                    "      \"purchaserName\": \"上海益丰长虹大药房有限公司鹤庆路二店\",\n" +
//                    "      \"productVlotno\": \"120231117\",\n" +
//                    "      \"saleTotalSplitRelationshipCode\": null,\n" +
//                    "      \"purchaserOriginalName\": \"上海益丰长虹大药房有限公司鹤庆路二店\",\n" +
//                    "      \"isAddName\": \"是\",\n" +
//                    "      \"mdmProductName\": \"盐酸二甲双胍缓释片\",\n" +
//                    "      \"productPrice\": null,\n" +
//                    "      \"status\": 1,\n" +
//                    "      \"saleQuantity\": \"4\",\n" +
//                    "      \"uploadId\": null,\n" +
//                    "      \"branchHospitalName\": null,\n" +
//                    "      \"phnName\": \"上海益丰长虹大药房有限公司鹤庆路二店\",\n" +
//                    "      \"sellerName\": \"上海益丰大药房连锁有限公司\",\n" +
//                    "      \"remark\": null,\n" +
//                    "      \"saleDate\": \"2024-04-28\",\n" +
//                    "      \"purchaserLevelNature\": \"药店\",\n" +
//                    "      \"purchaserAddress\": null,\n" +
//                    "      \"manufacturer\": \"上海上药信谊药厂有限公司(原上海信谊药厂有限公司)\",\n" +
//                    "      \"sellerCode\": \"PHN00269975\",\n" +
//                    "      \"isDeleted\": \"0\",\n" +
//                    "      \"originalProductSpecification\": \"0.5克*30片\",\n" +
//                    "      \"buyBranchHospital\": \"\",\n" +
//                    "      \"branchHospitalCode\": null,\n" +
//                    "      \"expirationDate\": null,\n" +
//                    "      \"salePhnCode\": \"PHN00269975\",\n" +
//                    "      \"phnCode\": \"PHN01203266\",\n" +
//                    "      \"originalProductCode\": \"\",\n" +
//                    "      \"updateUser\": \"Maggie\",\n" +
//                    "      \"updateTime\": \"2024-05-15\",\n" +
//                    "      \"clientLevel\": \"连锁药房\",\n" +
//                    "      \"statMonth\": \"2024-04\",\n" +
//                    "      \"isAdd\": \"1\",\n" +
//                    "      \"sealGeneralHospitalCode\": \"\",\n" +
//                    "      \"standardShippingAddress\": \"上海市闵行区鹤庆路536号底层\",\n" +
//                    "      \"purchaseAmount\": null,\n" +
//                    "      \"saleTotalSplitRelationship\": null,\n" +
//                    "      \"productCode\": null,\n" +
//                    "      \"createDept\": null,\n" +
//                    "      \"createTime\": \"2024-05-15\",\n" +
//                    "      \"purchaserTotalSplitRelationshipCode\": null,\n" +
//                    "      \"buyPhnCode\": \"PHN01203266\",\n" +
//                    "      \"tenantId\": \"000000\",\n" +
//                    "      \"createUser\": \"Maggie\",\n" +
//                    "      \"registerName\": \"上海益丰长虹大药房有限公司鹤庆路二店\"\n" +
//                    "    },\n" +
//                    "    {\n" +
//                    "      \"buyBranchHospitalCode\": \"\",\n" +
//                    "      \"productSpecification\": \"90mg*10片\",\n" +
//                    "      \"purchaserTotalSplitRelationship\": null,\n" +
//                    "      \"mdmCode\": \"P00306390\",\n" +
//                    "      \"simpleDataMonthTargetVo\": {\n" +
//                    "        \"managerNumber4\": \"TY0398\",\n" +
//                    "        \"managerNumber5\": \"TY0398\",\n" +
//                    "        \"managerNumber2\": \"TY0398\",\n" +
//                    "        \"managerNumber3\": \"TY0398\",\n" +
//                    "        \"managerNumber1\": null,\n" +
//                    "        \"description\": \"2024年指标\",\n" +
//                    "        \"employeeCode\": null,\n" +
//                    "        \"employeeNumber\": null,\n" +
//                    "        \"managerCode1\": \"127007\",\n" +
//                    "        \"orgCode\": \"PHN01796597\",\n" +
//                    "        \"department4\": \"医院推广部大区自留\",\n" +
//                    "        \"managerCode4\": \"145777\",\n" +
//                    "        \"department5\": \"医院推广部大区自留\",\n" +
//                    "        \"managerCode5\": \"145777\",\n" +
//                    "        \"department2\": \"C板块\",\n" +
//                    "        \"managerCode2\": \"145777\",\n" +
//                    "        \"department3\": \"医院推广部\",\n" +
//                    "        \"managerCode3\": \"145777\",\n" +
//                    "        \"employeeName\": null,\n" +
//                    "        \"department1\": \"上海信谊天一药业有限公司\",\n" +
//                    "        \"targetType\": null,\n" +
//                    "        \"managerName5\": \"移传萍\",\n" +
//                    "        \"managerName4\": \"移传萍\",\n" +
//                    "        \"managerName3\": \"移传萍\",\n" +
//                    "        \"managerName2\": \"移传萍\",\n" +
//                    "        \"managerName1\": \"周伟\",\n" +
//                    "        \"prodCode\": \"P00306390\",\n" +
//                    "        \"month\": \"202405\"\n" +
//                    "      },\n" +
//                    "      \"productUnit\": \"盒\",\n" +
//                    "      \"sealGeneralHospital\": \"\",\n" +
//                    "      \"productName\": \"盐酸地尔硫卓缓释片90mg*10片\",\n" +
//                    "      \"executionMonth\": \"2024-05\",\n" +
//                    "      \"purchaserCode\": \"PHN01715566\",\n" +
//                    "      \"clientAttribute\": \"三终端\",\n" +
//                    "      \"id\": \"950845447229288493\",\n" +
//                    "      \"dataUpdateTime\": null,\n" +
//                    "      \"originalProductName\": \"盐酸地尔硫卓缓释片\",\n" +
//                    "      \"originalSellerName\": \"\",\n" +
//                    "      \"buyGeneralHospitalCode\": \"\",\n" +
//                    "      \"serialNumber\": \"359258\",\n" +
//                    "      \"drugSers\": \"恬尔新系列\",\n" +
//                    "      \"sealBranchHospitalCode\": \"\",\n" +
//                    "      \"sealBranchHospital\": \"\",\n" +
//                    "      \"buyGeneralHospital\": \"\",\n" +
//                    "      \"dairyName\": null,\n" +
//                    "      \"businessMonth\": \"2024-04\",\n" +
//                    "      \"purchaserName\": \"上海市嘉定区安亭镇社区卫生服务中心\",\n" +
//                    "      \"productVlotno\": \"02231105\",\n" +
//                    "      \"saleTotalSplitRelationshipCode\": null,\n" +
//                    "      \"purchaserOriginalName\": \"上海市嘉定区安亭镇社区卫生服务中心\",\n" +
//                    "      \"isAddName\": \"是\",\n" +
//                    "      \"mdmProductName\": \"盐酸地尔硫卓缓释片\",\n" +
//                    "      \"productPrice\": null,\n" +
//                    "      \"status\": 1,\n" +
//                    "      \"saleQuantity\": \"20\",\n" +
//                    "      \"uploadId\": null,\n" +
//                    "      \"branchHospitalName\": null,\n" +
//                    "      \"phnName\": \"上海市嘉定区安亭镇社区卫生服务中心\",\n" +
//                    "      \"sellerName\": \"上海上药新亚医药有限公司\",\n" +
//                    "      \"remark\": null,\n" +
//                    "      \"saleDate\": \"2024-04-09\",\n" +
//                    "      \"purchaserLevelNature\": \"基层医疗卫生机构\",\n" +
//                    "      \"purchaserAddress\": \"中国上海市嘉定区安亭镇展阳路88号\",\n" +
//                    "      \"manufacturer\": \"上海信谊万象药业股份有限公司\",\n" +
//                    "      \"sellerCode\": \"PHN01786869\",\n" +
//                    "      \"isDeleted\": \"0\",\n" +
//                    "      \"originalProductSpecification\": \"90mg*10片\",\n" +
//                    "      \"buyBranchHospital\": \"\",\n" +
//                    "      \"branchHospitalCode\": null,\n" +
//                    "      \"expirationDate\": null,\n" +
//                    "      \"salePhnCode\": \"PHN01786869\",\n" +
//                    "      \"phnCode\": \"PHN01715566\",\n" +
//                    "      \"originalProductCode\": \"\",\n" +
//                    "      \"updateUser\": \"\",\n" +
//                    "      \"updateTime\": \"2024-02-04\",\n" +
//                    "      \"clientLevel\": \"\",\n" +
//                    "      \"statMonth\": \"2024-04\",\n" +
//                    "      \"isAdd\": \"1\",\n" +
//                    "      \"sealGeneralHospitalCode\": \"\",\n" +
//                    "      \"standardShippingAddress\": \"上海市嘉定区安亭镇民丰路1200号\",\n" +
//                    "      \"purchaseAmount\": null,\n" +
//                    "      \"saleTotalSplitRelationship\": null,\n" +
//                    "      \"productCode\": \"P00306390\",\n" +
//                    "      \"createDept\": null,\n" +
//                    "      \"createTime\": \"2024-02-04\",\n" +
//                    "      \"purchaserTotalSplitRelationshipCode\": null,\n" +
//                    "      \"buyPhnCode\": \"PHN01715566\",\n" +
//                    "      \"tenantId\": \"000000\",\n" +
//                    "      \"createUser\": \"\",\n" +
//                    "      \"registerName\": \"上海市嘉定区安亭镇社区卫生服务中心\"\n" +
//                    "    },\n" +
//                    "    {\n" +
//                    "      \"buyBranchHospitalCode\": \"\",\n" +
//                    "      \"productSpecification\": \"90mg*10片\",\n" +
//                    "      \"purchaserTotalSplitRelationship\": null,\n" +
//                    "      \"mdmCode\": \"P00306390\",\n" +
//                    "      \"simpleDataMonthTargetVo\": {\n" +
//                    "        \"managerNumber4\": \"TY0398\",\n" +
//                    "        \"managerNumber5\": \"TY0398\",\n" +
//                    "        \"managerNumber2\": \"TY0398\",\n" +
//                    "        \"managerNumber3\": \"TY0398\",\n" +
//                    "        \"managerNumber1\": null,\n" +
//                    "        \"description\": \"2024年指标\",\n" +
//                    "        \"employeeCode\": null,\n" +
//                    "        \"employeeNumber\": null,\n" +
//                    "        \"managerCode1\": \"127007\",\n" +
//                    "        \"orgCode\": \"PHN01796597\",\n" +
//                    "        \"department4\": \"医院推广部大区自留\",\n" +
//                    "        \"managerCode4\": \"145777\",\n" +
//                    "        \"department5\": \"医院推广部大区自留\",\n" +
//                    "        \"managerCode5\": \"145777\",\n" +
//                    "        \"department2\": \"C板块\",\n" +
//                    "        \"managerCode2\": \"145777\",\n" +
//                    "        \"department3\": \"医院推广部\",\n" +
//                    "        \"managerCode3\": \"145777\",\n" +
//                    "        \"employeeName\": null,\n" +
//                    "        \"department1\": \"上海信谊天一药业有限公司\",\n" +
//                    "        \"targetType\": null,\n" +
//                    "        \"managerName5\": \"移传萍\",\n" +
//                    "        \"managerName4\": \"移传萍\",\n" +
//                    "        \"managerName3\": \"移传萍\",\n" +
//                    "        \"managerName2\": \"移传萍\",\n" +
//                    "        \"managerName1\": \"周伟\",\n" +
//                    "        \"prodCode\": \"P00306390\",\n" +
//                    "        \"month\": \"202405\"\n" +
//                    "      },\n" +
//                    "      \"productUnit\": \"盒\",\n" +
//                    "      \"sealGeneralHospital\": \"\",\n" +
//                    "      \"productName\": \"盐酸地尔硫卓缓释片90mg*10片\",\n" +
//                    "      \"executionMonth\": \"2024-05\",\n" +
//                    "      \"purchaserCode\": \"PHN01715566\",\n" +
//                    "      \"clientAttribute\": \"三终端\",\n" +
//                    "      \"id\": \"950845447229288493\",\n" +
//                    "      \"dataUpdateTime\": null,\n" +
//                    "      \"originalProductName\": \"盐酸地尔硫卓缓释片\",\n" +
//                    "      \"originalSellerName\": \"\",\n" +
//                    "      \"buyGeneralHospitalCode\": \"\",\n" +
//                    "      \"serialNumber\": \"359257\",\n" +
//                    "      \"drugSers\": \"恬尔新系列\",\n" +
//                    "      \"sealBranchHospitalCode\": \"\",\n" +
//                    "      \"sealBranchHospital\": \"\",\n" +
//                    "      \"buyGeneralHospital\": \"\",\n" +
//                    "      \"dairyName\": null,\n" +
//                    "      \"businessMonth\": \"2024-04\",\n" +
//                    "      \"purchaserName\": \"上海市嘉定区安亭镇社区卫生服务中心\",\n" +
//                    "      \"productVlotno\": \"02231105\",\n" +
//                    "      \"saleTotalSplitRelationshipCode\": null,\n" +
//                    "      \"purchaserOriginalName\": \"上海市嘉定区安亭镇社区卫生服务中心\",\n" +
//                    "      \"isAddName\": \"是\",\n" +
//                    "      \"mdmProductName\": \"盐酸地尔硫卓缓释片\",\n" +
//                    "      \"productPrice\": null,\n" +
//                    "      \"status\": 1,\n" +
//                    "      \"saleQuantity\": \"10\",\n" +
//                    "      \"uploadId\": null,\n" +
//                    "      \"branchHospitalName\": null,\n" +
//                    "      \"phnName\": \"上海市嘉定区安亭镇社区卫生服务中心\",\n" +
//                    "      \"sellerName\": \"上海上药新亚医药有限公司\",\n" +
//                    "      \"remark\": null,\n" +
//                    "      \"saleDate\": \"2024-04-16\",\n" +
//                    "      \"purchaserLevelNature\": \"基层医疗卫生机构\",\n" +
//                    "      \"purchaserAddress\": \"中国上海市嘉定区安亭镇方南路401弄121号\",\n" +
//                    "      \"manufacturer\": \"上海信谊万象药业股份有限公司\",\n" +
//                    "      \"sellerCode\": \"PHN01786869\",\n" +
//                    "      \"isDeleted\": \"0\",\n" +
//                    "      \"originalProductSpecification\": \"90mg*10片\",\n" +
//                    "      \"buyBranchHospital\": \"\",\n" +
//                    "      \"branchHospitalCode\": null,\n" +
//                    "      \"expirationDate\": null,\n" +
//                    "      \"salePhnCode\": \"PHN01786869\",\n" +
//                    "      \"phnCode\": \"PHN01715566\",\n" +
//                    "      \"originalProductCode\": \"\",\n" +
//                    "      \"updateUser\": \"\",\n" +
//                    "      \"updateTime\": \"2024-02-04\",\n" +
//                    "      \"clientLevel\": \"\",\n" +
//                    "      \"statMonth\": \"2024-04\",\n" +
//                    "      \"isAdd\": \"1\",\n" +
//                    "      \"sealGeneralHospitalCode\": \"\",\n" +
//                    "      \"standardShippingAddress\": \"上海市嘉定区安亭镇民丰路1200号\",\n" +
//                    "      \"purchaseAmount\": null,\n" +
//                    "      \"saleTotalSplitRelationship\": null,\n" +
//                    "      \"productCode\": \"P00306390\",\n" +
//                    "      \"createDept\": null,\n" +
//                    "      \"createTime\": \"2024-02-04\",\n" +
//                    "      \"purchaserTotalSplitRelationshipCode\": null,\n" +
//                    "      \"buyPhnCode\": \"PHN01715566\",\n" +
//                    "      \"tenantId\": \"000000\",\n" +
//                    "      \"createUser\": \"\",\n" +
//                    "      \"registerName\": \"上海市嘉定区安亭镇社区卫生服务中心\"\n" +
//                    "    },\n" +
//                    "    {\n" +
//                    "      \"buyBranchHospitalCode\": \"\",\n" +
//                    "      \"productSpecification\": \"90mg*10片\",\n" +
//                    "      \"purchaserTotalSplitRelationship\": null,\n" +
//                    "      \"mdmCode\": \"P00306390\",\n" +
//                    "      \"simpleDataMonthTargetVo\": {\n" +
//                    "        \"managerNumber4\": \"TY0398\",\n" +
//                    "        \"managerNumber5\": \"TY0398\",\n" +
//                    "        \"managerNumber2\": \"TY0398\",\n" +
//                    "        \"managerNumber3\": \"TY0398\",\n" +
//                    "        \"managerNumber1\": null,\n" +
//                    "        \"description\": \"2024年指标\",\n" +
//                    "        \"employeeCode\": null,\n" +
//                    "        \"employeeNumber\": null,\n" +
//                    "        \"managerCode1\": \"127007\",\n" +
//                    "        \"orgCode\": \"PHN01796597\",\n" +
//                    "        \"department4\": \"医院推广部大区自留\",\n" +
//                    "        \"managerCode4\": \"145777\",\n" +
//                    "        \"department5\": \"医院推广部大区自留\",\n" +
//                    "        \"managerCode5\": \"145777\",\n" +
//                    "        \"department2\": \"C板块\",\n" +
//                    "        \"managerCode2\": \"145777\",\n" +
//                    "        \"department3\": \"医院推广部\",\n" +
//                    "        \"managerCode3\": \"145777\",\n" +
//                    "        \"employeeName\": null,\n" +
//                    "        \"department1\": \"上海信谊天一药业有限公司\",\n" +
//                    "        \"targetType\": null,\n" +
//                    "        \"managerName5\": \"移传萍\",\n" +
//                    "        \"managerName4\": \"移传萍\",\n" +
//                    "        \"managerName3\": \"移传萍\",\n" +
//                    "        \"managerName2\": \"移传萍\",\n" +
//                    "        \"managerName1\": \"周伟\",\n" +
//                    "        \"prodCode\": \"P00306390\",\n" +
//                    "        \"month\": \"202405\"\n" +
//                    "      },\n" +
//                    "      \"productUnit\": \"盒\",\n" +
//                    "      \"sealGeneralHospital\": \"\",\n" +
//                    "      \"productName\": \"盐酸地尔硫卓缓释片90mg*10片\",\n" +
//                    "      \"executionMonth\": \"2024-05\",\n" +
//                    "      \"purchaserCode\": \"PHN01203266\",\n" +
//                    "      \"clientAttribute\": \"二终端\",\n" +
//                    "      \"id\": \"950845447229288493\",\n" +
//                    "      \"dataUpdateTime\": null,\n" +
//                    "      \"originalProductName\": \"盐酸地尔硫䓬缓释片(恬尔新) 90毫克*10片 上海信谊万象药业股份有限公司\",\n" +
//                    "      \"originalSellerName\": \"\",\n" +
//                    "      \"buyGeneralHospitalCode\": \"\",\n" +
//                    "      \"serialNumber\": \"359256\",\n" +
//                    "      \"drugSers\": \"恬尔新系列\",\n" +
//                    "      \"sealBranchHospitalCode\": \"\",\n" +
//                    "      \"sealBranchHospital\": \"\",\n" +
//                    "      \"buyGeneralHospital\": \"\",\n" +
//                    "      \"dairyName\": \"上虹-鹤庆路二店\",\n" +
//                    "      \"businessMonth\": \"2024-04\",\n" +
//                    "      \"purchaserName\": \"上海益丰长虹大药房有限公司鹤庆路二店\",\n" +
//                    "      \"productVlotno\": \"02231103\",\n" +
//                    "      \"saleTotalSplitRelationshipCode\": null,\n" +
//                    "      \"purchaserOriginalName\": \"上海益丰长虹大药房有限公司鹤庆路二店\",\n" +
//                    "      \"isAddName\": \"是\",\n" +
//                    "      \"mdmProductName\": \"盐酸地尔硫卓缓释片\",\n" +
//                    "      \"productPrice\": null,\n" +
//                    "      \"status\": 1,\n" +
//                    "      \"saleQuantity\": \"2\",\n" +
//                    "      \"uploadId\": null,\n" +
//                    "      \"branchHospitalName\": null,\n" +
//                    "      \"phnName\": \"上海益丰长虹大药房有限公司鹤庆路二店\",\n" +
//                    "      \"sellerName\": \"上海益丰大药房连锁有限公司\",\n" +
//                    "      \"remark\": null,\n" +
//                    "      \"saleDate\": \"2024-04-14\",\n" +
//                    "      \"purchaserLevelNature\": \"药店\",\n" +
//                    "      \"purchaserAddress\": null,\n" +
//                    "      \"manufacturer\": \"上海信谊万象药业股份有限公司\",\n" +
//                    "      \"sellerCode\": \"PHN00269975\",\n" +
//                    "      \"isDeleted\": \"0\",\n" +
//                    "      \"originalProductSpecification\": \"90毫克*10片\",\n" +
//                    "      \"buyBranchHospital\": \"\",\n" +
//                    "      \"branchHospitalCode\": null,\n" +
//                    "      \"expirationDate\": null,\n" +
//                    "      \"salePhnCode\": \"PHN00269975\",\n" +
//                    "      \"phnCode\": \"PHN01203266\",\n" +
//                    "      \"originalProductCode\": \"\",\n" +
//                    "      \"updateUser\": \"\",\n" +
//                    "      \"updateTime\": \"2024-02-04\",\n" +
//                    "      \"clientLevel\": \"\",\n" +
//                    "      \"statMonth\": \"2024-04\",\n" +
//                    "      \"isAdd\": \"1\",\n" +
//                    "      \"sealGeneralHospitalCode\": \"\",\n" +
//                    "      \"standardShippingAddress\": \"上海市闵行区鹤庆路536号底层\",\n" +
//                    "      \"purchaseAmount\": null,\n" +
//                    "      \"saleTotalSplitRelationship\": null,\n" +
//                    "      \"productCode\": \"P00306390\",\n" +
//                    "      \"createDept\": null,\n" +
//                    "      \"createTime\": \"2024-02-04\",\n" +
//                    "      \"purchaserTotalSplitRelationshipCode\": null,\n" +
//                    "      \"buyPhnCode\": \"PHN01203266\",\n" +
//                    "      \"tenantId\": \"000000\",\n" +
//                    "      \"createUser\": \"\",\n" +
//                    "      \"registerName\": \"上海益丰长虹大药房有限公司鹤庆路二店\"\n" +
//                    "    },\n" +
//                    "    {\n" +
//                    "      \"buyBranchHospitalCode\": \"\",\n" +
//                    "      \"productSpecification\": \"24S*320HE\",\n" +
//                    "      \"purchaserTotalSplitRelationship\": null,\n" +
//                    "      \"mdmCode\": \"P00000552\",\n" +
//                    "      \"simpleDataMonthTargetVo\": {\n" +
//                    "        \"managerNumber4\": null,\n" +
//                    "        \"managerNumber5\": null,\n" +
//                    "        \"managerNumber2\": null,\n" +
//                    "        \"managerNumber3\": null,\n" +
//                    "        \"managerNumber1\": null,\n" +
//                    "        \"description\": null,\n" +
//                    "        \"employeeCode\": null,\n" +
//                    "        \"employeeNumber\": null,\n" +
//                    "        \"managerCode1\": null,\n" +
//                    "        \"orgCode\": null,\n" +
//                    "        \"department4\": null,\n" +
//                    "        \"managerCode4\": null,\n" +
//                    "        \"department5\": null,\n" +
//                    "        \"managerCode5\": null,\n" +
//                    "        \"department2\": null,\n" +
//                    "        \"managerCode2\": null,\n" +
//                    "        \"department3\": null,\n" +
//                    "        \"managerCode3\": null,\n" +
//                    "        \"employeeName\": null,\n" +
//                    "        \"department1\": null,\n" +
//                    "        \"targetType\": null,\n" +
//                    "        \"managerName5\": null,\n" +
//                    "        \"managerName4\": null,\n" +
//                    "        \"managerName3\": null,\n" +
//                    "        \"managerName2\": null,\n" +
//                    "        \"managerName1\": null,\n" +
//                    "        \"prodCode\": null,\n" +
//                    "        \"month\": null\n" +
//                    "      },\n" +
//                    "      \"productUnit\": \"盒\",\n" +
//                    "      \"sealGeneralHospital\": \"\",\n" +
//                    "      \"productName\": \"酚氨咖敏片\",\n" +
//                    "      \"executionMonth\": \"2024-05\",\n" +
//                    "      \"purchaserCode\": \"PHN01203266\",\n" +
//                    "      \"clientAttribute\": \"二终端\",\n" +
//                    "      \"id\": \"988125438415585283\",\n" +
//                    "      \"dataUpdateTime\": null,\n" +
//                    "      \"originalProductName\": \"酚氨咖敏片 12片*2板 上海信谊天平药业有限公司\",\n" +
//                    "      \"originalSellerName\": \"\",\n" +
//                    "      \"buyGeneralHospitalCode\": \"\",\n" +
//                    "      \"serialNumber\": \"359255\",\n" +
//                    "      \"drugSers\": \"未归类\",\n" +
//                    "      \"sealBranchHospitalCode\": \"\",\n" +
//                    "      \"sealBranchHospital\": \"\",\n" +
//                    "      \"buyGeneralHospital\": \"\",\n" +
//                    "      \"dairyName\": \"上虹-鹤庆路二店\",\n" +
//                    "      \"businessMonth\": \"2024-04\",\n" +
//                    "      \"purchaserName\": \"上海益丰长虹大药房有限公司鹤庆路二店\",\n" +
//                    "      \"productVlotno\": \"91240101\",\n" +
//                    "      \"saleTotalSplitRelationshipCode\": null,\n" +
//                    "      \"purchaserOriginalName\": \"上海益丰长虹大药房有限公司鹤庆路二店\",\n" +
//                    "      \"isAddName\": \"是\",\n" +
//                    "      \"mdmProductName\": \"酚氨咖敏片\",\n" +
//                    "      \"productPrice\": null,\n" +
//                    "      \"status\": 1,\n" +
//                    "      \"saleQuantity\": \"2\",\n" +
//                    "      \"uploadId\": null,\n" +
//                    "      \"branchHospitalName\": null,\n" +
//                    "      \"phnName\": \"上海益丰长虹大药房有限公司鹤庆路二店\",\n" +
//                    "      \"sellerName\": \"上海益丰大药房连锁有限公司\",\n" +
//                    "      \"remark\": null,\n" +
//                    "      \"saleDate\": \"2024-04-18\",\n" +
//                    "      \"purchaserLevelNature\": \"药店\",\n" +
//                    "      \"purchaserAddress\": null,\n" +
//                    "      \"manufacturer\": \"上海信谊天平药业有限公司\",\n" +
//                    "      \"sellerCode\": \"PHN00269975\",\n" +
//                    "      \"isDeleted\": \"0\",\n" +
//                    "      \"originalProductSpecification\": \"12片*2板\",\n" +
//                    "      \"buyBranchHospital\": \"\",\n" +
//                    "      \"branchHospitalCode\": null,\n" +
//                    "      \"expirationDate\": null,\n" +
//                    "      \"salePhnCode\": \"PHN00269975\",\n" +
//                    "      \"phnCode\": \"PHN01203266\",\n" +
//                    "      \"originalProductCode\": \"\",\n" +
//                    "      \"updateUser\": \"Maggie\",\n" +
//                    "      \"updateTime\": \"2024-05-15\",\n" +
//                    "      \"clientLevel\": \"连锁药房\",\n" +
//                    "      \"statMonth\": \"2024-04\",\n" +
//                    "      \"isAdd\": \"1\",\n" +
//                    "      \"sealGeneralHospitalCode\": \"\",\n" +
//                    "      \"standardShippingAddress\": \"上海市闵行区鹤庆路536号底层\",\n" +
//                    "      \"purchaseAmount\": null,\n" +
//                    "      \"saleTotalSplitRelationship\": null,\n" +
//                    "      \"productCode\": null,\n" +
//                    "      \"createDept\": null,\n" +
//                    "      \"createTime\": \"2024-05-15\",\n" +
//                    "      \"purchaserTotalSplitRelationshipCode\": null,\n" +
//                    "      \"buyPhnCode\": \"PHN01203266\",\n" +
//                    "      \"tenantId\": \"000000\",\n" +
//                    "      \"createUser\": \"Maggie\",\n" +
//                    "      \"registerName\": \"上海益丰长虹大药房有限公司鹤庆路二店\"\n" +
//                    "    },\n" +
//                    "    {\n" +
//                    "      \"buyBranchHospitalCode\": \"\",\n" +
//                    "      \"productSpecification\": \"90mg*10片\",\n" +
//                    "      \"purchaserTotalSplitRelationship\": null,\n" +
//                    "      \"mdmCode\": \"P00306390\",\n" +
//                    "      \"simpleDataMonthTargetVo\": {\n" +
//                    "        \"managerNumber4\": \"TY0398\",\n" +
//                    "        \"managerNumber5\": \"TY0398\",\n" +
//                    "        \"managerNumber2\": \"TY0398\",\n" +
//                    "        \"managerNumber3\": \"TY0398\",\n" +
//                    "        \"managerNumber1\": null,\n" +
//                    "        \"description\": \"2024年指标\",\n" +
//                    "        \"employeeCode\": null,\n" +
//                    "        \"employeeNumber\": null,\n" +
//                    "        \"managerCode1\": \"127007\",\n" +
//                    "        \"orgCode\": \"PHN01796597\",\n" +
//                    "        \"department4\": \"医院推广部大区自留\",\n" +
//                    "        \"managerCode4\": \"145777\",\n" +
//                    "        \"department5\": \"医院推广部大区自留\",\n" +
//                    "        \"managerCode5\": \"145777\",\n" +
//                    "        \"department2\": \"C板块\",\n" +
//                    "        \"managerCode2\": \"145777\",\n" +
//                    "        \"department3\": \"医院推广部\",\n" +
//                    "        \"managerCode3\": \"145777\",\n" +
//                    "        \"employeeName\": null,\n" +
//                    "        \"department1\": \"上海信谊天一药业有限公司\",\n" +
//                    "        \"targetType\": null,\n" +
//                    "        \"managerName5\": \"移传萍\",\n" +
//                    "        \"managerName4\": \"移传萍\",\n" +
//                    "        \"managerName3\": \"移传萍\",\n" +
//                    "        \"managerName2\": \"移传萍\",\n" +
//                    "        \"managerName1\": \"周伟\",\n" +
//                    "        \"prodCode\": \"P00306390\",\n" +
//                    "        \"month\": \"202405\"\n" +
//                    "      },\n" +
//                    "      \"productUnit\": \"盒\",\n" +
//                    "      \"sealGeneralHospital\": \"\",\n" +
//                    "      \"productName\": \"盐酸地尔硫卓缓释片90mg*10片\",\n" +
//                    "      \"executionMonth\": \"2024-05\",\n" +
//                    "      \"purchaserCode\": \"PHN01715566\",\n" +
//                    "      \"clientAttribute\": \"三终端\",\n" +
//                    "      \"id\": \"950845447229288493\",\n" +
//                    "      \"dataUpdateTime\": null,\n" +
//                    "      \"originalProductName\": \"盐酸地尔硫卓缓释片\",\n" +
//                    "      \"originalSellerName\": \"\",\n" +
//                    "      \"buyGeneralHospitalCode\": \"\",\n" +
//                    "      \"serialNumber\": \"359254\",\n" +
//                    "      \"drugSers\": \"恬尔新系列\",\n" +
//                    "      \"sealBranchHospitalCode\": \"\",\n" +
//                    "      \"sealBranchHospital\": \"\",\n" +
//                    "      \"buyGeneralHospital\": \"\",\n" +
//                    "      \"dairyName\": null,\n" +
//                    "      \"businessMonth\": \"2024-04\",\n" +
//                    "      \"purchaserName\": \"上海市嘉定区安亭镇社区卫生服务中心\",\n" +
//                    "      \"productVlotno\": \"02231105\",\n" +
//                    "      \"saleTotalSplitRelationshipCode\": null,\n" +
//                    "      \"purchaserOriginalName\": \"上海市嘉定区安亭镇社区卫生服务中心\",\n" +
//                    "      \"isAddName\": \"是\",\n" +
//                    "      \"mdmProductName\": \"盐酸地尔硫卓缓释片\",\n" +
//                    "      \"productPrice\": null,\n" +
//                    "      \"status\": 1,\n" +
//                    "      \"saleQuantity\": \"80\",\n" +
//                    "      \"uploadId\": null,\n" +
//                    "      \"branchHospitalName\": null,\n" +
//                    "      \"phnName\": \"上海市嘉定区安亭镇社区卫生服务中心\",\n" +
//                    "      \"sellerName\": \"上海上药新亚医药有限公司\",\n" +
//                    "      \"remark\": null,\n" +
//                    "      \"saleDate\": \"2024-04-16\",\n" +
//                    "      \"purchaserLevelNature\": \"基层医疗卫生机构\",\n" +
//                    "      \"purchaserAddress\": \"上海市嘉定区安亭镇民丰路1200号\",\n" +
//                    "      \"manufacturer\": \"上海信谊万象药业股份有限公司\",\n" +
//                    "      \"sellerCode\": \"PHN01786869\",\n" +
//                    "      \"isDeleted\": \"0\",\n" +
//                    "      \"originalProductSpecification\": \"90mg*10片\",\n" +
//                    "      \"buyBranchHospital\": \"\",\n" +
//                    "      \"branchHospitalCode\": null,\n" +
//                    "      \"expirationDate\": null,\n" +
//                    "      \"salePhnCode\": \"PHN01786869\",\n" +
//                    "      \"phnCode\": \"PHN01715566\",\n" +
//                    "      \"originalProductCode\": \"\",\n" +
//                    "      \"updateUser\": \"\",\n" +
//                    "      \"updateTime\": \"2024-02-04\",\n" +
//                    "      \"clientLevel\": \"\",\n" +
//                    "      \"statMonth\": \"2024-04\",\n" +
//                    "      \"isAdd\": \"1\",\n" +
//                    "      \"sealGeneralHospitalCode\": \"\",\n" +
//                    "      \"standardShippingAddress\": \"上海市嘉定区安亭镇民丰路1200号\",\n" +
//                    "      \"purchaseAmount\": null,\n" +
//                    "      \"saleTotalSplitRelationship\": null,\n" +
//                    "      \"productCode\": \"P00306390\",\n" +
//                    "      \"createDept\": null,\n" +
//                    "      \"createTime\": \"2024-02-04\",\n" +
//                    "      \"purchaserTotalSplitRelationshipCode\": null,\n" +
//                    "      \"buyPhnCode\": \"PHN01715566\",\n" +
//                    "      \"tenantId\": \"000000\",\n" +
//                    "      \"createUser\": \"\",\n" +
//                    "      \"registerName\": \"上海市嘉定区安亭镇社区卫生服务中心\"\n" +
//                    "    },\n" +
//                    "    {\n" +
//                    "      \"buyBranchHospitalCode\": \"\",\n" +
//                    "      \"productSpecification\": \"90mg*10片\",\n" +
//                    "      \"purchaserTotalSplitRelationship\": null,\n" +
//                    "      \"mdmCode\": \"P00306390\",\n" +
//                    "      \"simpleDataMonthTargetVo\": {\n" +
//                    "        \"managerNumber4\": \"TY0398\",\n" +
//                    "        \"managerNumber5\": \"TY0398\",\n" +
//                    "        \"managerNumber2\": \"TY0398\",\n" +
//                    "        \"managerNumber3\": \"TY0398\",\n" +
//                    "        \"managerNumber1\": null,\n" +
//                    "        \"description\": \"2024年指标\",\n" +
//                    "        \"employeeCode\": null,\n" +
//                    "        \"employeeNumber\": null,\n" +
//                    "        \"managerCode1\": \"127007\",\n" +
//                    "        \"orgCode\": \"PHN01796597\",\n" +
//                    "        \"department4\": \"医院推广部大区自留\",\n" +
//                    "        \"managerCode4\": \"145777\",\n" +
//                    "        \"department5\": \"医院推广部大区自留\",\n" +
//                    "        \"managerCode5\": \"145777\",\n" +
//                    "        \"department2\": \"C板块\",\n" +
//                    "        \"managerCode2\": \"145777\",\n" +
//                    "        \"department3\": \"医院推广部\",\n" +
//                    "        \"managerCode3\": \"145777\",\n" +
//                    "        \"employeeName\": null,\n" +
//                    "        \"department1\": \"上海信谊天一药业有限公司\",\n" +
//                    "        \"targetType\": null,\n" +
//                    "        \"managerName5\": \"移传萍\",\n" +
//                    "        \"managerName4\": \"移传萍\",\n" +
//                    "        \"managerName3\": \"移传萍\",\n" +
//                    "        \"managerName2\": \"移传萍\",\n" +
//                    "        \"managerName1\": \"周伟\",\n" +
//                    "        \"prodCode\": \"P00306390\",\n" +
//                    "        \"month\": \"202405\"\n" +
//                    "      },\n" +
//                    "      \"productUnit\": \"瓶\",\n" +
//                    "      \"sealGeneralHospital\": \"\",\n" +
//                    "      \"productName\": \"盐酸地尔硫卓缓释片90mg*10片\",\n" +
//                    "      \"executionMonth\": \"2024-05\",\n" +
//                    "      \"purchaserCode\": \"PHN01170131\",\n" +
//                    "      \"clientAttribute\": \"三终端\",\n" +
//                    "      \"id\": \"950845447229288493\",\n" +
//                    "      \"dataUpdateTime\": null,\n" +
//                    "      \"originalProductName\": \"盐酸地尔硫卓缓释片\",\n" +
//                    "      \"originalSellerName\": \"\",\n" +
//                    "      \"buyGeneralHospitalCode\": \"\",\n" +
//                    "      \"serialNumber\": \"359253\",\n" +
//                    "      \"drugSers\": \"恬尔新系列\",\n" +
//                    "      \"sealBranchHospitalCode\": \"\",\n" +
//                    "      \"sealBranchHospital\": \"\",\n" +
//                    "      \"buyGeneralHospital\": \"\",\n" +
//                    "      \"dairyName\": null,\n" +
//                    "      \"businessMonth\": \"2024-04\",\n" +
//                    "      \"purchaserName\": \"上海市宝山区罗店镇第三社区卫生服务中心\",\n" +
//                    "      \"productVlotno\": null,\n" +
//                    "      \"saleTotalSplitRelationshipCode\": \"PHN0034346\",\n" +
//                    "      \"purchaserOriginalName\": \"上海市宝山区罗店镇第三社区卫生服务中心\",\n" +
//                    "      \"isAddName\": \"是\",\n" +
//                    "      \"mdmProductName\": \"盐酸地尔硫卓缓释片\",\n" +
//                    "      \"productPrice\": null,\n" +
//                    "      \"status\": 1,\n" +
//                    "      \"saleQuantity\": \"50\",\n" +
//                    "      \"uploadId\": null,\n" +
//                    "      \"branchHospitalName\": null,\n" +
//                    "      \"phnName\": \"上海市宝山区罗店镇第三社区卫生服务中心\",\n" +
//                    "      \"sellerName\": \"上药宝山(上海)医药有限公司\",\n" +
//                    "      \"remark\": null,\n" +
//                    "      \"saleDate\": \"2024-04-29\",\n" +
//                    "      \"purchaserLevelNature\": \"基层医疗卫生机构\",\n" +
//                    "      \"purchaserAddress\": null,\n" +
//                    "      \"manufacturer\": \"上海信谊万象药业\",\n" +
//                    "      \"sellerCode\": \"PHN01782873\",\n" +
//                    "      \"isDeleted\": \"0\",\n" +
//                    "      \"originalProductSpecification\": \"90mg*10片*1板\",\n" +
//                    "      \"buyBranchHospital\": \"\",\n" +
//                    "      \"branchHospitalCode\": null,\n" +
//                    "      \"expirationDate\": null,\n" +
//                    "      \"salePhnCode\": \"PHN01782873\",\n" +
//                    "      \"phnCode\": \"PHN01170131\",\n" +
//                    "      \"originalProductCode\": \"\",\n" +
//                    "      \"updateUser\": \"\",\n" +
//                    "      \"updateTime\": \"2024-02-04\",\n" +
//                    "      \"clientLevel\": \"\",\n" +
//                    "      \"statMonth\": \"2024-04\",\n" +
//                    "      \"isAdd\": \"1\",\n" +
//                    "      \"sealGeneralHospitalCode\": \"\",\n" +
//                    "      \"standardShippingAddress\": \"上海市宝山区美文路255号\",\n" +
//                    "      \"purchaseAmount\": null,\n" +
//                    "      \"saleTotalSplitRelationship\": \"上药控股有限公司\",\n" +
//                    "      \"productCode\": \"P00306390\",\n" +
//                    "      \"createDept\": null,\n" +
//                    "      \"createTime\": \"2024-02-04\",\n" +
//                    "      \"purchaserTotalSplitRelationshipCode\": null,\n" +
//                    "      \"buyPhnCode\": \"PHN01170131\",\n" +
//                    "      \"tenantId\": \"000000\",\n" +
//                    "      \"createUser\": \"\",\n" +
//                    "      \"registerName\": \"上海市宝山区罗店镇第三社区卫生服务中心\"\n" +
//                    "    },\n" +
//                    "    {\n" +
//                    "      \"buyBranchHospitalCode\": \"\",\n" +
//                    "      \"productSpecification\": \"90mg*10片\",\n" +
//                    "      \"purchaserTotalSplitRelationship\": null,\n" +
//                    "      \"mdmCode\": \"P00306390\",\n" +
//                    "      \"simpleDataMonthTargetVo\": {\n" +
//                    "        \"managerNumber4\": \"TY0398\",\n" +
//                    "        \"managerNumber5\": \"TY0398\",\n" +
//                    "        \"managerNumber2\": \"TY0398\",\n" +
//                    "        \"managerNumber3\": \"TY0398\",\n" +
//                    "        \"managerNumber1\": null,\n" +
//                    "        \"description\": \"2024年指标\",\n" +
//                    "        \"employeeCode\": null,\n" +
//                    "        \"employeeNumber\": null,\n" +
//                    "        \"managerCode1\": \"127007\",\n" +
//                    "        \"orgCode\": \"PHN01796597\",\n" +
//                    "        \"department4\": \"医院推广部大区自留\",\n" +
//                    "        \"managerCode4\": \"145777\",\n" +
//                    "        \"department5\": \"医院推广部大区自留\",\n" +
//                    "        \"managerCode5\": \"145777\",\n" +
//                    "        \"department2\": \"C板块\",\n" +
//                    "        \"managerCode2\": \"145777\",\n" +
//                    "        \"department3\": \"医院推广部\",\n" +
//                    "        \"managerCode3\": \"145777\",\n" +
//                    "        \"employeeName\": null,\n" +
//                    "        \"department1\": \"上海信谊天一药业有限公司\",\n" +
//                    "        \"targetType\": null,\n" +
//                    "        \"managerName5\": \"移传萍\",\n" +
//                    "        \"managerName4\": \"移传萍\",\n" +
//                    "        \"managerName3\": \"移传萍\",\n" +
//                    "        \"managerName2\": \"移传萍\",\n" +
//                    "        \"managerName1\": \"周伟\",\n" +
//                    "        \"prodCode\": \"P00306390\",\n" +
//                    "        \"month\": \"202405\"\n" +
//                    "      },\n" +
//                    "      \"productUnit\": \"盒\",\n" +
//                    "      \"sealGeneralHospital\": \"\",\n" +
//                    "      \"productName\": \"盐酸地尔硫卓缓释片90mg*10片\",\n" +
//                    "      \"executionMonth\": \"2024-05\",\n" +
//                    "      \"purchaserCode\": \"PHN01715566\",\n" +
//                    "      \"clientAttribute\": \"三终端\",\n" +
//                    "      \"id\": \"950845447229288493\",\n" +
//                    "      \"dataUpdateTime\": null,\n" +
//                    "      \"originalProductName\": \"盐酸地尔硫卓缓释片\",\n" +
//                    "      \"originalSellerName\": \"\",\n" +
//                    "      \"buyGeneralHospitalCode\": \"\",\n" +
//                    "      \"serialNumber\": \"359252\",\n" +
//                    "      \"drugSers\": \"恬尔新系列\",\n" +
//                    "      \"sealBranchHospitalCode\": \"\",\n" +
//                    "      \"sealBranchHospital\": \"\",\n" +
//                    "      \"buyGeneralHospital\": \"\",\n" +
//                    "      \"dairyName\": null,\n" +
//                    "      \"businessMonth\": \"2024-04\",\n" +
//                    "      \"purchaserName\": \"上海市嘉定区安亭镇社区卫生服务中心\",\n" +
//                    "      \"productVlotno\": \"02231104\",\n" +
//                    "      \"saleTotalSplitRelationshipCode\": null,\n" +
//                    "      \"purchaserOriginalName\": \"上海市嘉定区安亭镇社区卫生服务中心\",\n" +
//                    "      \"isAddName\": \"是\",\n" +
//                    "      \"mdmProductName\": \"盐酸地尔硫卓缓释片\",\n" +
//                    "      \"productPrice\": null,\n" +
//                    "      \"status\": 1,\n" +
//                    "      \"saleQuantity\": \"-30\",\n" +
//                    "      \"uploadId\": null,\n" +
//                    "      \"branchHospitalName\": null,\n" +
//                    "      \"phnName\": \"上海市嘉定区安亭镇社区卫生服务中心\",\n" +
//                    "      \"sellerName\": \"上海上药新亚医药有限公司\",\n" +
//                    "      \"remark\": null,\n" +
//                    "      \"saleDate\": \"2024-04-12\",\n" +
//                    "      \"purchaserLevelNature\": \"基层医疗卫生机构\",\n" +
//                    "      \"purchaserAddress\": \"上海市嘉定区安亭镇民丰路1200号\",\n" +
//                    "      \"manufacturer\": \"上海信谊万象药业股份有限公司\",\n" +
//                    "      \"sellerCode\": \"PHN01786869\",\n" +
//                    "      \"isDeleted\": \"0\",\n" +
//                    "      \"originalProductSpecification\": \"90mg*10片\",\n" +
//                    "      \"buyBranchHospital\": \"\",\n" +
//                    "      \"branchHospitalCode\": null,\n" +
//                    "      \"expirationDate\": null,\n" +
//                    "      \"salePhnCode\": \"PHN01786869\",\n" +
//                    "      \"phnCode\": \"PHN01715566\",\n" +
//                    "      \"originalProductCode\": \"\",\n" +
//                    "      \"updateUser\": \"\",\n" +
//                    "      \"updateTime\": \"2024-02-04\",\n" +
//                    "      \"clientLevel\": \"\",\n" +
//                    "      \"statMonth\": \"2024-04\",\n" +
//                    "      \"isAdd\": \"1\",\n" +
//                    "      \"sealGeneralHospitalCode\": \"\",\n" +
//                    "      \"standardShippingAddress\": \"上海市嘉定区安亭镇民丰路1200号\",\n" +
//                    "      \"purchaseAmount\": null,\n" +
//                    "      \"saleTotalSplitRelationship\": null,\n" +
//                    "      \"productCode\": \"P00306390\",\n" +
//                    "      \"createDept\": null,\n" +
//                    "      \"createTime\": \"2024-02-04\",\n" +
//                    "      \"purchaserTotalSplitRelationshipCode\": null,\n" +
//                    "      \"buyPhnCode\": \"PHN01715566\",\n" +
//                    "      \"tenantId\": \"000000\",\n" +
//                    "      \"createUser\": \"\",\n" +
//                    "      \"registerName\": \"上海市嘉定区安亭镇社区卫生服务中心\"\n" +
//                    "    }\n" +
//                    "  ],\n" +
//                    "  \"total\": 55225,\n" +
//                    "  \"size\": 10,\n" +
//                    "  \"current\": 1,\n" +
//                    "  \"pages\": 5523,\n" +
//                    "  \"orders\": [],\n" +
//                    "  \"optimizeCountSql\": true,\n" +
//                    "  \"hitCount\": false,\n" +
//                    "  \"countId\": null,\n" +
//                    "  \"maxLimit\": null,\n" +
//                    "  \"searchCount\": true\n" +
//                    "},\"msg\":\"操作成功\"}\n";
            FileUtils.writeLog("CleanSales",result);
            JSONObject jsonObject = JSON.parseObject(result);

            if(!Boolean.valueOf(jsonObject.getString("success")))
            {
                return JsonResult.error(result);
            }
            CleanSalesResult ncSaleResult = JSONObject.parseObject(result,CleanSalesResult.class);
            MdmCleanSales mdmCleanSales;
            MdmInvoicingDirection entity;
            List<MdmCleanSales> directionList;
            List<MdmCleanSales> ncSaleList = new ArrayList<>();
            Map<String,Object> param = new HashMap<>();
            //处理返回的第一页数据
            for(CleanSales ncSale :ncSaleResult.getData().getRecords()) {
                mdmCleanSales = new MdmCleanSales();
                //PropertyUtils.copyProperties(mdmProduct, product);
                try {
                    BeanUtils.copyProperties(ncSale, mdmCleanSales);
                    mdmCleanSales.setTs(DateUtils.now());
                    param.clear();
                    //ncSaleList.add(mdmInvoicingDirection);
                    param.put("id", ncSale.getId());
                    directionList = cleanSalesMapper.selectByMap(param);
                    if (directionList.size() > 0) {
                        //entity = directionList.get(0);
                        cleanSalesMapper.update(mdmCleanSales, new QueryWrapper<MdmCleanSales>().eq("id", ncSale.getId()));
                    } else {
                        cleanSalesMapper.insert(mdmCleanSales);
                    }
                }
                catch(Exception ex)
                {
                //记录log
                    FileUtils.writeLog("error",JSON.toJSONString(mdmCleanSales));
                }
            }
//            SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH,false);
//            MdmInvoicingDirectionMapper studentMapperNew = sqlSession.getMapper(MdmInvoicingDirectionMapper.class);
//            ncSaleList.stream().forEach(student -> studentMapperNew.insert(student));
//            sqlSession.commit();
//            sqlSession.clearCache();

            //获取返回的总页数
            Integer totalPage = ncSaleResult.getData().getPages();
            //循环获取剩余数据
            for (int i = query.getPageCurrentPage()+1; i <=totalPage ; i++) {
                if(reqOrder.containsKey("pageCurrentPage")) {
                    reqOrder.replace("pageCurrentPage", String.valueOf(i));
                }
                else
                {
                    reqOrder.put("pageCurrentPage", String.valueOf(i));
                }
                ts = SHYPUtil.getCurrentTimestamp();
                //reqOrder.replace("timestamp", ts.toString());
                reqSign.replace("timestamp", ts.toString());
                nonceStr = SHYPUtil.generateNonceStr();
                //reqOrder.replace("nonce_str",nonceStr);
                reqSign.replace("nonce_str",nonceStr);
                sign = SHYPUtil.generateNCSignature(reqSign, CommonConfig.appTenantSecret, SHYPConstants.SignType.MD5);
                //reqOrder.replace("sign", sign);
                reqSign.replace("sign", sign);
                FileUtils.writeLog("MD5",sign);
                result = request.requestWithoutCert(SinepharmConstants.GETCLEANSALES, ts.toString(), reqSign,reqOrder, false, SHYPConstants.RequestMethod.POST);
                ncSaleResult = JSONObject.parseObject(result,CleanSalesResult.class);
                if(!Boolean.valueOf(JSONUtil.getByPath(JSONUtil.parse(result),"success").toString()))
                {
                    FileUtils.writeLog("error",reqOrder.toString());
                    FileUtils.writeLog("error",result);
                    continue;
                }
                param.clear();
                //处理返回的第一页数据
                for(CleanSales ncSale :ncSaleResult.getData().getRecords()) {
                    mdmCleanSales = new MdmCleanSales();
                    //PropertyUtils.copyProperties(mdmProduct, product);
                    try {
                        BeanUtils.copyProperties(ncSale, mdmCleanSales);
                        param.clear();
                        //ncSaleList.add(mdmInvoicingDirection);
                        param.put("id", ncSale.getId());
                        directionList = cleanSalesMapper.selectByMap(param);
                        if (directionList.size() > 0) {
                            //entity = directionList.get(0);
                            cleanSalesMapper.update(mdmCleanSales, new QueryWrapper<MdmCleanSales>().eq("id", ncSale.getId()));
                        } else {
                            cleanSalesMapper.insert(mdmCleanSales);
                        }
                    }
                    catch(Exception ex)
                    {
                    //记录log
                        FileUtils.writeLog("error",JSON.toJSONString(mdmCleanSales)+"接口返回:"+result);
                    }
                }
//                sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH,false);
//                MdmInvoicingDirectionMapper studentMapperNew2 = sqlSession.getMapper(MdmInvoicingDirectionMapper.class);
//                ncSaleList.stream().forEach(student -> studentMapperNew2.insert(student));
//                sqlSession.commit();
//                sqlSession.clearCache();
            }

            return JsonResult.success();
        }
        catch(Exception ex)
        {
            return JsonResult.error(ex.getMessage());
        }
    }

    @Override
    @SpecifyDataSource(value = DataSourceType.MDM)
    public JsonResult SaveCleanInventory(MDMQueryParam query) {
        try {
            Map<String, String> reqOrder = new TreeMap<>();
            reqOrder.put("tenantId", query.getTenantIdIgnore());
            reqOrder.put("tenant_key", URLEncoder.encode(CommonConfig.appTenantId,"UTF-8"));
//            reqOrder.put("businessMonth", query.getBusinessMonth());
//            reqOrder.put("executionMonth", query.getExecutionMonth());
//            reqOrder.put("starttime", URLEncoder.encode(DateUtil.format(query.getStarttime(),DateUtils.YYYY_MM_DD_HH_MM_SS),"UTF-8"));
//            reqOrder.put("endtime",URLEncoder.encode(DateUtil.format(query.getEndtime(),DateUtils.YYYY_MM_DD_HH_MM_SS),"UTF-8"));
            reqOrder.put("starttimeIgnore", URLEncoder.encode(query.getStarttimeIgnore(),"UTF-8"));
            reqOrder.put("endtimeIgnore",URLEncoder.encode(query.getEndtimeIgnore(),"UTF-8"));
//            if(!StringUtils.isEmpty(query.getDataUpdateStartDate())) {
//                reqOrder.put("dataUpdateStartDate", URLEncoder.encode(query.getDataUpdateStartDate(), "UTF-8"));
//            }
//            else
//            {
//                reqOrder.put("dataUpdateStartDate","");
//            }
//            if(!StringUtils.isEmpty(query.getDataUpdateStartDate())) {
//                reqOrder.put("dataUpdateEndDate", URLEncoder.encode(query.getDataUpdateStartDate(),"UTF-8"));
//            }
//            else
//            {
//                reqOrder.put("dataUpdateEndDate","");
//            }
            reqOrder.put("nonce_str",SHYPUtil.generateNonceStr());
            reqOrder.put("pageCurrentPage", String.valueOf(query.getPageCurrentPage()));
            reqOrder.put("pageSize", String.valueOf(query.getPageSize()));
            Long ts = SHYPUtil.getCurrentTimestamp();
            reqOrder.put("timestamp", ts.toString());
            String sign = SHYPUtil.generateSignature(reqOrder, CommonConfig.appTenantSecret, SHYPConstants.SignType.MD5);
            reqOrder.put("sign", sign);
            DDKJConfig config = new SinepharmNCConfig();
            MDMRequest request = new MDMRequest(config);
            String result = request.requestWithoutCert(SinepharmConstants.GETCLEANINVENTORY, ts.toString(), reqOrder, false, SHYPConstants.RequestMethod.POST);
            FileUtils.writeLog("CleanInventory",result);
            if(!Boolean.valueOf(JSONUtil.getByPath(JSONUtil.parse(result),"success").toString()))
            {
                return JsonResult.error(result);
            }
            CleanSalesResult ncSaleResult = JSONObject.parseObject(result,CleanSalesResult.class);
            MdmCleanSales mdmCleanSales;
            MdmInvoicingDirection entity;
            List<MdmCleanSales> directionList;
            List<MdmCleanSales> ncSaleList = new ArrayList<>();
            Map<String,Object> param = new HashMap<>();
            //处理返回的第一页数据
            for(CleanSales ncSale :ncSaleResult.getData().getRecords()) {
                mdmCleanSales = new MdmCleanSales();
                //PropertyUtils.copyProperties(mdmProduct, product);
                //try {
                BeanUtils.copyProperties(ncSale, mdmCleanSales);
                param.clear();
                //ncSaleList.add(mdmInvoicingDirection);
                param.put("id",ncSale.getId());
                directionList = cleanSalesMapper.selectByMap(param);
                if (directionList.size()>0)
                {
                    //entity = directionList.get(0);
                    cleanSalesMapper.update(mdmCleanSales,new QueryWrapper<MdmCleanSales>().eq("id",ncSale.getId()));
                }
                else {
                    cleanSalesMapper.insert(mdmCleanSales);
                }
                //}
                //catch(Exception ex)
                //{
                //记录log
                //}
            }
//            SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH,false);
//            MdmInvoicingDirectionMapper studentMapperNew = sqlSession.getMapper(MdmInvoicingDirectionMapper.class);
//            ncSaleList.stream().forEach(student -> studentMapperNew.insert(student));
//            sqlSession.commit();
//            sqlSession.clearCache();

            //获取返回的总页数
            Integer totalPage = ncSaleResult.getData().getPages();
            //循环获取剩余数据
            for (int i = 2; i <=totalPage ; i++) {
                reqOrder.replace("pageCurrentPage",String.valueOf(i));
                ts = SHYPUtil.getCurrentTimestamp();
                reqOrder.replace("timestamp", ts.toString());
                reqOrder.replace("nonce_str",SHYPUtil.generateNonceStr());
                sign = SHYPUtil.generateSignature(reqOrder, CommonConfig.appTenantSecret, SHYPConstants.SignType.MD5);
                reqOrder.replace("sign", sign);
                result = request.requestWithoutCert(SinepharmConstants.GETCLEANINVENTORY, ts.toString(), reqOrder, false, SHYPConstants.RequestMethod.POST);
                ncSaleResult = JSONObject.parseObject(result,CleanSalesResult.class);
                param.clear();
                //处理返回的第一页数据
                for(CleanSales ncSale :ncSaleResult.getData().getRecords()) {
                    mdmCleanSales = new MdmCleanSales();
                    //PropertyUtils.copyProperties(mdmProduct, product);
                    //try {
                    BeanUtils.copyProperties(ncSale, mdmCleanSales);
                    param.clear();
                    //ncSaleList.add(mdmInvoicingDirection);
                    param.put("id",ncSale.getId());
                    directionList = cleanSalesMapper.selectByMap(param);
                    if (directionList.size()>0)
                    {
                        //entity = directionList.get(0);
                        cleanSalesMapper.update(mdmCleanSales,new QueryWrapper<MdmCleanSales>().eq("id",ncSale.getId()));
                    }
                    else {
                        cleanSalesMapper.insert(mdmCleanSales);
                    }
                    //}
                    //catch(Exception ex)
                    //{
                    //记录log
                    //}
                }
//                sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH,false);
//                MdmInvoicingDirectionMapper studentMapperNew2 = sqlSession.getMapper(MdmInvoicingDirectionMapper.class);
//                ncSaleList.stream().forEach(student -> studentMapperNew2.insert(student));
//                sqlSession.commit();
//                sqlSession.clearCache();
            }

            return JsonResult.success(result);
        }
        catch(Exception ex)
        {
            return JsonResult.error(ex.getMessage());
        }
    }

    @Override
    @SpecifyDataSource(value = DataSourceType.MDM)
    public JsonResult SaveMonthTarget(MDMQueryParam query) {
        try {
            Map<String, String> reqOrder = new TreeMap<>();
            Map<String, String> reqSign = new TreeMap<>();
            //reqOrder.put("tenantId", query.getTenantIdIgnore());
            //reqOrder.put("tenant_key", URLEncoder.encode(CommonConfig.appTenantId,"UTF-8"));
            //reqSign.put("tenantId", query.getTenantIdIgnore());
            reqSign.put("tenant_key", URLEncoder.encode(CommonConfig.appTenantId,"UTF-8"));
//            reqOrder.put("businessMonth", query.getBusinessMonth());
//            reqOrder.put("executionMonth", query.getExecutionMonth());
//            reqOrder.put("starttime", URLEncoder.encode(DateUtil.format(query.getStarttime(),DateUtils.YYYY_MM_DD_HH_MM_SS),"UTF-8"));
//            reqOrder.put("endtime",URLEncoder.encode(DateUtil.format(query.getEndtime(),DateUtils.YYYY_MM_DD_HH_MM_SS),"UTF-8"));
            reqOrder.put("targetUpdateTime", URLEncoder.encode(query.getStarttimeIgnore(),"UTF-8"));
            //reqOrder.put("endtimeIgnore",URLEncoder.encode(query.getEndtimeIgnore(),"UTF-8"));
//            if(!StringUtils.isEmpty(query.getDataUpdateStartDate())) {
//                reqOrder.put("dataUpdateStartDate", URLEncoder.encode(query.getDataUpdateStartDate(), "UTF-8"));
//            }
//            else
//            {
//                reqOrder.put("dataUpdateStartDate","");
//            }
//            if(!StringUtils.isEmpty(query.getDataUpdateStartDate())) {
//                reqOrder.put("dataUpdateEndDate", URLEncoder.encode(query.getDataUpdateStartDate(),"UTF-8"));
//            }
//            else
//            {
//                reqOrder.put("dataUpdateEndDate","");
//            }
            String nonce_str = SHYPUtil.generateNonceStr();
            if(!StringUtils.isEmpty(query.getNonce_str())){
                nonce_str = query.getNonce_str();
            }
            //reqOrder.put("nonce_str",nonce_str);
            reqSign.put("nonce_str",nonce_str);
            reqOrder.put("pageCurrentPage", String.valueOf(query.getPageCurrentPage()));
            reqOrder.put("pageSize", String.valueOf(query.getPageSize()));
            reqOrder.put("targetStartMonth",query.getStartMonthIgnore());
            reqOrder.put("targetEndMonth",query.getEndMonthIgnore());
            Long ts = SHYPUtil.getCurrentTimestamp();
            if(StringUtils.isEmpty(query.getTimestamp())){
            //reqOrder.put("timestamp", ts.toString());
            reqSign.put("timestamp", ts.toString());
            }else{
                //reqOrder.put("timestamp", query.getTimestamp());
                reqSign.put("timestamp", query.getTimestamp());

            }

            String sign = SHYPUtil.generateSignature(reqSign, CommonConfig.appTenantSecret, SHYPConstants.SignType.MD5);
            //reqOrder.put("sign", sign);
            reqSign.put("sign", sign);

            DDKJConfig config = new SinepharmConfig();
            MDMRequest request = new MDMRequest(config);
            String result = request.requestWithoutCert(SinepharmConstants.GETMONTHTARGET, ts.toString(), reqSign,reqOrder, false, SHYPConstants.RequestMethod.POST);
            FileUtils.writeLog("MonthTarget",result);
            if(!Boolean.valueOf(JSONUtil.getByPath(JSONUtil.parse(result),"success").toString()))
            {
                return JsonResult.error(result);
            }
            MonthTargetResult ncSaleResult = JSONObject.parseObject(result,MonthTargetResult.class);
            MdmMonthTarget mdmCleanSales;
            MdmInvoicingDirection entity;
            List<MdmMonthTarget> directionList;
            List<MdmMonthTarget> ncSaleList = new ArrayList<>();
            Map<String,Object> param = new HashMap<>();
            //处理返回的第一页数据
            for(MonthTarget ncSale :ncSaleResult.getData().getRecords()) {
                mdmCleanSales = new MdmMonthTarget();
                //PropertyUtils.copyProperties(mdmProduct, product);
                //try {
                BeanUtils.copyProperties(ncSale, mdmCleanSales);
                param.clear();
                //ncSaleList.add(mdmInvoicingDirection);
                param.put("id",ncSale.getId());
                directionList = monthTargetMapper.selectByMap(param);
                if (directionList.size()>0)
                {
                    //entity = directionList.get(0);
                    monthTargetMapper.update(mdmCleanSales,new QueryWrapper<MdmMonthTarget>().eq("id",ncSale.getId()));
                }
                else {
                    monthTargetMapper.insert(mdmCleanSales);
                }
                //}
                //catch(Exception ex)
                //{
                //记录log
                //}
            }
//            SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH,false);
//            MdmInvoicingDirectionMapper studentMapperNew = sqlSession.getMapper(MdmInvoicingDirectionMapper.class);
//            ncSaleList.stream().forEach(student -> studentMapperNew.insert(student));
//            sqlSession.commit();
//            sqlSession.clearCache();

            //获取返回的总页数
            Integer totalPage = ncSaleResult.getData().getPages();
            //循环获取剩余数据
            for (int i = 2; i <=totalPage ; i++) {
                reqOrder.replace("pageCurrentPage",String.valueOf(i));
                ts = SHYPUtil.getCurrentTimestamp();
                //reqOrder.replace("timestamp", ts.toString());
                nonce_str = SHYPUtil.generateNonceStr();
                //reqOrder.replace("nonce_str",nonce_str);
                reqSign.replace("timestamp", ts.toString());
                reqSign.replace("nonce_str",nonce_str);
                sign = SHYPUtil.generateSignature(reqSign, CommonConfig.appTenantSecret, SHYPConstants.SignType.MD5);
                //reqOrder.replace("sign", sign);
                reqSign.replace("sign", sign);
                result = request.requestWithoutCert(SinepharmConstants.GETMONTHTARGET, ts.toString(), reqOrder, false, SHYPConstants.RequestMethod.POST);
                ncSaleResult = JSONObject.parseObject(result,MonthTargetResult.class);
                param.clear();
                //处理返回的第一页数据
                for(MonthTarget ncSale :ncSaleResult.getData().getRecords()) {
                    mdmCleanSales = new MdmMonthTarget();
                    //PropertyUtils.copyProperties(mdmProduct, product);
                    //try {
                    BeanUtils.copyProperties(ncSale, mdmCleanSales);
                    param.clear();
                    //ncSaleList.add(mdmInvoicingDirection);
                    param.put("id",ncSale.getId());
                    directionList = monthTargetMapper.selectByMap(param);
                    if (directionList.size()>0)
                    {
                        //entity = directionList.get(0);
                        monthTargetMapper.update(mdmCleanSales,new QueryWrapper<MdmMonthTarget>().eq("id",ncSale.getId()));
                    }
                    else {
                        monthTargetMapper.insert(mdmCleanSales);
                    }
                    //}
                    //catch(Exception ex)
                    //{
                    //记录log
                    //}
                }
//                sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH,false);
//                MdmInvoicingDirectionMapper studentMapperNew2 = sqlSession.getMapper(MdmInvoicingDirectionMapper.class);
//                ncSaleList.stream().forEach(student -> studentMapperNew2.insert(student));
//                sqlSession.commit();
//                sqlSession.clearCache();
            }

            return JsonResult.success(result);
        }
        catch(Exception ex)
        {
            return JsonResult.error(ex.getMessage());
        }
    }
}
