// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.admin.query.C37InfCkKpdBillQuery;
import com.javaweb.admin.vo.c37infckkpdbill.C37InfCkKpdBillListVo;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.constant.C37InfRkKpdBillConstant;
import com.javaweb.admin.entity.C37InfRkKpdBill;
import com.javaweb.admin.mapper.C37InfRkKpdBillMapper;
import com.javaweb.admin.query.C37InfRkKpdBillQuery;
import com.javaweb.admin.service.IC37InfRkKpdBillService;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.c37infrkkpdbill.C37InfRkKpdBillInfoVo;
import com.javaweb.admin.vo.c37infrkkpdbill.C37InfRkKpdBillListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  * 红字销售订单 服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2022-11-26
  */
@Service
public class C37InfRkKpdBillServiceImpl extends BaseServiceImpl<C37InfRkKpdBillMapper, C37InfRkKpdBill> implements IC37InfRkKpdBillService {

    @Autowired
    private C37InfRkKpdBillMapper c37InfRkKpdBillMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult getList(BaseQuery query) {
        C37InfRkKpdBillQuery c37InfRkKpdBillQuery = (C37InfRkKpdBillQuery) query;
        // 查询条件
        QueryWrapper<C37InfRkKpdBill> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("danj_no","kaip_time","danw_no","caigou_staff","caoz_staff","hanghao","shangp_no","num","lot","shengc_char","youx_char","yans_rlt","price","amount","tuih_reason","zt","kh_zzbm","yez_id");
        if(!StringUtils.isEmpty(c37InfRkKpdBillQuery.getDanwNo()))
        {
            queryWrapper.like("danw_no",c37InfRkKpdBillQuery.getDanwNo());
        }
        if(!StringUtils.isEmpty(c37InfRkKpdBillQuery.getDanjNo()))
        {
            queryWrapper.like("danj_no",c37InfRkKpdBillQuery.getDanjNo());
        }
        if(!StringUtils.isEmpty(c37InfRkKpdBillQuery.getCaigouStaff()))
        {
            queryWrapper.like("caigou_staff",c37InfRkKpdBillQuery.getCaigouStaff());
        }
        if(!StringUtils.isEmpty(c37InfRkKpdBillQuery.getShangpNo()))
        {
            queryWrapper.like("shangp_no",c37InfRkKpdBillQuery.getShangpNo());
        }
        if(!StringUtils.isEmpty(c37InfRkKpdBillQuery.getLot()))
        {
            queryWrapper.like("lot",c37InfRkKpdBillQuery.getLot());
        }
        if(!StringUtils.isEmpty(c37InfRkKpdBillQuery.getYansRlt()))
        {
            queryWrapper.like("yans_rlt",c37InfRkKpdBillQuery.getYansRlt());
        }
        if(!StringUtils.isEmpty(c37InfRkKpdBillQuery.getZt()))
        {
            queryWrapper.eq("zt",c37InfRkKpdBillQuery.getZt());
        }        // queryWrapper.orderByDesc("id");

        // 获取数据列表
        IPage<C37InfRkKpdBill> page = new Page<>(c37InfRkKpdBillQuery.getPage(), c37InfRkKpdBillQuery.getLimit());
        IPage<C37InfRkKpdBill> pageData = c37InfRkKpdBillMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            C37InfRkKpdBillListVo c37InfRkKpdBillListVo = Convert.convert(C37InfRkKpdBillListVo.class, x);
            return c37InfRkKpdBillListVo;
        });
        return JsonResult.success(pageData);
    }
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult GetXsddList(BaseQuery query) {
        C37InfRkKpdBillQuery c37InfRkKpdBillQuery = (C37InfRkKpdBillQuery) query;

        // 获取数据列表
        if(c37InfRkKpdBillQuery.getPage()==null)
        {
            c37InfRkKpdBillQuery.setPage(1);
            c37InfRkKpdBillQuery.setLimit(100000);
        }
        IPage<C37InfRkKpdBillListVo> page = new Page<>(c37InfRkKpdBillQuery.getPage(), c37InfRkKpdBillQuery.getLimit());
        IPage<C37InfRkKpdBillListVo> pageData = c37InfRkKpdBillMapper.GetXsddList(page, c37InfRkKpdBillQuery);
        return JsonResult.success(pageData);
    }

    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult GetXsddDetailList(BaseQuery query) {
        C37InfRkKpdBillQuery c37InfRkKpdBillQuery = (C37InfRkKpdBillQuery) query;

        // 获取数据列表
        if(c37InfRkKpdBillQuery.getPage()==null)
        {
            c37InfRkKpdBillQuery.setPage(1);
            c37InfRkKpdBillQuery.setLimit(100000);
        }
        IPage<C37InfRkKpdBillListVo> page = new Page<>(c37InfRkKpdBillQuery.getPage(), c37InfRkKpdBillQuery.getLimit());
        IPage<C37InfRkKpdBillListVo> pageData = c37InfRkKpdBillMapper.GetXsddDetailList(page, c37InfRkKpdBillQuery);
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        C37InfRkKpdBill entity = (C37InfRkKpdBill) super.getInfo(id);
        // 返回视图Vo
        C37InfRkKpdBillInfoVo c37InfRkKpdBillInfoVo = new C37InfRkKpdBillInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, c37InfRkKpdBillInfoVo);
        return c37InfRkKpdBillInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(C37InfRkKpdBill entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
        } else {
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(C37InfRkKpdBill entity) {
        entity.setMark(0);
        return super.delete(entity);
    }

}