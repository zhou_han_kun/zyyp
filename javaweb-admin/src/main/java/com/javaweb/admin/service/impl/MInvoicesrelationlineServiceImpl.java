// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.admin.entity.MInvoicesrelationline;
import com.javaweb.admin.mapper.MInvoicesrelationlineMapper;
import com.javaweb.admin.query.MInvoicesrelationlineQuery;
import com.javaweb.admin.service.IMInvoicesrelationlineService;
import com.javaweb.admin.vo.minvoicesrelationline.MInvoicesrelationlineInfoVo;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
  * <p>
  * 两票关系明细表 服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2024-01-21
  */
@Service
public class MInvoicesrelationlineServiceImpl extends BaseServiceImpl<MInvoicesrelationlineMapper, MInvoicesrelationline> implements IMInvoicesrelationlineService {

    @Autowired
    private MInvoicesrelationlineMapper mInvoicesrelationlineMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        MInvoicesrelationlineQuery mInvoicesrelationlineQuery = (MInvoicesrelationlineQuery) query;
        // 查询条件
        QueryWrapper<MInvoicesrelationline> queryWrapper = new QueryWrapper<>();
        // queryWrapper.orderByDesc("id");
        if(!StringUtils.isNull(mInvoicesrelationlineQuery.getBusinessid()))
        {
            queryWrapper.eq("relationbusinessid",mInvoicesrelationlineQuery.getBusinessid());
        }else {
            queryWrapper.eq("relationbusinessid","");
        }

        // 获取数据列表
        IPage<MInvoicesrelationline> page = new Page<>(mInvoicesrelationlineQuery.getPage(), mInvoicesrelationlineQuery.getLimit());
        IPage<MInvoicesrelationline> pageData = mInvoicesrelationlineMapper.selectPage(page, queryWrapper);
//        pageData.convert(x -> {
//            MInvoicesrelationlineListVo mInvoicesrelationlineListVo = Convert.convert(MInvoicesrelationlineListVo.class, x);
//            return mInvoicesrelationlineListVo;
//        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        MInvoicesrelationline entity = (MInvoicesrelationline) super.getInfo(id);
        // 返回视图Vo
        MInvoicesrelationlineInfoVo mInvoicesrelationlineInfoVo = new MInvoicesrelationlineInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, mInvoicesrelationlineInfoVo);
        return mInvoicesrelationlineInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(MInvoicesrelationline entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
            entity.setUpdateTime(DateUtils.now());
            entity.setUpdateUser(ShiroUtils.getUserId());
        } else {
            entity.setCreateTime(DateUtils.now());
            entity.setCreateUser(ShiroUtils.getUserId());
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(MInvoicesrelationline entity) {
        entity.setUpdateTime(DateUtils.now());
        entity.setUpdateUser(1);
        entity.setMark(0);
        return super.delete(entity);
    }

}