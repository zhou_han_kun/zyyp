// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.admin.entity.*;
import com.javaweb.admin.mapper.*;
import com.javaweb.admin.query.VSyPoDetailQuery;
import com.javaweb.admin.service.INcServiceLogService;
import com.javaweb.admin.vo.c37infckgttzdbill.C37InfCkGttzdBillListVo;
import com.javaweb.admin.vo.syinvspec.SyInvspecListVo;
import com.javaweb.admin.vo.vsypodetail.VSyPoDetailListVo;
import com.javaweb.admin.vo.vsysodetail.VSySoDetailListVo;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.utils.*;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.query.SySyxxQuery;
import com.javaweb.admin.service.ISySyxxService;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.sysyxx.SySyxxInfoVo;
import com.javaweb.admin.vo.sysyxx.SySyxxListVo;
import com.shyp.daodikeji.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

/**
  * <p>
  *  服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2023-03-28
  */
@Service
public class SySyxxServiceImpl extends BaseServiceImpl<SySyxxMapper, SySyxx> implements ISySyxxService {

    @Autowired
    private SySyxxMapper sySyxxMapper;

    @Autowired
    private VSySoDetailMapper vSySoDetailMapper;

    @Autowired
    private VSyPoDetailMapper vSyPoDetailMapper;

    @Autowired
    INcServiceLogService ncServiceLogService;

    @Autowired
    SyPoDetailLogMapper poDetailLogMapper;

    @Autowired
    VSyOnhandMapper onhandMapper;

    @Autowired
    private SyInvspecMapper syInvspecMapper;
    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        SySyxxQuery sySyxxQuery = (SySyxxQuery) query;
        // 查询条件
        QueryWrapper<SySyxx> queryWrapper = new QueryWrapper<>();
        // queryWrapper.orderByDesc("id");
        if(!StringUtils.isEmpty(sySyxxQuery.getMaterialCode()))
        {
            queryWrapper.like("material_code",sySyxxQuery.getMaterialCode());
        }
        if(!StringUtils.isEmpty(sySyxxQuery.getBatchCode()))
        {
            queryWrapper.like("batch_code",sySyxxQuery.getBatchCode());
        }
        if(!StringUtils.isEmpty(sySyxxQuery.getRiqiBegin()))
        {
            queryWrapper.ge("sale_date",sySyxxQuery.getRiqiBegin());
        }
        if(!StringUtils.isEmpty(sySyxxQuery.getRiqiEnd()))
        {
            queryWrapper.le("sale_date",sySyxxQuery.getRiqiEnd());
        }
        queryWrapper.orderByDesc("id");

        // 获取数据列表
        IPage<SySyxx> page = new Page<>(sySyxxQuery.getPage(), sySyxxQuery.getLimit());
        IPage<SySyxx> pageData = sySyxxMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            SySyxxListVo sySyxxListVo = Convert.convert(SySyxxListVo.class, x);
            return sySyxxListVo;
        });
        return JsonResult.success(pageData);
    }


    @Override
    public JsonResult getYpkcList(VSyPoDetailQuery query) {
        IPage<VSyOnhand> page = new Page<>(query.getPage(), query.getLimit());
        IPage<VSyOnhand> pageData = onhandMapper.getYpkcList(page, query);
        return JsonResult.success(pageData);

    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        SySyxx entity = (SySyxx) super.getInfo(id);
        // 返回视图Vo
        SySyxxInfoVo sySyxxInfoVo = new SySyxxInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, sySyxxInfoVo);
        return sySyxxInfoVo;
    }

    @Override
    public JsonResult SaveSyxx(BaseQuery query) {
        try {
            SySyxxQuery syxxQuery = (SySyxxQuery)query;
            Map<String, String> reqOrder = new TreeMap<>();
            reqOrder.put("app_id", CommonConfig.appId);
            reqOrder.put("enterprise_id", CommonConfig.enterpriseId);
            reqOrder.put("material_code", syxxQuery.getMaterialCode());
            reqOrder.put("batch_code", syxxQuery.getBatchCode());
            Long ts = SHYPUtil.getCurrentTimestamp();
            reqOrder.put("timestamp", ts.toString());
            String sign = SHYPUtil.generateDaodiSignature(reqOrder, CommonConfig.appSecret, SHYPConstants.SignType.MD5);
            reqOrder.put("sign", sign);
            DDKJConfig config = new SHYPConfig();
            SHYPRequest request = new SHYPRequest(config);
            String result = request.requestWithoutCert(SHYPConstants.ASSOCIATION_TRACE_URL_SUFFIX, ts.toString(), reqOrder, false, SHYPConstants.RequestMethod.GET);

            AssociationTrace trace = JSONObject.parseObject(result, AssociationTrace.class);
            SySyxx entity;
            for (DataList data : trace.getData().getData_list()) {
                entity = new SySyxx();
                Map<String,Object> param = new HashMap<>();
                param.put("xh_trace_code",data.getXh_trace_code());
                sySyxxMapper.deleteByMap(param);
                entity.setSaleDate(data.getSale_date());
                entity.setMaterialName(data.getMaterial_name());
                entity.setMaterialName(data.getMaterial_name());
                entity.setMaterialCode(data.getMaterial_code());
                entity.setXhTraceCode(data.getXh_trace_code());
                entity.setPackageSpecification(data.getPackage_specification());
                entity.setCustomerName(data.getCustomer_name());
                entity.setBatchCode(data.getBatch_code());
                entity.setFilingNo(data.getFiling_no());
                entity.setSpecification(data.getSpecification());
                entity.setMaterialCategoryName(data.getMaterial_category_name());
                entity.setTs(DateUtils.now());
                //this.edit(entity);
                entity.setCreateUser(1);
                entity.setCreateTime(DateUtils.now());
                sySyxxMapper.insert(entity);
            }
            return JsonResult.success(trace);
        }
        catch(Exception ex)
        {
            return JsonResult.error(ex.getMessage());
        }


    }


    @Override
    public JsonResult ZyyptYprk(String query) {
        try {
            //SySyxxQuery syxxQuery = (SySyxxQuery)query;
            SoapRequestParam requestParam = JSONObject.parseObject(query, SoapRequestParam.class);
            String parameterObj = JSON.toJSONString(requestParam.getParams().get(0).getParameterObj());
            String encryptData = SM2Utils.getInstance().encrypt(parameterObj,CommonConfig.dataKey);
            Params sourceParam = requestParam.getParams().get(0);
            EncryptSoapRequestParam postData = new EncryptSoapRequestParam();
            postData.setId(requestParam.getId());
            postData.setMethod(requestParam.getMethod());
            EncryptParms encryptParms = new EncryptParms();
            encryptParms.setCorpId(sourceParam.getCorpId());
            encryptParms.setSystemId(sourceParam.getSystemId());
            encryptParms.setService(sourceParam.getService());
            encryptParms.setParameterObj(encryptData);
            List<EncryptParms> listParam = new ArrayList<>();
            listParam.add(encryptParms);
            postData.setParams(listParam);

            //requestParam.getParams().get(0).setParameterObj(encryptParam);
            String strPostData = JSON.toJSONString(postData);
            Map<String, String> reqOrder = new TreeMap<>();
            reqOrder.put("corpid", CommonConfig.corpId);
            reqOrder.put("systemid", CommonConfig.systemId);
            String ts = DateUtils.dateTimeNow("yyyyMMddHHmmssSSS");
            reqOrder.put("timestamp", ts);
            String sign = SHYPUtil.generateSM2Signature(reqOrder, CommonConfig.signKey);
            reqOrder.put("sign", sign);
            DDKJConfig config = new ZYYPTConfig();
            SHYPRequest request = new SHYPRequest(config);
            String result = request.postData("", ts, reqOrder, strPostData,false);

            ZYYPTResult ret = JSONObject.parseObject(result, ZYYPTResult.class);
            return JsonResult.success(ret);
        }
        catch(Exception ex)
        {
            return JsonResult.error(ex.getMessage());
        }


    }

    @Override
    public JsonResult ZyyptYprk(String[] orderNos) {
        if(orderNos==null || orderNos.length==0)
        {
            return JsonResult.error("缺少单据号参数");
        }
        String danj_no = orderNos[0];
        NcServiceLog log = new NcServiceLog();
        String temp = "";
        String strPostData="";
        try
        {
            //根据单据号获取采购订单数据
            VSyPoDetailListVo poDetail = vSyPoDetailMapper.getPoDetail(danj_no);
            if(poDetail==null)
            {
                return JsonResult.error("未查询到入库流水号:"+danj_no+"对应的采购入库单信息");
            }
            //判断转换数量
            if(poDetail.getConvertNum()==null)
            {
                return JsonResult.error("物料号:"+poDetail.getInvcode()+"未维护转换数量");
            }

            //获取追溯信息
            SySyxx syxx = getSySyxx(poDetail.getTbdm(),poDetail.getVlotno());
            if(syxx==null)
            {
                return JsonResult.error("未查询到物料号:"+poDetail.getTbdm()+"-批次号:"+poDetail.getVlotno()+"对应的溯源信息");
            }
            ParameterObj parameterObj = new ParameterObj();
            SoapRequestParam requestParam = new SoapRequestParam();
            List<Params> params = new ArrayList<>();
            Params pItem = new Params();
            pItem.setCorpId(CommonConfig.corpId);
            pItem.setSystemId(CommonConfig.systemId);
            pItem.setService("SY022");
            requestParam.setId("1");
            requestParam.setMethod("SoaRequest");
            requestParam.setParams(params);
            parameterObj.setHostCode("C3");
            parameterObj.setInstockNumber(poDetail.getPkInvoiceB());
            parameterObj.setEntId(CommonConfig.entId);
            parameterObj.setEntName("上海信谊天一药业有限公司");
            parameterObj.setTraceCode(syxx.getXhTraceCode());
            parameterObj.setYpName(poDetail.getInvname());
            parameterObj.setPackGuige(poDetail.getInvspec());
            parameterObj.setMedicinalOrigin(poDetail.getOrigin());
            parameterObj.setSourceFlag("2");
            parameterObj.setManufacturer(poDetail.getCustname());
            parameterObj.setProductBatch(poDetail.getVlotno());
            parameterObj.setArrivalTime(poDetail.getDbilldate());
            BigDecimal nastNum = poDetail.getConvertNum();
            parameterObj.setNum(String.valueOf(nastNum));
            parameterObj.setUnit("KG");
            parameterObj.setCheckResult("合格");
            parameterObj.setEnviornment("阴凉干燥");
            parameterObj.setYpCode(poDetail.getTbdm());
            parameterObj.setFilingNo(syxx.getFilingNo());
            parameterObj.setSincerity("0");
            pItem.setParameterObj(parameterObj);
            params.add(pItem);
            requestParam.setParams(params);
            temp = JSON.toJSONString(requestParam);

            String parameterStr = JSON.toJSONString(parameterObj);
            String encryptData = SM2Utils.getInstance().encrypt(parameterStr,CommonConfig.dataKey);
            EncryptSoapRequestParam postData = new EncryptSoapRequestParam();
            Params sourceParam = requestParam.getParams().get(0);
            postData.setId(requestParam.getId());
            postData.setMethod(requestParam.getMethod());
            EncryptParms encryptParms = new EncryptParms();
            encryptParms.setCorpId(sourceParam.getCorpId());
            encryptParms.setSystemId(sourceParam.getSystemId());
            encryptParms.setService(sourceParam.getService());
            encryptParms.setParameterObj(encryptData);
            List<EncryptParms> listParam = new ArrayList<>();
            listParam.add(encryptParms);
            postData.setParams(listParam);
            strPostData = JSON.toJSONString(postData);
            Map<String, String> reqOrder = new TreeMap<>();
            reqOrder.put("corpid", CommonConfig.corpId);
            reqOrder.put("systemid", CommonConfig.systemId);
            String ts = DateUtils.dateTimeNow("yyyyMMddHHmmssSSS");
            reqOrder.put("timestamp", ts);
            String sign = SHYPUtil.generateSM2Signature(reqOrder, CommonConfig.signKey);
            reqOrder.put("sign", sign);
            DDKJConfig config = new ZYYPTConfig();
            SHYPRequest request = new SHYPRequest(config);
            String result = request.postData("", ts, reqOrder, strPostData,false);
            ZYYPTResult ret = JSONObject.parseObject(result, ZYYPTResult.class);
            log.setSubmitNo(danj_no);
            log.setRequestBody("原始报文:\r\n"+temp+"\r\n提交接口的加密报文:\r\n"+strPostData);
            log.setSubmitTime(DateUtils.now());
            log.setResponseBody(result);
            log.setResponseCode(String.valueOf(ret.getCode()));
            log.setResponseResult(ret.getMessage());
            ncServiceLogService.save(log);
            SyPoDetailLog poDetailLog = new SyPoDetailLog();
            poDetailLog.setOrderType("PO");
            poDetailLog.setPkInvoiceB(poDetail.getPkInvoiceB());
            if(ret.getCode()==0)
            {
                poDetailLog.setStatus("1");
                poDetailLogMapper.insert(poDetailLog);
                return JsonResult.success(ret);
            }
            else
            {
                poDetailLog.setStatus("0");
                poDetailLogMapper.insert(poDetailLog);
                return JsonResult.error(ret.getMessage());
            }


        }
        catch(Exception ex)
        {
            log.setSubmitNo(danj_no);
            log.setRequestBody("原始报文:\r\n"+temp+"\r\n提交接口的加密报文:\r\n"+strPostData);
            log.setSubmitTime(DateUtils.now());
            log.setResponseBody(ex.getMessage());
            log.setResponseCode("500");
            log.setResponseResult("");
            ncServiceLogService.save(log);
            return JsonResult.error(ex.getMessage());
        }
    }

    private SySyxx getSySyxx(String ypCode,String vlotno)
    {
        //根据新统编代码获取老统编代码
        Map<String,Object> param = new HashMap<>();
        param.put("tbdm",ypCode);
        List<SyInvspec> list = syInvspecMapper.selectByMap(param);
        if(list.size()==0)
        {
            return null;
        }
        String material_code = list.get(0).getLtbdm();


        QueryWrapper<SySyxx> queryWrapper = new QueryWrapper<>();
        // queryWrapper.orderByDesc("id");
        queryWrapper.eq("material_code",material_code);
        queryWrapper.eq("batch_code",vlotno);
        queryWrapper.orderByDesc("xh_trace_code");

        // 获取数据列表
        IPage<SySyxx> page = new Page<>(1, 1);
        IPage<SySyxx> pageData = sySyxxMapper.selectPage(page, queryWrapper);
        if(pageData.getRecords().size()>0)
        {
            SySyxx syxx = pageData.getRecords().get(0);
            return syxx;
        }
        else
        {
            return null;
        }


    }


    private String getInstockNo(String ypCode,String vlotno)
    {
        QueryWrapper<VSyPoDetail> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("pk_invoice_b");
        queryWrapper.eq("tbdm",ypCode);
        queryWrapper.eq("vlotno",vlotno);
        queryWrapper.orderByDesc("dbilldate");

        // 获取数据列表
        IPage<VSyPoDetail> page = new Page<>(1, 1);
        IPage<VSyPoDetail> pageData = vSyPoDetailMapper.selectPage(page, queryWrapper);
        if(pageData.getSize()>0)
        {
            VSyPoDetail poDetail = pageData.getRecords().get(0);
            return poDetail.getPkInvoiceB();
        }
        else
        {
            return "";
        }


    }


    @Override
    //饮片经营-采购入库
    public JsonResult ZyyptYpck(String[] orderNos) {
        if(orderNos==null || orderNos.length==0)
        {
            return JsonResult.error("缺少单据号参数");
        }
        String danj_no = orderNos[0];
        NcServiceLog log = new NcServiceLog();
        String temp = "";
        String strPostData="";
        try
        {
            //根据单据号获取销售订单数据
            //根据单据号获取采购订单数据
            VSySoDetailListVo soDetail = vSySoDetailMapper.getSoDetail(danj_no);
            if(soDetail==null)
            {
                return JsonResult.error("未查询到销售出库流水号:"+danj_no+"对应的销售出库信息");
            }
            //判断转换数量
            if(soDetail.getConvertNum()==null)
            {
                return JsonResult.error("物料号:"+soDetail.getInvcode()+"未维护转换数量");
            }
            //VSySoDetail soDetail = vSySoDetailMapper.selectOne(new QueryWrapper<VSySoDetail>().eq("vsrccode",danj_no));

            //生成内部追溯码
            String trace_code="";
            String dbilldate = soDetail.getDbilldate();
            QueryWrapper<SyPoDetailLog> queryWrapperPk = new QueryWrapper<SyPoDetailLog>().eq("pk_invoice_b",danj_no);
            if(poDetailLogMapper.selectCount(queryWrapperPk)==0) {
                trace_code = "31" + CommonConfig.entId + "YP" + DateUtils.parseDateToStr("yyyyMMdd", DateUtil.parse(dbilldate)) + StringUtils.right(DateUtils.dateTimeNow(), 5);
            }
            else
            {
                IPage<SyPoDetailLog> page = new Page<>(1, 1);
                IPage<SyPoDetailLog> pageData = poDetailLogMapper.selectPage(page, queryWrapperPk);
                SyPoDetailLog poDetailLog = pageData.getRecords().get(0);
                trace_code = poDetailLog.getTraceCode();
            }

            ParameterObj parameterObj = new ParameterObj();
            SoapRequestParam requestParam = new SoapRequestParam();
            List<Params> params = new ArrayList<>();
            Params pItem = new Params();
            pItem.setCorpId(CommonConfig.corpId);
            pItem.setSystemId(CommonConfig.systemId);
            pItem.setService("SY023");
            requestParam.setId("1");
            requestParam.setMethod("SoaRequest");
            parameterObj.setHostCode("C3");
            parameterObj.setSaleNumber(soDetail.getCsaleinvoicebid());
            parameterObj.setEntId(CommonConfig.entId);
            parameterObj.setTraceCode(trace_code);
            parameterObj.setSelfCode(trace_code);
            parameterObj.setYpName(soDetail.getInvname());
            parameterObj.setYpGuige(soDetail.getInvspec());
            parameterObj.setInstockNo(getInstockNo(soDetail.getTbdm(),soDetail.getVlotno()));
            parameterObj.setSaleTime(soDetail.getDbilldate());
            parameterObj.setNum(String.valueOf(soDetail.getConvertNum()));
            parameterObj.setUnit("KG");
            parameterObj.setProductBatch(soDetail.getVlotno());
            parameterObj.setContact(soDetail.getCustname());
            parameterObj.setYpCode(soDetail.getTbdm());
            parameterObj.setWhereabouts("5");
            pItem.setParameterObj(parameterObj);
            params.add(pItem);
            requestParam.setParams(params);
            temp = JSON.toJSONString(requestParam);
//            parameterObj.setArrivalTime("");
//            parameterObj.setCheckResult("");
//            parameterObj.setEntName("");
//            parameterObj.setEnviornment("");
//            parameterObj.setFilingNo("");
//            parameterObj.setInstockNumber("");
//            parameterObj.setManufacturer("");
//            parameterObj.setMedicinalOrigin("");
//            parameterObj.setPackGuige("");
//            parameterObj.setSincerity("");
//            parameterObj.setSourceFlag("");
            String parameterStr = JSON.toJSONString(parameterObj);
            String encryptData = SM2Utils.getInstance().encrypt(parameterStr,CommonConfig.dataKey);
            EncryptSoapRequestParam postData = new EncryptSoapRequestParam();
            Params sourceParam = requestParam.getParams().get(0);
            postData.setId(requestParam.getId());
            postData.setMethod(requestParam.getMethod());
            EncryptParms encryptParms = new EncryptParms();
            encryptParms.setCorpId(sourceParam.getCorpId());
            encryptParms.setSystemId(sourceParam.getSystemId());
            encryptParms.setService(sourceParam.getService());
            encryptParms.setParameterObj(encryptData);
            List<EncryptParms> listParam = new ArrayList<>();
            listParam.add(encryptParms);
            postData.setParams(listParam);
            strPostData = JSON.toJSONString(postData);
            Map<String, String> reqOrder = new TreeMap<>();
            reqOrder.put("corpid", CommonConfig.corpId);
            reqOrder.put("systemid", CommonConfig.systemId);
            String ts = DateUtils.dateTimeNow("yyyyMMddHHmmssSSS");
            reqOrder.put("timestamp", ts);
            String sign = SHYPUtil.generateSM2Signature(reqOrder, CommonConfig.signKey);
            reqOrder.put("sign", sign);
            DDKJConfig config = new ZYYPTConfig();
            SHYPRequest request = new SHYPRequest(config);
            String result = request.postData("", ts, reqOrder, strPostData,false);

            ZYYPTResult ret = JSONObject.parseObject(result, ZYYPTResult.class);
            log.setSubmitNo(danj_no);
            log.setRequestBody("原始报文:\r\n"+temp+"\r\n提交接口的加密报文:\r\n"+strPostData);
            log.setSubmitTime(DateUtils.now());
            log.setResponseBody(result);
            log.setResponseCode(String.valueOf(ret.getCode()));
            log.setResponseResult(ret.getMessage());
            ncServiceLogService.save(log);
            SyPoDetailLog poDetailLog = new SyPoDetailLog();
            poDetailLog.setOrderType("PO");
            poDetailLog.setTraceCode(trace_code);
            poDetailLog.setPkInvoiceB(soDetail.getCsaleinvoicebid());
            QueryWrapper<SyPoDetailLog> queryWrapper = new QueryWrapper<SyPoDetailLog>().eq("trace_code",trace_code);
            if(ret.getCode()==0)
            {
                poDetailLog.setStatus("1");
                //if(poDetailLogMapper.selectCount(queryWrapper)==0) {
                    poDetailLogMapper.insert(poDetailLog);
                //}
                //else
                //{
                //    poDetailLogMapper.update(poDetailLog,queryWrapper);
                //}
                return JsonResult.success(ret);
            }
            else
            {
                poDetailLog.setStatus("0");
                //if(poDetailLogMapper.selectCount(queryWrapper)==0) {
                    poDetailLogMapper.insert(poDetailLog);
                //}
                return JsonResult.error(ret.getMessage());
            }

        }
        catch(Exception ex)
        {
            return JsonResult.error(ex.getMessage());
        }
    }

    @Override
    //饮片经营-销售出库
    public JsonResult ZyyptYpck(String query) {
        try {
            //SySyxxQuery syxxQuery = (SySyxxQuery)query;
            SoapRequestParam requestParam = JSONObject.parseObject(query, SoapRequestParam.class);
            String parameterObj = JSON.toJSONString(requestParam.getParams().get(0).getParameterObj());
            String encryptData = SM2Utils.getInstance().encrypt(parameterObj,CommonConfig.dataKey);
            Params sourceParam = requestParam.getParams().get(0);
            EncryptSoapRequestParam postData = new EncryptSoapRequestParam();
            postData.setId(requestParam.getId());
            postData.setMethod(requestParam.getMethod());
            EncryptParms encryptParms = new EncryptParms();
            encryptParms.setCorpId(sourceParam.getCorpId());
            encryptParms.setSystemId(sourceParam.getSystemId());
            encryptParms.setService(sourceParam.getService());
            encryptParms.setParameterObj(encryptData);
            List<EncryptParms> listParam = new ArrayList<>();
            listParam.add(encryptParms);
            postData.setParams(listParam);

            //requestParam.getParams().get(0).setParameterObj(encryptParam);
            String strPostData = JSON.toJSONString(postData);
            Map<String, String> reqOrder = new TreeMap<>();
            reqOrder.put("corpid", CommonConfig.corpId);
            reqOrder.put("systemid", CommonConfig.systemId);
            String ts = DateUtils.dateTimeNow("yyyyMMddHHmmssSSS");
            reqOrder.put("timestamp", ts);
            String sign = SHYPUtil.generateSM2Signature(reqOrder, CommonConfig.signKey);
            reqOrder.put("sign", sign);
            DDKJConfig config = new ZYYPTConfig();
            SHYPRequest request = new SHYPRequest(config);
            String result = request.postData("", ts, reqOrder, strPostData,false);

            ZYYPTResult ret = JSONObject.parseObject(result, ZYYPTResult.class);
            return JsonResult.success(ret);
        }
        catch(Exception ex)
        {
            return JsonResult.error(ex.getMessage());
        }


    }

    @Override
    //追溯码作废
    public JsonResult ZyyptZsmzf(String traceCode) {
        if(StringUtils.isEmpty(traceCode))
        {
            return JsonResult.error("追溯码不能为空");
        }
        NcServiceLog log = new NcServiceLog();
        String temp = "";
        String strPostData="";
        try
        {
            //根据单据号获取采购订单数据
            ParameterObj parameterObj = new ParameterObj();
            SoapRequestParam requestParam = new SoapRequestParam();
            List<Params> params = new ArrayList<>();
            Params pItem = new Params();
            pItem.setCorpId(CommonConfig.corpId);
            pItem.setSystemId(CommonConfig.systemId);
            pItem.setService("SY024");
            requestParam.setId("1");
            requestParam.setMethod("SoaRequest");
            requestParam.setParams(params);
            parameterObj.setHostCode("C3");
            parameterObj.setTraceCode(traceCode);
            parameterObj.setDeleted("2");
            pItem.setParameterObj(parameterObj);
            params.add(pItem);
            requestParam.setParams(params);
            temp = JSON.toJSONString(requestParam);

            String parameterStr = JSON.toJSONString(parameterObj);
            String encryptData = SM2Utils.getInstance().encrypt(parameterStr,CommonConfig.dataKey);
            EncryptSoapRequestParam postData = new EncryptSoapRequestParam();
            Params sourceParam = requestParam.getParams().get(0);
            postData.setId(requestParam.getId());
            postData.setMethod(requestParam.getMethod());
            EncryptParms encryptParms = new EncryptParms();
            encryptParms.setCorpId(sourceParam.getCorpId());
            encryptParms.setSystemId(sourceParam.getSystemId());
            encryptParms.setService(sourceParam.getService());
            encryptParms.setParameterObj(encryptData);
            List<EncryptParms> listParam = new ArrayList<>();
            listParam.add(encryptParms);
            postData.setParams(listParam);
            strPostData = JSON.toJSONString(postData);
            Map<String, String> reqOrder = new TreeMap<>();
            reqOrder.put("corpid", CommonConfig.corpId);
            reqOrder.put("systemid", CommonConfig.systemId);
            String ts = DateUtils.dateTimeNow("yyyyMMddHHmmssSSS");
            reqOrder.put("timestamp", ts);
            String sign = SHYPUtil.generateSM2Signature(reqOrder, CommonConfig.signKey);
            reqOrder.put("sign", sign);
            DDKJConfig config = new ZYYPTConfig();
            SHYPRequest request = new SHYPRequest(config);
            String result = request.postData("", ts, reqOrder, strPostData,false);
            ZYYPTResult ret = JSONObject.parseObject(result, ZYYPTResult.class);
            log.setSubmitNo(traceCode);
            log.setRequestBody("原始报文:\r\n"+temp+"\r\n提交接口的加密报文:\r\n"+strPostData);
            log.setSubmitTime(DateUtils.now());
            log.setResponseBody(result);
            log.setResponseCode(String.valueOf(ret.getCode()));
            log.setResponseResult(ret.getMessage());
            ncServiceLogService.save(log);
            return JsonResult.success(ret.getResult());

        }
        catch(Exception ex)
        {
            log.setSubmitNo(traceCode);
            log.setRequestBody("原始报文:\r\n"+temp+"\r\n提交接口的加密报文:\r\n"+strPostData);
            log.setSubmitTime(DateUtils.now());
            log.setResponseBody(ex.getMessage());
            log.setResponseCode("500");
            log.setResponseResult("");
            ncServiceLogService.save(log);
            return JsonResult.error(ex.getMessage());
        }
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(SySyxx entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
            entity.setUpdateUser(ShiroUtils.getUserId());
            entity.setUpdateTime(DateUtils.now());
        } else {
            entity.setCreateUser(ShiroUtils.getUserId());
            entity.setCreateTime(DateUtils.now());
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(SySyxx entity) {
        entity.setUpdateUser(1);
        entity.setUpdateTime(DateUtils.now());
        entity.setMark(0);
        return super.delete(entity);
    }

}