package com.javaweb.admin.service;

import com.javaweb.admin.entity.YConverRate;
import com.javaweb.admin.entity.YgOrderDto;
import com.javaweb.admin.entity.YgptAddrContrast;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.utils.JsonResult;

public interface IYgOrderService {
    JsonResult GetYgOrderList(BaseQuery query);
    JsonResult ExportToNC(YgOrderDto dto);
    JsonResult ExportToYGPT(YgOrderDto dto);
    JsonResult ExportPSDToYGPT(YgOrderDto dto);
    JsonResult OrderResponseToYGPT(YgOrderDto dto);

    JsonResult PsdConfirmToYGPT(YgOrderDto dto);
    JsonResult FpConfirmToYGPT(YgOrderDto dto);

    JsonResult YQ029ToYGPT(YgOrderDto dto);
    JsonResult YQ030ToYGPT(YgOrderDto dto);
    JsonResult YQ031ToYGPT(YgOrderDto dto);
    JsonResult YQ032ToYGPT(YgOrderDto dto);


    JsonResult GetNCInvoiceList(BaseQuery query);

    JsonResult GetNC56InvoiceList(BaseQuery query);

    //获取配送的数据
    JsonResult GetNCDispatchingList(BaseQuery query);
    JsonResult GetDispatchDetail(String psdh);

    JsonResult GetNCSaleInvoice(String fph);

    JsonResult GetYConverRateList(BaseQuery query);
    JsonResult GetYgptAddrContrastList(BaseQuery query);
    JsonResult GetNCInvList(BaseQuery query);
    JsonResult GetNC56InvList(BaseQuery query);
    JsonResult GetNCCustList(BaseQuery query);
    JsonResult GetNC56CustList(BaseQuery query);
    JsonResult UpdateInvCode(YConverRate entity);
    JsonResult UpdateCustAddr(YgptAddrContrast entity);
    JsonResult GetNCInvInfoList(BaseQuery query);
    JsonResult GetNCCustInfoList(BaseQuery query);
    JsonResult GetNCCustAddrList(BaseQuery query);
}
