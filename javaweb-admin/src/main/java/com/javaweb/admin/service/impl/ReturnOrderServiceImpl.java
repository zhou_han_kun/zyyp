package com.javaweb.admin.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.admin.entity.ReturnOrder;
import com.javaweb.admin.entity.ReturnOrderDetail;
import com.javaweb.admin.entity.VSyOnhand;
import com.javaweb.admin.mapper.ReturnOrderMapper;
import com.javaweb.admin.query.C37InfYpyhjlQuery;
import com.javaweb.admin.query.ReturnQuery;
import com.javaweb.admin.service.IReturnOrderService;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReturnOrderServiceImpl extends BaseServiceImpl<ReturnOrderMapper,ReturnOrder> implements IReturnOrderService {
    @Autowired
    private ReturnOrderMapper returnOrderMapper;
    @Override
    public JsonResult getReturnOrderList(BaseQuery query) {
        ReturnQuery returnQuery = (ReturnQuery) query;
        IPage<ReturnOrder> page = new Page<>(query.getPage(), query.getLimit());
        IPage<ReturnOrder> pageData = returnOrderMapper.getReturnOrderList(page, returnQuery);
        return JsonResult.success(pageData);

    }

    @Override
    public JsonResult getOrderDetail(String csaleorderid) {
        List<ReturnOrderDetail> list = returnOrderMapper.gerOrderDetail(csaleorderid);
        return JsonResult.success(list);
    }
}
