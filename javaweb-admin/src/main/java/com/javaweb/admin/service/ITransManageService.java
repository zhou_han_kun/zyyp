package com.javaweb.admin.service;

import com.javaweb.admin.entity.MCarInfo;
import com.javaweb.admin.entity.MDriverInfo;
import com.javaweb.admin.entity.NCCustAddrDoc;
import com.javaweb.admin.query.*;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.utils.JsonResult;

public interface ITransManageService {
    JsonResult getTransUserList(BaseQuery query);

    JsonResult saveTransUser(MDriverInfo entity);

    JsonResult getCarInfoList(BaseQuery query);

    JsonResult saveCarInfo(MCarInfo entity);

    JsonResult getCustAddrList(BaseQuery query);

    JsonResult saveCustAddr(NCCustAddrDoc entity);

    JsonResult getGpsDataList(BaseQuery query);

    JsonResult getTempdataList(BaseQuery query);


    JsonResult getRecordList(BaseQuery query);

    JsonResult getDetail(BaseQuery query);

    JsonResult getTempdata1and2List(BaseQuery query);

    JsonResult getTransCoordinates(BaseQuery query);

    JsonResult getOrderForGoods(BaseQuery query);

    JsonResult getSmsevaluate(BaseQuery query);

    JsonResult getTransDetail(BaseQuery query);

    JsonResult smsevaluateReissue(SmsevaluateReissueQuery query);

    JsonResult getCustevaluateList(BaseQuery query);

    JsonResult custevaluateReissue(CustevaluateReissueQuery query);
}
