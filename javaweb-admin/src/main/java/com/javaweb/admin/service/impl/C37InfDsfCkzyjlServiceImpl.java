// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.constant.C37InfDsfCkzyjlConstant;
import com.javaweb.admin.entity.C37InfDsfCkzyjl;
import com.javaweb.admin.mapper.C37InfDsfCkzyjlMapper;
import com.javaweb.admin.query.C37InfDsfCkzyjlQuery;
import com.javaweb.admin.service.IC37InfDsfCkzyjlService;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.c37infdsfckzyjl.C37InfDsfCkzyjlInfoVo;
import com.javaweb.admin.vo.c37infdsfckzyjl.C37InfDsfCkzyjlListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  *  服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2023-03-23
  */
@Service
public class C37InfDsfCkzyjlServiceImpl extends BaseServiceImpl<C37InfDsfCkzyjlMapper, C37InfDsfCkzyjl> implements IC37InfDsfCkzyjlService {

    @Autowired
    private C37InfDsfCkzyjlMapper c37InfDsfCkzyjlMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult getList(BaseQuery query) {
        C37InfDsfCkzyjlQuery c37InfDsfCkzyjlQuery = (C37InfDsfCkzyjlQuery) query;
        // 查询条件
        QueryWrapper<C37InfDsfCkzyjl> queryWrapper = new QueryWrapper<>();
        // queryWrapper.orderByDesc("id");
        queryWrapper.select("chandi,baoz_num,yaop_category,jixing,danw_no,cbdj,lot,price,yez_no,cunc_condition,fuh_staff,zhongy_haitat,piz_no,shengchan_date,baoz_danw,amount,youx_date,chuk_riqi,yew_type,hanghao,maker,danw_name,shij_lss,shij_num,xians_loc,yez_id,jihua_num,kh_zzbm,riqi_date,cbje,yaop_guig,yewdj_no,danj_no,fhy,shij_js,jihua_lss,chinese_name,scrq,zt,ssxkcyr,shangp_no");
        if(!StringUtils.isEmpty(c37InfDsfCkzyjlQuery.getDanjNo()))
        {
            queryWrapper.like("danj_no",c37InfDsfCkzyjlQuery.getDanjNo());
        }
        if(!StringUtils.isEmpty(c37InfDsfCkzyjlQuery.getChineseName()))
        {
            queryWrapper.like("chinese_name",c37InfDsfCkzyjlQuery.getChineseName());
        }
        if(!StringUtils.isEmpty(c37InfDsfCkzyjlQuery.getLot()))
        {
            queryWrapper.like("lot",c37InfDsfCkzyjlQuery.getLot());
        }
        if(!StringUtils.isEmpty(c37InfDsfCkzyjlQuery.getRiqiBegin()))
        {
            queryWrapper.ge("riqi_date",c37InfDsfCkzyjlQuery.getRiqiBegin());
        }
        if(!StringUtils.isEmpty(c37InfDsfCkzyjlQuery.getRiqiEnd()))
        {
            queryWrapper.le("riqi_date",c37InfDsfCkzyjlQuery.getRiqiEnd());
        }
        queryWrapper.orderByDesc("riqi_date");

        // 获取数据列表
        IPage<C37InfDsfCkzyjl> page = new Page<>(c37InfDsfCkzyjlQuery.getPage(), c37InfDsfCkzyjlQuery.getLimit());
        IPage<C37InfDsfCkzyjl> pageData = c37InfDsfCkzyjlMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            C37InfDsfCkzyjlListVo c37InfDsfCkzyjlListVo = Convert.convert(C37InfDsfCkzyjlListVo.class, x);
            return c37InfDsfCkzyjlListVo;
        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        C37InfDsfCkzyjl entity = (C37InfDsfCkzyjl) super.getInfo(id);
        // 返回视图Vo
        C37InfDsfCkzyjlInfoVo c37InfDsfCkzyjlInfoVo = new C37InfDsfCkzyjlInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, c37InfDsfCkzyjlInfoVo);
        return c37InfDsfCkzyjlInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(C37InfDsfCkzyjl entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
        } else {
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(C37InfDsfCkzyjl entity) {
        entity.setMark(0);
        return super.delete(entity);
    }

}