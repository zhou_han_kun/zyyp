package com.javaweb.admin.service;

import com.javaweb.admin.entity.SytLocalbillMain;
import com.javaweb.admin.entity.SytLocalbillMainDto;
import com.javaweb.common.utils.JsonResultSYT;

public interface ISytLocalbillMainService {
    JsonResultSYT saveLocalbillMain(SytLocalbillMainDto entity);
}
