// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.constant.C37InfYpyhjlConstant;
import com.javaweb.admin.entity.C37InfYpyhjl;
import com.javaweb.admin.mapper.C37InfYpyhjlMapper;
import com.javaweb.admin.query.C37InfYpyhjlQuery;
import com.javaweb.admin.service.IC37InfYpyhjlService;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.c37infypyhjl.C37InfYpyhjlInfoVo;
import com.javaweb.admin.vo.c37infypyhjl.C37InfYpyhjlListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  * 养护记录表 服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2022-11-26
  */
@Service
public class C37InfYpyhjlServiceImpl extends BaseServiceImpl<C37InfYpyhjlMapper, C37InfYpyhjl> implements IC37InfYpyhjlService {

    @Autowired
    private C37InfYpyhjlMapper c37InfYpyhjlMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult getList(BaseQuery query) {
        C37InfYpyhjlQuery c37InfYpyhjlQuery = (C37InfYpyhjlQuery) query;
        // 查询条件
        QueryWrapper<C37InfYpyhjl> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("riqi_date","shangp_no","chinese_name","yaop_guig","jixing","cunc_condition","baoz_danw",
                "baoz_num","maker","piz_no","zhongy_haitat","yez_id","yaop_category","lot","shengchan_date","youx_date",
                "num","gjrq","clyj","kuc_state","is_zdyh","remark","yez_no","scrq","zt","jih_date","xians_loc",
                "yangh_staff","hanghao","danj_no");
        // queryWrapper.orderByDesc("id");
        if(!StringUtils.isEmpty(c37InfYpyhjlQuery.getShangpNo()))
        {
            queryWrapper.like("shangp_no",c37InfYpyhjlQuery.getShangpNo());
        }
        if(!StringUtils.isEmpty(c37InfYpyhjlQuery.getRiqiBegin()))
        {
            queryWrapper.ge("riqi_date",c37InfYpyhjlQuery.getRiqiBegin());
        }
        if(!StringUtils.isEmpty(c37InfYpyhjlQuery.getRiqiEnd()))
        {
            queryWrapper.le("riqi_date",c37InfYpyhjlQuery.getRiqiEnd());
        }

        if(!StringUtils.isEmpty(c37InfYpyhjlQuery.getChineseName()))
        {
            queryWrapper.like("chinese_name",c37InfYpyhjlQuery.getChineseName());
        }
        if(!StringUtils.isEmpty(c37InfYpyhjlQuery.getYaopGuig()))
        {
            queryWrapper.like("yaop_guig",c37InfYpyhjlQuery.getYaopGuig());
        }
        if(!StringUtils.isEmpty(c37InfYpyhjlQuery.getJixing()))
        {
            queryWrapper.like("jixing",c37InfYpyhjlQuery.getJixing());
        }
        if(!StringUtils.isEmpty(c37InfYpyhjlQuery.getLot()))
        {
            queryWrapper.like("lot",c37InfYpyhjlQuery.getLot());
        }
        if(!StringUtils.isEmpty(c37InfYpyhjlQuery.getMaker()))
        {
            queryWrapper.like("maker",c37InfYpyhjlQuery.getMaker());
        }
        if(!StringUtils.isEmpty(c37InfYpyhjlQuery.getPizNo()))
        {
            queryWrapper.like("piz_no",c37InfYpyhjlQuery.getPizNo());
        }
        if(!StringUtils.isEmpty(c37InfYpyhjlQuery.getZt()))
        {
            queryWrapper.eq("zt",c37InfYpyhjlQuery.getZt());
        }
        // 获取数据列表
        IPage<C37InfYpyhjl> page = new Page<>(c37InfYpyhjlQuery.getPage(), c37InfYpyhjlQuery.getLimit());
        IPage<C37InfYpyhjl> pageData = c37InfYpyhjlMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            C37InfYpyhjlListVo c37InfYpyhjlListVo = Convert.convert(C37InfYpyhjlListVo.class, x);
            return c37InfYpyhjlListVo;
        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        C37InfYpyhjl entity = (C37InfYpyhjl) super.getInfo(id);
        // 返回视图Vo
        C37InfYpyhjlInfoVo c37InfYpyhjlInfoVo = new C37InfYpyhjlInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, c37InfYpyhjlInfoVo);
        return c37InfYpyhjlInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(C37InfYpyhjl entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
        } else {
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(C37InfYpyhjl entity) {
        entity.setMark(0);
        return super.delete(entity);
    }

}