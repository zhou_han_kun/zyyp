// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.admin.query.SpbcInvcodeQuery;
import com.javaweb.admin.vo.spbc.SpbcInvcodeVo;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.config.UploadFileConfig;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.constant.SpbcInvsetConstant;
import com.javaweb.admin.entity.SpbcInvset;
import com.javaweb.admin.mapper.SpbcInvsetMapper;
import com.javaweb.admin.query.SpbcInvsetQuery;
import com.javaweb.admin.service.ISpbcInvsetService;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.spbcinvset.SpbcInvsetInfoVo;
import com.javaweb.admin.vo.spbcinvset.SpbcInvsetListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.Serializable;
import java.util.*;

/**
  * <p>
  *  服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2023-11-27
  */
@Service
public class SpbcInvsetServiceImpl extends BaseServiceImpl<SpbcInvsetMapper, SpbcInvset> implements ISpbcInvsetService {

    @Autowired
    private SpbcInvsetMapper spbcInvsetMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        SpbcInvsetQuery spbcInvsetQuery = (SpbcInvsetQuery) query;

        // 获取数据列表
        IPage<SpbcInvset> page = new Page<>(spbcInvsetQuery.getPage(), spbcInvsetQuery.getLimit());
        IPage<SpbcInvset> pageData = spbcInvsetMapper.getSpbcInvsetList(page, spbcInvsetQuery);
        pageData.convert(x -> {
            SpbcInvsetListVo spbcInvsetListVo = Convert.convert(SpbcInvsetListVo.class, x);
            return spbcInvsetListVo;
        });
        return JsonResult.success(pageData);
    }

    @Override
    public JsonResult GetSpbcInvcodeList(BaseQuery query) {
        SpbcInvcodeQuery spbcInvcodeQuery = (SpbcInvcodeQuery) query;
        IPage<SpbcInvcodeVo> page = new Page<>(spbcInvcodeQuery.getPage(), spbcInvcodeQuery.getLimit());
        IPage<SpbcInvcodeVo> pageData = spbcInvsetMapper.GetSpbcInvcodeList(page, spbcInvcodeQuery);
        return JsonResult.success(pageData);
    }

    @Override
    public JsonResult GetSpbcInvsetDetailList(BaseQuery query) {
        SpbcInvcodeQuery spbcInvcodeQuery = (SpbcInvcodeQuery) query;
        IPage<SpbcInvcodeVo> page = new Page<>(spbcInvcodeQuery.getPage(), spbcInvcodeQuery.getLimit());
        IPage<SpbcInvcodeVo> pageData = spbcInvsetMapper.GetSpbcInvsetDetailList(page, spbcInvcodeQuery);
        return JsonResult.success(pageData);
    }


    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        SpbcInvset entity = (SpbcInvset) super.getInfo(id);
        // 返回视图Vo
        SpbcInvsetInfoVo spbcInvsetInfoVo = new SpbcInvsetInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, spbcInvsetInfoVo);
        return spbcInvsetInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(SpbcInvset entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
            entity.setUpdateUser(ShiroUtils.getUserId());
            entity.setUpdateTime(DateUtils.now());
        } else {
            entity.setCreateUser(ShiroUtils.getUserId());
            entity.setCreateTime(DateUtils.now());
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(SpbcInvset entity) {
        entity.setUpdateUser(1);
        entity.setUpdateTime(DateUtils.now());
        entity.setMark(0);
        return super.delete(entity);
    }

    @Override
    public JsonResult deleteByIds(Integer[] spbcinvsetIds) {
        try {
            for(Integer id : spbcinvsetIds) {
                spbcInvsetMapper.deleteById(id,ShiroUtils.getUserId());
            }
            return JsonResult.success();
        }
        catch(Exception ex)
        {
            return JsonResult.error(ex.getMessage());
        }
    }

    @Override
    public JsonResult uploadFile(MultipartFile file) {
        String dateStr = DateUtils.dateTimeNow();
        String fileName = dateStr + '_' + file.getOriginalFilename();
        if(file.getSize()>1024*1024*5){
            return JsonResult.error("上传文件大小不能超过5M");
        }
        File filePath = new File("");
        //当前项目下路径+静态资源实际存储路径
        String path=filePath.getAbsolutePath();
        String templatePath = UploadFileConfig.uploadFolder;
        templatePath=templatePath.replace('\\','/');
        File dests = new File(templatePath);
        File dest = new File(dests, fileName);
        //文件上传-覆盖
        try {
            // 检测是否存在目录
            if(!dests.exists()){
                dests.mkdirs();
            }
            // getParentFile获取上一级
            if (!dest.getParentFile().exists()) {
                dest.getParentFile().mkdirs();
            }
            file.transferTo(dest);

            return JsonResult.success(fileName);//上传成功,文件名以逗号隔开
        } catch (Exception e) {
            return JsonResult.error("上传失败，失败原因："+e.getMessage());
        }
    }

    @Override
    public JsonResult updateMark(Integer id,String mark) {
        UpdateWrapper<SpbcInvset> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("mark",mark).set("approver",ShiroUtils.getUserInfo().getRealname()).set("approve_time",DateUtils.now()).eq("id",id);
        spbcInvsetMapper.update(new SpbcInvset(), updateWrapper);
        return JsonResult.success();
    }

    @Override
    public JsonResult copyById(Integer id){
        SpbcInvset spbcInvset = new SpbcInvset();
        spbcInvsetMapper.copyById(id, ShiroUtils.getUserId(), ShiroUtils.getUserInfo().getRealname(), spbcInvset);
        spbcInvsetMapper.copyDetailByPid(id, spbcInvset.getId(), ShiroUtils.getUserId());
        return JsonResult.success();
    }

    @Override
    public JsonResult getValidatedList(BaseQuery query){
        SpbcInvsetQuery spbcInvsetQuery = (SpbcInvsetQuery) query;
        // 查询条件
        QueryWrapper<SpbcInvset> queryWrapper = new QueryWrapper<>();
        queryWrapper.ge("enddate", (DateUtils.getDate()+" 00:00:00"));
        queryWrapper.eq("mark",2);
        queryWrapper.orderByDesc("id");

        // 获取数据列表
        IPage<SpbcInvset> page = new Page<>(spbcInvsetQuery.getPage(), spbcInvsetQuery.getLimit());
        IPage<SpbcInvset> pageData = spbcInvsetMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            SpbcInvsetListVo spbcInvsetListVo = Convert.convert(SpbcInvsetListVo.class, x);
            return spbcInvsetListVo;
        });
        return JsonResult.success(pageData);
    }
    @Override
    public JsonResult getAllList(BaseQuery query){
        SpbcInvsetQuery spbcInvsetQuery = (SpbcInvsetQuery) query;
        // 查询条件
        QueryWrapper<SpbcInvset> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("mark",2);
        queryWrapper.orderByDesc("id");

        // 获取数据列表
        IPage<SpbcInvset> page = new Page<>(spbcInvsetQuery.getPage(), spbcInvsetQuery.getLimit());
        IPage<SpbcInvset> pageData = spbcInvsetMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            SpbcInvsetListVo spbcInvsetListVo = Convert.convert(SpbcInvsetListVo.class, x);
            return spbcInvsetListVo;
        });
        return JsonResult.success(pageData);
    }

    @Override
    public Integer getSpbcNum(Integer id){
        return spbcInvsetMapper.getSpbcNum(id);
    }

}