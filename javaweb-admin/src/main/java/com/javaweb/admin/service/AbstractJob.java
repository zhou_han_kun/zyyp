package com.javaweb.admin.service;

import com.javaweb.admin.config.ScheduledConfig;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;

import java.util.concurrent.ScheduledFuture;

/**
        * @author liangxuelong
        * @version 1.0
        * @description: 定时任务抽象类
        * @date 2021/8/27 10:36
        */
public abstract class AbstractJob implements Runnable {
    @Setter
    @Getter
    private String customerCode;

    public Logger logger = LoggerFactory.getLogger(AbstractJob.class);

    private ScheduledFuture<?> scheduledFuture;

    @Autowired
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;

    /**
     * 获取任务名称
     */
    public abstract String getName();

    /**
     * 开始执行任务
     */
    public abstract void execute();


    /**
     * @description:  创建定时任务
     * @param: cron  时间表达式
     * @return: void
     * @author liangxuelong
     * @date: 2021/8/27
     */
    public void createJob(String cron){
        try {
            scheduledFuture =  threadPoolTaskScheduler.schedule(this, new CronTrigger(cron));
            // 将 scheduledFuture 保存下来用于停止任务使用
            ScheduledConfig.cache.put(this.getName(), scheduledFuture);
        } catch (Exception e) {
            logger.error("创建定时任务 【"+getName()+"】 失败！");
            e.printStackTrace();
        }
    }

    /**
     * 定时任务执行之前
     */
    public void begin(){
        logger.info("------开始执行【"+getName()+"】任务------");
    };

    /**
     * 定时任务执行之后
     */
    public void end(){
        logger.info("------结束执行【"+getName()+"】任务------");
    };
    /**
     * 取消执行任务
     */
    public void cancel(){
        try {
            if(scheduledFuture!=null)
                scheduledFuture.cancel(true);
            logger.info("取消执行【"+getName()+"】 任务！");
        } catch (Exception e) {
            logger.error("取消执行 【"+getName()+"】失败 ！");
            e.printStackTrace();
        }
    }
    @Override
    public void run(){
        begin();
        execute();
        end();
    };
}


