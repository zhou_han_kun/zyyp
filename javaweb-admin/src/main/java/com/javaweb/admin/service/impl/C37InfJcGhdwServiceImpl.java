// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.constant.C37InfJcGhdwConstant;
import com.javaweb.admin.entity.C37InfJcGhdw;
import com.javaweb.admin.mapper.C37InfJcGhdwMapper;
import com.javaweb.admin.query.C37InfJcGhdwQuery;
import com.javaweb.admin.service.IC37InfJcGhdwService;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.c37infjcghdw.C37InfJcGhdwInfoVo;
import com.javaweb.admin.vo.c37infjcghdw.C37InfJcGhdwListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  * 供应商资料 服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2022-11-24
  */
@Service
public class C37InfJcGhdwServiceImpl extends BaseServiceImpl<C37InfJcGhdwMapper, C37InfJcGhdw> implements IC37InfJcGhdwService {

    @Autowired
    private C37InfJcGhdwMapper c37InfJcGhdwMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult getList(BaseQuery query) {
        C37InfJcGhdwQuery c37InfJcGhdwQuery = (C37InfJcGhdwQuery) query;
        // 查询条件
        QueryWrapper<C37InfJcGhdw> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("lius_no","danw_no","danw_name","beactive","gengx_time","lianx_staff","diz_phone","zt","yez_id","yez_no","yez_flg","lianxr_phone","yingyzz_no","yingyzz_youxq","jyxkz_no","jyxkz_youxq","gmp_no","gmp_youxq","ltxkz_no","ltxkz_youxq","yljgxkz_no","yljgxkz_youxq","qxjyxkz_no","qxjyxkz_youxq","papz_no","papz_youxq","kh_zzbm");
        if(!StringUtils.isEmpty(c37InfJcGhdwQuery.getDanwNo()))
        {
            queryWrapper.like("danw_no",c37InfJcGhdwQuery.getDanwNo());
        }
        if(!StringUtils.isEmpty(c37InfJcGhdwQuery.getDanwName()))
        {
            queryWrapper.like("danw_name",c37InfJcGhdwQuery.getDanwName());
        }
        if(!StringUtils.isEmpty(c37InfJcGhdwQuery.getRiqiBegin()))
        {
            queryWrapper.ge("gengx_time",c37InfJcGhdwQuery.getRiqiBegin());
        }
        if(!StringUtils.isEmpty(c37InfJcGhdwQuery.getRiqiEnd()))
        {
            queryWrapper.le("gengx_time",c37InfJcGhdwQuery.getRiqiEnd()+" 23:59:59");
        }
        queryWrapper.orderByDesc("lius_no");
        // queryWrapper.orderByDesc("id");

        // 获取数据列表
        IPage<C37InfJcGhdw> page = new Page<>(c37InfJcGhdwQuery.getPage(), c37InfJcGhdwQuery.getLimit());
        IPage<C37InfJcGhdw> pageData = c37InfJcGhdwMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            C37InfJcGhdwListVo c37InfJcGhdwListVo = Convert.convert(C37InfJcGhdwListVo.class, x);
            return c37InfJcGhdwListVo;
        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        C37InfJcGhdw entity = (C37InfJcGhdw) super.getInfo(id);
        // 返回视图Vo
        C37InfJcGhdwInfoVo c37InfJcGhdwInfoVo = new C37InfJcGhdwInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, c37InfJcGhdwInfoVo);
        return c37InfJcGhdwInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(C37InfJcGhdw entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
        } else {
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(C37InfJcGhdw entity) {
        entity.setMark(0);
        return super.delete(entity);
    }

}