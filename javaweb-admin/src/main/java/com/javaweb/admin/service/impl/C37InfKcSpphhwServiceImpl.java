// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.admin.constant.C37InfKcSpphhwConstant;
import com.javaweb.admin.entity.C37InfKcSpphhw;
import com.javaweb.admin.mapper.C37InfKcSpphhwMapper;
import com.javaweb.admin.query.C37InfKcSpphhwQuery;
import com.javaweb.admin.service.IC37InfKcSpphhwService;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.admin.vo.c37infkcspphhw.C37InfKcSpphhwInfoVo;
import com.javaweb.admin.vo.c37infkcspphhw.C37InfKcSpphhwListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  * 库存上传单表 服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2022-11-26
  */
@Service
public class C37InfKcSpphhwServiceImpl extends BaseServiceImpl<C37InfKcSpphhwMapper, C37InfKcSpphhw> implements IC37InfKcSpphhwService {

    @Autowired
    private C37InfKcSpphhwMapper c37InfKcSpphhwMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    @SpecifyDataSource(value = DataSourceType.JZT)
    public JsonResult getList(BaseQuery query) {
        C37InfKcSpphhwQuery c37InfKcSpphhwQuery = (C37InfKcSpphhwQuery) query;
        // 查询条件
        QueryWrapper<C37InfKcSpphhw> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("yez_no","danw_name","shangp_no","chinese_name","baoz_num","baoz_danw","yaop_guig","price","je","maker"
                ,"lot","shengchan_date","youx_date","num","js","lss","scrq","rukyz_num","chukyk_num","buhyz_num","buhyk_num"
                ,"zait_num","suod_num","zt","xians_loc","ssxkcyr");
        // queryWrapper.orderByDesc("id");
        if(!StringUtils.isEmpty(c37InfKcSpphhwQuery.getShangpNo()))
        {
            queryWrapper.like("shangp_no",c37InfKcSpphhwQuery.getShangpNo());
        }
        if(!StringUtils.isEmpty(c37InfKcSpphhwQuery.getChineseName()))
        {
            queryWrapper.like("chinese_name",c37InfKcSpphhwQuery.getChineseName());
        }
        if(!StringUtils.isEmpty(c37InfKcSpphhwQuery.getYaopGuig()))
        {
            queryWrapper.like("yaop_guig",c37InfKcSpphhwQuery.getYaopGuig());
        }
        if(!StringUtils.isEmpty(c37InfKcSpphhwQuery.getLot()))
        {
            queryWrapper.like("lot",c37InfKcSpphhwQuery.getLot());
        }
        if(!StringUtils.isEmpty(c37InfKcSpphhwQuery.getDanwName()))
        {
            queryWrapper.like("danw_name",c37InfKcSpphhwQuery.getDanwName());
        }
        if(!StringUtils.isEmpty(c37InfKcSpphhwQuery.getMaker()))
        {
            queryWrapper.like("maker",c37InfKcSpphhwQuery.getMaker());
        }
        if(!StringUtils.isEmpty(c37InfKcSpphhwQuery.getZt()))
        {
            queryWrapper.eq("zt",c37InfKcSpphhwQuery.getZt());
        }


        // 获取数据列表
        IPage<C37InfKcSpphhw> page = new Page<>(c37InfKcSpphhwQuery.getPage(), c37InfKcSpphhwQuery.getLimit());
        IPage<C37InfKcSpphhw> pageData = c37InfKcSpphhwMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            C37InfKcSpphhwListVo c37InfKcSpphhwListVo = Convert.convert(C37InfKcSpphhwListVo.class, x);
            return c37InfKcSpphhwListVo;
        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        C37InfKcSpphhw entity = (C37InfKcSpphhw) super.getInfo(id);
        // 返回视图Vo
        C37InfKcSpphhwInfoVo c37InfKcSpphhwInfoVo = new C37InfKcSpphhwInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, c37InfKcSpphhwInfoVo);
        return c37InfKcSpphhwInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(C37InfKcSpphhw entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
        } else {
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(C37InfKcSpphhw entity) {
        entity.setMark(0);
        return super.delete(entity);
    }

}