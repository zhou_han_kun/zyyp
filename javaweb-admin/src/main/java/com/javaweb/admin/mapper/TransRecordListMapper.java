package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.RecordListDetail;
import com.javaweb.admin.entity.RecordListTempdata;
import com.javaweb.admin.entity.TransRecordList;
import com.javaweb.admin.query.RecordListDetailQuery;
import com.javaweb.admin.query.RecordListTempdataQuery;
import com.javaweb.admin.query.TransRecordListQuery;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface TransRecordListMapper extends BaseMapper<TransRecordList> {
    IPage<TransRecordList> getTransRecordList(IPage<TransRecordList> page, @Param("req") TransRecordListQuery req);

    @Select("select invcode, invname,SendGoodsNum,cbatchid, RejectGoodsNum  from D_SendGoodsDetailinfo with(nolock) \n" +
            "      where CgoldtaxDMCodeID=#{req.cgoldtaxdmcodeid}")
    IPage<RecordListDetail> getDetail(IPage<RecordListDetail> page, RecordListDetailQuery req);

    IPage<RecordListTempdata> getTempdata1and2List(IPage<RecordListTempdata> page, @Param("req") RecordListTempdataQuery req);
}
