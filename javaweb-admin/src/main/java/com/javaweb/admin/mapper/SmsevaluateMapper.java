package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.*;
import com.javaweb.admin.query.SmsevaluateQuery;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface SmsevaluateMapper extends BaseMapper<Smsevaluate> {
    IPage<Smsevaluate> getSmsevaluate(IPage<Smsevaluate> page, @Param("req") SmsevaluateQuery req);

     SmsevaluateStatus getStatus(@Param("req") Smsevaluate req);


    List<CustVoicecode> getCustVoicecode(@Param("req")Smsevaluate req);

    @Select(" select NC_custaddrdoc.linkPhone,NC_custaddrdoc.linkUser from NC_custaddrdoc where NC_custaddrdoc.custcode=#{req.custcode} and addrname=#{req.CustcodeSendAddr} " +
            " and NC_custaddrdoc.linkPhone is not null and NC_custaddrdoc.linkUser is not null ")
    PhoneAndUser getPhoneAndUser(@Param("req")Smsevaluate req);
    @Update("update D_SendMessageLog set message=#{message} where id=#{id}")
    void updateMessageLog(String message,String id);


}
