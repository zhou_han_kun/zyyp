// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.Spbc;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.admin.query.SpbcCustomerQuery;
import com.javaweb.admin.query.SpbcQuery;
import com.javaweb.admin.query.SpbcSummaryQuery;
import com.javaweb.admin.vo.spbc.SpbcCustAddr;
import com.javaweb.admin.vo.spbc.SpbcCustomer;
import com.javaweb.admin.vo.spbc.SpbcListVo;
import com.javaweb.admin.vo.spbc.SpbcSummary;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 鲲鹏
 * @since 2023-11-27
 */
public interface SpbcMapper extends BaseMapper<Spbc> {
    IPage<SpbcCustomer> getSpbcCustomerList(IPage<SpbcCustomer> page, @Param("req") SpbcCustomerQuery req);
    List<SpbcSummary> getSpbcSummaryList(@Param("req") SpbcSummaryQuery req);
    List<SpbcSummary> getSpbcInvoiceList(@Param("req") SpbcSummaryQuery req);

    List<SpbcSummary> getSpbcDetailList(@Param("req") SpbcSummaryQuery req);

    List<SpbcSummary> getSpbcInvoiceDetail(@Param("req") SpbcSummaryQuery req);

    List<SpbcSummary> getSpbcDetail(@Param("req") SpbcSummaryQuery req);

    List<String> getBcyjById(@Param("id") Integer id);
    List<String> getCustAddrById(@Param("id") Integer id);

    @Update("update sys_spbc set mark=0, update_user=#{updateUser}, update_time=getdate() where id=#{id}")
    void deleteById(@Param("id")Integer id, @Param("updateUser") Integer updateUser);

    @Update("update sys_spbc_detail set mark=0, update_user=#{updateUser}, update_time=getdate() where pid=#{pid} and mark=1")
    void deleteSpbcDetailByPid(@Param("pid")Integer pid, @Param("updateUser") Integer updateUser);

    @Update("update sys_spbc_invoice set mark=0, update_user=#{updateUser}, update_time=getdate() where pid=#{pid} and mark=1")
    void deleteSpbcInvoiceByPid(@Param("pid")Integer pid, @Param("updateUser") Integer updateUser);

    @Update("update sys_spbc_detail set mark=0, update_user=#{updateUser}, update_time=getdate() where id=#{id}")
    void deleteSpbcDetailById(@Param("id")Integer id, @Param("updateUser") Integer updateUser);

    @Update("update sys_spbc_invoice set mark=0, update_user=#{updateUser}, update_time=getdate() where did=#{did} and mark=1")
    void deleteSpbcInvoiceByDid(@Param("did")Integer did, @Param("updateUser") Integer updateUser);

    @Select("select id from sys_spbc_detail where pid=#{pid} and mark=1")
    List<Integer> getSpbcDetailIdList(@Param("pid")Integer pid);

    String getNewSqdh(@Param("sqdh") String sqdh);

    @Insert("insert into sys_spbc_yj values(#{id}, #{yjid})")
    void insertBcyj(@Param("id") Integer id, @Param("yjid") String yjid);

    @Insert("insert into sys_spbc_custaddr values(#{id}, #{custaddr})")
    void insertCustAddr(@Param("id") Integer id, @Param("custaddr") String custaddr);

    @Delete("delete from sys_spbc_yj where spbc_id=#{id}")
    void deleteYjById(@Param("id") Integer id);

    @Delete("delete from sys_spbc_custaddr where spbc_id=#{id}")
    void deleteCustAddrById(@Param("id") Integer id);

    List<SpbcCustAddr> getCustAddrList(@Param("custcode") String custcode);

    IPage<Spbc> getSpbcList(IPage<Spbc> page, @Param("ew") SpbcQuery spbcQuery);

}
