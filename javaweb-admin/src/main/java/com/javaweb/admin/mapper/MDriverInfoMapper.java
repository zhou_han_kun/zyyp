package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.MDriverInfo;
import com.javaweb.admin.entity.NCInvoice;
import com.javaweb.admin.query.NCInvoiceQuery;
import com.javaweb.admin.query.TransUserQuery;
import org.apache.ibatis.annotations.Param;

public interface MDriverInfoMapper extends BaseMapper<MDriverInfo> {
    IPage<MDriverInfo> getDriverInfoList(IPage<MDriverInfo> page, @Param("req") TransUserQuery req);

}
