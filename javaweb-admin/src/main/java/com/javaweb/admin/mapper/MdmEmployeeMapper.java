package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.admin.entity.MdmEmployee;

public interface MdmEmployeeMapper extends BaseMapper<MdmEmployee> {
}
