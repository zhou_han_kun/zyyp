package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.YgptAddrContrast;
import com.javaweb.admin.query.YgptAddrContrastQuery;
import org.apache.ibatis.annotations.Param;

public interface YgptAddrContrastMapper extends BaseMapper<YgptAddrContrast> {
    IPage<YgptAddrContrast> GetYgptAddrContrastList(IPage<YgptAddrContrast> page, @Param("req") YgptAddrContrastQuery req);
}
