package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.NCCustAddrDoc;
import com.javaweb.admin.query.NCCustAddrDocQuery;
import org.apache.ibatis.annotations.Param;

public interface NCCustAddrDocMapper extends BaseMapper<NCCustAddrDoc> {
    IPage<NCCustAddrDoc> getCustAddrList(IPage<NCCustAddrDoc> page,@Param("req") NCCustAddrDocQuery req);
}
