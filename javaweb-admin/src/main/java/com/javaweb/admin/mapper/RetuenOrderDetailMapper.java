package com.javaweb.admin.mapper;

import com.javaweb.admin.entity.ReturnOrderDetail;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface RetuenOrderDetailMapper {
    @Select("select * from vw_returnorder_detail where csaleorderid=#{csaleorderid}")
    List<ReturnOrderDetail> getReturnOrderDetail(String csaleorderid);
}
