package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.admin.entity.MdmCleanSales;

public interface MdmCleanSalesMapper extends BaseMapper<MdmCleanSales> {
}
