package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.TempData;
import com.javaweb.admin.query.TempdataQuery;
import org.apache.ibatis.annotations.Param;

public interface TempDataMapper extends BaseMapper<TempData> {

    IPage<TempData> getTempDataList(IPage<TempData> page,@Param("req") TempdataQuery req);
}
