package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.VSyOnhand;
import com.javaweb.admin.query.C37InfCkGttzdBillQuery;
import com.javaweb.admin.query.VSyPoDetailQuery;
import com.javaweb.admin.vo.c37infckgttzdbill.C37InfCkGttzdBillListVo;
import com.javaweb.common.common.BaseQuery;
//import com.sun.xml.internal.rngom.parse.host.Base;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface VSyOnhandMapper extends BaseMapper<VSyOnhand> {
    List<VSyOnhand> getInvcodeUploadList();

    IPage<VSyOnhand> getYpkcList(IPage<VSyOnhand> page, @Param("req") VSyPoDetailQuery req);

}
