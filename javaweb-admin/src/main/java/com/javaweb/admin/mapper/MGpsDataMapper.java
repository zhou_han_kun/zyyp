package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.GpsData;
import com.javaweb.admin.entity.OrderForGoods;
import com.javaweb.admin.query.GpsDataQuery;
import com.javaweb.admin.query.OrderForGoodsQuery;
import com.javaweb.admin.query.RecordListTempdataQuery;
import com.javaweb.admin.query.TransCoordinatesQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

public interface MGpsDataMapper extends BaseMapper<GpsData> {

    IPage<GpsData> getGpsDataList(IPage<GpsData> page, @Param("req") GpsDataQuery req);

    List<GpsData> getTransCoordinatesList(@Param("req") TransCoordinatesQuery req);

    List<OrderForGoods> getOrderForGoods(@Param("req") OrderForGoodsQuery req);
}
