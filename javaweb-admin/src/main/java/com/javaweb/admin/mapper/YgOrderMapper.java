package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.YgOrder;
import com.javaweb.admin.query.YgOrderQuery;
import org.apache.ibatis.annotations.Param;

public interface YgOrderMapper extends BaseMapper<YgOrder> {
    IPage<YgOrder> GetYgOrderList(IPage<YgOrder> page, @Param("req") YgOrderQuery req);


}
