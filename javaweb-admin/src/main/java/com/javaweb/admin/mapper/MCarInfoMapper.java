package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.MCarInfo;
import com.javaweb.admin.entity.MDriverInfo;
import com.javaweb.admin.query.MCarInfoQuery;
import com.javaweb.admin.query.TransUserQuery;
import org.apache.ibatis.annotations.Param;

public interface MCarInfoMapper extends BaseMapper<MCarInfo> {
    IPage<MCarInfo> getCarInfoList(IPage<MCarInfo> page, @Param("req") MCarInfoQuery req);

    MCarInfo getHotdeviceid(@Param("req") MCarInfoQuery req);
}
