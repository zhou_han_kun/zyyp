package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.admin.entity.NCInvInfoDoc;

public interface NCInvInfoDocMapper extends BaseMapper<NCInvInfoDoc> {
}
