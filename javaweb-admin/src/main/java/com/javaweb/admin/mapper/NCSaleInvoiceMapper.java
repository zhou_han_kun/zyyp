package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.admin.entity.NCSaleInvoice;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface NCSaleInvoiceMapper extends BaseMapper<NCSaleInvoice> {
//    IPage<NCSaleInvoice> GetNCSaleInvoiceList(IPage<NCSaleInvoice> page, @Param("req") NCInvoiceQuery req);

    @Select("select * from v_ygpt_saleinvoice where fph=#{fph}")
    List<NCSaleInvoice> GetNCSaleInvoiceList(String fph);
}
