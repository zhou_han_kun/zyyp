// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.InvoiceDetail;
import com.javaweb.admin.entity.Invprice;
import com.javaweb.admin.entity.MInvoicesrelation;
import com.javaweb.admin.entity.PurchaseRecord;
import com.javaweb.admin.query.InvoiceDetailQuery;
import com.javaweb.admin.query.MInvoicesrelationQuery;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 两票关系主表 Mapper 接口
 * </p>
 *
 * @author 鲲鹏
 * @since 2024-01-21
 */
public interface MInvoicesrelationMapper extends BaseMapper<MInvoicesrelation> {
    IPage<MInvoicesrelation> GetInvoicesrelationList(IPage<MInvoicesrelation> page, @Param("req") MInvoicesrelationQuery req);

    IPage<MInvoicesrelation> getList(IPage<MInvoicesrelation> page, MInvoicesrelationQuery req);
    @Select("select * from d_sosaleinvoicedetailinfo where Csaleid = #{req.csaleid}")
    IPage<InvoiceDetail> getInvoiceDetailList(IPage<InvoiceDetail> page, InvoiceDetailQuery req);


    @Select("select DPI.vinvoicecode ,dpi.vinvoicedm ,DPI.custname,DPDI.invcode,DPDI.invname,DPDI.ninvoicenum from D_PoinvoiceDetailInfo DPDI left join D_PoinvoiceInfo DPI on DPDI.cinvoiceid=dpi.cinvoiceid where DPDI.cinvoice_bid=(select Cinvoice_bid from d_sopoconnectdetail where Csaleid=#{req.csaleid})")
    IPage<PurchaseRecord> getPurchaseRecordList(IPage<PurchaseRecord> page, InvoiceDetailQuery req);
}
