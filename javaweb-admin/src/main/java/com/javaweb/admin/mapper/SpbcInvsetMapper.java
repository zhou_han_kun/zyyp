// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.SpbcInvset;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.admin.query.SpbcInvcodeQuery;
import com.javaweb.admin.query.SpbcInvsetQuery;
import com.javaweb.admin.query.VSyPoDetailQuery;
import com.javaweb.admin.vo.spbc.SpbcInvcodeVo;
import com.javaweb.admin.vo.vsypodetail.VSyPoDetailListVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 鲲鹏
 * @since 2023-11-27
 */
public interface SpbcInvsetMapper extends BaseMapper<SpbcInvset> {
    IPage<SpbcInvcodeVo> GetSpbcInvcodeList(IPage<SpbcInvcodeVo> page, @Param("req") SpbcInvcodeQuery req);

    IPage<SpbcInvcodeVo> GetSpbcInvsetDetailList(IPage<SpbcInvcodeVo> page, @Param("req") SpbcInvcodeQuery req);

    void deleteById(@Param("id") Integer id, @Param("updateUser") Integer updateUser);

    void copyById(@Param("id") Integer id, @Param("createUser") Integer createUser, @Param("createUserName") String createUserName, @Param("spbcInvset") SpbcInvset spbcInvset);

    void copyDetailByPid(@Param("pid") Integer pid, @Param("newPid") Integer newPid, @Param("createUser") Integer createUser);

    Integer getSpbcNum(@Param("id") Integer id);

    IPage<SpbcInvset> getSpbcInvsetList(IPage<SpbcInvset> page, @Param("ew") SpbcInvsetQuery spbcInvsetQuery);
}
