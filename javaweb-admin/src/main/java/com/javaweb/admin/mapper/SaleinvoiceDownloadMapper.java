package com.javaweb.admin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.admin.entity.filenameRule.InvoiceDownloadView;
import com.javaweb.admin.entity.filenameRule.SaleinvoiceDownload;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SaleinvoiceDownloadMapper extends BaseMapper<SaleinvoiceDownload> {
    @Select("select t1.ntotalorigmny," +
            "              isnull(t1.VNOTE,'') as VNOTE," +
            "       m.id," +
            "       m.vbillcode," +
            "       m.ts," +
            "       m.id," +
            "       m.custcode," +
            "       m.vbillcode," +
            "       m.pdffilename," +
            "       m.dbilldate" +
            " from ODS_NC_SO_SALEINVOICE as t1 left join dbo.ods_nc_so_saleinvoice_download as m on t1.VGOLDTAXCODE = m.vbillcode " +
            " where m.custcode=#{customerCode}")
    List<SaleinvoiceDownload> getSaleinvoiceDownload(String customerCode);


    @Select("select * from v_nc_saleinvoice_download")
    List<InvoiceDownloadView> getInvoiceDownloadList();
}
