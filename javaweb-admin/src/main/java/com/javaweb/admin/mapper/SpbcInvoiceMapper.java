package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.admin.entity.SpbcInvoice;

public interface SpbcInvoiceMapper extends BaseMapper<SpbcInvoice> {
}
