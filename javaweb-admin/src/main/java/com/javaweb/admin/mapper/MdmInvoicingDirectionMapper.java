package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.admin.entity.MdmInvoicingDirection;

public interface MdmInvoicingDirectionMapper extends BaseMapper<MdmInvoicingDirection> {
}
