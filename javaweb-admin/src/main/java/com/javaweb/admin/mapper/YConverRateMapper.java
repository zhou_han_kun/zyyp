package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.YConverRate;
import com.javaweb.admin.query.YConverRateQuery;
import org.apache.ibatis.annotations.Param;

public interface YConverRateMapper extends BaseMapper<YConverRate> {
    IPage<YConverRate> GetYConverRateList(IPage<YConverRate> page, @Param("req") YConverRateQuery req);
}
