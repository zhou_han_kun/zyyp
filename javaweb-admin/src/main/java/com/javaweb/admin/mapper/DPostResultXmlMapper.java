package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.admin.entity.PostResultXml;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface DPostResultXmlMapper extends BaseMapper<PostResultXml> {
    @Select("select * from d_postresultxml where postid=#{postid order by ts desc}")
    List<PostResultXml> getLogList(String postid);
}
