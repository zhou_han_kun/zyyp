package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.ReturnOrder;
import com.javaweb.admin.entity.ReturnOrderDetail;
import com.javaweb.admin.query.ReturnQuery;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ReturnOrderMapper extends BaseMapper<ReturnOrder> {
    IPage<ReturnOrder> getReturnOrderList(IPage<ReturnOrder> page, @Param("req") ReturnQuery req);

    @Select("select * from vw_returnorder_detail where csaleorderid=#{csaleorderid}")
    List<ReturnOrderDetail> gerOrderDetail(String csaleorderid);
}
