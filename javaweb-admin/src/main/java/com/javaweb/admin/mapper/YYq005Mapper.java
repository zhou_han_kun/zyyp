// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.admin.entity.NCCustAddrDoc;
import com.javaweb.admin.entity.NCCustInfoDoc;
import com.javaweb.admin.entity.YYq005;
import com.javaweb.admin.entity.YYq005Detail;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 订单代填主表 Mapper 接口
 * </p>
 *
 * @author 鲲鹏
 * @since 2023-12-31
 */
public interface YYq005Mapper extends BaseMapper<YYq005> {
    @Select("select distinct yybm,custcode,custname,psdbm,custaddrname from vw_ygptaddr where (yybm like concat('%',#{custcode},'%') and yymc like concat('%',#{custname},'%')) or (custcode like concat('%',#{custcode},'%') and custname like concat('%',#{custname},'%'))")
    List<NCCustInfoDoc> getCustomerList(String custcode, String custname);

    @Select("select distinct custaddrname as addrname from vw_ygptaddr where custcode=#{custcode} order by custaddrname")
    List<NCCustAddrDoc> getCustomerAddressList(String custcode);

    @Select("select * from vw_cust_invlist where yybm=#{yybm} and invcode like concat('%',#{invcode},'%') and invname like concat('%',#{invname},'%')   order by invcode")
    List<YYq005Detail> getCustInvList(String yybm, String invcode, String invname);

    @Select("select * from y_yq005_detail where replaceno=#{replaceno} order by zxspbm")
    List<YYq005Detail> getDetailList(String replaceno);

    @Select("select * from vw_all_invlist where invcode like concat('%',#{invcode},'%') and invname like concat('%',#{invname},'%')   order by invcode")
    List<YYq005Detail> getAllInvList(String yybm,String invcode,String invname);
}
