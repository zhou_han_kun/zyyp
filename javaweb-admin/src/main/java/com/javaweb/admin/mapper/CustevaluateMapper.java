package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.Custevaluate;
import com.javaweb.admin.entity.PhoneAndUser;
import com.javaweb.admin.query.CustevaluateQuery;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface CustevaluateMapper extends BaseMapper<Custevaluate> {
    IPage<Custevaluate> getCustevaluateList(IPage<Custevaluate> page,@Param("req") CustevaluateQuery req);

}
