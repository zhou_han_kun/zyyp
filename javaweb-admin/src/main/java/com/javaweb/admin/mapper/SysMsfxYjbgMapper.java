package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.admin.entity.SysMsfxYjbg;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SysMsfxYjbgMapper extends BaseMapper<SysMsfxYjbg> {
    @Select("select * from sys_msfx_yjbg where down_file_status=0")
    List<SysMsfxYjbg> getDownloadYjbgList();


}
