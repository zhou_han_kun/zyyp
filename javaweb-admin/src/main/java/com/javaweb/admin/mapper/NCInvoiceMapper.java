package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.NCInvoice;
import com.javaweb.admin.query.NCInvoiceQuery;
import org.apache.ibatis.annotations.Param;

public interface NCInvoiceMapper extends BaseMapper<NCInvoice> {
    IPage<NCInvoice> GetNCInvoiceList(IPage<NCInvoice> page, @Param("req") NCInvoiceQuery req);
}
