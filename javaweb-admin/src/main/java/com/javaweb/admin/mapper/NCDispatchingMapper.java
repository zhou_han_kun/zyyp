package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.NCDispatching;
import com.javaweb.admin.entity.NCDispatchingDetail;
import com.javaweb.admin.query.NCDispatchingQuery;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface NCDispatchingMapper extends BaseMapper<NCDispatching> {
    IPage<NCDispatching> GetNCDispatchingList(IPage<NCDispatching> page, @Param("req") NCDispatchingQuery req);

    @Select("select * from v_ygpt_dispatching_b where psdh=#{psdh} order by psdtm")
    List<NCDispatchingDetail> GetDispatchDetail(String psdh);
}
