package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sinepharm.pharmeyes.com.NCList;

public interface MdmCustomerNCListMapper extends BaseMapper<NCList> {
}
