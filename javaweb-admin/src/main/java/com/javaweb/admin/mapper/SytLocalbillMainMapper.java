package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.admin.entity.SytLocalbillMain;

public interface SytLocalbillMainMapper extends BaseMapper<SytLocalbillMain> {
}
