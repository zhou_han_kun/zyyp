package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.admin.entity.TransDetail;
import com.javaweb.admin.query.TransDetailQuery;
import org.apache.ibatis.annotations.Param;

public interface TransDetailMapper extends BaseMapper<TransDetail> {
    IPage<TransDetail> getTransDetail(IPage<TransDetail> page,@Param("req") TransDetailQuery req);
}
