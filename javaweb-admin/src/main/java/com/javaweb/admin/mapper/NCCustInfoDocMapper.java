package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.admin.entity.NCCustInfoDoc;

public interface NCCustInfoDocMapper extends BaseMapper<NCCustInfoDoc> {
}
