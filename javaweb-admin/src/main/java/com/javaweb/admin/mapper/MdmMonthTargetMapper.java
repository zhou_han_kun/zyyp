package com.javaweb.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.admin.entity.MdmMonthTarget;

public interface MdmMonthTargetMapper extends BaseMapper<MdmMonthTarget> {
}
