package com.javaweb.admin.mapper;

import com.javaweb.admin.entity.filenameRule.RuleInvoiceno;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SaleInvoiceMapper {
    @Select("select distinct so_invoice.CSALEINVOICEID,so_invoice.vbillcode,so_invoice.DBILLDATE from" +
            "    ods_nc_so_saleinvoice_b so_invoice_b" +
            "        left join ods_nc_so_SALEinvoice so_invoice on" +
            " so_invoice_b.CSALEINVOICEID=so_invoice.CSALEINVOICEID" +
            " where so_invoice.VPRINTCUSTNAME=#{custName}")
    List<RuleInvoiceno> getInvoiceno(String custName);
}
