// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.vo.c37infypyhjl;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 养护记录表表单Vo
 * </p>
 *
 * @author 鲲鹏
 * @since 2022-11-26
 */
@Data
public class C37InfYpyhjlInfoVo {

    /**
     * 养护记录表ID
     */
    private Integer id;

    /**
     * 日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date riqiDate;

    /**
     * 商品编号
     */
    private String shangpNo;

    /**
     * 商品名称
     */
    private String chineseName;

    /**
     * 药品规格
     */
    private String yaopGuig;

    /**
     * 剂型
     */
    private String jixing;

    /**
     * 存储条件
     */
    private String cuncCondition;

    /**
     * 包装单位
     */
    private String baozDanw;

    /**
     * 包装数量
     */
    private String baozNum;

    /**
     * 生产厂家
     */
    private String maker;

    /**
     * 批转文号
     */
    private String pizNo;

    /**
     * 
     */
    private String zhongyHaitat;

    /**
     * 
     */
    private String yezId;

    /**
     * 
     */
    private String yaopCategory;

    /**
     * 批号
     */
    private String lot;

    /**
     * 生产日期
     */
    private String shengchanDate;

    /**
     * 有效期
     */
    private String youxDate;

    /**
     * 数量
     */
    private BigDecimal num;

    /**
     * 购进日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date gjrq;

    /**
     * 
     */
    private String clyj;

    /**
     * 库存状态
     */
    private String kucState;

    /**
     * 
     */
    private String isZdyh;

    /**
     * 备注
     */
    private String remark;

    /**
     * 
     */
    private String yezNo;

    /**
     * 
     */
    private String scrq;

    /**
     * 下传状态
     */
    private String zt;

    /**
     * 
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date jihDate;

    /**
     * 
     */
    private String xiansLoc;

    /**
     * 
     */
    private String yanghStaff;

    /**
     * 行号
     */
    private Integer hanghao;

    /**
     * 单据号
     */
    private String danjNo;

}