package com.javaweb.admin.vo.spbc;

import lombok.Data;

@Data
public class SpbcCustAddr {
    private String custaddress;
    private String pkaddress;

}
