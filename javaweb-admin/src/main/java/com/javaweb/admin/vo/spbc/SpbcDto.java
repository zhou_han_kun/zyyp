package com.javaweb.admin.vo.spbc;

import com.javaweb.admin.entity.Spbc;
import com.javaweb.admin.entity.SpbcInvoice;
import lombok.Data;
import java.util.Map;

@Data
public class SpbcDto {
    private Spbc headInfo;
    private SpbcSummary[] spbcSummaries;
    private Map<String, SpbcInvoice[]> spbcInvoices;
}
