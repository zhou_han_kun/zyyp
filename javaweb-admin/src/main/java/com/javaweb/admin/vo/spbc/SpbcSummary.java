package com.javaweb.admin.vo.spbc;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SpbcSummary {
    private String id;
    private String invcode;
    private String invname;
    private String gg;
    private String custcode;
    private String custname;
    private String vlotno;
    private String dbilldate;
    private String vgoldtaxcode;
    private String csaleinvoicebid;

    private BigDecimal nqtorigtaxprice;
    private BigDecimal nqtorigprice;
    private BigDecimal nastnum;
    private BigDecimal norigmny;
    private BigDecimal quantity;
    private BigDecimal norigtaxmny;
    private BigDecimal remainnum;
    private BigDecimal balancenum;
    private BigDecimal nowsaleprice;
    private BigDecimal diffprice;
    private BigDecimal balanceamount;
    private BigDecimal nowsaleprice1;
    private BigDecimal nowsaleprice2;
    private String pkaddress;
    private String custaddress;
    private String manufacturer;
    private String ywy;
    private String lastbilldate;
    private String materialunit;
    private String cgy;

    private String dprodate;
    private String vinvaliddate;
}
