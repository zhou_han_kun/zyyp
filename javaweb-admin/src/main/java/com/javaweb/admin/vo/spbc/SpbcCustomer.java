package com.javaweb.admin.vo.spbc;

import lombok.Data;

@Data
public class SpbcCustomer {
    private String custcode;
    private String custname;
    private String psnname;
    private String deptname;
    private String provinces;
    private String addrname;
    private String custtype;
    private String custaddress;
    private String pkaddress;
    private String gpoarea;

}
