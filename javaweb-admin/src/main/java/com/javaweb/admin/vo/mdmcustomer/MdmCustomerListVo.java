// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.vo.mdmcustomer;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 列表Vo
 * </p>
 *
 * @author 鲲鹏
 * @since 2023-07-25
 */
@Data
public class MdmCustomerListVo {

    /**
    * ID
    */
    private Integer id;

    /**
     * 
     */
    private String dataCode;

    /**
     * 
     */
    private String datasSource;

    /**
     * 
     */
    private String registerName;

    /**
     * 
     */
    private String formerName;

    /**
     * 
     */
    private String registerProvinceName;

    /**
     * 
     */
    private String registerProvinceCode;

    /**
     * 
     */
    private String registerCityName;

    /**
     * 
     */
    private String registerCityCode;

    /**
     * 
     */
    private String registerCountyName;

    /**
     * 
     */
    private String registerCountyCode;

    /**
     * 
     */
    private String registerAddress;

    /**
     * 
     */
    private String registerAddressLongitudeBaidu;

    /**
     * 
     */
    private String registerAddressLatitudeBaidu;

    /**
     * 
     */
    private String registerAddressLongitudeGaode;

    /**
     * 
     */
    private String registerAddressLatitudeGaode;

    /**
     * 
     */
    private String registerAddressMapRunError;

    /**
     * 
     */
    private String registerAddressMapRunStatus;

    /**
     * 
     */
    private String uniformSocialCreditCode;

    /**
     * 
     */
    private String attributeLevelFirst;

    /**
     * 
     */
    private String attributeLevelSecond;

    /**
     * 
     */
    private String attributeLevelThird;

    /**
     * 
     */
    private String operationStatus;

    /**
     * 
     */
    private String humanSocialLevel;

    /**
     * 
     */
    private String humanSocialRank;

    /**
     * 
     */
    private String healthCommissionLevel;

    /**
     * 
     */
    private String healthCommissionCode;

    /**
     * 
     */
    private String clientAttribute;

    /**
     * 
     */
    private String clientLevel;

    /**
     * 
     */
    private String clientClassify;

    /**
     * 
     */
    private String remark;

    /**
     * 
     */
    private String discountAgreementType;

    /**
     * 
     */
    private String ncSysCode;

    /**
     * 
     */
    private String hongYingSysCode;

    /**
     * 
     */
    private String crmSysCode;

    /**
     * 
     */
    private String areaManagementSysCode;

    /**
     * 
     */
    private String mergeDestinationName;

    /**
     * 
     */
    private String mergeDestinationCode;

    /**
     * 
     */
    private Integer isHis;

    /**
     * 
     */
    private Integer version;

    /**
     * 
     */
    private Integer isValid;

    /**
     * 
     */
    private Integer isElectronicCommerce;

    /**
     * 
     */
    private Integer isFlowOpen;

    /**
     * 
     */
    private String chainHeadQuarterName;

    /**
     * 
     */
    private String chainHeadQuarterCode;

    /**
     * 
     */
    private String mergeUserName;

    /**
     * 
     */
    private String businessStatus;

    /**
     * 
     */
    private String operation;

    /**
     * 
     */
    private String addTime;

    /**
     * 
     */
    private Integer addType;

    /**
     * 
     */
    private String businessPerson;

    /**
     * 
     */
    private String businessDept;

    /**
     * 
     */
    private String entityHongYingSysCode;

    /**
     * 
     */
    private String entityRegisterName;

    /**
     * 
     */
    private String generalHeadquarters;

    /**
     * 
     */
    private String branchOffice;

    /**
     * 
     */
    private String remarks;

    /**
     * 
     */
    private String operationStatusName;

    /**
     * 
     */
    private String isElectronicCommerceName;

    /**
     * 
     */
    private String isFlowOpenName;

    /**
     * 
     */
    private String tenantName;

    /**
     * 
     */
    private String tenantId;

    /**
     * 
     */
    private String updateUserName;

    /**
     * 
     */
    private String mthHospMedicineIncome;

    /**
     * 
     */
    private String mthHospClinicAmount;

    /**
     * 
     */
    private String mthHospBunkAmount;

    /**
     * 
     */
    private String mthHospMicroEcologyQuantity;

    /**
     * 
     */
    private String mthDigestiveDeptClinicAmount;

    /**
     * 
     */
    private String mthPaediatricsDeptClinicAmount;

    /**
     * 
     */
    private String mthPaediatricsDeptInpatientAmount;

    /**
     * 
     */
    private String mthNeonatologyDeptClinicAmount;

    /**
     * 
     */
    private String mthNeonatologyDeptInpatientAmount;

    /**
     * 
     */
    private String mthDermatologyDeptClinicAmount;

    /**
     * 
     */
    private String mthDigestiveDeptInpatientAmount;

    /**
     * 
     */
    private String distributorLabel;

    /**
     * 
     */
    private String electronicCommerceLabel;

    /**
     * 
     */
    private String hisName;

}