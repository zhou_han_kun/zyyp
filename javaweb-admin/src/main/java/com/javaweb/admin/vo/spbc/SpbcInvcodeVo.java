package com.javaweb.admin.vo.spbc;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SpbcInvcodeVo {
    private String invcode;
    private String invname;
    private String gg;
    private String vmanufacturer;
    private String cmaterialvid;
    private Integer pid;
    private Integer id;
    private BigDecimal balanceamountback;
    private String zxspbm;
    private BigDecimal nowsaleprice;
    private BigDecimal balanceamountapply;
    private String cgy;
    private String materialunit;
}
