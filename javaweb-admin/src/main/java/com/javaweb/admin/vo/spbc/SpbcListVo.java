// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.vo.spbc;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 列表Vo
 * </p>
 *
 * @author 鲲鹏
 * @since 2023-11-27
 */
@Data
public class SpbcListVo {

    /**
    * ID
    */
    private Integer id;

    /**
     * 
     */
    private String sqrq;

    /**
     *
     */
    private String sqdh;

    /**
     *
     */
    private String sqr;

    /**
     *
     */
    private String custname;

    /**
     *
     */
    private String custcode;

    /**
     *
     */
    private String bcyjnf;

    /**
     *
     */
    private String bcyjqs;

    /**
     *
     */
    private String bclx;

    private String khlx;
    private Date createTime;
    private Date updateTime;
    private Date submitTime;
    /**
     *
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date ts;

    /**
     *
     */
    private String checkFlag;
    private String uploadfile;
    private String isOverdue;
    private String begindate;
    private String enddate;

    /**
     * 
     */
    private String pkaddress;
    private String custaddress;
    private String gpoarea;

    private List<String> bcyjList;
    private List<String> custAddr;

    private boolean saleOrderSubstuted;
    private boolean reOrderSubstuted;
    private boolean saleInvoicePost;
    private boolean reInvoiceUnrelated;
}