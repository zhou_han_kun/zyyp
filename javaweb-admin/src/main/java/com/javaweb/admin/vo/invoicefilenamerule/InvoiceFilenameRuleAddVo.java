package com.javaweb.admin.vo.invoicefilenamerule;

import lombok.Data;

@Data
public class InvoiceFilenameRuleAddVo {
    private Integer id;
    private String customerCode;

    /**
     *
     */
    private String customerName;

    /**
     *
     */
    private String[] pdfruleField;
    private String pdfrules;
    private String[] zipruleField;
    private String ziprules;

}
