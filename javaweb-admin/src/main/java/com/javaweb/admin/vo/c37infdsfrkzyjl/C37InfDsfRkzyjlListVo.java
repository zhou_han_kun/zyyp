// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.vo.c37infdsfrkzyjl;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 列表Vo
 * </p>
 *
 * @author 鲲鹏
 * @since 2023-03-23
 */
@Data
public class C37InfDsfRkzyjlListVo {

    /**
     * 
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date riqiDate;

    /**
     * 
     */
    private String danjNo;

    private String yewdjNo;
    /**
     * 
     */
    private String danwNo;

    /**
     * 
     */
    private String danwName;

    /**
     * 
     */
    private String caigStaff;

    /**
     * 
     */
    private String shouhStaff;

    /**
     * 
     */
    private String shangpNo;

    /**
     * 
     */
    private String chineseName;

    /**
     * 
     */
    private String yaopGuig;

    /**
     * 
     */
    private String maker;

    /**
     * 
     */
    private String chandi;

    /**
     * 
     */
    private String pizNo;

    /**
     * 
     */
    private String zhongyHaitat;

    /**
     * 
     */
    private String jixing;

    /**
     * 
     */
    private BigDecimal baozNum;

    /**
     * 
     */
    private String baozDanw;

    /**
     * 
     */
    private BigDecimal shijNum;

    /**
     * 
     */
    private BigDecimal shijJs;

    /**
     * 
     */
    private BigDecimal shijLss;

    /**
     * 
     */
    private BigDecimal price;

    /**
     * 
     */
    private BigDecimal amount;

    /**
     * 
     */
    private String xiansLoc;

    /**
     * 
     */
    private String lot;

    /**
     * 
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date shengchanDate;

    /**
     * 
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date youxDate;

    /**
     * 
     */
    private String cuncCondition;

    /**
     * 
     */
    private String qiyDate;

    /**
     * 
     */
    private String jilyType;

    /**
     * 
     */
    private String yunsType;

    /**
     * 
     */
    private String yunsDanw;

    /**
     * 
     */
    private String fayDidian;

    /**
     * 
     */
    private String chepNo;

    /**
     * 
     */
    private String yansTemp;

    /**
     * 
     */
    private String yezId;

    /**
     * 
     */
    private String yezNo;

    /**
     * 
     */
    private String zt;

    /**
     * 
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date scrq;

    /**
     * 
     */
    private Integer hanghao;

    /**
     * 
     */
    private String yewType;

    /**
     * 
     */
    private String khZzbm;

    /**
     * 
     */
    private String ssxkcyr;

    /**
     * 
     */
    private BigDecimal bhgNum;

    /**
     * 
     */
    private String chulFanga;

    /**
     * 
     */
    private String wenkWay;

    /**
     * 
     */
    private String jushouReason;

    /**
     * 
     */
    private String yansRlt;

    /**
     * 
     */
    private BigDecimal num;

    /**
     * 
     */
    private String zhijStaff;

}