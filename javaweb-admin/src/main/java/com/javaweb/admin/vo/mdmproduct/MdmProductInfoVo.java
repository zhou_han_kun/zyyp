// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.vo.mdmproduct;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 表单Vo
 * </p>
 *
 * @author 鲲鹏
 * @since 2023-07-24
 */
@Data
public class MdmProductInfoVo {

    /**
     * ID
     */
    private Integer id;

    /**
     * 
     */
    private String atc;

    /**
     * 
     */
    private String standardCode;

    /**
     * 
     */
    private String casNo;

    /**
     * 
     */
    private String placeOfOrigin;

    /**
     * 
     */
    private String productOrMaterial;

    /**
     * 
     */
    private String country;

    /**
     * 
     */
    private String domesticImports;

    /**
     * 
     */
    private String specifications;

    /**
     * 
     */
    private String managementType;

    /**
     * 
     */
    private String managementTypeName;

    /**
     * 
     */
    private String measuringUnit;

    /**
     * 
     */
    private String dosageForm;

    /**
     * 
     */
    private String dosageFormName;

    /**
     * 
     */
    private String mdmCode;

    /**
     * 
     */
    private String distributingResultInformation;

    /**
     * 
     */
    private String detailCategory;

    /**
     * 
     */
    private String detailCategoryName;

    /**
     * 
     */
    private String productName;

    /**
     * 
     */
    private String otc;

    /**
     * 
     */
    private String generalSpecialDrug;

    /**
     * 
     */
    private String approvalNumber;

    /**
     * 
     */
    private String productionEnterprise;

    /**
     * 
     */
    private String whetherExport;

    /**
     * 
     */
    private String whetherEssentialMedicine;

    /**
     * 
     */
    private String whetherInsuranceDirectory;

    /**
     * 
     */
    private String tradeName;

    /**
     * 
     */
    private Integer mdmCodeStatus;

    /**
     * 
     */
    private String timeStamp;

    /**
     * 
     */
    private String categories;

    /**
     * 
     */
    private String categoriesName;

    /**
     * 
     */
    private String foreignNames;

    /**
     * 
     */
    private String outsourcing;

    /**
     * 
     */
    private String minimumPackingUnit;

    /**
     * 
     */
    private String minimumPackingUnitName;

    /**
     * 
     */
    private String minimumPackingSpecification;

    /**
     * 
     */
    private Integer isDeleted;

    /**
     * 
     */
    private String updateTime;

    /**
     * 
     */
    private Integer addType;

    /**
     * 
     */
    private String entityMdmCode;

    /**
     * 
     */
    private String entityProductName;

}