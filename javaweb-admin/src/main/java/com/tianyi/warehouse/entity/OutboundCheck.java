package com.tianyi.warehouse.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.javaweb.common.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wms_outbound_check")
public class OutboundCheck extends BaseEntity {
    private String cgeneralbid;
    private Integer checkUser1;
    private Date checkTime1;
    private Integer checkUser2;
    private Date checkTime2;
    @TableField(exist = false)
    private String checkUserName1;
    @TableField(exist = false)
    private String checkUserName2;
}
