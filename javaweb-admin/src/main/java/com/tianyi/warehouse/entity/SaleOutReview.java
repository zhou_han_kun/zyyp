package com.tianyi.warehouse.entity;
import java.util.List;

public class SaleOutReview {
    private String initid;
    private String pk_org;
    private List<DetailList> detailList;
    public void setInitid(String initid) {
        this.initid = initid;
    }
    public String getInitid() {
        return initid;
    }

    public void setPk_org(String pk_org) {
        this.pk_org = pk_org;
    }
    public String getPk_org() {
        return pk_org;
    }

    public void setDetailList(List<DetailList> detailList) {
        this.detailList = detailList;
    }
    public List<DetailList> getDetailList() {
        return detailList;
    }
}
