package com.tianyi.warehouse.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName(value = "v_wms_outboundcheck_detail")
public class KdlPurchaseinDetail {
    private String vbillcode;
    private String invcode;
    private String comdescription;
    private String comdescription2;
    private String invname;
    private String materialspec;
    private String materialform;
    private String nnum;
    private String measname;
    private String vlotno;
    private String dprodate;
    private String vinvaliddate;
    private String mainmeasrate;
    private String status;
    private String rackname;
    private String shortname;
    private String addrname;
    private String salesunit;
    private String vmanufacturer;
    private String shcyr;
    private String billtypename;
    private String category;
    private String outcategory;
    private String storename;
    private String billmaker;
    private String deptname;
    private String psnname;
    private String code;
    private String cgeneralhid;
    private String cgeneralbid;
    @TableField(exist = false)
    private String custcode;
    @TableField(exist = false)
    private String dbilldate;
    @TableField(exist = false)
    private String custname;
    @TableField(exist = false)
    private String custname2;
    @TableField(exist = false)
    private String prodinvaliddate;
    //箱数
    @TableField(exist = false)
    private String pcs;
    //零头数
    @TableField(exist = false)
    private String lts;
    @TableField(exist = false)
    private String def30;
    @TableField(exist = false)
    private String vbdef9;
    private Integer checkuser1;
    private Integer checkuser2;
    private String checkusername1;
    private String checkusername2;
    private String checkdate1;
    private String checkdate2;

    private String rackstock;
    private String areaname;
    private String custmemo;


}
