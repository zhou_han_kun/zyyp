package com.tianyi.warehouse.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tianyi.warehouse.entity.OutboundCheck;
import org.apache.ibatis.annotations.Select;

public interface OutboundCheckMapper extends BaseMapper<OutboundCheck> {

    @Select("  select t1.*,usr.realname checkusername1,usr2.realname checkusername2 from wms_outbound_check t1 left join sys_user usr on t1.check_user1=usr.id left join sys_user usr2 on t1.check_user2=usr2.id where t1.cgeneralbid=#{cgeneralbid}")
    OutboundCheck getCheckInfo(String cgeneralbid);
}
