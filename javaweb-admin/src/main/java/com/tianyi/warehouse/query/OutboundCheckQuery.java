package com.tianyi.warehouse.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

@Data
public class OutboundCheckQuery extends BaseQuery {
    private String[] racklist;
    private String[] billcodelist;
    private String rack;
    private String vbillcode;
    private String custcode;
    private String outcategory;
    private String addrname;
    private String riqiBegin;
    private String riqiEnd;
    private String status;
    private String cgeneralbid;
    private Integer checkUser;

}
