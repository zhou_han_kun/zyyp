package com.tianyi.warehouse.controller;

import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import com.tianyi.warehouse.query.KdlPurchaseinQuery;
import com.tianyi.warehouse.query.OutboundCheckQuery;
import com.tianyi.warehouse.service.IKdlPurchaseinService;
import com.tianyi.warehouse.service.IOutboundCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/outboundcheck")
public class OutboundCheckController {

    @Autowired
    private IKdlPurchaseinService kdlPurchaseinService;

    @Autowired
    private IOutboundCheckService outboundCheckService;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    //@RequiresPermissions("sys:kdlpurchasein:index")
    @PostMapping("/index")
    public JsonResult index(@RequestBody OutboundCheckQuery query) {
        return kdlPurchaseinService.getPurchaseinList(query);
    }

    @PostMapping("/detail")
    public JsonResult detail(@RequestBody OutboundCheckQuery query) {
        return kdlPurchaseinService.getPurchaseinDetailList(query);
    }
    @PostMapping("/uncheck")
    public JsonResult uncheck(@RequestBody OutboundCheckQuery query) {
        return kdlPurchaseinService.getPurchaseinUnchecklList(query);
    }

    @PostMapping("/savecheck")
    public JsonResult savecheck(@RequestBody OutboundCheckQuery query) {
        return outboundCheckService.saveOurboundCheck(query.getCgeneralbid(),query.getCheckUser());
    }

    @PostMapping("/cancelcheck")
    public JsonResult cancelcheck(@RequestBody OutboundCheckQuery query) {
        return outboundCheckService.cancelOurboundCheck(query.getCgeneralbid());
    }

    @PostMapping("/cancelsecondcheck")
    public JsonResult cancelsecondcheck(@RequestBody OutboundCheckQuery query) {
        return outboundCheckService.cancelSecondCheck(query.getCgeneralbid(), query.getCheckUser());
    }

    @PostMapping("/secondcheck")
    public JsonResult secondcheck(@RequestBody OutboundCheckQuery query) {
        return outboundCheckService.secondCheck(query.getCgeneralbid(),query.getCheckUser());
    }




}
