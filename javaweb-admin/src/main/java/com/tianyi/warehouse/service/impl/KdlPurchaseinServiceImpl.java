// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.tianyi.warehouse.service.impl;

import cn.hutool.core.util.XmlUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.admin.entity.NcServiceLog;
import com.tianyi.warehouse.entity.KdlPurchasein;
import com.tianyi.warehouse.entity.KdlPurchaseinDetail;
import com.tianyi.warehouse.mapper.KdlPurchaseinMapper;
import com.tianyi.warehouse.query.KdlPurchaseinQuery;
import com.tianyi.warehouse.query.OutboundCheckQuery;
import com.tianyi.warehouse.service.IKdlPurchaseinService;
import com.javaweb.admin.service.INcServiceLogService;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.common.config.DataSourceType;
import com.javaweb.common.config.SpecifyDataSource;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
  * <p>
  * 采购质检订单回传 服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2024-01-24
  */
@Service
public class KdlPurchaseinServiceImpl extends BaseServiceImpl<KdlPurchaseinMapper, KdlPurchasein> implements IKdlPurchaseinService {

    @Autowired
    private KdlPurchaseinMapper kdlPurchaseinMapper;


    @Autowired
    INcServiceLogService ncServiceLogService;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    @SpecifyDataSource(value = DataSourceType.MASTER)
    public JsonResult getList(BaseQuery query) {
        KdlPurchaseinQuery kdlPurchaseinQuery = (KdlPurchaseinQuery) query;
        // 查询条件
        QueryWrapper<KdlPurchasein> queryWrapper = new QueryWrapper<>();
        // queryWrapper.orderByDesc("id");
        if(!StringUtils.isEmpty(kdlPurchaseinQuery.getVendorpo())){
            queryWrapper.like("vendorpo",kdlPurchaseinQuery.getVendorpo());
        }
        if(!StringUtils.isEmpty(kdlPurchaseinQuery.getCahpo())){
            queryWrapper.like("cahpo",kdlPurchaseinQuery.getCahpo());
        }
        if(!StringUtils.isEmpty(kdlPurchaseinQuery.getRiqiBegin())){
            queryWrapper.gt("grdate",kdlPurchaseinQuery.getRiqiBegin());
        }
        if(!StringUtils.isEmpty(kdlPurchaseinQuery.getRiqiEnd())){
            queryWrapper.lt("grdate",kdlPurchaseinQuery.getRiqiEnd());
        }


        // 获取数据列表
        IPage<KdlPurchasein> page = new Page<>(kdlPurchaseinQuery.getPage(), kdlPurchaseinQuery.getLimit());
        IPage<KdlPurchasein> pageData = kdlPurchaseinMapper.selectPage(page, queryWrapper);
//        pageData.convert(x -> {
//            KdlPurchaseinListVo kdlPurchaseinListVo = Convert.convert(KdlPurchaseinListVo.class, x);
//            return kdlPurchaseinListVo;
//        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    @SpecifyDataSource(value = DataSourceType.MASTER)
    public Object getInfo(Serializable id) {
        KdlPurchasein entity = (KdlPurchasein) super.getInfo(id);
        // 返回视图Vo
        KdlPurchasein kdlPurchaseinInfoVo = new KdlPurchasein();
        // 拷贝属性
        BeanUtils.copyProperties(entity, kdlPurchaseinInfoVo);
        return kdlPurchaseinInfoVo;
    }


    /*
     * 获取康德乐采购质检明细数据
     */
    @Override
    public JsonResult getPurchaseinDetailList(BaseQuery query) {
        OutboundCheckQuery outboundCheckQuery = (OutboundCheckQuery) query;
        outboundCheckQuery.setRacklist(outboundCheckQuery.getRack().split(","));
        IPage<KdlPurchaseinDetail> page = new Page<>(outboundCheckQuery.getPage(), outboundCheckQuery.getLimit());
        IPage<KdlPurchaseinDetail> pageData = kdlPurchaseinMapper.getPurchaseinDetailList(page, outboundCheckQuery);
        return JsonResult.success(pageData);
    }

    @Override
    public JsonResult getPurchaseinUnchecklList(BaseQuery query) {
        OutboundCheckQuery outboundCheckQuery = (OutboundCheckQuery) query;
        outboundCheckQuery.setRacklist(outboundCheckQuery.getRack().split(","));
        IPage<KdlPurchaseinDetail> page = new Page<>(outboundCheckQuery.getPage(), outboundCheckQuery.getLimit());
        IPage<KdlPurchaseinDetail> pageData = kdlPurchaseinMapper.getPurchaseinUncheckList(page, outboundCheckQuery);
        return JsonResult.success(pageData);
    }

    @Override
    public JsonResult getPurchaseinList(BaseQuery query) {
        OutboundCheckQuery outboundCheckQuery = (OutboundCheckQuery) query;
        outboundCheckQuery.setRacklist(outboundCheckQuery.getRack().split(","));
        // 获取数据列表
        IPage<KdlPurchasein> page = new Page<>(outboundCheckQuery.getPage(), outboundCheckQuery.getLimit());
        IPage<KdlPurchasein> pageData = kdlPurchaseinMapper.getPurchaseinList(page, outboundCheckQuery);
        return JsonResult.success(pageData);
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(KdlPurchasein entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
        } else {
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(KdlPurchasein entity) {
        entity.setMark(0);
        return super.delete(entity);
    }

}