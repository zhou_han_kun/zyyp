package com.tianyi.warehouse.service;

import com.javaweb.common.utils.JsonResult;

public interface IOutboundCheckService {
    JsonResult saveOurboundCheck(String cgeneralbid,Integer checkuser);
    JsonResult cancelOurboundCheck(String cgeneralbid);

    JsonResult secondCheck(String cgeneralbid,Integer checkuser);
    JsonResult cancelSecondCheck(String cgeneralbid,Integer checkuser);
}
