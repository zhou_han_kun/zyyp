package com.sphchina.esb.webservice;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonObject;
import com.javaweb.admin.entity.C37InfCkScBillNew;
import com.javaweb.admin.entity.C37InfRkScBillNew;
import com.javaweb.admin.entity.NcServiceLog;
import com.javaweb.admin.service.IC37InfCkScBillNewService;
import com.javaweb.admin.service.IC37InfRkScBillNewService;
import com.javaweb.admin.service.INcServiceLogService;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.config.DynamicDataSource;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.StringUtils;
import org.apache.axis.AxisFault;
import org.apache.axis.client.Service;

import com.sphchina.esb.webservice.DefaultWebServiceRequest;
import com.sphchina.esb.webservice.DefaultWebServiceResponse;
import com.sphchina.esb.webservice.ISoapWebServiceServiceSoapBindingStub;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Service
public class SendToReviewImpl implements ISendToReviewLog {

	/**
	 * ��־��¼
	 */
	@Autowired
	INcServiceLogService ncServiceLogService;

	@Autowired
	IC37InfCkScBillNewService c37InfCkScBillNewService;

	@Autowired
	IC37InfRkScBillNewService c37InfRkScBillNewService;

	@Override
	public JsonResult callEsb(String body,String ServiceId) {
		String info = "";
		String endPoint= CommonConfig.ncServiceUrl;
		if(endPoint==null){
			JsonResult.error("NC WebSevice服务地址未配置，请检查");
		}
		ISoapWebServiceServiceSoapBindingStub esb ; 
		DefaultWebServiceResponse resp = null;
		Date dt=new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		DefaultWebServiceRequest  df=new DefaultWebServiceRequest();
		df.setMsgBody(body);
		df.setMsgSendTime(format.format(dt));
		df.setServiceId(ServiceId);
		df.setSourceAppCode("E9");//Դϵͳ��־��
		ESBDto dto = JSON.parseObject(body,ESBDto.class);
		NcServiceLog log = new NcServiceLog();
		String danj_no = dto.body.length>0?dto.body[0].getDanj_no():"";
		log.setSubmitNo(danj_no);
		log.setRequestBody(df.getMsgBody());
		log.setSubmitTime(DateUtils.now());
		try {
			esb = new ISoapWebServiceServiceSoapBindingStub(new URL(endPoint), new Service());
			resp = esb.receiveMessage(df);
//			info=resp.getReturnMessage().replaceAll("\\\\","");
			info=resp.getReturnMessage();
			JSONObject result = JSON.parseObject(info);
			if(result.getString("flag").equals("Y"))
			{
				log.setResponseBody(info);
				log.setResponseCode(result.getString("flag"));
				log.setResponseResult(result.getString("msg"));

				//log.setResponseCode("N");
				//log.setResponseResult("{flag=Y, msg=当前数据项PRICE为空;当前数据项vdef1为空;}");
				ncServiceLogService.save(log);
				c37InfCkScBillNewService.UpdateZT(danj_no,"Y");
			}

		} catch (AxisFault e) {
			e.printStackTrace();
			log.setResponseBody(e.getMessage());
			log.setResponseCode("500");
			log.setResponseResult("");
			ncServiceLogService.save(log);
			return JsonResult.error(e.getMessage()+"Return:"+info);
		} catch (Exception e) {
			log.setResponseBody(e.getMessage());
			log.setResponseCode("500");
			log.setResponseResult("");
			ncServiceLogService.save(log);
			e.printStackTrace();
			return JsonResult.error(e.getMessage()+"Return:"+info);
		}
				
		return JsonResult.success(info);
	}

	@Override
	public JsonResult callEsbKdl(String billType,String billno,String body) {
		String info = "";
		String endPoint= CommonConfig.ncServiceUrl;
		if(endPoint==null){
			JsonResult.error("NC WebSevice服务地址未配置，请检查");
		}
		ISoapWebServiceServiceSoapBindingStub esb ;
		DefaultWebServiceResponse resp = null;
		Date dt=new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		DefaultWebServiceRequest  df=new DefaultWebServiceRequest();
		df.setMsgBody(body);
		df.setMsgSendTime(format.format(dt));
		df.setServiceId("E0BO01");
		df.setSourceAppCode("E0");//Դϵͳ��־��
		//NCDto dto = JSON.parseObject(body,NCDto.class);
		NcServiceLog log = new NcServiceLog();
		//String danj_no = dto.body.length>0?dto.body[0].getDanj_no():"";
		log.setSubmitNo(billno+"_"+billType);
		log.setRequestBody(df.getMsgBody());
		log.setSubmitTime(DateUtils.now());
		try {
			esb = new ISoapWebServiceServiceSoapBindingStub(new URL(endPoint), new Service());
			resp = esb.receiveMessage(df);
//			info=resp.getReturnMessage().replaceAll("\\\\","");
			info=resp.getReturnMessage();
			JSONObject result = JSON.parseObject(info);
			log.setResponseBody(info);
			log.setResponseCode(result.getString("result"));
			log.setResponseResult(result.getString("msg"));
			String datasource= DynamicDataSource.getDataSource();
			DynamicDataSource.setDataSource("system");
			ncServiceLogService.save(log);
			DynamicDataSource.setDataSource(datasource);
			if("Y".equals(result.getString("result"))){
				return JsonResult.success();
			}else{
				return JsonResult.error(result.getString("msg"));
			}

		} catch (AxisFault e) {
			e.printStackTrace();
			log.setResponseBody(e.getMessage());
			log.setResponseCode("500");
			log.setResponseResult("");
			ncServiceLogService.save(log);
			return JsonResult.error(e.getMessage()+"Return:"+info);
		} catch (Exception e) {
			log.setResponseBody(e.getMessage());
			log.setResponseCode("500");
			log.setResponseResult("");
			ncServiceLogService.save(log);
			e.printStackTrace();
			return JsonResult.error(e.getMessage()+"Return:"+info);
		}

	}


	//ESB推送采购入库
	@Override
	public JsonResult callCGRK(String[] djhs, String ServiceId) {
		String info = "";

		String endPoint= CommonConfig.ncServiceUrl;
		if(endPoint==null){
			return JsonResult.error("NC WebSevice服务地址未配置，请检查");
		}
		if(djhs==null || djhs.length==0)
		{
			return JsonResult.error("缺少单据号参数");
		}
		String danj_no = djhs[0];
		//根据单据号获取中间表的采购入库明细
		List<C37InfRkScBillNew> detail = c37InfRkScBillNewService.DetailList(danj_no);
		ESBDto dto = new ESBDto();
		dto.orgcode = CommonConfig.ncOrgCode;
		dto.type="采购入库";
		POrderInfo[] item = new POrderInfo[detail.size()];
		int index = 0;
		for (C37InfRkScBillNew v: detail) {
			item[index] = new POrderInfo();
			item[index].setDanj_no(v.getDanjNo());
			item[index].setRiqi_char(v.getRiqiChar());
			item[index].setDanw_no(v.getDanwNo());
			item[index].setCaigou_staff(v.getCaigouStaff());
			item[index].setShouh_staff(v.getShouhStaff());
			item[index].setHanghao(v.getHanghao());
			item[index].setShangp_no(v.getShangpNo());
			item[index].setLot(v.getLot());
			item[index].setShengc_char(v.getShengcChar());
			item[index].setYoux_char(v.getYouxChar());
			item[index].setRuk_type(v.getRukType());
			item[index].setJih_num(v.getJihNum());
			item[index].setNum(v.getNum());
			item[index].setPrice(v.getPrice());
			item[index].setRuk_no(v.getRukNo());
			item[index].setHangh_cgd(v.getHanghCgd());
			item[index].setZhij_staff(v.getZhijStaff());
			item[index].setZt(v.getZt());
			item[index].setVdef1("1");
			item[index].setVdef2(v.getYansRlt());
			index++;
		}
		dto.body = item;
		String body = JSON.toJSONString(dto);
		ISoapWebServiceServiceSoapBindingStub esb ;
		DefaultWebServiceResponse resp = null;
		Date dt=new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		DefaultWebServiceRequest  df=new DefaultWebServiceRequest();
		df.setMsgBody(body);
		df.setMsgSendTime(format.format(dt));
		df.setServiceId(ServiceId);
		df.setSourceAppCode("E9");//Դϵͳ��־��

		NcServiceLog log = new NcServiceLog();
		log.setSubmitNo(danj_no+"_"+dto.type);
		log.setRequestBody(df.getMsgBody());
		log.setSubmitTime(DateUtils.now());
		try {
			esb = new ISoapWebServiceServiceSoapBindingStub(new URL(endPoint), new Service());
			resp = esb.receiveMessage(df);
//			info=resp.getReturnMessage().replaceAll("\\\\","");
			info=resp.getReturnMessage();
			JSONObject result = JSON.parseObject(info);
			log.setResponseBody(info);
			log.setResponseCode(result.getString("flag"));
			log.setResponseResult(result.getString("msg"));
			ncServiceLogService.save(log);
			if(result.getString("flag").equals("Y"))
			{

				//log.setResponseCode("N");
				//log.setResponseResult("{flag=Y, msg=当前数据项PRICE为空;当前数据项vdef1为空;}");
				c37InfRkScBillNewService.UpdateZT(danj_no,"N");
				return JsonResult.success(result.getString("msg"));
			}
			else
			{
				if(result.getString("msg").contains(CommonConfig.ncBillKey))
				{
					c37InfRkScBillNewService.UpdateZT(danj_no,"N");
					return JsonResult.error(result.getString("msg"));
				}
				else {
					//log.setResponseCode("N");
					//log.setResponseResult("{flag=Y, msg=当前数据项PRICE为空;当前数据项vdef1为空;}");
					return JsonResult.error(result.getString("msg"));
				}

			}

		} catch (AxisFault e) {
			e.printStackTrace();
			log.setResponseBody(e.getMessage());
			log.setResponseCode("500");
			log.setResponseResult("");
			ncServiceLogService.save(log);
			return JsonResult.error(e.getMessage()+"Return:"+info);
		} catch (Exception e) {
			log.setResponseBody(e.getMessage());
			log.setResponseCode("500");
			log.setResponseResult("");
			ncServiceLogService.save(log);
			e.printStackTrace();
			return JsonResult.error(e.getMessage()+"Return:"+info);
		}


	}

	//ESB推送采购退货
	@Override
	public JsonResult callGJTC(String[] djhs, String ServiceId) {
		String info = "";

		String endPoint= CommonConfig.ncServiceUrl;
		if(endPoint==null){
			return JsonResult.error("NC WebSevice服务地址未配置，请检查");
		}
		if(djhs==null || djhs.length==0)
		{
			return JsonResult.error("缺少单据号参数");
		}
		String danj_no = djhs[0];
		//根据单据号获取中间表的采购入库明细
		List<C37InfCkScBillNew> detail = c37InfCkScBillNewService.DetailList(danj_no);
		ESBDto dto = new ESBDto();
		dto.orgcode = CommonConfig.ncOrgCode;
		dto.type="购进退出";
		POrderInfo[] item = new POrderInfo[detail.size()];
		int index = 0;
		for (C37InfCkScBillNew v: detail) {
			item[index] = new POrderInfo();
			item[index].setDanj_no(v.getDanjNo());
			item[index].setChuk_no(v.getChukNo());
			item[index].setDanw_no(v.getDanwNo());
			item[index].setRiqi_char(v.getRiqiChar());
			item[index].setYew_staff(v.getYewStaff());
			if(StringUtils.isEmpty(v.getTihWay()))
			{
				item[index].setTih_way("4");
			}
			else {
				item[index].setTih_way(v.getTihWay());
			}
			item[index].setYew_type(v.getYewType());
			item[index].setDanjxc_side(v.getDanjxcSide());
			item[index].setHanghao(v.getHanghao());
			item[index].setShangp_no(v.getShangpNo());
			item[index].setJih_num(v.getJihNum());
			item[index].setNum(v.getNum());
			item[index].setLot(v.getLot());
			item[index].setShengc_char(v.getShengcChar());
			item[index].setYoux_char(v.getYouxChar());
			item[index].setChongh_flg(v.getChonghFlg());
			item[index].setTuih_reason(v.getTuihReason());
			item[index].setZt(v.getZt());
			item[index].setKub(v.getKub());
			item[index].setVdef1("1");
			item[index].setVdef2(v.getKub());
			index++;
		}
		dto.body = item;
		String body = JSON.toJSONString(dto);
		ISoapWebServiceServiceSoapBindingStub esb ;
		DefaultWebServiceResponse resp = null;
		Date dt=new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		DefaultWebServiceRequest  df=new DefaultWebServiceRequest();
		df.setMsgBody(body);
		df.setMsgSendTime(format.format(dt));
		df.setServiceId(ServiceId);
		df.setSourceAppCode("E9");//Դϵͳ��־��

		NcServiceLog log = new NcServiceLog();
		log.setSubmitNo(danj_no+"_"+dto.type);
		log.setRequestBody(df.getMsgBody());
		log.setSubmitTime(DateUtils.now());
		try {
			esb = new ISoapWebServiceServiceSoapBindingStub(new URL(endPoint), new Service());
			resp = esb.receiveMessage(df);
//			info=resp.getReturnMessage().replaceAll("\\\\","");
			info=resp.getReturnMessage();
			JSONObject result = JSON.parseObject(info);
			log.setResponseBody(info);
			log.setResponseCode(result.getString("flag"));
			log.setResponseResult(result.getString("msg"));
			ncServiceLogService.save(log);
			if(result.getString("flag").equals("Y"))
			{

				//log.setResponseCode("N");
				//log.setResponseResult("{flag=Y, msg=当前数据项PRICE为空;当前数据项vdef1为空;}");
				c37InfCkScBillNewService.UpdateZT(danj_no,"N");
				return JsonResult.success(result.getString("msg"));
			}
			else
			{
				if(result.getString("msg").contains(CommonConfig.ncBillKey))
				{
					c37InfCkScBillNewService.UpdateZT(danj_no,"N");
					return JsonResult.error(result.getString("msg"));
				}
				else {
					//log.setResponseCode("N");
					//log.setResponseResult("{flag=Y, msg=当前数据项PRICE为空;当前数据项vdef1为空;}");
					return JsonResult.error(result.getString("msg"));
				}

			}

		} catch (AxisFault e) {
			e.printStackTrace();
			log.setResponseBody(e.getMessage());
			log.setResponseCode("500");
			log.setResponseResult("");
			ncServiceLogService.save(log);
			return JsonResult.error(e.getMessage()+"Return:"+info);
		} catch (Exception e) {
			log.setResponseBody(e.getMessage());
			log.setResponseCode("500");
			log.setResponseResult("");
			ncServiceLogService.save(log);
			e.printStackTrace();
			return JsonResult.error(e.getMessage()+"Return:"+info);
		}
	}

	//ESB推送销售出库
	@Override
	public JsonResult callXSCK(String[] djhs, String ServiceId) {
		String info = "";

		String endPoint= CommonConfig.ncServiceUrl;
		if(endPoint==null){
			return JsonResult.error("NC WebSevice服务地址未配置，请检查");
		}
		if(djhs==null || djhs.length==0)
		{
			return JsonResult.error("缺少单据号参数");
		}
		String danj_no = djhs[0];
		//根据单据号获取中间表的采购入库明细
		List<C37InfCkScBillNew> detail = c37InfCkScBillNewService.DetailList(danj_no);
		ESBDto dto = new ESBDto();
		dto.orgcode = CommonConfig.ncOrgCode;
		dto.type="销售出库";
		POrderInfo[] item = new POrderInfo[detail.size()];
		int index = 0;
		for (C37InfCkScBillNew v: detail) {
			item[index] = new POrderInfo();
			item[index].setDanj_no(v.getDanjNo());
			item[index].setChuk_no(v.getChukNo());
			item[index].setDanw_no(v.getDanwNo());
			item[index].setRiqi_char(v.getRiqiChar());
			item[index].setYew_staff(v.getYewStaff());
			item[index].setTih_way(v.getTihWay());
			item[index].setYew_type(v.getYewType());
			item[index].setDanjxc_side(v.getDanjxcSide());
			item[index].setHanghao(v.getHanghao());
			item[index].setShangp_no(v.getShangpNo());
			item[index].setJih_num(v.getJihNum());
			item[index].setNum(v.getNum());
			item[index].setLot(v.getLot());
			item[index].setShengc_char(v.getShengcChar());
			item[index].setYoux_char(v.getYouxChar());
			item[index].setChongh_flg(v.getChonghFlg());
			if(StringUtils.isEmpty(v.getTuihReason()))
			{
				item[index].setTuih_reason(".");
			}
			else {
				item[index].setTuih_reason(v.getTuihReason());
			}
			item[index].setZt(v.getZt());
			item[index].setKub(v.getKub());
			item[index].setVdef1("1");
			item[index].setVdef2(v.getKub());
			index++;
		}
		dto.body = item;
		String body = JSON.toJSONString(dto);
		ISoapWebServiceServiceSoapBindingStub esb ;
		DefaultWebServiceResponse resp = null;
		Date dt=new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		DefaultWebServiceRequest  df=new DefaultWebServiceRequest();
		df.setMsgBody(body);
		df.setMsgSendTime(format.format(dt));
		df.setServiceId(ServiceId);
		df.setSourceAppCode("E9");//Դϵͳ��־��

		NcServiceLog log = new NcServiceLog();
		log.setSubmitNo(danj_no+"_"+dto.type);
		log.setRequestBody(df.getMsgBody());
		log.setSubmitTime(DateUtils.now());
		try {
			esb = new ISoapWebServiceServiceSoapBindingStub(new URL(endPoint), new Service());
			resp = esb.receiveMessage(df);
//			info=resp.getReturnMessage().replaceAll("\\\\","");
			info=resp.getReturnMessage();
			JSONObject result = JSON.parseObject(info);
			log.setResponseBody(info);
			log.setResponseCode(result.getString("flag"));
			log.setResponseResult(result.getString("msg"));
			ncServiceLogService.save(log);
			if(result.getString("flag").equals("Y"))
			{

				//log.setResponseCode("N");
				//log.setResponseResult("{flag=Y, msg=当前数据项PRICE为空;当前数据项vdef1为空;}");
				c37InfCkScBillNewService.UpdateZT(danj_no,"N");
				return JsonResult.success("",result.getString("msg"));
			}
			else
			{
				//若返回信息中包含[单据号重复]关键字,则更新状态为N
				if(result.getString("msg").contains(CommonConfig.ncBillKey))
				{
					c37InfCkScBillNewService.UpdateZT(danj_no,"N");
					return JsonResult.error(result.getString("msg"));
				}
				else {
					//log.setResponseCode("N");
					//log.setResponseResult("{flag=Y, msg=当前数据项PRICE为空;当前数据项vdef1为空;}");
					return JsonResult.error(result.getString("msg"));
				}

			}

		} catch (AxisFault e) {
			e.printStackTrace();
			log.setResponseBody(e.getMessage());
			log.setResponseCode("500");
			log.setResponseResult("");
			ncServiceLogService.save(log);
			return JsonResult.error(e.getMessage()+"Return:"+info);
		} catch (Exception e) {
			log.setResponseBody(e.getMessage());
			log.setResponseCode("500");
			log.setResponseResult("");
			ncServiceLogService.save(log);
			e.printStackTrace();
			return JsonResult.error(e.getMessage()+"Return:"+info);
		}
	}

	//ESB推送销售退回
	@Override
	public JsonResult callXSTH(String[] djhs, String ServiceId) {
		String info = "";

		String endPoint= CommonConfig.ncServiceUrl;
		if(endPoint==null){
			return JsonResult.error("NC WebSevice服务地址未配置，请检查");
		}
		if(djhs==null || djhs.length==0)
		{
			return JsonResult.error("缺少单据号参数");
		}
		String danj_no = djhs[0];
		//根据单据号获取中间表的采购入库明细
		List<C37InfRkScBillNew> detail = c37InfRkScBillNewService.DetailList(danj_no);
		ESBDto dto = new ESBDto();
		dto.orgcode = CommonConfig.ncOrgCode;
		dto.type="销售退回";
		POrderInfo[] item = new POrderInfo[detail.size()];
		int index = 0;
		for (C37InfRkScBillNew v: detail) {
			item[index] = new POrderInfo();
			item[index].setDanj_no(v.getDanjNo());
			item[index].setRiqi_char(v.getRiqiChar());
			item[index].setDanw_no(v.getDanwNo());
			item[index].setCaigou_staff(v.getCaigouStaff());
			item[index].setShouh_staff(v.getShouhStaff());
			item[index].setHanghao(v.getHanghao());
			item[index].setShangp_no(v.getShangpNo());
			item[index].setLot(v.getLot());
			item[index].setShengc_char(v.getShengcChar());
			item[index].setYoux_char(v.getYouxChar());
			item[index].setRuk_type(v.getRukType());
			item[index].setJih_num(v.getJihNum());
			item[index].setNum(v.getNum());
			item[index].setPrice(v.getPrice());
			item[index].setRuk_no(v.getRukNo());
			item[index].setHangh_cgd(v.getHanghCgd());
			item[index].setZhij_staff(v.getZhijStaff());
			item[index].setZt(v.getZt());
			item[index].setVdef1("1");
			item[index].setVdef2(v.getYansRlt());
			index++;
		}
		dto.body = item;
		String body = JSON.toJSONString(dto);
		ISoapWebServiceServiceSoapBindingStub esb ;
		DefaultWebServiceResponse resp = null;
		Date dt=new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		DefaultWebServiceRequest  df=new DefaultWebServiceRequest();
		df.setMsgBody(body);
		df.setMsgSendTime(format.format(dt));
		df.setServiceId(ServiceId);
		df.setSourceAppCode("E9");//Դϵͳ��־��

		NcServiceLog log = new NcServiceLog();
		log.setSubmitNo(danj_no+"_"+dto.type);
		log.setRequestBody(df.getMsgBody());
		log.setSubmitTime(DateUtils.now());
		try {
			esb = new ISoapWebServiceServiceSoapBindingStub(new URL(endPoint), new Service());
			resp = esb.receiveMessage(df);
//			info=resp.getReturnMessage().replaceAll("\\\\","");
			info=resp.getReturnMessage();
			JSONObject result = JSON.parseObject(info);
			log.setResponseBody(info);
			log.setResponseCode(result.getString("flag"));
			log.setResponseResult(result.getString("msg"));
			ncServiceLogService.save(log);
			if(result.getString("flag").equals("Y"))
			{

				//log.setResponseCode("N");
				//log.setResponseResult("{flag=Y, msg=当前数据项PRICE为空;当前数据项vdef1为空;}");
				c37InfRkScBillNewService.UpdateZT(danj_no,"N");
				return JsonResult.success("",result.getString("msg"));
			}
			else
			{
				if(result.getString("msg").contains(CommonConfig.ncBillKey))
				{
					c37InfRkScBillNewService.UpdateZT(danj_no,"N");
					return JsonResult.error(result.getString("msg"));
				}
				else {
					//log.setResponseCode("N");
					//log.setResponseResult("{flag=Y, msg=当前数据项PRICE为空;当前数据项vdef1为空;}");
					return JsonResult.error(result.getString("msg"));
				}

			}

		} catch (AxisFault e) {
			e.printStackTrace();
			log.setResponseBody(e.getMessage());
			log.setResponseCode("500");
			log.setResponseResult("");
			ncServiceLogService.save(log);
			return JsonResult.error(e.getMessage()+"Return:"+info);
		} catch (Exception e) {
			log.setResponseBody(e.getMessage());
			log.setResponseCode("500");
			log.setResponseResult("");
			ncServiceLogService.save(log);
			e.printStackTrace();
			return JsonResult.error(e.getMessage()+"Return:"+info);
		}
	}
}
