package com.sphchina.esb.webservice;

import lombok.Data;

import java.util.List;

@Data
public class ESBDto {
    public POrderInfo[] body;
    public String type;
    public String orgcode;
}
