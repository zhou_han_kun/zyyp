package com.sphchina.esb.webservice;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class POrderInfo {
    private String danj_no;
    private String chuk_no;
    private String riqi_char;
    private String danw_no;
    private String caigou_staff;
    private String shouh_staff;
    private String yew_staff;
    private String tih_way;
    private String yew_type;
    private String danjxc_side;
    private String chongh_flg;
    private String tuih_reason;
    private String kub;
    private Integer hanghao;
    private String shangp_no;
    private String lot;
    private String shengc_char;
    private String youx_char;
    private String ruk_type;
    private BigDecimal jih_num;
    private BigDecimal num;
    private String price;
    private String ruk_no;
    private Integer hangh_cgd;
    private String zhij_staff;
    private String zt;
    private String pingx_no;
    private String yans_rlt;
    private String kh_zzbm;
    private String yez_id;
    private String vdef1;
    private String vdef2;

}
