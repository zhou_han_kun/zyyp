/**
 * ISoapWebServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sphchina.esb.webservice;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.XMLType;

public class ISoapWebServiceServiceLocator extends org.apache.axis.client.Service implements ISoapWebServiceService {

    public ISoapWebServiceServiceLocator() {
    	}


    public ISoapWebServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ISoapWebServiceServiceLocator(String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for receiveMessagePort
    private String receiveMessagePort_address = "http://10.11.100.202:8089/soap";

    public String getreceiveMessagePortAddress() {
        return receiveMessagePort_address;
    }

    // The WSDD service name defaults to the port name.
    private String receiveMessagePortWSDDServiceName = "receiveMessagePort";

    public String getreceiveMessagePortWSDDServiceName() {
        return receiveMessagePortWSDDServiceName;
    }

    public void setreceiveMessagePortWSDDServiceName(String name) {
        receiveMessagePortWSDDServiceName = name;
    }

    public ReceiveMessage_PortType getreceiveMessagePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(receiveMessagePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getreceiveMessagePort(endpoint);
    }

    public ReceiveMessage_PortType getreceiveMessagePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ISoapWebServiceServiceSoapBindingStub _stub = new ISoapWebServiceServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getreceiveMessagePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setreceiveMessagePortEndpointAddress(String address) {
        receiveMessagePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ReceiveMessage_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                ISoapWebServiceServiceSoapBindingStub _stub = new ISoapWebServiceServiceSoapBindingStub(new java.net.URL(receiveMessagePort_address), this);
                _stub.setPortName(getreceiveMessagePortWSDDServiceName());
                return _stub;
            }
        }
        catch (Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("receiveMessagePort".equals(inputPortName)) {
            return getreceiveMessagePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webservice.esb.sphchina.com", "ISoapWebServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webservice.esb.sphchina.com", "receiveMessagePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(String portName, String address) throws javax.xml.rpc.ServiceException {
        
if ("receiveMessagePort".equals(portName)) {
            setreceiveMessagePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
