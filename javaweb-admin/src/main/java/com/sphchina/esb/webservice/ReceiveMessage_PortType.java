/**
 * ReceiveMessage_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sphchina.esb.webservice;

public interface ReceiveMessage_PortType extends java.rmi.Remote {
	
    public DefaultWebServiceResponse receiveMessage(DefaultWebServiceRequest defaultWebServiceRequest) throws java.rmi.RemoteException;
}
