/**
 * DefaultWebServiceRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sphchina.esb.webservice;

public class DefaultWebServiceRequest  implements java.io.Serializable {
	
    private String msgBody;

    private String msgSendTime;

    private String password;

    private String serviceId;

    private String sourceAppCode;

    public DefaultWebServiceRequest() {
    }

    public DefaultWebServiceRequest(
           String msgBody,
           String msgSendTime,
           String password,
           String serviceId,
           String sourceAppCode) {
           this.msgBody = msgBody;
           this.msgSendTime = msgSendTime;
           this.password = password;
           this.serviceId = serviceId;
           this.sourceAppCode = sourceAppCode;
    }


    /**
     * Gets the msgBody value for this DefaultWebServiceRequest.
     * 
     * @return msgBody
     */
    public String getMsgBody() {
        return msgBody;
    }


    /**
     * Sets the msgBody value for this DefaultWebServiceRequest.
     * 
     * @param msgBody
     */
    public void setMsgBody(String msgBody) {
        this.msgBody = msgBody;
    }


    /**
     * Gets the msgSendTime value for this DefaultWebServiceRequest.
     * 
     * @return msgSendTime
     */
    public String getMsgSendTime() {
        return msgSendTime;
    }


    /**
     * Sets the msgSendTime value for this DefaultWebServiceRequest.
     * 
     * @param msgSendTime
     */
    public void setMsgSendTime(String msgSendTime) {
        this.msgSendTime = msgSendTime;
    }


    /**
     * Gets the password value for this DefaultWebServiceRequest.
     * 
     * @return password
     */
    public String getPassword() {
        return password;
    }


    /**
     * Sets the password value for this DefaultWebServiceRequest.
     * 
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }


    /**
     * Gets the serviceId value for this DefaultWebServiceRequest.
     * 
     * @return serviceId
     */
    public String getServiceId() {
        return serviceId;
    }


    /**
     * Sets the serviceId value for this DefaultWebServiceRequest.
     * 
     * @param serviceId
     */
    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }


    /**
     * Gets the sourceAppCode value for this DefaultWebServiceRequest.
     * 
     * @return sourceAppCode
     */
    public String getSourceAppCode() {
        return sourceAppCode;
    }


    /**
     * Sets the sourceAppCode value for this DefaultWebServiceRequest.
     * 
     * @param sourceAppCode
     */
    public void setSourceAppCode(String sourceAppCode) {
        this.sourceAppCode = sourceAppCode;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof DefaultWebServiceRequest)) return false;
        DefaultWebServiceRequest other = (DefaultWebServiceRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.msgBody==null && other.getMsgBody()==null) || 
             (this.msgBody!=null &&
              this.msgBody.equals(other.getMsgBody()))) &&
            ((this.msgSendTime==null && other.getMsgSendTime()==null) || 
             (this.msgSendTime!=null &&
              this.msgSendTime.equals(other.getMsgSendTime()))) &&
            ((this.password==null && other.getPassword()==null) || 
             (this.password!=null &&
              this.password.equals(other.getPassword()))) &&
            ((this.serviceId==null && other.getServiceId()==null) || 
             (this.serviceId!=null &&
              this.serviceId.equals(other.getServiceId()))) &&
            ((this.sourceAppCode==null && other.getSourceAppCode()==null) || 
             (this.sourceAppCode!=null &&
              this.sourceAppCode.equals(other.getSourceAppCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMsgBody() != null) {
            _hashCode += getMsgBody().hashCode();
        }
        if (getMsgSendTime() != null) {
            _hashCode += getMsgSendTime().hashCode();
        }
        if (getPassword() != null) {
            _hashCode += getPassword().hashCode();
        }
        if (getServiceId() != null) {
            _hashCode += getServiceId().hashCode();
        }
        if (getSourceAppCode() != null) {
            _hashCode += getSourceAppCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DefaultWebServiceRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.esb.sphchina.com", "DefaultWebServiceRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgBody");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservice.esb.sphchina.com", "msgBody"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgSendTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservice.esb.sphchina.com", "msgSendTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservice.esb.sphchina.com", "password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservice.esb.sphchina.com", "serviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceAppCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservice.esb.sphchina.com", "sourceAppCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
