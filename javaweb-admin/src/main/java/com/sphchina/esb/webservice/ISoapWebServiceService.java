/**
 * ISoapWebServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sphchina.esb.webservice;

public interface ISoapWebServiceService extends javax.xml.rpc.Service {
	
    public String getreceiveMessagePortAddress();

    public ReceiveMessage_PortType getreceiveMessagePort() throws javax.xml.rpc.ServiceException;

    public ReceiveMessage_PortType getreceiveMessagePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
