package com.sphchina.esb.webservice;

import com.javaweb.common.utils.JsonResult;

public interface ISendToReviewLog {
    JsonResult callEsb(String body, String ServiceId);
    JsonResult callEsbKdl(String billType,String billno,String body);
    JsonResult callCGRK(String[] djhs, String ServiceId);
    JsonResult callGJTC(String[] body, String ServiceId);
    JsonResult callXSCK(String[] body, String ServiceId);
    JsonResult callXSTH(String[] body, String ServiceId);
}
