package com.sphsine.sso.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sphsine.sso.entity.SsoOrg;

public interface SsoOrgMapper extends BaseMapper<SsoOrg> {
}
