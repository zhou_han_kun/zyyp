package com.sphsine.sso.entity;

public class AccountMsgData {
    private AccountBody msgBody;
    public void setMsgBody(AccountBody msgBody) {
        this.msgBody = msgBody;
    }
    public AccountBody getMsgBody() {
        return msgBody;
    }
}
