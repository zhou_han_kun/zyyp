package com.sphsine.sso.entity;

import java.math.BigInteger;

/**
 * Auto-generated: 2024-04-28 10:55:55
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class MsgBody {

    private String objtype;
    private String name;
    private String code;
    private String orgcode;
    private String porgid;
    private String type;
    private String oguid;
    private boolean isdisabled;
    private String opertype;
    private String username;
    private String fullname;
    private BigInteger orgid;
    private String email;
    private String mobile;
    private String employeeno;
    private String uguid;

    public void setObjtype(String objtype) {
        this.objtype = objtype;
    }
    public String getObjtype() {
        return objtype;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getCode() {
        return code;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }
    public String getOrgcode() {
        return orgcode;
    }

    public void setPorgid(String porgid) {
        this.porgid = porgid;
    }
    public String getPorgid() {
        return porgid;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getType() {
        return type;
    }

    public void setOguid(String oguid) {
        this.oguid = oguid;
    }
    public String getOguid() {
        return oguid;
    }

    public void setIsdisabled(boolean isdisabled) {
        this.isdisabled = isdisabled;
    }
    public boolean getIsdisabled() {
        return isdisabled;
    }

    public void setOpertype(String opertype) {
        this.opertype = opertype;
    }
    public String getOpertype() {
        return opertype;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public BigInteger getOrgid() {
        return orgid;
    }

    public void setOrgid(BigInteger orgid) {
        this.orgid = orgid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmployeeno() {
        return employeeno;
    }

    public void setEmployeeno(String employeeno) {
        this.employeeno = employeeno;
    }

    public String getUguid() {
        return uguid;
    }

    public void setUguid(String uguid) {
        this.uguid = uguid;
    }
}