package com.sphsine.sso.entity;

import java.math.BigInteger;

public class AccountBody {
        private String objtype;
        private String username;
        private String fullname;
        private BigInteger orgid;
        private boolean isdisabled;
        private String email;
        private String mobile;
        private String employeeno;
        private String uguid;
        private String opertype;

        public void setObjtype(String objtype) {
            this.objtype = objtype;
        }

        public String getObjtype() {
            return objtype;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUsername() {
            return username;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getFullname() {
            return fullname;
        }

        public void setOrgid(BigInteger orgid) {
            this.orgid = orgid;
        }

        public BigInteger getOrgid() {
            return orgid;
        }

        public void setIsdisabled(boolean isdisabled) {
            this.isdisabled = isdisabled;
        }

        public boolean getIsdisabled() {
            return isdisabled;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getEmail() {
            return email;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getMobile() {
            return mobile;
        }

        public void setEmployeeno(String employeeno) {
            this.employeeno = employeeno;
        }

        public String getEmployeeno() {
            return employeeno;
        }

        public void setUguid(String uguid) {
            this.uguid = uguid;
        }

        public String getUguid() {
            return uguid;
        }

        public void setOpertype(String opertype) {
            this.opertype = opertype;
        }

        public String getOpertype() {
            return opertype;
        }
}

