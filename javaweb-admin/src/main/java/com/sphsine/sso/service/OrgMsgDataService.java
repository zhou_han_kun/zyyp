package com.sphsine.sso.service;

import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.SsoJsonResult;
import com.sphsine.sso.entity.OrgMsgData;

public interface OrgMsgDataService {
    SsoJsonResult syncorg(OrgMsgData data);
    JsonResult getList(BaseQuery query);
    JsonResult add(OrgMsgData entity);
    JsonResult edit(OrgMsgData entity);
}
