package com.sphsine.sso.service;

import com.javaweb.common.utils.SsoJsonResult;
import com.sphsine.sso.entity.AccountMsgData;

public interface AccountService {
    SsoJsonResult syncaccount(AccountMsgData data);
}
