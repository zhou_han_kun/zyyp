package com.sphsine.sso.service.impl;

import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.SsoJsonResult;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.utils.ShiroUtils;
import com.sphsine.sso.entity.MsgBody;
import com.sphsine.sso.entity.OrgMsgData;
import com.sphsine.sso.entity.SsoOrg;
import com.sphsine.sso.enums.OperType;
import com.sphsine.sso.mapper.SsoOrgMapper;
import com.sphsine.sso.service.OrgMsgDataService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class OrgMagDataServiceImpl  implements OrgMsgDataService {

    @Autowired
    SsoOrgMapper ssoOrgMapper;

    @Override
    public SsoJsonResult syncorg(OrgMsgData data) {
        String operType = "";
        MsgBody body = null;
        try {
            if(StringUtils.isNull(data) || StringUtils.isNull(data.getMsgBody())){
                return SsoJsonResult.error(1,"msgBody不能为空");
            }
            body= data.getMsgBody();
            operType = OperType.valueOf("UPDATED").name().toUpperCase();
            //检验opertype
            Map<String, Boolean> enumMap = new HashMap<>();
            enumMap.put(OperType.CREATED.name(), true);
            enumMap.put(OperType.UPDATED.name(), true);
            if (!enumMap.containsKey(body.getOpertype())) {
                return SsoJsonResult.error("操作类型:"+body.getOpertype()+"无效,必须是CREATED或UPDATED");
            }
            if(StringUtils.isEmpty(body.getName())){
                return SsoJsonResult.error("组织名称不能为空");
            }
            if(StringUtils.isEmpty(body.getCode())){
                return SsoJsonResult.error("组织Key不能为空");
            }
            if(StringUtils.isEmpty(body.getOrgcode())){
                return SsoJsonResult.error("组织编码不能为空");
            }
            //若是修改，oguid必须输入
            if(body.getOpertype().toUpperCase().equals(operType)) {
                if(StringUtils.isEmpty(body.getOguid())){
                    return SsoJsonResult.error("更新组织，oguid必须输入");
                }
            }

            Map<String,Object> param = new HashMap<>();
            //如果是二级组织，判断二级组织的父id是否存在
            if(!StringUtils.isEmpty(body.getPorgid())){
                param.put("id",body.getPorgid());
                if (ssoOrgMapper.selectByMap(param).size() == 0){
                    return SsoJsonResult.error("上级ID"+body.getPorgid()+"不存在，二级组织"+(body.getOpertype().toUpperCase().equals(operType)?"更新":"创建")+"失败");
                }
            }
            param.clear();
            param.put("id",body.getOguid());
            //数据更新，判断id是否存在
            if(body.getOpertype().toUpperCase().equals(operType)) {
                if (ssoOrgMapper.selectByMap(param).size() == 0){
                    return SsoJsonResult.error(body.getOguid()+"不存在，更新操作失败");
                }
            }else{
                param.clear();
                param.put("code",body.getCode());
                if (ssoOrgMapper.selectByMap(param).size() > 0){
                    return SsoJsonResult.error(body.getName()+"("+body.getOrgcode()+")"+"已经存在，新增操作失败");
                }
            }
            SsoOrg entity = new SsoOrg();
            BeanUtils.copyProperties(body,entity);
            entity.setFullname(body.getName());
            if(body.getOpertype().equals(operType)) {
                entity.setId(body.getOguid());
                entity.setUpdateTime(DateUtils.now());
                entity.setUpdateUser(ShiroUtils.getUserId());
                ssoOrgMapper.updateById(entity);
            }else{
                entity.setCreateUser(ShiroUtils.getUserId());
                entity.setCreateTime(DateUtils.now());
                ssoOrgMapper.insert(entity);
            }
            return SsoJsonResult.success("",entity.getId());
        }
        catch(Exception ex) {
            if(body.getOpertype().toUpperCase().equals(operType)){
                if(ex.getMessage().toLowerCase().contains("duplicate")) {
                    return SsoJsonResult.error(1, "组织更新失败:" + data.getMsgBody().getOrgcode()+"已存在");
                }else{
                    return SsoJsonResult.error(1, "组织更新失败。" + ex.getMessage());
                }
            }else{
                return SsoJsonResult.error(1,"组织创建失败。"+ex.getMessage());
            }
        }
    }

    @Override
    public JsonResult getList(BaseQuery query) {
        return null;
    }

    @Override
    public JsonResult add(OrgMsgData entity) {
        return null;
    }

    @Override
    public JsonResult edit(OrgMsgData entity) {
        return null;
    }

}
