package com.sphsine.sso.controller;

import com.javaweb.common.annotation.Log;
import com.javaweb.common.enums.LogType;
import com.javaweb.common.utils.SsoJsonResult;
import com.sphsine.sso.entity.AccountBody;
import com.sphsine.sso.entity.AccountMsgData;
import com.sphsine.sso.entity.OrgMsgData;
import com.sphsine.sso.service.AccountService;
import com.sphsine.sso.service.OrgMsgDataService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sso")
public class SsoController {
    @Autowired
    OrgMsgDataService orgMsgDataService;
    @Autowired
    AccountService accountService;

    @Log(title = "SSO组织账号同步", logType = LogType.OTHER)
    @PostMapping("/syncorg")
    public SsoJsonResult syncorg(@RequestBody OrgMsgData data) {
        if(data.getMsgBody().getObjtype().equals("ORG")) {
            return orgMsgDataService.syncorg(data);
        }
        else
        {
            AccountMsgData accountData = new AccountMsgData();
            accountData.setMsgBody(new AccountBody());
            BeanUtils.copyProperties(data.getMsgBody(),accountData.getMsgBody());
            return accountService.syncaccount(accountData);
        }
    }

    @PostMapping("/syncaccount")
    public SsoJsonResult syncaccount(@RequestBody AccountMsgData data) {
        return accountService.syncaccount(data);
    }

}
