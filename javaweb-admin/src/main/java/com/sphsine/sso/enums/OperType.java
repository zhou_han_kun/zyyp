package com.sphsine.sso.enums;

/**
 * 操作状态
 */
public enum OperType {

    /**
     * 新增
     */
    CREATED,

    /**
     * 更新
     */
    UPDATE,

    UPDATED

}
