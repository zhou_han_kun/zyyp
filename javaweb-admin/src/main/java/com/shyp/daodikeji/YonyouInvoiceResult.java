package com.shyp.daodikeji;

public class YonyouInvoiceResult {
    private String code;
    private String message;
    private YonyouInvoiceData data;
    public void setCode(String code) {
        this.code = code;
    }
    public String getCode() {
        return code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }

    public void setData(YonyouInvoiceData data) {
        this.data = data;
    }
    public YonyouInvoiceData getData() {
        return data;
    }
}
