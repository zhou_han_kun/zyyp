package com.shyp.daodikeji;


public class GatewayAddressData {
    private String tokenUrl;
    private String gatewayUrl;
    public void setTokenUrl(String url) {
        this.tokenUrl = url;
    }
    public String getTokenUrl() {
        return tokenUrl;
    }

    public void setGatewayUrl(String url) {
        this.gatewayUrl = url;
    }
    public String getGatewayUrl() {
        return gatewayUrl;
    }
}
