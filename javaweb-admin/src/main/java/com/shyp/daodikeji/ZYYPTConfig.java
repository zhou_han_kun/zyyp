package com.shyp.daodikeji;

import com.javaweb.common.config.CommonConfig;

public class ZYYPTConfig extends DDKJConfig{
    @Override
    public String getAppID() {
        return CommonConfig.appId;
    }

    @Override
    public String getEnterpriseId() {
        return CommonConfig.enterpriseId;
    }

    @Override
    public Boolean useMock() {
        return CommonConfig.useMock;
    }

    @Override
    public String getSecret() {
        return CommonConfig.appSecret;
    }


    @Override
    public ISHYPDomain getSHYPDomain() {
        return new ZYYPTDomain();
    }
}
