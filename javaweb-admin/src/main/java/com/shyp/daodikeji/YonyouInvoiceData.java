package com.shyp.daodikeji;

public class YonyouInvoiceData {
    private String yypdf;
    private String taxpdf;
    private String taxofd;
    private String taxxml;
    public void setYypdf(String val) {
        this.yypdf = val;
    }
    public String getYypdf() {
        return yypdf;
    }

    public String getTaxpdf() {
        return taxpdf;
    }

    public void setTaxpdf(String taxpdf) {
        this.taxpdf = taxpdf;
    }

    public String getTaxofd() {
        return taxofd;
    }

    public void setTaxofd(String taxofd) {
        this.taxofd = taxofd;
    }

    public String getTaxxml() {
        return taxxml;
    }

    public void setTaxxml(String taxxml) {
        this.taxxml = taxxml;
    }
}
