package com.shyp.daodikeji;

public class TokenData {
    private int expire;
    private String accessToken;
    public void setExpire(int expire) {
        this.expire = expire;
    }
    public int getExpire() {
        return expire;
    }

    public void setAccessToken(String access_token) {
        this.accessToken = access_token;
    }
    public String getAccessToken() {
        return accessToken;
    }
}
