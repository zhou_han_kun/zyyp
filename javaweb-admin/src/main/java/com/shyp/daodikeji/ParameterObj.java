/**
 * Copyright 2023 bejson.com
 */
package com.shyp.daodikeji;
import java.util.Date;

/**
 * Auto-generated: 2023-03-30 17:2:44
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ParameterObj {

    private String hostCode;
    private String instockNumber;
    private String instockNo;
    private String saleNumber;
    private String entId;
    private String entName;
    private String traceCode;
    private String selfCode;
    private String ypName;
    private String packGuige;
    private String ypGuige;
    private String medicinalOrigin;
    private String sourceFlag;
    private String manufacturer;
    private String productBatch;
    private String arrivalTime;
    private String saleTime;
    private String contact;
    private String num;
    private String unit;
    private String checkResult;
    private String enviornment;
    private String ypCode;
    private String filingNo;
    private String sincerity;
    private String whereabouts;
    private String deleted;
    public void setHostCode(String hostCode) {
        this.hostCode = hostCode;
    }
    public String getHostCode() {
        return hostCode;
    }

    public void setInstockNumber(String instockNumber) {
        this.instockNumber = instockNumber;
    }
    public String getInstockNumber() {
        return instockNumber;
    }

    public void setEntId(String entId) {
        this.entId = entId;
    }
    public String getEntId() {
        return entId;
    }

    public void setEntName(String entName) {
        this.entName = entName;
    }
    public String getEntName() {
        return entName;
    }

    public void setTraceCode(String traceCode) {
        this.traceCode = traceCode;
    }
    public String getTraceCode() {
        return traceCode;
    }

    public void setYpName(String ypName) {
        this.ypName = ypName;
    }
    public String getYpName() {
        return ypName;
    }

    public void setPackGuige(String packGuige) {
        this.packGuige = packGuige;
    }
    public String getPackGuige() {
        return packGuige;
    }

    public void setMedicinalOrigin(String medicinalOrigin) {
        this.medicinalOrigin = medicinalOrigin;
    }
    public String getMedicinalOrigin() {
        return medicinalOrigin;
    }

    public void setSourceFlag(String sourceFlag) {
        this.sourceFlag = sourceFlag;
    }
    public String getSourceFlag() {
        return sourceFlag;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    public String getManufacturer() {
        return manufacturer;
    }

    public void setProductBatch(String productBatch) {
        this.productBatch = productBatch;
    }
    public String getProductBatch() {
        return productBatch;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setNum(String num) {
        this.num = num;
    }
    public String getNum() {
        return num;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
    public String getUnit() {
        return unit;
    }

    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult;
    }
    public String getCheckResult() {
        return checkResult;
    }

    public void setEnviornment(String enviornment) {
        this.enviornment = enviornment;
    }
    public String getEnviornment() {
        return enviornment;
    }

    public void setYpCode(String ypCode) {
        this.ypCode = ypCode;
    }
    public String getYpCode() {
        return ypCode;
    }

    public void setFilingNo(String filingNo) {
        this.filingNo = filingNo;
    }
    public String getFilingNo() {
        return filingNo;
    }

    public void setSincerity(String sincerity) {
        this.sincerity = sincerity;
    }
    public String getSincerity() {
        return sincerity;
    }

    public String getInstockNo() {
        return instockNo;
    }

    public void setInstockNo(String instockNo) {
        this.instockNo = instockNo;
    }

    public String getSaleNumber() {
        return saleNumber;
    }

    public void setSaleNumber(String saleNumber) {
        this.saleNumber = saleNumber;
    }

    public String getSelfCode() {
        return selfCode;
    }

    public void setSelfCode(String selfCode) {
        this.selfCode = selfCode;
    }

    public String getSaleTime() {
        return saleTime;
    }

    public void setSaleTime(String saleTime) {
        this.saleTime = saleTime;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getWhereabouts() {
        return whereabouts;
    }

    public void setWhereabouts(String whereabouts) {
        this.whereabouts = whereabouts;
    }

    public String getYpGuige() {
        return ypGuige;
    }

    public void setYpGuige(String ypGuige) {
        this.ypGuige = ypGuige;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }
}