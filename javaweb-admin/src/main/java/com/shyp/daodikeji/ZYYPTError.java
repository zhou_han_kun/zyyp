package com.shyp.daodikeji;

/**
 * Copyright 2023 bejson.com
 */

/**
 * Auto-generated: 2023-03-31 10:22:27
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ZYYPTError {

    private int code;
    private String message;
    private String data;
    public void setCode(int code) {
        this.code = code;
    }
    public int getCode() {
        return code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }

    public void setData(String data) {
        this.data = data;
    }
    public String getData() {
        return data;
    }

}
