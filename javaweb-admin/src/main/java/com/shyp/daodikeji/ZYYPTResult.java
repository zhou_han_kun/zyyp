package com.shyp.daodikeji;

/**
 * Copyright 2023 bejson.com
 */

/**
 * Auto-generated: 2023-03-31 10:22:27
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ZYYPTResult {

    private String id;
    private int code;
    private String message;
    private String result;
    private ZYYPTError error;
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }

    public void setCode(int code) {
        this.code = code;
    }
    public int getCode() {
        return code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }

    public void setResult(String result) {
        this.result = result;
    }
    public String getResult() {
        return result;
    }

    public void setError(ZYYPTError error) {
        this.error = error;
    }
    public ZYYPTError getError() {
        return error;
    }

}
