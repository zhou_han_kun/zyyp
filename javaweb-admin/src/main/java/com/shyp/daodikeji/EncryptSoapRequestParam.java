package com.shyp.daodikeji;

import java.util.List;

public class EncryptSoapRequestParam {
    private String id;
    private String method;
    private List<EncryptParms> params;
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }

    public void setMethod(String method) {
        this.method = method;
    }
    public String getMethod() {
        return method;
    }

    public void setParams(List<EncryptParms> params) {
        this.params = params;
    }
    public List<EncryptParms> getParams() {
        return params;
    }
}
