package com.shyp.daodikeji;

/**
 * Auto-generated: 2023-03-30 17:2:44
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Params {

    private String CorpId;
    private String SystemId;
    private String Service;
    private ParameterObj ParameterObj;
    public void setCorpId(String CorpId) {
        this.CorpId = CorpId;
    }
    public String getCorpId() {
        return CorpId;
    }

    public void setSystemId(String SystemId) {
        this.SystemId = SystemId;
    }
    public String getSystemId() {
        return SystemId;
    }

    public void setService(String Service) {
        this.Service = Service;
    }
    public String getService() {
        return Service;
    }

    public void setParameterObj(ParameterObj ParameterObj) {
        this.ParameterObj = ParameterObj;
    }
    public ParameterObj getParameterObj() {
        return ParameterObj;
    }

}
