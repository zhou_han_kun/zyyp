package com.shyp.daodikeji;

import java.io.InputStream;

public abstract class DDKJConfig {

    /**
     * 获取 App ID
     *
     * @return App ID
     */
    public abstract String getAppID();

    public abstract Boolean useMock();
    /**
     * 获取 API 密钥
     *
     * @return API密钥
     */
    public abstract String getSecret();


    public abstract String getEnterpriseId();
    /**
     * HTTP(S) 连接超时时间，单位毫秒
     *
     * @return
     */
    public int getHttpConnectTimeoutMs() {
        return 6*1000;
    }

    /**
     * HTTP(S) 读数据超时时间，单位毫秒
     *
     * @return
     */
    public int getHttpReadTimeoutMs() {
        return 180*1000;
    }

    /**
     * 获取WXPayDomain, 用于多域名容灾自动切换
     * @return
     */
    public abstract ISHYPDomain getSHYPDomain();

}
