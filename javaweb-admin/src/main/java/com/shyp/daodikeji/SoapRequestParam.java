/**
 * Copyright 2023 bejson.com
 */
package com.shyp.daodikeji;
import java.util.List;

/**
 * Auto-generated: 2023-03-30 17:2:44
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class SoapRequestParam {

    private String id;
    private String method;
    private List<Params> params;
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }

    public void setMethod(String method) {
        this.method = method;
    }
    public String getMethod() {
        return method;
    }

    public void setParams(List<Params> params) {
        this.params = params;
    }
    public List<Params> getParams() {
        return params;
    }

}