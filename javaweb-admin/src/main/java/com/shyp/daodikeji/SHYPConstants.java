package com.shyp.daodikeji;

import org.apache.http.client.HttpClient;

public class SHYPConstants {

    public enum SignType {
        MD5, HMACSHA256
    }

    public enum RequestMethod {
        GET, POST
    }
    public static final String DOMAIN_API = "shyp.daodikeji.com/open-api/v1";
    public static final String DOMAIN_API2 = "sh.dev.daodikeji.cn/open-api/v1";
    public static final String DOMAIN_MOCK_API = "mockjs.xiaoyaoji.cn/mock/1mDb67vQpxA/open-api/v1";

    public static final String ZYYPT_DOMAIN_API = "61.129.134.231:30303/api/service/gateway";
    public static final String ZYYPT_DOMAIN_TEST_API = "61.129.134.241:30304/api/service/gateway";
    public static final String ZYYPT_UPLOAD_DOMAIN_API = "61.129.134.231:30303/File/UploadFile";
    public static final String ZYYPT_UPLOAD_DOMAIN_TEST_API = "61.129.134.241:30304/File/UploadFile";


    public static final String DOMAIN_API_AUTH = "https://c2.yonyoucloud.com/iuap-api-auth";
    public static final String TOKEN_URL_SUFFIX     = "/open-auth/selfAppAuth/getAccessToken?a=1";
    public static final String YONYOUGatewayAddress =  "https://apigateway.yonyoucloud.com/open-auth/dataCenter/getGatewayAddress";



    public static final String FAIL     = "FAIL";
    public static final String SUCCESS  = "SUCCESS";
    public static final String HMACSHA256 = "HMAC-SHA256";
    public static final String MD5 = "MD5";

    public static final String FIELD_SIGN = "sign";
    public static final String FIELD_SIGN_TYPE = "sign_type";

    public static final String WXPAYSDK_VERSION = "WXPaySDK/3.0.9";
    public static final String USER_AGENT = WXPAYSDK_VERSION +
            " (" + System.getProperty("os.arch") + " " + System.getProperty("os.name") + " " + System.getProperty("os.version") +
            ") Java/" + System.getProperty("java.version") + " HttpClient/" + HttpClient.class.getPackage().getImplementationVersion();

    public static final String ASSOCIATION_TRACE_URL_SUFFIX     = "/association_trace_code";
    public static final String ASSOCIATION_PIECES_URL_SUFFIX     = "/decoction_pieces_trace_code";



}
