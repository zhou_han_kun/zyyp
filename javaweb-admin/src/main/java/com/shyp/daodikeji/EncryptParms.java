package com.shyp.daodikeji;

public class EncryptParms {
    private String CorpId;
    private String SystemId;
    private String Service;
    private String ParameterObj;
    public void setCorpId(String CorpId) {
        this.CorpId = CorpId;
    }
    public String getCorpId() {
        return CorpId;
    }

    public void setSystemId(String SystemId) {
        this.SystemId = SystemId;
    }
    public String getSystemId() {
        return SystemId;
    }

    public void setService(String Service) {
        this.Service = Service;
    }
    public String getService() {
        return Service;
    }

    public void setParameterObj(String ParameterObj) {
        this.ParameterObj = ParameterObj;
    }
    public String getParameterObj() {
        return ParameterObj;
    }
}
