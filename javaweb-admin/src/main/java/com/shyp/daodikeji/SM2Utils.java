package com.shyp.daodikeji;


/**
 * 1. @description: 国密SM2加解密
 * 2. @author: xh
 * 3. @time: 2022/7/14
 */
public class SM2Utils {
    //单例模式
    private static SM2Utils instance;
    public static SM2Utils getInstance() {
        if (instance == null) {
            instance = new SM2Utils();
        }
        return instance;
    }

    /**
     * 加签： 加密签名 (使用 签名私钥)
     * @param raw 待加签的数据
     * @param privateKey 签名私钥
     * @return 加签后的数据, hex
     */
    public String sign(String raw, String privateKey) {
        try {
            return Sm2Lib.sign(raw, privateKey);
        }catch (Exception e){
            log("Sm2Util.sign Exception:"+e.getMessage());
            return null;
        }
    }

    /**
     * 验签： 签名校验 (使用 公钥)
     * @param raw  明文数据
     * @param sign 密文数据，hex 格式
     * @param publicKey 验签公钥
     * @return 验签结果 true/false
     */
    public boolean verify(String raw, String sign, String publicKey) {
        try {
            return Sm2Lib.verify(raw, publicKey, sign);
        } catch (Exception e) {
            log("Sm2Util.verify Exception:"+e.getMessage());
        }
        return false;
    }



    /**
     * 生成秘钥对(工具函数)
     * @return 0 公钥， 1私钥
     */
    public String[] genKey() {
        return Sm2Lib.genKey();
    }


    /**
     * 数据加密(使用 公钥)
     * @param data 待加密数据
     * @param publicKey 数据加密公钥
     * @return 加密后的密文数据
     */
    public String encrypt(String data, String publicKey) {
        try{
            return  Sm2Lib.encrypt(data, publicKey);
        }catch(Exception e){
            log("Sm2Util.encrypt Exception: "+ e);
            e.printStackTrace();
            return null;
        }
    }


    /**
     * 数据解密 (默认  私钥)
     * @param data 密文数据
     * @param privateKey 数据解密私钥
     * @return 解密后的明文数据
     */
    public String decrypt(String data, String privateKey)  {
        try {
            return Sm2Lib.decrypt(data, privateKey);
        }catch(Exception e){
            log("Sm2Util.decrypt Exception: "+ e);
            return null;
        }
    }

    private void log(String msg){
        System.out.println(msg);
    }
}

