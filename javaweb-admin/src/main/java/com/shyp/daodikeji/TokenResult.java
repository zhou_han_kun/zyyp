package com.shyp.daodikeji;

public class TokenResult {
    private String code;
    private String message;
    private TokenData data;
    public void setCode(String code) {
        this.code = code;
    }
    public String getCode() {
        return code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }

    public void setData(TokenData data) {
        this.data = data;
    }
    public TokenData getData() {
        return data;
    }
}
