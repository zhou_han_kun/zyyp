package com.shyp.daodikeji;

import java.util.Date;

/**
 * Auto-generated: 2023-03-29 13:10:38
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class DataList {

    private String specification;
    private String batch_code;
    private String xh_trace_code;
    private String material_category_name;
    private String package_specification;
    private String sale_date;
    private String material_name;
    private String filing_no;
    private String customer_name;
    private String material_code;
    public void setSpecification(String specification) {
        this.specification = specification;
    }
    public String getSpecification() {
        return specification;
    }

    public void setBatch_code(String batch_code) {
        this.batch_code = batch_code;
    }
    public String getBatch_code() {
        return batch_code;
    }

    public void setXh_trace_code(String xh_trace_code) {
        this.xh_trace_code = xh_trace_code;
    }
    public String getXh_trace_code() {
        return xh_trace_code;
    }

    public void setMaterial_category_name(String material_category_name) {
        this.material_category_name = material_category_name;
    }
    public String getMaterial_category_name() {
        return material_category_name;
    }

    public void setPackage_specification(String package_specification) {
        this.package_specification = package_specification;
    }
    public String getPackage_specification() {
        return package_specification;
    }

    public void setSale_date(String sale_date) {
        this.sale_date = sale_date;
    }
    public String getSale_date() {
        return sale_date;
    }

    public void setMaterial_name(String material_name) {
        this.material_name = material_name;
    }
    public String getMaterial_name() {
        return material_name;
    }

    public void setFiling_no(String filing_no) {
        this.filing_no = filing_no;
    }
    public String getFiling_no() {
        return filing_no;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }
    public String getCustomer_name() {
        return customer_name;
    }

    public void setMaterial_code(String material_code) {
        this.material_code = material_code;
    }
    public String getMaterial_code() {
        return material_code;
    }

}