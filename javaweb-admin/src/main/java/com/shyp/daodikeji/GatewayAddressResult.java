package com.shyp.daodikeji;

public class GatewayAddressResult {
    private String code;
    private String message;
    private GatewayAddressData data;
    public void setCode(String code) {
        this.code = code;
    }
    public String getCode() {
        return code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }

    public void setData(GatewayAddressData data) {
        this.data = data;
    }
    public GatewayAddressData getData() {
        return data;
    }
}
